const log = require('../../log')
const changeOperations = require('../changes/changeOperations')
const dbExecutePromise = require('./executors/dbExecutePromise')
const executeUpdateQuery = require('./executors/executeUpdateQuery')
const executeSelectByIdQuery = require('./executors/executeSelectByIdQuery')
const executeInsertChange = require('./executors/executeInsertChangeQuery')
const executePropertyChanges = require('./executors/executeUpdatePropertyChangeQueries')

const attemptToUpdateDb = async (connection, schema, bindParams, rowId, userId, skipAudit) => {
  log.info({domain: schema.tableShort, id: rowId}, 'update db')
  const auditingRequired = !skipAudit && schema.columns.filter((column) => column.audit === true).length > 0

  let objectBefore
  if (auditingRequired) {
    objectBefore = await executeSelectByIdQuery(connection, schema, rowId)
  }

  await executeUpdateQuery(connection, schema, bindParams, rowId)

  if (auditingRequired) {
    const insertChangeResult = await executeInsertChange(connection, schema, rowId, userId, bindParams, changeOperations.update)
    await executePropertyChanges(connection, schema, insertChangeResult.id, objectBefore, bindParams)
  }

  return await executeSelectByIdQuery(connection, schema, rowId)
}

module.exports = (schema, bindParams, rowId, userId, skipAudit) => {
  const dbPromiseChainCreator = (connection) => attemptToUpdateDb(connection, schema, bindParams, rowId, userId, skipAudit)
  return dbExecutePromise(dbPromiseChainCreator)
}
