const {dbSchemaName} = require('../../config')
const {numberConverter} = require('../../db/converters')
const validateIdAndText = require('../../common/validateIdTextField')
const {contractTypes, contractStatuses} = require('../../referenceData.json')
const contractTypeValidator = validateIdAndText(contractTypes, 'Invalid Contract Type')

const zeroDefault = () => 0

const table = 'contract_derivative_type'

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: 'derivative_type_id',
  // sequence: sequence,
  orderBy: 'contract_id',
  columns: [
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'derivativeTypeId', column: 'derivative_type_id'}
  ],
  required: {
    new: ['contractId', 'derivativeTypeId'],
    update: []
  }
}
