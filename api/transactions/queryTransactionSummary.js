require('require-sql')
const log = require('../../log')
const schema = require('./transactionSchema')
const summarySqlSelect = require('./summarySqlSelect.sql')
const summarySqlWhereClause = require('./summarySqlWhereClause.sql')
const dbExecutePromise = require('../db/executors/dbExecutePromise')
const dbExecuteOne = require('../db/executors/dbExecuteOne')

const buildSummarySql = (transactionWhereClause) =>
  (transactionWhereClause.whereClause)
  ? `${summarySqlSelect} WHERE ${summarySqlWhereClause} AND ${transactionWhereClause.whereClause.replace(/where/ig, '')}`
  : `${summarySqlSelect} WHERE ${summarySqlWhereClause}`

const mapTransactionResultToJson = ({rows}) => {
  if (rows && rows.length === 1) {
    return {
      deposits: rows[0].DEPOSITS * 1,
      withdrawals: rows[0].WITHDRAWALS * 1,
      adjustments: rows[0].ADJUSTMENTS * 1,
      feeReceipts: rows[0].FEE_RECEIPTS * 1,
      feeAdjustments: rows[0].FEE_ADJUSTMENTS * 1,
      terminations: rows[0].TERMINATIONS * 1,
      amountBvPercent: rows[0].AMOUNT_BV_PERCENT * 1,
      withdrawalBvPercent: rows[0].WITHDRAWL_BV_PERCENT * 1,
      depositBvPercent: rows[0].DEPOSIT_BV_PERCENT * 1
    }
  } else {
    return {}
  }
}

const queryIt = (sql, bindParams) =>
  dbExecutePromise((connection) =>
    dbExecuteOne(connection, {
      sql,
      bindParams,
      executeOptions: {},
      mapResult: mapTransactionResultToJson
    }))

module.exports = async (req, transactionWhereClause) => {
  log.info({domain: schema.tableShort}, 'query transaction summary')

  const sql = buildSummarySql(transactionWhereClause)

  return await queryIt(sql, transactionWhereClause.bindParams)
}
