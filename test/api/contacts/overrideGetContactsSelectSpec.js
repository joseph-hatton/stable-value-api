const proxyquire = require('../../proxyquire')

const columns = ['something', 'else']
const SQL = 'SOME SQL'
const bodyAfterRemoval = {something: 'something value', else: 'else value'}
const bodyBeforeRemoval = Object.assign({toBeIgnored: true}, bodyAfterRemoval)

const contact1 = {contactId: 1, contractId: '1', contractNumber: 'a1', shortPlanName: 's1', status: 'st1', contactContractId: '6'}
const contact2b = {contactId: 2, contractId: '2', contractNumber: 'b1', shortPlanName: 's2', status: 'st3', contactContractId: '7'}
const contact2c = {contactId: 2, contractId: '3', contractNumber: 'c1', shortPlanName: 's3', status: 'st3', contactContractId: '8'}

describe('get contacts with company and contracts', () => {
  let target
  beforeEach(() => {
    target = proxyquire('api/contacts/overrideGetContactsSelect', {
      './overrideGetContactsSelect.sql': SQL
    })
  })

  it('has an expected sql', () =>
    target.sql.should.eql(SQL))

  describe('maps db results', () => {
    it('returns empty results when passed empty', () =>
      target.mapper([]).should.eql([]))

    it('returns contacts with contracts moved to arrays', () =>
      target.mapper([contact1, contact2b]).should.eql([
        {contactId: 1, contracts: [{contractId: 1, contractNumber: 'a1', shortPlanName: 's1', status: 'st1', contactContractId: 6}]},
        {contactId: 2, contracts: [{contractId: 2, contractNumber: 'b1', shortPlanName: 's2', status: 'st3', contactContractId: 7}]}
      ]))

      it('returns merged duplicate contact ids', () =>
      target.mapper([contact1, contact2b, contact2c]).should.eql([
        {contactId: 1, contracts: [{contractId: 1, contractNumber: 'a1', shortPlanName: 's1', status: 'st1', contactContractId: 6}]},
        {contactId: 2, contracts: [{contractId: 2, contractNumber: 'b1', shortPlanName: 's2', status: 'st3', contactContractId: 7}, {contractId: 3, contractNumber: 'c1', shortPlanName: 's3', status: 'st3', contactContractId: 8}]}
      ]))
  })
})
