const getStatus = require('./getStatus')

const getAuthHeaders = headers => [
  'client-id',
  'user-id'
]
.map(headerName => headers[headerName])

const getResponse = (headers, ok, error) => {
  const [clientId, userId] = getAuthHeaders(headers)
  return Object.assign(
    {ok},
    error && {error: error.message || String(error)},
    clientId && {clientId},
    userId && {userId}
  )
}

module.exports = ({headers}, res, next) =>
  getStatus()
  .then(({error}) =>
    res.status(error ? 500 : 200).json(getResponse(headers, !error, error))
  )
  .catch(next)
