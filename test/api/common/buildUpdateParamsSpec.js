const proxyquire = require('../../proxyquire')

const columns = ['something', 'else']
const userId = 'The User'
const bodyAfterRemoval = {something: 'something value', else: 'else value'}
const bodyBeforeRemoval = Object.assign({toBeIgnored: true}, bodyAfterRemoval)

describe('build update params', () => {
  let target
  beforeEach(() => {
    target = proxyquire('api/common/buildUpdateParams', {
      './removeImmutableAndPkColumnsForUpdate': sinon.stub().returns(bodyAfterRemoval)
    })
  })

  it('has an expected value from passed in body', () =>
    target(columns, userId, bodyBeforeRemoval).something.should.eql('something value'))

  it('has an second expected value from passed in body', () =>
    target(columns, userId, bodyBeforeRemoval).else.should.eql('else value'))

  it('should not have removed property', () =>
    target(columns, userId, bodyBeforeRemoval).should.not.have.property('toBeIgnored'))

  it('should not have modified id if not in columns', () =>
    target(columns, userId, bodyBeforeRemoval).should.not.have.property('modifiedId'))

  it('should not have modified date if not in columns', () =>
    target(columns, userId, bodyBeforeRemoval).should.not.have.property('modifiedDate'))

  it('should have modified id if in columns', () =>
    target([{name: 'modifiedId'}], userId, bodyBeforeRemoval).modifiedId.should.eql(userId))

  it('should have modified date if in columns', () =>
    target([{name: 'modifiedId'}], userId, bodyBeforeRemoval).modifiedDate.should.exist)
})
