const log = require('../../log')
const dbExecutePromise = require('./executors/dbExecutePromise')
const dbExecuteOne = require('./executors/dbExecuteOne')
const buildInsertQuery = require('./queries/buildInsertQuery')

module.exports = (schema, bindParams) =>
  dbExecutePromise((connection) =>
    dbExecuteOne(connection, buildInsertQuery(schema, bindParams, false)))
