/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

const params = {params: true}
const headers = ['header']
const body = {body: true}

describe('create market value api', () => {
  let target, res, resJson, next, insertIntoDb, selectByIdFromDb, getUserId, buildDefaultParams, validateThenCallApi, getCreditingRatesByContract
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    insertIntoDb = sinon.stub().resolves({result: true})
    selectByIdFromDb = sinon.stub().resolves({status: 'ACTIVE'})
    getUserId = sinon.stub().resolves('a user')
    buildDefaultParams = sinon.stub().resolves({params: true})
    validateThenCallApi = sinon.stub().resolves('done')
    getCreditingRatesByContract = sinon.stub().resolves([])
    target = proxyquire('api/creditingRates/marketValue/createMarketValueApi', {
      '../../db/insertIntoDb': insertIntoDb,
      '../../db/selectByIdFromDb': selectByIdFromDb,
      '../../getUserId': getUserId,
      '../../common/buildDefaultParams': buildDefaultParams,
      '../../common/validateThenCallApi': validateThenCallApi,
      '../creditingRate/getCreditingRatesByContract': getCreditingRatesByContract
    })
  })

  it('validateThenCallApi was called', async () => {
    const result = await target({params, headers, body}, res, next)
    validateThenCallApi.should.have.been.called
  })

  describe('attempt to create', () => {
    let attemptToUpdate, apiParams
    beforeEach(async () => {
     await target({params, headers, body}, res, next)
      apiParams = {contractId: 54, userId: 'a user', creditingRateId: 552}
      attemptToUpdate = validateThenCallApi.getCall(0).args[5]
    })

    it('null contract throws error', async () => {
      selectByIdFromDb.resolves(null)
      try {
        await attemptToUpdate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Only Active Contracts can have Market Values added.')
      }
    })

    it('pending contract throws error', async () => {
      selectByIdFromDb.resolves({status: 'PENDING'})
      try {
        await attemptToUpdate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Only Active Contracts can have Market Values added.')
      }
    })

    it('too many crediting rates with same reference data throws error', async () => {
      getCreditingRatesByContract.resolves(['one', 'two'])
      try {
        await attemptToUpdate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Reference date must be unique for each market value for the contract.')
      }
    })

    it('sends insert result to response', async () => {
      await attemptToUpdate(schema, body, res, next, apiParams)
      resJson.should.have.been.calledWithExactly({result: true})
    })
  })
})
