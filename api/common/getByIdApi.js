const createError = require('http-errors')
const log = require('../../log')
const selectByIdFromDb = require('../db/selectByIdFromDb')

module.exports = (schema) =>
  async (req, res, next) => {
    log.info({domain: schema.tableShort, id: req.params.id}, 'get by id api')

    const result = await selectByIdFromDb(schema, req.params.id)
    if (result) {
      res.status(200).json(result)
    } else {
      throw createError.NotFound('Requested resource does not exist.')
    }
  }
