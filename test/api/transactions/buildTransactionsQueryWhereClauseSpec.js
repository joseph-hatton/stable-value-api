const target = require('../../../api/transactions/buildTransactionsQueryWhereClause')
const schema = require('../../../api/transactions/transactionSchema')

const testParam = (queryParams, bindParams, whereClause) =>
  target(schema, {query: queryParams}).should.eql({bindParams, whereClause, pagination: {pageNumber: 1, pageSize: 50}})

const testSortByParam = (queryParams, orderBy) =>
  target(schema, {query: queryParams}).should.eql({bindParams: {}, whereClause: '', orderBy, pagination: {pageNumber: 1, pageSize: 50}})

describe('build transactions query where clause', () => {
  it('with no query parameters', () => {
    testParam({}, {}, '')
  })

  it('with contract id query parameter', () => {
    testParam({contractId: 55}, {contractId: 55}, 'WHERE transaction.contract_id = :contractId')
  })

  it('with contract number query parameter', () => {
    testParam({contractNumber: 'RGA112341234'}, {contractNumber: 'RGA112341234'}, 'WHERE contract.contract_number = :contractNumber')
  })

  it('with contract short plan name query parameter', () => {
    testParam({shortPlanName: 'Some short plan name'}, {shortPlanName: 'Some short plan name'}, 'WHERE contract.short_plan_name = :shortPlanName')
  })

  it('with transaction type query parameter', () => {
    testParam({transactionType: 'DEPOSIT'}, {transactionType: 'DEPOSIT'}, 'WHERE transaction.transaction_type = :transactionType')
  })

  it('with transaction detail query parameter', () => {
    testParam({detail: 'INITIAL_DEPOSIT'}, {detail: 'INITIAL_DEPOSIT'}, 'WHERE transaction.detail = :detail')
  })

  it('with transaction effective date from query parameter', () => {
    testParam({from: '2017-12-13T00:00:00.000Z'}, {fromEffectiveDate: new Date('2017-12-13T00:00:00.000Z')}, 'WHERE transaction.effective_date >= :fromEffectiveDate')
  })

  it('with transaction effective date to query parameter', () => {
    testParam({to: '2017-12-13T00:00:00.000Z'}, {toEffectiveDate: new Date('2017-12-13T00:00:00.000Z')}, 'WHERE transaction.effective_date <= :toEffectiveDate')
  })

  describe('with amount query parameter', () => {
    it('and no comparator', () => {
      testParam({amount: 888.12}, {amount: 888.12}, 'WHERE transaction.amount = :amount')
    })

    it('and eq comparator', () => {
      testParam({amount: 888.12, amountComparator: 'eq'}, {amount: 888.12}, 'WHERE transaction.amount = :amount')
    })

    it('and lt comparator', () => {
      testParam({amount: 888.12, amountComparator: 'lt'}, {amount: 888.12}, 'WHERE transaction.amount < :amount')
    })

    it('and lte comparator', () => {
      testParam({amount: 888.12, amountComparator: 'lte'}, {amount: 888.12}, 'WHERE transaction.amount <= :amount')
    })

    it('and gt comparator', () => {
      testParam({amount: 888.12, amountComparator: 'gt'}, {amount: 888.12}, 'WHERE transaction.amount > :amount')
    })

    it('and gte comparator', () => {
      testParam({amount: 888.12, amountComparator: 'gte'}, {amount: 888.12}, 'WHERE transaction.amount >= :amount')
    })
  })

  const testSortParameter = (parameter, sql) => {
    describe(parameter, () => {
      it('with no sortOrder', () => {
        testSortByParam({sortBy: parameter}, sql)
      })

      it('sort order desc', () => {
        testSortByParam({sortBy: parameter, sortOrder: 'DESC'}, `${sql} DESC`)
      })

      it('sort order asc', () => {
        testSortByParam({sortBy: parameter, sortOrder: 'ASC'}, sql)
      })
    })
  }

  describe('sort by', () => {
    testSortParameter('contractId', 'transaction.contract_id')

    testSortParameter('transactionType', 'transaction.transaction_type')

    testSortParameter('effectiveDate', 'transaction.effective_date')

    testSortParameter('detail', 'transaction.detail')

    testSortParameter('amount', 'transaction.amount')

    testSortParameter('comments', 'transaction.comments')

    testSortParameter('shortPlanName', 'contract.short_plan_name')

    testSortParameter('contractNumber', 'contract.contract_number')

    testSortParameter('beginningBookValue', 'balance.beginning_book_value')

    testSortParameter('createdDate', 'transaction.created_date')

    it('invalid sort parameter ignored', () => {
      target(schema, {query: {sortBy: 'something else'}}).should.eql({bindParams: {}, whereClause: '', pagination: {pageNumber: 1, pageSize: 50}})
    })
  })
})
