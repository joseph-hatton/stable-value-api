require('require-sql')
const _ = require('lodash')
const log = require('../../log')
const dbExecutePromise = require('../db/executors/dbExecutePromise')
const dbExecuteOne = require('../db/executors/dbExecuteOne')
const contactCompanyQuery = require('./contactCompanyQuery.sql')
const schema = require('./contractSchema')
const convertRowResultFromDbValue = require('../db/converters/convertRowResultFromDbValue')

const getContactCompanyQuery = result => {
  return _(result.rows)
  .groupBy('CONTRACT_ID')
  .map((list) => ({
    contactCompanyNames: _(list).map((item) => item.NAME).sort().value(),
    ...(convertRowResultFromDbValue(schema.columns, _.omit(list[0], ['NAME'])))
  }))
  .value()
}

module.exports = async () => {
  log.info({domain: schema.tableShort}, 'get contact company names')

  const dbRequest = {
    sql: contactCompanyQuery,
    bindParams: {},
    mapResult: getContactCompanyQuery,
    executeOptions: {
      maxRows: 1000
    }
  }

  return await dbExecutePromise(connection => dbExecuteOne(connection, dbRequest))
}
