const log = require('../../log')
const getUserId = require('../getUserId')
const updateDb = require('../db/updateDb')
const buildUpdateParams = require('../common/buildUpdateParams')
const validateThenCallApi = require('../common/validateThenCallApi')
const schema = require('./riskScorecardsSchema')
const flattenRiskScorecardForInsertOrUpdateParam = require('./flattenRiskScorecardForInsertOrUpdateParam')
const getAdjustedRiskScorecardById = require('./getAdjustedRiskScorecardById')

const attemptUpdateApi = async (schema, body, res, next, {userId, params}) => {
  const flattenedBody = flattenRiskScorecardForInsertOrUpdateParam(body)
  const updateParams = buildUpdateParams(schema.columns, userId, flattenedBody)
  const result = await updateDb(schema, updateParams, params.id, userId)
  const adjustedScoreCard = await getAdjustedRiskScorecardById(params.id)
  return res.status(200).json(adjustedScoreCard)
}

module.exports = async ({ query, params, headers, body }, res, next) => {
  log.info({domain: schema.tableShort, id: params.id}, 'update risk scorecard by id api')
  const userId = getUserId(headers)
  return await validateThenCallApi(schema, body, schema.required.update, res, next, attemptUpdateApi, {userId, params})
}
