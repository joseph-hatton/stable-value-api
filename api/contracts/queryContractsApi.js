const omit = require('lodash/omit')
const log = require('../../log')
const simpleSelectFromDb = require('../db/simpleSelectFromDb')
const convertRowResultFromDbValue = require('../db/converters/convertRowResultFromDbValue')
const getManyContractsWithAssignmentState = require('./getManyContractsWithAssignmentState')
const getCurrentlyEffectiveEditHistory = require('./getCurrentlyEffectiveEditHistory')
const getContractsWithContactCompanyNames = require('./getContractsWithContactCompanyNames')
const schema = require('./contractSchema')

const mapper = ({rows}) => rows.map((row) => convertRowResultFromDbValue(schema.columns, row))

const mapRowWithHistoriesToJustAssignmentStateAndManagerId = (rowByContractId) => {
  const mappedRow = omit(rowByContractId, 'editHistories')
  if (rowByContractId.editHistories.length > 0) {
    mappedRow.assignmentState = getCurrentlyEffectiveEditHistory(mappedRow.status, rowByContractId.editHistories, 'assignmentState')
    mappedRow.managerId = getCurrentlyEffectiveEditHistory(mappedRow.status, rowByContractId.editHistories, 'managerId')
  }
  return mappedRow
}

const buildResponse = (things) => ({
  total: things.length,
  pageNumber: null,
  pageSize: null,
  results: things
})

module.exports = async (req, res) => {
  const {query: {contactCompanies}} = req
  log.info({domain: schema.tableShort, contactCompanies}, 'query contracts api')
  if (contactCompanies) {
    const contracts = await getContractsWithContactCompanyNames()
    return res.json(buildResponse(contracts))
  } else {
    const rows = await simpleSelectFromDb(getManyContractsWithAssignmentState.sql, mapper)
    const rowsRolledUpToContractId = getManyContractsWithAssignmentState.mapper(rows)
    const mappedRows = rowsRolledUpToContractId.map(mapRowWithHistoriesToJustAssignmentStateAndManagerId)
    res.json(buildResponse(mappedRows))
  }
}
