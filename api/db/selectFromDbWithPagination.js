const omit = require('lodash/omit')
const log = require('../../log')
const dbExecutePromise = require('./executors/dbExecutePromise')
const buildSelectQuery = require('./queries/buildSelectQuery')
const buildSelectCountQuery = require('./queries/buildSelectCountQuery')
const dbExecuteInOrder = require('./executors/dbExecuteInOrder')

const removePagingRowNumField = (row) => omit(row, 'r')

const areTherePaginationParameters = (whereAndParams) => !!(whereAndParams && whereAndParams.pagination && (whereAndParams.pagination.pageNumber || whereAndParams.pagination.pageSize))

module.exports = (schema, whereAndParams) => {
  return dbExecutePromise(async (connection) => {
    const selectQuery = buildSelectQuery(schema, true, whereAndParams)
    const queries = [selectQuery]
    const isPaging = areTherePaginationParameters(whereAndParams)
    if (isPaging) {
      const selectCountQuery = buildSelectCountQuery(schema, whereAndParams)
      queries.push(selectCountQuery)
    }

    const rows = await dbExecuteInOrder(connection, queries)

    const isPagingResult = !!(rows && rows.length === 2 && rows[1][0] && rows[1][0].count)

    log.info({domain: schema.tableShort, pagination: whereAndParams.pagination, isPaging, isPagingResult, rows, queries}, 'rows result')

    const hasPaginationAndIsPaging = isPaging && whereAndParams && whereAndParams.pagination && whereAndParams
    return {
      total: (isPagingResult) ? rows[1][0].count * 1 : rows[0].length,
      pageNumber: (hasPaginationAndIsPaging && whereAndParams.pagination.pageNumber) ? whereAndParams.pagination.pageNumber : null,
      pageSize: (hasPaginationAndIsPaging && whereAndParams.pagination.pageSize) ? whereAndParams.pagination.pageSize : null,
      results: (isPagingResult) ? rows[0].map(removePagingRowNumField) : rows[0]
    }
  })
}
