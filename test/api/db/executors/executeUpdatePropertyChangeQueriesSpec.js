/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const OBJECT_BEFORE = {before: true}
const OBJECT_AFTER = {before: true}
const PROP_CHANGES = ['PROP_CHANGE']

describe('execute insert update property change queries', () => {
  let target, dbExecuteInOrder, connection, buildUpdatePropertyChangeQueries
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteInOrder = sinon.stub().resolves('ONE_EXECUTED')
    buildUpdatePropertyChangeQueries = sinon.stub().returns(PROP_CHANGES)
    target = proxyquire('api/db/executors/executeUpdatePropertyChangeQueries', {
      './dbExecuteInOrder': dbExecuteInOrder,
      '../queries/buildUpdatePropertyChangeQueries': buildUpdatePropertyChangeQueries
    })
  })

  it('returns execute result', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute in order should only be called once', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER)
    .then(() => dbExecuteInOrder.callCount.should.eql(1)))

  it('execute in order should only be called with', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER)
    .then(() => dbExecuteInOrder.should.have.been.calledWithExactly(connection, PROP_CHANGES)))

  it('build insert propery change queries should have been called with', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER)
    .then(() => buildUpdatePropertyChangeQueries.should.have.been.calledWithExactly(schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER)))

  it('execute in order not called if no change queries', () => {
    buildUpdatePropertyChangeQueries.returns([])
    target(connection, schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER)
    dbExecuteInOrder.should.have.been.not.been.called
  })

  it('execute in order returns other message if no change queries', () => {
    buildUpdatePropertyChangeQueries.returns([])
    target(connection, schema, ROW_ID, OBJECT_BEFORE, OBJECT_AFTER).should.eql('no property change queries')
  })
})
