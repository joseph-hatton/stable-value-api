const proxyquire = require('../../proxyquire')

describe('healthcheck index', () => {
  let healthIndex, ping, status

  beforeEach(() => {
    ping = 'ping'
    status = 'status'
    healthIndex = proxyquire('api/health/index', {
      './ping': ping,
      './status': status
    })
  })

  it('has ping and status', () => {
    return healthIndex.should.eql({ping, status})
  })
})
