const log = require('../../../log')
const selectFromDb = require('../../db/selectFromDb')
const schema = require('../../feeDistributions/feeDistributionsSchema')
const applyPayment = require('../../invoices/applyPayment')
const getInvoiceById = require('../../invoices/getInvoiceById')
const deleteFeeDistribution = require('./deleteFeeDistribution')

const applyPaymentAndDelete = async (dbFeeDistribution, dbTransaction, userId) => {
  if (dbFeeDistribution.invoiceId) {
    const invoice = await getInvoiceById(dbFeeDistribution.invoiceId)
    await applyPayment(invoice, dbFeeDistribution.amount * -1, dbFeeDistribution.adjustment * -1, userId)
  }
  log.warn({domain: schema.tableShort, transactionId: dbTransaction.transactionId, dbFeeDistribution}, 'would have deleted fee distribution with different invoice id (non null)')
  await deleteFeeDistribution(dbTransaction.transactionId, dbFeeDistribution, userId)
}

module.exports = async (dbTransaction, feeDistributions, userId) => {
  const dbFeeDistributions = await selectFromDb(schema, {whereClause: 'WHERE transaction_id = :transactionId', bindParams: {transactionId: dbTransaction.transactionId}})
  const feeDistributionsWithDiffInvoiceId = dbFeeDistributions.filter((dbFeeDistribution) =>
    feeDistributions.filter((clientFeeDistribution) => clientFeeDistribution.invoiceId === dbFeeDistribution.invoiceId || (!clientFeeDistribution.invoiceId && !dbFeeDistribution.invoiceId)).length === 0)

  log.info({domain: schema.tableShort, transactionId: dbTransaction.transactionId, dbFeeDistributions, feeDistributionsWithDiffInvoiceId}, 'fee distributions with different invoice ids (non null)')

  for (const dbFeeDistribution of feeDistributionsWithDiffInvoiceId) {
    await applyPaymentAndDelete(dbFeeDistribution, dbTransaction, userId)
  }
}
