const createError = require('http-errors')
const log = require('../../../log')
const schema = require('../contractSchema')
const transitionContractInWorkflow = require('./transitionContractInWorkflow')
const deleteForecasts = require('../../creditingRates/forecasts/deleteForecasts')
const buildUpdateParams = require('../../common/buildUpdateParams')
const updateDb = require('../../db/updateDb')

const validateCanTransition = async ({contract, userId}) => {
  if (contract.status !== 'ACTIVE') {
    return 'Only ACTIVE Contracts may be scheduled for pending termination.'
  }
}

const callBeforeChangingState = async ({contract, userId, terminationDate, statedMaturityDate}) => {
  const contractId = contract.contractId
  log.info({domain: schema.tableShort, id: contractId, userId}, 'schedule pending termination contract')

  await deleteForecasts(contractId, terminationDate, userId)
  const updateParams = buildUpdateParams(schema.columns, userId, {statedMaturityDate})
  await updateDb(schema, updateParams, contractId, userId)
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'schedulePendingTermination',
  nextContractStatus: 'PENDING_TERMINATION',
  validateCanTransition,
  callBeforeChangingState
})
