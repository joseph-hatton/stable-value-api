require('require-sql')
const sql = require('./overrideGetManyContactContractsSelect.sql')
const omit = require('lodash/omit')
const buildCustomSelectMapper = require('../../common/buildCustomSelectMapper')

const uniqueRowProperty = 'contactContractId'

const buildNewTopLevelObjectFromRow = (row) => row

const appendChildToTopLevel = () => ''

module.exports = {
  sql,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
