const proxyquire = require('../../proxyquire')

describe('health cache', () => {
  let caching

  beforeEach(() => {
    caching = sinon.stub().returns('cache')
  })

  const apply = () =>
    proxyquire('api/health/cache', {
      'cache-manager': {caching}
    })

  it('creates cache', () => {
    apply().should.equal('cache')
    caching.should.have.been.calledWithExactly({store: 'memory', max: 100, ttl: 5 * 60})
  })
})
