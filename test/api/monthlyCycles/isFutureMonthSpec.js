const proxyquire = require('../../proxyquire')
const schema = require('../../../api/monthlyCycles/monthlyCyclesSchema')

describe('is future month', () => {
  let target, getLatestCycle
  beforeEach(() => {
    getLatestCycle = sinon.stub().resolves({startDate: new Date('2013-11-20T00:00:00.000Z'), closed: false})
    target = proxyquire('api/monthlyCycles/isFutureMonth', {
      './getLatestCycle': getLatestCycle
    })
  })

  it('before start', async () => {
    const result = await target('2013-10-10T00:00:00.000Z')
    result.should.have.been.eql(false)
  })

  it('same as start', async () => {
    const result = await target('2013-11-20T00:00:00.000Z')
    result.should.have.been.eql(false)
  })

  it('after start, same month', async () => {
    const result = await target('2013-11-30T00:00:00.000Z')
    result.should.have.been.eql(false)
  })

  it('after start, diff month', async () => {
    const result = await target('2013-12-01T00:00:00.000Z')
    result.should.have.been.eql(true)
  })
})
