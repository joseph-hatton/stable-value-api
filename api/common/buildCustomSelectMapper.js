const log = require('../../log')

module.exports = (uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel) =>
  (dbResults) => {
    // log.info({results: (dbResults) ? dbResults.length : 0}, 'custom select mapper start')
    const finalResults = []
    dbResults.forEach((result) => {
      let topLevel = finalResults.find((r) => r[uniqueRowProperty] === result[uniqueRowProperty])
      if (!topLevel) {
        topLevel = buildNewTopLevelObjectFromRow(result)
        finalResults.push(topLevel)
      }
      appendChildToTopLevel(topLevel, result)
    })
    return finalResults
  }
