/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

describe('db converters index', () => {
  let target, beginningOfDayDateConverter, booleanConverter, bpsConverter, dateConverter, numberConverter, percentConverter
  beforeEach(() => {
    beginningOfDayDateConverter = sinon.stub()
    booleanConverter = sinon.stub()
    bpsConverter = sinon.stub()
    dateConverter = sinon.stub()
    numberConverter = sinon.stub()
    percentConverter = sinon.stub()
    target = proxyquire('api/db/converters', {
      './beginningOfDayDateConverter': beginningOfDayDateConverter,
      './booleanConverter': booleanConverter,
      './bpsConverter': bpsConverter,
      './dateConverter': dateConverter,
      './numberConverter': numberConverter,
      './percentConverter': percentConverter
    })
  })

  it('has beginning of day date converter', () => target.beginningOfDayDateConverter.should.eql(beginningOfDayDateConverter))

  it('has boolean converter', () => target.booleanConverter.should.eql(booleanConverter))

  it('has bps converter', () => target.bpsConverter.should.eql(bpsConverter))

  it('has date converter', () => target.dateConverter.should.eql(dateConverter))

  it('has number converter', () => target.numberConverter.should.eql(numberConverter))

  it('has percent converter', () => target.percentConverter.should.eql(percentConverter))
})
