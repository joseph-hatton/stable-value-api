const {schema} = require('../../data/testSchema')
const target = require('../../../api/common/buildWhereClauseAndParamsWithPaging')

const contractIdRequest = {query: {contractId: 55}}

const contractIdRequestWithPaginationInfo = {query: {contractId: 55, pageNumber: 18, pageSize: 11}}

const contractIdRequestParser = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.contractId) {
    response.bindParams.contractId = req.query.contractId * 1
    whereClauseBits.push('someTable.contract_id = :contractId')
  }
}

describe('build where clause and params with paging', () => {
  it('with no query parameters', () => {
    target(schema, {}).should.eql({bindParams: {}, whereClause: '', pagination: {pageNumber: null, pageSize: null}})
  })

  it('ignores parameters by default', () => {
    target(schema, contractIdRequest).should.eql({bindParams: {}, whereClause: '', pagination: {pageNumber: null, pageSize: null}})
  })

  it('with contract id query parameter', () => {
    target(schema, contractIdRequest, [contractIdRequestParser]).should.eql({bindParams: {contractId: 55}, whereClause: 'WHERE someTable.contract_id = :contractId', pagination: {pageNumber: null, pageSize: null}})
  })

  it('with contract id query parameter', () => {
    target(schema, contractIdRequestWithPaginationInfo, [contractIdRequestParser]).should.eql({bindParams: {contractId: 55}, whereClause: 'WHERE someTable.contract_id = :contractId', pagination: {pageNumber: 18, pageSize: 11}})
  })
})
