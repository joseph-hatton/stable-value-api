const validateSchema = require('../validateSchema')
const target = require('../../../api/invoices/invoicesSchema')
const {dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

describe('invoice schema', () => {
  validateSchema(target, {
    tableShort: 'invoice',
    table: 'sch_stbv.invoice',
    tableId: 'invoice_id',
    sequence: 'sch_stbv.invoice_seq.NEXTVAL',
    orderBy: 'contract_id',
    columns: [
      {name: 'invoiceId', column: 'invoice_id', sequence: 'sch_stbv.invoice_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'invoiceNumber', column: 'invoice_number', converter: numberConverter},
      {name: 'invoiceDate', column: 'invoice_date', converter: dateConverter},
      {name: 'dueDate', column: 'due_date', converter: dateConverter},
      {name: 'beginningBalance', column: 'beginning_balance', converter: numberConverter},
      {name: 'endingBalance', column: 'ending_balance', converter: numberConverter},
      {name: 'feesAccrued', column: 'fees_accrued', converter: numberConverter},
      {name: 'feesReceived', column: 'fees_received', converter: numberConverter},
      {name: 'adjustments', converter: numberConverter},
      {name: 'outstandingBalance', column: 'outstanding_balance', converter: numberConverter},
      {name: 'beginDate', column: 'begin_date', converter: dateConverter},
      {name: 'endDate', column: 'end_date', converter: dateConverter}
    ],
    required: {
      new: ['contractId', 'invoiceDate', 'dueDate', 'beginningBalance', 'endingBalance', 'feesAccrued', 'feesReceived', 'adjustments', 'outstandingBalance', 'beginDate', 'endDate'],
      update: ['contractId', 'invoiceDate', 'dueDate', 'beginningBalance', 'endingBalance', 'feesAccrued', 'feesReceived', 'adjustments', 'outstandingBalance', 'beginDate', 'endDate']
    }
  })
})
