/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

const userId = 'a user'
const insertParams = {insertParams: true}

describe('add initial crediting rate', () => {
  let target, selectFromDb, contract, effectiveDate, referenceDate, insertIntoDb, buildDefaultParams
  beforeEach(() => {
    contract = {
      contractId: 55,
      rinitialCreditingRate: 82,
      effectiveDate: '2017-11-13T00:00:00.000Z',
      initialCoveredMarketValue: 5.2,
      bookValue: 78
    }
    effectiveDate = '2017-11-13T00:00:00.000Z'
    referenceDate = '2017-12-01T00:00:00.000Z'
    selectFromDb = sinon.stub().resolves([])
    insertIntoDb = sinon.stub().withArgs(schema, insertParams, userId).resolves('insert response')
    buildDefaultParams = sinon.stub().withArgs(schema.columns, userId, contract).returns(insertParams)
    target = proxyquire('api/creditingRates/creditingRate/addInitialCreditingRate', {
      '../../db/selectFromDb': selectFromDb,
      '../../db/insertIntoDb': insertIntoDb,
      '../../common/buildDefaultParams': buildDefaultParams
    })
  })

  it('returns null if there are already existing non-forecasts', async () => {
    selectFromDb.resolves(['one'])
    const result = await target(contract, effectiveDate, userId)
    should.not.exist(result)
  })

  it('returns insert into db result if no existing non-forecasts', async () => {
    const result = await target(contract, effectiveDate, userId)
    result.should.eql('insert response')
  })

  it('selectFromDb called with', async () => {
    await target(contract, effectiveDate, userId)
    selectFromDb.getCall(0).args[0].should.eql(schema)
    selectFromDb.getCall(0).args[1].whereClause.should.eql('WHERE sch_stbv.crediting_rate.contract_id = :contractId and sch_stbv.crediting_rate.status != \'FORECAST\'')
    selectFromDb.getCall(0).args[1].bindParams.should.eql({contractId: contract.contractId})
  })

  const buildContractExpectation = (overrides = {}) => {
    return Object.assign({}, {
      contractId: contract.contractId,
      effectiveDate: contract.effectiveDate,
      rate: contract.initialCreditingRate,
      status: 'APPROVED',
      referenceDate: contract.effectiveDate,
      locked: true,
      marketValue: contract.initialCoveredMarketValue,
      annualEffectiveYield: 1,
      duration: 1.0,
      bookValue: contract.initialCoveredBookValue,
      modifiedId: userId
    }, overrides)
  }

  it('buildDefaultParams called with', async () => {
    await target(contract, effectiveDate, userId)
    buildDefaultParams.should.have.been.called
    buildDefaultParams.getCall(0).args[0].should.eql({columns: schema.columns})
    buildDefaultParams.getCall(0).args[1].should.eql(buildContractExpectation())
  })

  it('buildDefaultParams called with defaulted market value if no initial', async () => {
    contract.initialCoveredMarketValue = null
    await target(contract, effectiveDate, userId)
    buildDefaultParams.should.have.been.called
    buildDefaultParams.getCall(0).args[0].should.eql({columns: schema.columns})
    buildDefaultParams.getCall(0).args[1].should.eql(buildContractExpectation({ marketValue: 0 }))
  })
})
