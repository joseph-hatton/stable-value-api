const _ = require('lodash')
const log = require('../../log')
const dbExecutePromise = require('../db/executors/dbExecutePromise')
const dbExecuteOne = require('../db/executors/dbExecuteOne')

const schema = require('../contractEditHistories/contractEditHistorySchema')

const managerContractsQuery = `SELECT contract_id, manager_id, effective_date, company.name FROM ${schema.table} INNER JOIN sch_stbv.COMPANY ON manager_id = company.COMPANY_ID WHERE manager_id is not null`

const dbManagerToPairs = (dbManager) => ([dbManager.CONTRACT_ID, {managerId: 1 * dbManager.MANAGER_ID, name: dbManager.NAME}])

const getContractManagers = result => {
  const contractManagers = _(result.rows)
    .groupBy('CONTRACT_ID')
    .map((effectiveDates) => {
      if (effectiveDates.length === 1) {
        return dbManagerToPairs(effectiveDates[0])
      } else {
        const latestManagerForContract = _.orderBy(effectiveDates, 'EFFECTIVE_DATE', 'desc')[0]
        return dbManagerToPairs(latestManagerForContract)
      }
    })
    .fromPairs()
    .value()
  return contractManagers
}

module.exports = async () => {
  log.info({
    domain: schema.tableShort
  }, 'get contract managers')

  const dbRequest = {
    sql: managerContractsQuery,
    bindParams: {},
    mapResult: getContractManagers,
    executeOptions: {
      maxRows: 1000
    }
  }

  return await dbExecutePromise(connection => dbExecuteOne(connection, dbRequest))
}
