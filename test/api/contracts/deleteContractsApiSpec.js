/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contracts/contractSchema')

const user = 'a user'
const contractId = 5

describe('delete contract api', () => {
  let target, req, res, resJson, next, body, getUserId, deleteFromDb, deleteByIdFromDb

  beforeEach(() => {
    body = { status: 'ACTIVE' }
    req = { params: {id: contractId}, headers: sinon.spy(), body: body }
    resJson = sinon.spy()
    res = { status: sinon.stub().returns({ json: resJson }) }
    next = sinon.spy()
    getUserId = sinon.stub().withArgs(req.headers).returns(user)
    deleteFromDb = sinon.stub().resolves('update response')
    deleteByIdFromDb = sinon.stub()
    target = proxyquire('api/contracts/deleteContractApi', {
      '../db/deleteFromDb': deleteFromDb,
      '../db/deleteByIdFromDb': deleteByIdFromDb,
      '../getUserId': getUserId
    })
  })

  it('gets user id', async () => {
    await target(req, res, next)
    getUserId.callCount.should.equal(1)
  })

  it('deletes from contract edit history table', async () => {
    await target(req, res, next)
    deleteFromDb.should.have.been.calledWithExactly('delete from sch_stbv.contract_edit_history where contract_id = :contractId', {contractId})
  })

  it('deletes from contract derivative type table', async () => {
    await target(req, res, next)
    deleteFromDb.should.have.been.calledWithExactly('delete from sch_stbv.contract_derivative_type where contract_id = :contractId', {contractId})
  })

  it('deletes from contract table', async () => {
    await target(req, res, next)
    deleteByIdFromDb.should.have.been.calledWithExactly(schema, contractId, user)
  })

  it('sends contract id to json', async () => {
    await target(req, res, next)
    resJson.should.have.been.calledWithExactly({id: contractId})
  })
})
