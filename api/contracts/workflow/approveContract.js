const log = require('../../../log')
const calculateAndSaveCreditingRateForecast = require('../../creditingRates/forecasts/calculateAndSaveCreditingRateForecast')
const addInitialCreditingRate = require('../../creditingRates/creditingRate/addInitialCreditingRate')
const getContractEditHistories = require('../../contractEditHistories/getContractEditHistories')
const updateContractEditHistory = require('../../contractEditHistories/updateContractEditHistory')
const schema = require('../contractSchema')
const transitionContractInWorkflow = require('./transitionContractInWorkflow')

const validateCanTransition = async ({contract, userId}) => {
  if (contract.status !== 'SUBMITTED') {
    return 'Only SUBMITTED Contracts may be approved.'
  } else if (contract.modifiedId === userId) {
    return 'A Contract cannot be approved by the last person who modified it.'
  }
}

const actuallyApprove = async ({contract, userId}) => {
  await calculateAndSaveCreditingRateForecast(contract)
  const contractId = contract.contractId
  log.info({domain: schema.tableShort, id: contractId, userId}, 'calculated and saved crediting rate forecasts')

  await addInitialCreditingRate(contract)

  log.info({domain: schema.tableShort, id: contractId, userId}, 'added initial crediting rate')

  const editHistories = await getContractEditHistories(contractId)
  log.info({domain: schema.tableShort, id: contractId, userId, editHistories: editHistories.length}, 'edit histories found')
  if (editHistories.length === 1) {
    await updateContractEditHistory({effectiveDate: contract.effectiveDate}, editHistories[0].contractEditHistoryId, userId)
    log.info({domain: schema.tableShort, id: contractId, userId}, 'edit history updated')
  }
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'approve',
  nextContractStatus: 'ACTIVE',
  validateCanTransition,
  callBeforeChangingState: actuallyApprove
})
