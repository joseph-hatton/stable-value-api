/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const contractSchema = require('../../../../api/contracts/contractSchema')

const userId = 'a user'
const contractId = 5

describe('undo pending termination contract', () => {
  let target, transitionContractInWorkflow, calculateAndSaveCreditingRateForecast, buildUpdateParams, updateDb
  beforeEach(() => {
    calculateAndSaveCreditingRateForecast = sinon.stub().resolves('calculate done')
    transitionContractInWorkflow = sinon.stub().returns('base returned')
    buildUpdateParams = sinon.stub().returns('updateParams')
    updateDb = sinon.stub().resolves('update done')
    target = proxyquire('api/contracts/workflow/undoPendingTerminationContract', {
      './transitionContractInWorkflow': transitionContractInWorkflow,
      '../../creditingRates/forecasts/calculateAndSaveCreditingRateForecast': calculateAndSaveCreditingRateForecast,
      '../../common/buildUpdateParams': buildUpdateParams,
      '../../db/updateDb': updateDb
    })
  })

  it('returns the base result', () => {
    target.should.eql('base returned')
  })

  it('base should have been called once', () => {
    transitionContractInWorkflow.should.have.been.called
  })

  it('sets action verb', () => {
    transitionContractInWorkflow.getCall(0).args[0].actionVerb.should.eql('undoScheduledPendingTermination')
  })

  it('sets next contract status', () => {
    transitionContractInWorkflow.getCall(0).args[0].nextContractStatus.should.eql('ACTIVE')
  })

  it('sets a callBeforeChangingState', () => {
    should.exist(transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState)
  })

  describe('validate can transition', () => {
    let asyncValidate
    beforeEach(() => {
      asyncValidate = transitionContractInWorkflow.getCall(0).args[0].validateCanTransition
    })

    it('fails with PENDING contract', async () => {
      const result = await asyncValidate({contract: {status: 'PENDING'}})

      result.should.eql('Only PENDING_TERMINATION Contracts may be undone from a scheduled pending termination.')
    })

    it('passes with PENDING_TERMINATION contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'PENDING_TERMINATION'}})

      should.not.exist(result)
    })
  })

  describe('callBeforeChangingState', () => {
    let callBeforeChangingState
    beforeEach(() => {
      callBeforeChangingState = transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState
    })

    it('calls calculate and save forecasts', async () => {
      await callBeforeChangingState({contract: {status: 'PENDING'}})

      calculateAndSaveCreditingRateForecast.should.have.been.calledWithExactly({status: 'PENDING'})
    })

    it('resets contract stated maturity date', async () => {
      await callBeforeChangingState({contract: {status: 'PENDING'}, userId})

      buildUpdateParams.should.have.been.calledWithExactly(contractSchema.columns, userId, {statedMaturityDate: new Date('2999-01-01T00:00:00.000Z')})
      updateDb.should.have.been.called
      updateDb.callCount.should.eql(1)
      updateDb.getCall(0).args[1].should.eql('updateParams')
    })
  })
})
