const {dbSchemaName} = require('../config')
const {booleanConverter, dateConverter, numberConverter} = require('../db/converters')

const table = 'monthly_cycle'
const tableId = 'monthly_cycle_id'
const sequence = `${dbSchemaName}.monthly_cycle_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: `${table}.start_date desc`,
  columns: [
    {name: 'monthlyCycleId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'startDate', column: 'start_date', converter: dateConverter},
    {name: 'endDate', column: 'end_date', converter: dateConverter},
    {name: 'closed', converter: booleanConverter}
  ],
  required: {
    new: ['startDate', 'endDate'],
    update: ['startDate', 'endDate']
  }
}
