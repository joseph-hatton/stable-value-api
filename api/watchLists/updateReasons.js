const buildReasonWatchListInsertParams = require('./buildReasonWatchListInsertParams')
const reasonWatchListSchema = require('./reasonWatchListSchema')
const selectFromDb = require('../db/selectFromDb')
const deleteFromDb = require('../db/deleteFromDb')
const insertIntoDb = require('../db/insertIntoDb')
const log = require('../../log')
const union = require('lodash/union')

module.exports = async (body, watchListId, userId, SKIP_AUDIT) => {
    const deleteReasonWatchListSql = `delete from ${reasonWatchListSchema.table} where watch_list_id = :watchListId`
    const reasonIds = (i) => i.reasonId

    let reasonsCurrentIds = await selectFromDb(reasonWatchListSchema, { whereClause: `WHERE ${reasonWatchListSchema.table}.watch_list_id = :id`, bindParams: {id: watchListId} })
    reasonsCurrentIds = reasonsCurrentIds.map(reasonIds)
    let reasonsNewIds = body.reasons.map(reasonIds)
    let unionOfNewAndOldReasons = union(reasonsCurrentIds, reasonsNewIds)

    if (reasonsCurrentIds.length !== unionOfNewAndOldReasons.length || reasonsNewIds.length !== unionOfNewAndOldReasons.length) {
      await deleteFromDb(deleteReasonWatchListSql, {watchListId})
      await Promise.all(body.reasons.map(async (reason) => {
        const reasonWatchListInsertParams = buildReasonWatchListInsertParams(userId, body, watchListId, reason.reasonId)
        return await insertIntoDb(reasonWatchListSchema, reasonWatchListInsertParams, userId, SKIP_AUDIT)
      }))
    }
}
