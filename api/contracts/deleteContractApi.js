const createError = require('http-errors')
const log = require('../../log')
const {isPointingToProductionDatabase} = require('../config')
const deleteFromDb = require('../db/deleteFromDb')
const deleteByIdFromDb = require('../db/deleteByIdFromDb')
const getUserId = require('../getUserId')
const schema = require('./contractSchema')
const contractEditHistorySchema = require('../contractEditHistories/contractEditHistorySchema')
const contractDerivativeTypeSchema = require('../contracts/derivativeTypes/contractDerivativeTypeSchema')

module.exports = async ({ params, headers, body }, res, next) => {
  const contractId = params.id
  const userId = getUserId(headers)

  log.info({domain: schema.tableShort, id: contractId, userId, isPointingToProductionDatabase}, 'delete contract by id api')

  if (isPointingToProductionDatabase) {
    throw createError.Forbidden('The delete contract api is disabled in production.')
  } else {
    await deleteFromDb(`delete from ${contractEditHistorySchema.table} where contract_id = :contractId`, {contractId})

    await deleteFromDb(`delete from ${contractDerivativeTypeSchema.table} where contract_id = :contractId`, {contractId})

    await deleteByIdFromDb(schema, contractId, userId)

    return res.status(200).json({id: contractId})
  }
}
