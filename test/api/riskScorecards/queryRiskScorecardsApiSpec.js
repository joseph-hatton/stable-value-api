const proxyquire = require('../../proxyquire')
const schema = require('../../../api/riskScorecards/riskScorecardsSchema')

describe('query risk scorecards api', () => {
  const scorecards = [{ riskScorecardId: 'aId', name: 'a' }, { riskScorecardId: 'bId', name: 'b' }]
  const adjustedScorecards = scorecards.map((scorecard) => {
    scorecard.adjusted = true
    return scorecard
  })
  const request = { headers: 'abc' }

  let response, target, selectFromDb, buildContractIdWhereClause, getLookupsAndAdjustRiskScorecards

  beforeEach(() => {
    response = {
      json: sinon.spy()
    }
    buildContractIdWhereClause = sinon.stub().withArgs(schema, request).returns('buildContractIdWhereClause result')
    selectFromDb = sinon.stub().withArgs(schema, 'buildContractIdWhereClause result').resolves(scorecards)
    getLookupsAndAdjustRiskScorecards = sinon.stub().withArgs(scorecards).resolves(adjustedScorecards)

    target = proxyquire('api/riskScorecards/queryRiskScorecardsApi', {
      '../db/selectFromDb': selectFromDb,
      '../contracts/buildContractIdWhereClause': buildContractIdWhereClause,
      './getLookupsAndAdjustRiskScorecards': getLookupsAndAdjustRiskScorecards
    })
  })

  it('adjusts scorecards and returns result', async () => {
    const result = await target(request, response)
    response.json.should.have.been.calledWithExactly({
      total: adjustedScorecards.length,
      startId: adjustedScorecards[0].riskScorecardId,
      limit: null,
      results: adjustedScorecards}
    )
  })
})
