const transitionContractInWorkflow = require('./transitionContractInWorkflow')

const validateCanTransition = async ({contract, userId}) => {
  if (!['PENDING', 'PENDING_JASPER'].includes(contract.status)) {
    return 'Only PENDING or PENDING_JASPER Contracts may be inactivated.'
  }
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'inactivate',
  nextContractStatus: 'INACTIVE',
  validateCanTransition
})
