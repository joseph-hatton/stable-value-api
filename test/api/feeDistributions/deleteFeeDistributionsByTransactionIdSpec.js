/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

describe('delete fee distribution by id', () => {
  let target, deleteByIdFromDb, selectFromDb, getInvoiceById, applyPayment, deleteFeeDistribution, invoice, schema

  beforeEach(() => {
    schema = sinon.stub()
    deleteByIdFromDb = sinon.stub()
    selectFromDb = sinon.stub()
    getInvoiceById = sinon.stub()
    applyPayment = sinon.stub()
    deleteFeeDistribution = sinon.stub()
    invoice = sinon.stub()

    target = proxyquire('api/feeDistributions/deleteFeeDistributionsByTransactionId', {
      '../feeDistributions/feeDistributionsSchema': schema,
      '../db/deleteByIdFromDb': deleteByIdFromDb,
      '../db/selectFromDb': selectFromDb,
      '../invoices/getInvoiceById': getInvoiceById,
      '../invoices/applyPayment': applyPayment
    })
  })

  it('deletes the transaction record', async () => {
    const transactionId = 542
    const user = 'tester'
    const invoiceId = 402
    const feeDistributionId = 123
    const amount = 5000

    selectFromDb.withArgs(schema, {
      whereClause: 'WHERE transaction_id = :transactionId',
      bindParams: {
        transactionId: transactionId
      }
    }).resolves([{feeDistributionId, invoiceId, amount, adjustment: 0}])
    getInvoiceById.withArgs(invoiceId).resolves(invoice)

    await target(transactionId, user)

    applyPayment.should.have.been.calledWithExactly(invoice, amount * -1, 0, user)
    deleteByIdFromDb.should.have.been.calledWithExactly(schema, feeDistributionId, user)
  })
})
