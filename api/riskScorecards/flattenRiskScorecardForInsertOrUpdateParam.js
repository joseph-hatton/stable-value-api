const requiredFields = ['cashFlow', 'demographics', 'competingFunds', 'planSponsor', 'planSvFundBalances', 'planDesign', 'fundManagement', 'wrappedPortfolio', 'marketBookRatio', 'overallUnadjusted', 'overallAdjusted', 'pooledFundBalances', 'pooledFundDesign']

const addScoreAndComments = (flattened, body, field) => {
  if (body[field]) {
    flattened[field] = body[field].score
    flattened[`${field}Comments`] = (body[field].comments) ? body[field].comments : null
  }
}

const flattenBodyForInsert = (body) => {
  const flattened = {
    contractId: body.contractId,
    effectiveDate: body.effectiveDate
  }
  requiredFields.forEach((field) => addScoreAndComments(flattened, body, field))
  return flattened
}

module.exports = (body, includeContractType = false) => {
  const flattened = {
    contractId: body.contractId,
    effectiveDate: body.effectiveDate
  }
  if (includeContractType) {
    flattened.contractType = body.contractType
  }
  requiredFields.forEach((field) => addScoreAndComments(flattened, body, field))
  return flattened
}
