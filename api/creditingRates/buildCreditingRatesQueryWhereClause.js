const _ = require('lodash')
const moment = require('moment')
const log = require('../../log')
const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const sortByKeys = {
  managerId: 'crediting_rate.contract_id',
  contractId: 'crediting_rate.contract_id',
  contractNumber: 'contract.contract_id',
  shortPlanName: 'contract.short_plan_name',
  status: 'crediting_rate.status',
  marketValue: 'crediting_rate.market_value',
  bookValue: 'balance.ending_book_value',
  mvbvRatio: 'mvbv_ratio',
  annualEffectiveYield: 'crediting_rate.annual_effective_yield',
  duration: 'crediting_rate.duration',
  grossRate: 'crediting_rate.gross_rate',
  rate: 'crediting_rate.rate',
  effectiveDate: 'crediting_rate.effective_date',
  referenceDate: 'crediting_rate.reference_date'
}

const queryColumns = _.keys(sortByKeys)

const whereQueryParam = (attribute, sqlBit, attributeConverter = (val) => val, bindVariableName, bind = true) =>
  (schema, req, whereClauseBits, response) => {
    if (req.query && req.query[attribute]) {
      whereClauseBits.push(_.isFunction(sqlBit) ? sqlBit(req.query[attribute]) : sqlBit)
      if (bind) {
        response.bindParams[bindVariableName || attribute] = attributeConverter(req.query[attribute])
      }
    }
  }

const fromDate = (val) => {
  return new Date(val)
}

const toDate = (val) => {
  return moment(val).endOf('day').toDate()
}

const whereContractStatusIn = (schema, req, whereClauseBits) => {
  whereClauseBits.push(`contract.status in ('PENDING_TERMINATION', 'ACTIVE')`)
}

const whereEffectiveDateInNearFuture = (schema, req, whereClauseBits) => {
  whereClauseBits.push(`crediting_rate.EFFECTIVE_DATE between sysdate and (sysdate + 31)`)
}

const whereContractIdIn = whereQueryParam('contractIds', contractIds => `${sortByKeys.contractId} in (${contractIds.join(', ')})`, null, null, false)
const whereCreditingRateContractIdEquals = whereQueryParam('contractId', `${sortByKeys.contractId} = :contractId`, (val) => val * 1)
const whereStatusEquals = whereQueryParam('status', `${sortByKeys.status} = :status`)
const whereFromEffectiveDate = whereQueryParam('fromEffectiveDate', `${sortByKeys.effectiveDate} >= :fromEffectiveDate`, fromDate, 'fromEffectiveDate')
const whereToEffectiveDate = whereQueryParam('toEffectiveDate', `${sortByKeys.effectiveDate} <= :toEffectiveDate`, toDate, 'toEffectiveDate')
const whereFromReferenceDate = whereQueryParam('fromReferenceDate', `${sortByKeys.referenceDate} >= :fromReferenceDate`, fromDate, 'fromReferenceDate')
const whereToReferenceDate = whereQueryParam('toReferenceDate', `${sortByKeys.referenceDate} <= :toReferenceDate`, toDate, 'toReferenceDate')

const addOrderBy = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.sortBy && sortByKeys[req.query.sortBy]) {
    response.orderBy = sortByKeys[req.query.sortBy]
    if (req.query && req.query.sortOrder && req.query.sortOrder.toUpperCase() === 'DESC') {
      response.orderBy += ' DESC'
    }
  }
}

module.exports = (schema, req) => {
  if (!req.query.pageNumber || req.query.pageNumber * 1 === 0) {
    req.query.pageNumber = 1
  }
  if (!req.query.pageSize) {
    req.query.pageSize = 50
  }
  const clauseBuilders = [
    whereContractStatusIn,
    whereContractIdIn,
    whereCreditingRateContractIdEquals,
    whereStatusEquals,
    whereFromEffectiveDate,
    whereToEffectiveDate,
    whereFromReferenceDate,
    whereToReferenceDate
  ]

  const hasQueryColumn = _(req.query)
    .pickBy(value => value)
    .keys()
    .some(param => queryColumns.includes(param))
  if (!hasQueryColumn) {
    clauseBuilders.push(whereEffectiveDateInNearFuture)
  }

  clauseBuilders.push(addOrderBy)

  return buildWhereClauseAndParamsWithPaging(schema, req, clauseBuilders)
}
