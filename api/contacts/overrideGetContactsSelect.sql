SELECT  contact.*,
        company.name as company_name,
        contact_contract.contract_id,
        contact_contract.contact_contract_id,
        contract.contract_number,
        contract.short_plan_name,
        contract.status
FROM sch_stbv.contact 
LEFT OUTER JOIN sch_stbv.company 
  ON contact.company_id = company.company_id
LEFT OUTER JOIN sch_stbv.contact_contract 
  ON contact.contact_id = contact_contract.contact_id
LEFT OUTER JOIN sch_stbv.contract 
  ON contact_contract.contract_id = contract.contract_id