const proxyquire = require('../../proxyquire')
const {schema} = require('../../data/testSchema')
const defaultParams = {a: true}
const deleteResult = {rowsAffected: 1}
const user = 'THE USER'
const THE_ID = 42
const VALIDATE_THEN_CALL_API_RESULT = 'VALIDATE_THEN_CALL_API_RESULT'

describe('delete by id api', () => {
  const companies = [{ id: 'aId', name: 'a' }, { id: 'bId', name: 'b' }]
  const request = { params: {id: THE_ID} }

  let response, target, next, getUserId, responseStatusSend, responseJson, deleteByIdFromDb

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    responseJson = sinon.spy()
    response = {
      json: responseJson,
      status: sinon.stub().returns({ send: responseStatusSend, json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    const query = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)
    deleteByIdFromDb = sinon.stub().resolves(deleteResult)
    target = proxyquire('api/common/deleteByIdApi', {
      '../getUserId': getUserId,
      '../db/deleteByIdFromDb': deleteByIdFromDb
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
    })

    it('sends expected data into insert into', async () => {
      await target(schema)(request, response, next)

      deleteByIdFromDb.should.have.been.calledWithExactly(schema, THE_ID, user)
    })

    it('sends insert results to response', async () => {
      await target(schema)(request, response, next)

      response.json.should.have.been.calledWithExactly(deleteResult)
    })
  })
})
