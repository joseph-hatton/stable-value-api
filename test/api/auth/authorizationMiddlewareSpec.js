/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

describe('authorizationMiddleware', () => {
  let target, config, getUserId, isAuthorized, determineEntitlements, mem, app, entitlements, req, res, next
  beforeEach(() => {
    config = {
      responses: { unauthorized: { status: 400, message: 'not allowed' } }
    }
    getUserId = sinon.stub().returns({})
    isAuthorized = sinon.stub()
    mem = sinon.stub()
    entitlements = {
      'urlKey': {
        'method': 'method'
      }
    }
    req = {}
    res = {
      status: () => res,
      send: sinon.stub()
    }
    next = sinon.stub()
    determineEntitlements = 'determineEntitlements'
    app = {
      method: sinon.stub()
    }
  })
  const setupTarget = () => {
    target = proxyquire('api/auth/authorizationMiddleware', {
      '../getUserId': getUserId,
      './isAuthorized': isAuthorized,
      './determineEntitlements': determineEntitlements,
      'mem': mem,
      './entitlements': entitlements,
      '../config': config
    })
  }
  const callEntitlementMiddleware = async () => {
    target(app)
    await app.method.getCall(0).args[1](req, res, next)
  }
  describe('isDeployed', () => {
    beforeEach(() => {
      config.isDeployed = true
      setupTarget()
    })

    it('gets userId', async () => {
      await callEntitlementMiddleware()
      getUserId.should.have.been.called
    })

    it('calls isAuthorized', async () => {
      config.isDeployed = true
      await callEntitlementMiddleware()
      isAuthorized.should.have.been.called
    })

    it('calls determineEntitlements with mem', async () => {
      await callEntitlementMiddleware()
      mem.should.have.been.calledWith(determineEntitlements)
    })

    it('calls response send', async () => {
      await callEntitlementMiddleware()
      res.send.should.have.been.called
    })

    it('calls response send', async () => {
      await callEntitlementMiddleware()
      res.send.should.have.been.called
    })

    it('next should not be called', async () => {
      await callEntitlementMiddleware()
      next.should.not.have.been.called
    })

    it('should call next if authorized', async () => {
      config.isDeployed = true
      isAuthorized.returns(true)

      await callEntitlementMiddleware()

      res.send.should.not.have.been.called
      next.should.have.been.called
    })
  })

  describe('notDeployed', () => {
    beforeEach(() => {
      config.isDeployed = false
      setupTarget()
    })

    it('should call send', async () => {
      isAuthorized.returns(true)

      await callEntitlementMiddleware()

      res.send.should.have.not.been.called
      next.should.have.been.called
    })
  })
})
