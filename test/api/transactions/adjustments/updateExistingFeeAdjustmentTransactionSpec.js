const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/transactions/transactionSchema')

const dbTransaction = {
  adjustmentTransactionId: 5,
  contractId: 88,
  effectiveDate: 'yesterday'
}

const userId = 'somebody'

describe('update existing fee adjustment transaction', () => {
  let target, updateDb, buildUpdateParams
  beforeEach(() => {
    updateDb = sinon.stub().resolves('done')
    buildUpdateParams = sinon.stub().returns('transaction params')
    target = proxyquire('api/transactions/adjustments/updateExistingFeeAdjustmentTransaction', {
      '../../db/updateDb': updateDb,
      '../../common/buildUpdateParams': buildUpdateParams
    })
  })

  it('update db called with', async () => {
    await target(5, dbTransaction, 999, userId)
    updateDb.should.have.been.calledWithExactly(schema, 'transaction params', 5)
  })

  it('build update params called with', async () => {
    await target(5, dbTransaction, 999, userId)
    buildUpdateParams.should.have.been.calledWithExactly(schema.columns, userId, {
      amount: 999,
      contractId: dbTransaction.contractId,
      effectiveDate: dbTransaction.effectiveDate
    })
  })
})
