const log = require('../../log')
const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const sortByKeys = {
  contractId: 'transaction.contract_id',
  transactionType: 'transaction.transaction_type',
  effectiveDate: 'transaction.effective_date',
  detail: 'transaction.detail',
  amount: 'transaction.amount',
  comments: 'transaction.comments',
  shortPlanName: 'contract.short_plan_name',
  contractNumber: 'contract.contract_number',
  beginningBookValue: 'balance.beginning_book_value',
  createdDate: 'transaction.created_date'
}

const comparators = {
  lt: '<',
  lte: '<=',
  eq: '=',
  gte: '>=',
  gt: '>'
}

const whereQueryParam = (attribute, sqlBit, attributeConverter = (val) => val, bindVariableName) =>
  (schema, req, whereClauseBits, response) => {
    if (req.query && req.query[attribute]) {
      whereClauseBits.push(sqlBit)
      response.bindParams[bindVariableName || attribute] = attributeConverter(req.query[attribute])
    }
  }

const toDate = (val) => {
  log.info({date: val}, 'converting date!')
  return new Date(val)
}

const whereTransactionContractIdEquals = whereQueryParam('contractId', `${sortByKeys.contractId} = :contractId`, (val) => val * 1)
const whereTransactionTypeEquals = whereQueryParam('transactionType', `${sortByKeys.transactionType} = :transactionType`)
const whereDetailEquals = whereQueryParam('detail', `${sortByKeys.detail} = :detail`)
const whereShortPlanNameEquals = whereQueryParam('shortPlanName', `${sortByKeys.shortPlanName} = :shortPlanName`)
const whereContractNumberEquals = whereQueryParam('contractNumber', `${sortByKeys.contractNumber} = :contractNumber`)
const whereFromEffectiveDate = whereQueryParam('from', `${sortByKeys.effectiveDate} >= :fromEffectiveDate`, toDate, 'fromEffectiveDate')
const whereToEffectiveDate = whereQueryParam('to', `${sortByKeys.effectiveDate} <= :toEffectiveDate`, toDate, 'toEffectiveDate')

const whereAmount = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.amount) {
    const comparator = (req.query.amountComparator && comparators[req.query.amountComparator.toLowerCase()]) ? comparators[req.query.amountComparator.toLowerCase()] : comparators.eq
    const sqlBit = `${sortByKeys.amount} ${comparator} :amount`
    whereClauseBits.push(sqlBit)
    response.bindParams.amount = req.query.amount
  }
}

const addOrderBy = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.sortBy && sortByKeys[req.query.sortBy]) {
    response.orderBy = sortByKeys[req.query.sortBy]
    if (req.query && req.query.sortOrder && req.query.sortOrder.toUpperCase() === 'DESC') {
      response.orderBy += ' DESC'
    }
  }
}

module.exports = (schema, req) => {
  if (!req.query.pageNumber || req.query.pageNumber * 1 === 0) {
    req.query.pageNumber = 1
  }
  if (!req.query.pageSize) {
    req.query.pageSize = 50
  }
  return buildWhereClauseAndParamsWithPaging(schema, req, [
    whereTransactionContractIdEquals,
    whereContractNumberEquals,
    whereShortPlanNameEquals,
    whereTransactionTypeEquals,
    whereDetailEquals,
    whereFromEffectiveDate,
    whereToEffectiveDate,
    whereAmount,
    addOrderBy
  ])
}
// { whereClause, bindParams, sortOrder, executeOptions, pagination }
