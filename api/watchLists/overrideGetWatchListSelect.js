require('require-sql')
const overrideGetWatchListSelect = require('./overrideGetWatchListSelect.sql')

const omit = require('lodash/omit')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')

const uniqueRowProperty = 'watchListId'

const buildNewTopLevelObjectFromRow = (row) =>
Object.assign({actionSteps: [], reasons: []}, omit(row, 'actionStepId', 'actionStepDescription', 'reasonId', 'reasonDescription'))

const appendChildToTopLevel = (topLevel, row) => {
  if (topLevel.actionSteps.filter((i) => i.actionStepId === row.actionStepId * 1).length === 0 && row.actionStepId && row.actionStepDescription) {
    topLevel.actionSteps.push({actionStepId: row.actionStepId * 1, description: row.actionStepDescription})
  }

  if (topLevel.reasons.filter((i) => i.reasonId === row.reasonId * 1).length === 0 && row.reasonId && row.reasonDescription) {
    topLevel.reasons.push({reasonId: row.reasonId * 1, description: row.reasonDescription})
  }
}

module.exports = {
  sql: overrideGetWatchListSelect,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
