const validateSchema = require('../../validateSchema')
const target = require('../../../../api/contracts/contacts/contactContractSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../../api/common/auditColumns')
const {booleanConverter, numberConverter} = require('../../../../api/db/converters')
const overrideGetManyContactContractsSelect = require('../../../../api/contracts/contacts/overrideGetManyContactContractsSelect')

const falseDefault = () => false

describe('contact contract schema', () => {
  validateSchema(target, {
    tableShort: 'contact_contract',
    table: 'sch_stbv.contact_contract',
    tableId: 'contact_contract_id',
    sequence: 'sch_stbv.contact_contract_seq.NEXTVAL',
    orderBy: 'contact_contract_id',
    columns: [
      {name: 'contactContractId', column: 'contact_contract_id', sequence: 'sch_stbv.contact_contract_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      version,
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'contactId', column: 'contact_id', converter: numberConverter},
      {name: 'receiveInvoice', column: 'receive_invoice', defaultValue: falseDefault, converter: booleanConverter},
      {name: 'receiveStatement', column: 'receive_statement', defaultValue: falseDefault, converter: booleanConverter},
      {name: 'receiveScheduleA', column: 'receive_schedule_a', defaultValue: falseDefault, converter: booleanConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId
    ],
    required: {
      new: ['contractId', 'contactId', 'receiveInvoice', 'receiveStatement', 'receiveScheduleA'],
      update: []
    },
    overrideSelectClause: {
      selectOne: overrideGetManyContactContractsSelect,
      selectMany: overrideGetManyContactContractsSelect
    }
  })
})
