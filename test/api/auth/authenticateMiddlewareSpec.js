/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

describe('authenticate middleware', () => {
  const request = { headers: 'abc' }

  let response, target, next, getUserId, responseStatusSend, selectFromDb

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    response = {
      json: sinon.spy(),
      status: sinon.stub().returns({ send: responseStatusSend })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    target = proxyquire('api/auth/authenticateMiddleware', {
      '../getUserId': getUserId
    })
  })

  it('with invalid user', () => {
    target(request, response, next)
    responseStatusSend.should.have.been.called
  })

  it('with valid user', () => {
    getUserId.withArgs(request.headers).returns('joe')
    target(request, response, next)
    next.should.have.been.called
  })
})
