const camelCase = require('lodash/camelCase')
const log = require('../../log')
const {riskCategories} = require('../referenceData.json')

const getWeightByPlan = (riskLookup, planType) => riskLookup[`${planType}Weight`]
const fieldsToAdjust = ['cashFlow', 'demographics', 'competingFunds', 'planSponsor', 'planSvFundBalances', 'planDesign', 'fundManagement', 'wrappedPortfolio', 'marketBookRatio', 'pooledFundBalances', 'pooledFundDesign']

const getGradeAdjustment = (gradesWithDescription, field, score, planType) => {
  let adjustment = 0.0
  let grade = 'F'
  if (score != null) {
    const matchingRiskLookups = gradesWithDescription.filter((gradesWithDescription) => parseInt(score) >= gradesWithDescription.lowLimit && parseInt(score) <= gradesWithDescription.highLimit)
    if (matchingRiskLookups.length > 0 && planType) {
      adjustment = getWeightByPlan(matchingRiskLookups[0], planType)
      grade = matchingRiskLookups[0].grade
    } else {
      log.warn({field, score, planType, gradesWithDescription, matchingRiskLookups}, 'a grade we couldn\'t match with')
    }
  }
  return {adjustment, grade}
}

const calculateOverallUnadjustedWeight = (gradesWithDescription, categories, riskScorecard, planType) => {
  log.info({id: riskScorecard.riskScorecardId, planType}, 'calculating overall unadjusted weight')
  let overallUnadjustedWeight = 0.0
  if (planType) {
    categories.forEach(category => {
      const categoryField = camelCase(category.description)
      const score = riskScorecard[categoryField]
      overallUnadjustedWeight += score ? score * getWeightByPlan(category, planType) : 0
    })
  }
  overallUnadjustedWeight = (overallUnadjustedWeight / 100.0).toFixed(1)
  return {
    score: overallUnadjustedWeight * 1,
    grade: getGradeAdjustment(gradesWithDescription, 'overallUnadjusted', overallUnadjustedWeight, planType).grade,
    comments: riskScorecard.overallUnadjustedComments
  }
}

const calculateOverallAdjustedWeight = (gradesWithDescription, categories, riskScorecard, planType, adjustedScorecardWithGrades) => {
  log.info({id: riskScorecard.riskScorecardId, planType}, 'calculating overall adjusted weight')
  let overallAdjustedWeight = 0.0
  let overallAdjustedTotal = 0.0
  categories.forEach(category => {
    const categoryField = camelCase(category.description)
    const categoryWeight = getWeightByPlan(category, planType)
    const value = adjustedScorecardWithGrades[categoryField].adjustment * categoryWeight
    adjustedScorecardWithGrades[categoryField].adjustedWeight = value
    adjustedScorecardWithGrades[categoryField].categoryWeight = categoryWeight
    overallAdjustedTotal += value / 100.0
  })
  fieldsToAdjust.forEach((field) => {
    overallAdjustedTotal = (overallAdjustedTotal === 0.0) ? 1.0 : overallAdjustedTotal
    adjustedScorecardWithGrades[field].adjustedWeight = (adjustedScorecardWithGrades[field].adjustedWeight / overallAdjustedTotal).toFixed(12) * 1
  })
  categories.forEach(category => {
    const categoryField = camelCase(category.description)
    const score = riskScorecard[categoryField]
    overallAdjustedWeight += score ? score * adjustedScorecardWithGrades[categoryField].adjustedWeight : 0
  })
  overallAdjustedWeight = (overallAdjustedWeight / 100.0).toFixed(1)
  return {
    score: overallAdjustedWeight * 1,
    grade: getGradeAdjustment(gradesWithDescription, 'overallAdjusted', overallAdjustedWeight, planType).grade,
    comments: riskScorecard.overallAdjustedComments
  }
}

const filterJustGradesWithGradeDescription = (riskLookups) =>
  riskLookups.filter((riskLookup) => riskLookup.scoreType === 'GRADE')
  .map((lookup) => {
    const lookupToReturn = Object.assign({}, lookup)
    const matchingRiskCategories = riskCategories.filter((category) => category.id === lookup.description)
    if (matchingRiskCategories.length > 0) {
      lookupToReturn.grade = matchingRiskCategories[0].text
    }
    return lookupToReturn
  })

const adjustScorecardGrades = (gradesWithDescription, categories, riskScorecard) => {
  const planType = camelCase(riskScorecard.contractType)
  log.info({id: riskScorecard.riskScorecardId, planType}, 'adjusting scorecard\'s grades')
  const adjustedScorecardWithGrades = {
    riskScorecardId: riskScorecard.riskScorecardId,
    version: riskScorecard.version,
    contractId: riskScorecard.contractId,
    effectiveDate: riskScorecard.effectiveDate
  }
  fieldsToAdjust.forEach((field) => {
    const fieldResponse = getGradeAdjustment(gradesWithDescription, field, riskScorecard[field], planType)
    log.info({fieldResponse, field}, 'getGradeAdjustment response')
    adjustedScorecardWithGrades[field] = Object.assign({ score: riskScorecard[field], comments: riskScorecard[`${field}Comments`] }, fieldResponse)
  })

  adjustedScorecardWithGrades.overallUnadjusted = calculateOverallUnadjustedWeight(gradesWithDescription, categories, riskScorecard, planType)
  adjustedScorecardWithGrades.overallAdjusted = calculateOverallAdjustedWeight(gradesWithDescription, categories, riskScorecard, planType, adjustedScorecardWithGrades)

  log.info({id: riskScorecard.riskScorecardId, planType, adjustedScorecardWithGrades}, 'finished adjusting scorecard\'s grades')
  return adjustedScorecardWithGrades
}

module.exports = (riskLookups, riskScorecardsByContract) => {
  log.info({riskLookups, riskScorecardsByContract}, 'adjust risk scorecards')
  const gradesWithDescription = filterJustGradesWithGradeDescription(riskLookups)
  const categories = riskLookups.filter((lookup) => lookup.scoreType === 'CATEGORY')

  return riskScorecardsByContract.map((riskScorecard) => adjustScorecardGrades(gradesWithDescription, categories, riskScorecard))
}
