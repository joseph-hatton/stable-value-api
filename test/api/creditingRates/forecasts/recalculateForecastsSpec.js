const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

const contract = {contractId: 55}
const userId = 'me'

describe('recalculate forecasts', () => {
  let target, getLatestCycle, deleteForecasts, calculateAndSaveCreditingRateForecast
  beforeEach(() => {
    getLatestCycle = sinon.stub().resolves({startDate: '2017-08-17T22:08:32.833Z'})
    deleteForecasts = sinon.spy()
    calculateAndSaveCreditingRateForecast = sinon.spy()
    target = proxyquire('api/creditingRates/forecasts/recalculateForecasts', {
      '../../monthlyCycles/getLatestCycle': getLatestCycle,
      './deleteForecasts': deleteForecasts,
      './calculateAndSaveCreditingRateForecast': calculateAndSaveCreditingRateForecast
    })
  })

  it('calls delete forecasts', async () => {
    await target(contract, userId)
    deleteForecasts.should.have.been.calledWithExactly(contract.contractId, '2017-08-01T00:00:00.000Z', userId)
  })

  it('calls calculate and save crediting rate forecasts', async () => {
    await target(contract, userId)
    calculateAndSaveCreditingRateForecast.should.have.been.calledWithExactly(contract, '2017-08-01T00:00:00.000Z')
  })
})
