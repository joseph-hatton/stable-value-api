const log = require('../../../log')
const buildUpdateParams = require('../../common/buildUpdateParams')
const updateDb = require('../../db/updateDb')
const schema = require('../contractSchema')

module.exports = async (contractId, userId, status, skipUpdate = false) => {
  log.info({domain: schema.tableShort, id: contractId, status, skipUpdate}, 'update contract status')
  const updateParams = buildUpdateParams(schema.columns, userId, {status})
  await updateDb(schema, updateParams, contractId, userId, skipUpdate)
}
