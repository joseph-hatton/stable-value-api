/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')
const contractSchema = require('../../../../api/contracts/contractSchema')

const params = {params: true}
const headers = ['header']
const body = {body: true}

describe('update market value api', () => {
  let target, res, resJson, next, updateDb, updateDbResult, selectByIdFromDb, getUserId, buildUpdateParams, validateThenCallApi, getCreditingRatesByContract, checkForMarketValues
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    updateDbResult = {marketValue: 'mv', referenceDate: new Date()}
    updateDb = sinon.stub().resolves(updateDbResult)
    selectByIdFromDb = sinon.stub()
    selectByIdFromDb.withArgs(contractSchema, 54).resolves({status: 'ACTIVE'})
    selectByIdFromDb.withArgs(schema, 552).resolves({status: 'AWAITING', creditingRateId: 552})
    getUserId = sinon.stub().resolves('a user')
    buildUpdateParams = sinon.stub().resolves('yep')
    validateThenCallApi = sinon.stub().resolves('yep')
    getCreditingRatesByContract = sinon.stub().resolves([{status: 'AWAITING', creditingRateId: 552}])
    checkForMarketValues = sinon.stub().resolves('yep')
    target = proxyquire('api/creditingRates/marketValue/updateMarketValueApi', {
      '../../db/updateDb': updateDb,
      '../../db/selectByIdFromDb': selectByIdFromDb,
      '../../getUserId': getUserId,
      '../../common/buildUpdateParams': buildUpdateParams,
      '../../common/validateThenCallApi': validateThenCallApi,
      '../../valuations/checkForMarketValues': checkForMarketValues,
      '../creditingRate/getCreditingRatesByContract': getCreditingRatesByContract
    })
  })

  it('validateThenCallApi was called', async () => {
    const result = await target({params, headers, body}, res, next)
    validateThenCallApi.should.have.been.called
  })

  describe('attempt to update', () => {
    let attemptToUpdate, apiParams
    beforeEach(async () => {
     await target({params, headers, body}, res, next)
      apiParams = {contractId: 54, userId: 'a user', creditingRateId: 552}
      attemptToUpdate = validateThenCallApi.getCall(0).args[5]
    })

    it('null contract throws error', async () => {
      selectByIdFromDb.resolves(null)
      try {
        await attemptToUpdate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Only Awaiting OR Forecast Crediting Rates on Active Contracts can be updated.')
      }
    })

    it('pending contract throws error', async () => {
      selectByIdFromDb.resolves({status: 'PENDING'})
      try {
        await attemptToUpdate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Only Awaiting OR Forecast Crediting Rates on Active Contracts can be updated.')
      }
    })

    it('reference date not unique', async () => {
      getCreditingRatesByContract.resolves([{status: 'AWAITING', marketValue: 'mv', creditingRateId: 999}])
      try {
        await attemptToUpdate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Reference date must be unique for each market value for the contract.')
      }
    })

    it('update response sent', async () => {
      await attemptToUpdate(schema, body, res, next, apiParams)
      resJson.should.have.been.calledWithExactly(updateDbResult)
    })

    it('checks market value if market value changed', async () => {
      await attemptToUpdate(schema, body, res, next, apiParams)
      checkForMarketValues.should.have.been.calledWithExactly(updateDbResult.referenceDate)
    })

    it('does not check market value if market value same', async () => {
      updateDb.resolves({marketValue: 'something else', referenceDate: updateDbResult.referenceDate})
      await attemptToUpdate(schema, body, res, next, apiParams)
      checkForMarketValues.should.have.been.calledWithExactly(updateDbResult.referenceDate)
    })
  })
})
