const moment = require('moment')
const log = require('../../log')
const schema = require('./monthlyCyclesSchema')
const getLatestCycle = require('./getLatestCycle')

module.exports = async (dateString) => {
  log.info({domain: schema.tableShort, date: dateString}, 'is month open')
  const dateAsMoment = moment(dateString).utc()
  const latestCycle = await getLatestCycle()
  const latestCycleStart = moment(latestCycle.startDate).utc()
  const isOpen = (!latestCycle.closed &&
    dateAsMoment.get('year') === latestCycleStart.get('year') &&
    dateAsMoment.get('month') === latestCycleStart.get('month'))

  log.info({domain: schema.tableShort, date: dateString, isOpen, latestCycle}, 'is month open result')
  return isOpen
}
