const validateSchema = require('../../validateSchema')
const target = require('../../../../api/contracts/derivativeTypes/contractDerivativeTypeSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../../api/common/auditColumns')
const {numberConverter} = require('../../../../api/db/converters')

describe('contract derivative type schema', () => {
  validateSchema(target, {
    tableShort: 'contract_derivative_type',
    table: 'sch_stbv.contract_derivative_type',
    tableId: 'derivative_type_id',
    orderBy: 'contract_id',
    columns: [
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'derivativeTypeId', column: 'derivative_type_id'}
    ],
    required: {
      new: ['contractId', 'derivativeTypeId'],
      update: []
    }
  })
})
