const target = require('../../../../api/creditingRates/creditingRate/calculateNetRate')

const createArgs = (grossRate, premiumRate, managerFeeRate, advisorFeeRate, creditingRateType, netOfThirdPartyFees, creditingRateManagerFee, creditingRateAdvisorFee, creditingRateRoundDecimals) =>
  ({grossRate, premiumRate, managerFeeRate, advisorFeeRate, creditingRateType, netOfThirdPartyFees, creditingRateManagerFee, creditingRateAdvisorFee, creditingRateRoundDecimals})

describe('calculate crediting rate net rate', () => {
  beforeEach(() => {
  })

  it('just gross rate 1', () => {
    target(createArgs(15.0, 200, 300, 400, 'GROSS', false, false, false, 2)).should.eql(15.0)
  })

  it('just gross rate 2', () => {
    target(createArgs(15.0, 200, 300, 400, 'GROSS', false, true, true, 2)).should.eql(15.0)
  })

  it('with premium rate', () => {
    target(createArgs(15.0, 200, 300, 400, 'NET', true, false, false, 2)).should.eql(13.0000)
  })

  it('with manager fee', () => {
    target(createArgs(15.0, 200, 300, 400, 'NET', true, true, false, 2)).should.eql(10.0000)
  })

  it('with advisor fee', () => {
    target(createArgs(15.0, 200, 300, 400, 'NET', true, false, true, 2)).should.eql(9.0000)
  })

  it('with no net of third party fees', () => {
    target(createArgs(15.0, 200, 300, 400, 'NET', false, true, true, 2)).should.eql(13.0000)
  })
})
