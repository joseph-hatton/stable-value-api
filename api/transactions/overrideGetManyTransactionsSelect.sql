SELECT  transaction.*,
        contract.contract_number,
        contract.short_plan_name,
				contract.stated_maturity_date,
        balance.balance_id,
        balance.beginning_book_value,
        parent.transaction_id as parent_transaction_id
FROM sch_stbv.transaction
INNER JOIN sch_stbv.contract
  ON  transaction.contract_id = contract.contract_id
LEFT OUTER JOIN sch_stbv.balance
  ON  transaction.contract_id = balance.contract_id
  AND transaction.effective_date = balance.effective_date
LEFT OUTER JOIN sch_stbv.transaction parent
ON transaction.transaction_id = parent.adjustment_transaction_id
