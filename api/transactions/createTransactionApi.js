const createError = require('http-errors')
const log = require('../../log')
const buildDefaultParams = require('../common/buildDefaultParams')
const validateThenCallApi = require('../common/validateThenCallApi')
const contractSchema = require('../contracts/contractSchema')
const schedulePendingTerminationContract = require('../contracts/workflow/schedulePendingTerminationContract')
const insertIntoDb = require('../db/insertIntoDb')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const getUserId = require('../getUserId')
const schema = require('./transactionSchema')
const validateEffectiveDateInOpenMonth = require('./validators/validateEffectiveDateInOpenMonth')
const updateAdjustments = require('./adjustments/updateAdjustments')
const updateDistributions = require('./distributions/updateDistributions')

const FEE_RECEIPT = 'FEE_RECEIPT'

const buildInsertParams = (columns, userId, body) =>
  buildDefaultParams({columns}, body,
  {
    locked: false,
    createdId: userId,
    modifiedId: userId
  })

const attemptInsertApi = async (schema, body, res, next, {userId, contract}) => {
  log.info({domain: schema.tableShort, transactionType: body.transactionType}, 'attempt to insert transaction')

  const insertParams = buildInsertParams(schema.columns, userId, body)
  const dbTransaction = await insertIntoDb(schema, insertParams, userId)

  if (body.transactionType === FEE_RECEIPT) {
    await updateDistributions(dbTransaction, body.feeDistributions, body.amount, userId)
    await updateAdjustments(dbTransaction, body, userId)
  } else if (body.transactionType === 'TERMINATION') {
    const statedMaturityDate = body.statedMaturityDate || dbTransaction.effectiveDate
    await schedulePendingTerminationContract({contract, userId, terminationDate: dbTransaction.effectiveDate, statedMaturityDate})
  }
  const result = await selectByIdFromDb(schema, dbTransaction.transactionId)
  res.status(201).json(result)
}

// The effective date cannot be before the contract effective date

module.exports = async ({ headers, body }, res, next) => {
  log.info({domain: schema.tableShort, transaction: body}, 'create transaction api')
  const userId = getUserId(headers)
  const contract = await selectByIdFromDb(contractSchema, body.contractId)
  if (contract.status !== 'ACTIVE' && body.transactionType === 'TERMINATION') {
    throw createError.BadRequest('Contract must be Active to add transactions.')
  }
  if (body.transactionType === FEE_RECEIPT && (!body.feeDistributions || body.feeDistributions.length === 0)) {
    throw createError.BadRequest('A Fee Receipt transaction must have fee distributions specified')
  }
  const errMessage = await validateEffectiveDateInOpenMonth(body, contract)
  if (errMessage) {
    throw createError.BadRequest(errMessage)
  }
  return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptInsertApi, {userId, contract})
}
