const createController = require('./createController')

module.exports = createController({
  getPic: require('../profiles/getProfilePicApi')
})
