/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const {schema} = require('../../data/testSchema')

const id = 55

describe('select by id from db', () => {
  let target, connection, dbExecutePromise, executeSelectByIdQuery

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves('DB_EXECUTE_PROMISE_RESULT')
    connection = sinon.spy()
    executeSelectByIdQuery = sinon.stub().resolves('executed select by id query')
    target = proxyquire('api/db/selectByIdFromDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/executeSelectByIdQuery': executeSelectByIdQuery
    })
  })

  const executeTarget = async (mapResult) => {
    const result = await target(schema, id)
    return await dbExecutePromise.getCall(0).args[0](connection)
  }

  describe('execute promise', () => {
    it('called once', async () => {
      await executeTarget()
      dbExecutePromise.callCount.should.eql(1)
    })

    it('returns result', async () => {
      const result = await executeTarget()
      result.should.eql('executed select by id query')
    })

    it('db execute one called with', async () => {
      const result = await executeTarget()
      executeSelectByIdQuery.should.have.been.calledWithExactly(connection, schema, id)
    })
  })
})
