const target = require('../../../../api/db/queries/buildSimpleSqlQuery')

describe('build simple sql query', () => {
  let mapResult = sinon.stub()
  it('sets sql', () =>
    target('some sql', mapResult).sql.should.equal('some sql'))

  it('sets executeOptions', () =>
    target('some sql', mapResult).executeOptions.maxRows.should.eql(1000))

  it('sets mapResult', () =>
    target('some sql', mapResult).mapResult.should.eql(mapResult))
})
