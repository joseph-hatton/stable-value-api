const {dbSchemaName} = require('../config')
const {version} = require('../common/auditColumns')
const {booleanConverter, numberConverter} = require('../db/converters')

const table = 'action_step'
const tableId = 'action_step_id'
const sequence = `${dbSchemaName}.action_steps_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'action_step_id',
  columns: [
    {name: 'actionStepId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'description'},
    {name: 'standardSw', column: 'standard_sw', converter: booleanConverter}
  ],
  required: {
    new: ['actionStepId', 'version', 'description', 'standardSw'],
    update: ['actionStepId', 'version', 'description', 'standardSw']
  }
}
