/* eslint-disable no-unused-expressions */
const target = require('../../../api/contracts/getCurrentlyEffectiveEditHistory')

const contractId = 5
const hours = 1000 * 60 * 60
describe('get currently effective edit history', () => {
  let status
  const currentTime = new Date().getTime()
  const editHistories = [
    {effectiveDate: (new Date(currentTime - hours * 5)).toISOString(), fieldOnOldestAndMiddle: 'a-oldest'},
    {effectiveDate: (new Date(currentTime + hours * 5)).toISOString(), fieldOnlyOnFuture: 'b-future', fieldOnFutureAndMiddle: 'c-future'},
    {effectiveDate: (new Date(currentTime - hours * 2)).toISOString(), fieldOnOldestAndMiddle: 'a-middle', fieldOnFutureAndMiddle: 'c-middle'}
  ]

  describe('for pending contract', () => {
    beforeEach(() => {
      status = 'PENDING'
    })
    it('get newest fieldOnOldestAndMiddle', async () => {
      target(status, editHistories, 'fieldOnOldestAndMiddle').should.eql('a-middle')
    })

    it('get newest fieldOnlyOnFuture', async () => {
      target(status, editHistories, 'fieldOnlyOnFuture').should.eql('b-future')
    })

    it('get newest fieldOnFutureAndMiddle', async () => {
      target(status, editHistories, 'fieldOnFutureAndMiddle').should.eql('c-future')
    })
  })

  describe('for active contract', () => {
    beforeEach(() => {
      status = 'ACTIVE'
    })
    it('get newest fieldOnOldestAndMiddle', async () => {
      target(status, editHistories, 'fieldOnOldestAndMiddle').should.eql('a-middle')
    })

    it('get newest fieldOnlyOnFuture', async () => {
      should.not.exist(target(status, editHistories, 'fieldOnlyOnFuture'))
    })

    it('get newest fieldOnFutureAndMiddle', async () => {
      target(status, editHistories, 'fieldOnFutureAndMiddle').should.eql('c-middle')
    })
  })
})
