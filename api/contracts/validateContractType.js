const validateIdAndText = require('../common/validateIdTextField')
const {contractTypes} = require('../referenceData.json')
module.exports = validateIdAndText(contractTypes, 'Invalid Contract Type')
