const {dbSchemaName} = require('../config')
const {version} = require('../common/auditColumns')
const {booleanConverter, numberConverter} = require('../db/converters')

const table = 'reason'
const tableId = 'reason_id'
const sequence = `${dbSchemaName}.reasons_seq.NEXTVAL`

const zeroDefault = () => 0
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'reason_id',
  columns: [
    {name: 'reasonId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'description'},
    {name: 'standardSw', column: 'standard_sw', converter: booleanConverter}
  ],
  required: {
    new: ['version', 'description', 'standardSw'],
    update: ['reasonId', 'version', 'description', 'standardSw']
  }
}
