const target = require('../../../api/common/removeImmutableAndPkColumnsForUpdate')

const AN_EXPECTED_VALUE = 'an expected value'
const AN_UNEXPECTED_VALUE = 'we shouldnt see this'

describe('remove immutable and pk columns for update', () => {
  let columns, params

  beforeEach(() => {
    columns = [
      {
        name: 'immutableColumn',
        immutable: true
      },
      {
        name: 'primaryKeyColumn',
        pk: true
      },
      {
        name: 'column1'
      },
      {
        name: 'column2'
      }
    ]
  })

  it('ignres column that is not in list of columns', () =>
    target(columns, {notThere: AN_UNEXPECTED_VALUE}).should.eql({})
  )

  it('ignores immutable column', () =>
    target(columns, {immutableColumn: AN_UNEXPECTED_VALUE}).should.eql({})
  )

  it('ignores primary key column', () =>
    target(columns, {primaryKeyColumn: AN_UNEXPECTED_VALUE}).should.eql({})
  )

  it('handles expected columns', () => {
    const expectedParam = {column1: AN_EXPECTED_VALUE}
    return target(columns, expectedParam).should.eql(expectedParam)
  })

  it('handles multiple expected columns', () => {
    const expectedParam = {column1: AN_EXPECTED_VALUE, column2: 'something else'}
    return target(columns, expectedParam).should.eql(expectedParam)
  })
})
