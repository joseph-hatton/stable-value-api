require('require-sql')
const moment = require('moment')
const monthlySql = require('./getMonthlyFeesByContractId.sql')
const mapInvoicesAndFees = require('./mapInvoicesAndFees')
const simpleSelectFromDb = require('../../db/simpleSelectFromDb')
const isLeapYear = require('../isLeapYear')
const template = require('lodash/template')

const DATE_FORMAT = 'MM/DD/YYYY'

module.exports = async (date, contractId, balancesTable) => {
  date = date.startOf('month')
  const dateStr = date.clone().subtract(1, 'month').endOf('month').format(DATE_FORMAT)
  const queryCompiled = template(monthlySql)
  const query = queryCompiled({start: dateStr, end: dateStr, contractId})
  const table = await simpleSelectFromDb(query, mapInvoicesAndFees)
  let balance = table.results.find((i) => i.contractId === contractId)

  const balances = []
  const effectiveDate = date.clone()
  balance.rate = (balance.balanceRate * 1)
  balance.accruedFeeMtd = 0

  balance.deposits = 0
  balance.withdrawals = 0
  balance.adjustments = 0
  balance.feeReceived = 0
  balance.feeAdjustments = 0

  const numberOfDaysInAYear = isLeapYear(date.year()) ? 366 : 365
  const premiumRate = balance.latestPremiumRate * 1
  const feeRate = premiumRate > 0 ? premiumRate : 0
  const dailyFeeRate = Math.pow(1 + feeRate, 1 / numberOfDaysInAYear) - 1
  const dailyRate = Math.pow(1 + balance.rate, 1 / numberOfDaysInAYear) - 1

  const endDay = date.endOf('month').date()

  for (let i = 0; i < endDay; i++) {
    balance.effectiveDate = effectiveDate.format()
    balance.beginningBookValue = balance.endingBookValue * 1
    balance.beginningFee = balance.endingFee * 1
    balance.interest = dailyRate * (balance.beginningBookValue + balance.deposits + balance.withdrawals + balance.adjustments)
    balance.endingBookValue = balance.beginningBookValue + balance.deposits + balance.withdrawals + balance.adjustments + balance.interest
    balance.accruedFee = dailyFeeRate * (balance.beginningBookValue + balance.deposits + balance.withdrawals + balance.adjustments + balance.interest)
    balance.accruedFeeMtd = balance.accruedFeeMtd + balance.accruedFee
    balance.endingFee = balance.endingFee - balance.feeReceived + balance.feeAdjustments + balance.accruedFee

    balances.push(Object.assign({}, balance))

    effectiveDate.add(1, 'day')
  }

  balancesTable.results = balances
  balancesTable.total = balances.length
}
