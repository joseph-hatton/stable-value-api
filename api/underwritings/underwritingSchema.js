const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {dateConverter, numberConverter, percentConverter} = require('../db/converters')
const validateIdAndText = require('../common/validateIdTextField')
const {contractTypes, contractStatuses} = require('../referenceData.json')
const contractTypeValidator = validateIdAndText(contractTypes, 'Invalid Contract Type')

const table = 'underwriting'
const tableId = 'underwriting_id'
const sequence = `${dbSchemaName}.underwriting_seq.NEXTVAL`

const zeroDefault = () => 0
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'contract_id',
  columns: [
    {name: 'underwritingId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    createdDate,
    {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
    {name: 'contributionRate', column: 'contribution_rate', converter: percentConverter},
    {name: 'withdrawalRate', column: 'withdrawal_rate', converter: percentConverter},
    {name: 'withdrawalVolatilityRate', column: 'withdrawal_volatility_rate', converter: percentConverter},
    {name: 'maximumWithdrawalRate', column: 'maximum_withdrawal_rate', converter: percentConverter},
    {name: 'bufferLevelRate', column: 'buffer_level_rate', converter: percentConverter},
    {name: 'extTerminationRate', column: 'ext_termination_rate', converter: percentConverter},
    {name: 'putQueueWithdrawalRate', column: 'put_queue_withdrawal_rate', converter: percentConverter, defaultValue: zeroDefault},
    {name: 'putLengthMonths', column: 'put_length_months', converter: numberConverter, defaultValue: zeroDefault},
    {name: 'maximumPutQueueRate', column: 'maximum_put_queue_rate', converter: percentConverter, defaultValue: zeroDefault},
    {name: 'underwritingComments', column: 'underwriting_comments'},
    createdId,
    modifiedDate,
    modifiedId
  ],
  required: {
    new: ['contractId', 'effectiveDate', 'contributionRate', 'contributionRate', 'withdrawalRate', 'withdrawalVolatilityRate', 'maximumWithdrawalRate', 'bufferLevelRate', 'extTerminationRate'],
    update: []
  }
}
