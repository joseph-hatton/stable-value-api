/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/contracts/contractSchema')

const userId = 'a user'
const contractId = 5
const request = { params: { id: contractId }, headers: { userId } }
describe('transition contract in workflow api', () => {
  let target, res, resJson, body, contract, selectByIdFromDb, getUserId, transitionContractInWorkflow, targetArgs
  beforeEach(() => {
    body = { status: 'ACTIVE' }
    resJson = sinon.spy()
    res = { status: sinon.stub().returns({ json: resJson }) }
    getUserId = sinon.stub().withArgs(request.headers).returns(userId)
    contract = {status: 'ACTIVE'}
    selectByIdFromDb = sinon.stub()
    transitionContractInWorkflow = sinon.stub()
    targetArgs = {actionVerb: 'testingSomeAction', transitionContractInWorkflow}
    target = proxyquire('api/contracts/workflow/transitionContractInWorkflowApi', {
      '../../db/selectByIdFromDb': selectByIdFromDb,
      '../../getUserId': getUserId,
      './transitionContractInWorkflow': transitionContractInWorkflow
    })
  })

  it('throws error when contract doesn\'t exist', async () => {
    selectByIdFromDb.withArgs(schema, contractId).resolves(null)
    try {
      await target(targetArgs)(request, res)
      throw new Error('it should not get here')
    } catch (err) {
      err.message.should.eql('Contract not found.')
    }
  })

  it('throws error when contract is active', async () => {
    selectByIdFromDb.withArgs(schema, contractId).resolves(contract)
    transitionContractInWorkflow.resolves('Unable to approve contract.')
    try {
      await target(targetArgs)(request, res)
      throw new Error('it should not get here')
    } catch (err) {
      err.message.should.eql('Unable to approve contract.')
    }
  })

  it('selects contract by id', async () => {
    selectByIdFromDb.withArgs(schema, contractId).resolves(contract)
    await target(targetArgs)(request, res)

    resJson.should.have.been.calledWithExactly(contract)
  })
})
