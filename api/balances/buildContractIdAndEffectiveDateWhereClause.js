const log = require('../../log')
const whereContractIdAndEffectiveDate = require('./whereContractIdAndEffectiveDate')
module.exports = (schema, req) => {
  log.info({params: req.params, query: req.query}, 'building where clause')
  const contractId = (req.params && req.params.contractId) ? req.params.contractId : null
  const effectiveDate = (req.query && req.query.effectiveDate) ? req.query.effectiveDate : null
  return whereContractIdAndEffectiveDate(contractId, {effectiveDate})
}
