const calculateCreditingRateForecastDates = require('./calculateCreditingRateForecastDates')
const createForecasts = require('./createForecasts')

module.exports = async (contract, startingDate) => {
  const forecastDates = calculateCreditingRateForecastDates(contract, startingDate)
  return await createForecasts(contract.contractId, forecastDates)
}
