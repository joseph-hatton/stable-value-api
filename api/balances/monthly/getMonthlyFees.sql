select
balance.contract_id  as "contractId",
contract.contract_number as "contractNumber",
contract.short_plan_name as "shortPlanName",
sum(decode(balance.effective_date, to_date('<%= start %>', 'MM/DD/YYYY'), beginning_Book_Value, 0.0 )) as "beginningBookValue",
sum(decode(balance.effective_date, to_date('<%= end %>', 'MM/DD/YYYY'), ending_Book_Value, 0.0 )) as "endingBookValue",
sum(deposits) as "deposits",
sum(withdrawals) as "withdrawals",
sum(adjustments) as "adjustments",
sum(interest) as "interest",
sum(accrued_Fee) as "accruedFee",
sum(fee_Received) as "feeReceived",
sum(fee_Adjustments) as "feeAdjustments",
max(balance.rate) as "balanceRate",
 max((select ceh.premium_rate 
      from SCH_STBV.CONTRACT_EDIT_HISTORY ceh
      where 
      ceh.CONTRACT_ID = balance.CONTRACT_ID
      and ceh.premium_rate is not null
      and ceh.effective_date = 
          (select max(effective_date) from SCH_STBV.CONTRACT_EDIT_HISTORY ceh2 
           where ceh2.CONTRACT_ID = ceh.CONTRACT_ID 
           and ceh2.premium_rate is not null))) as "latestPremiumRate",
sum(decode(balance.effective_date, to_date('<%= start %>', 'MM/DD/YYYY'), beginning_Fee, 0.0 )) as "beginningFee",
sum(decode(balance.effective_date, to_date('<%= end %>', 'MM/DD/YYYY'), ending_Fee, 0.0 )) as "endingFee",
sum(decode(balance.effective_date, to_date('<%= end %>', 'MM/DD/YYYY'), accrued_Fee_MTD, 0.0 )) as "accruedFeeMtd"
from SCH_STBV.balance balance, SCH_STBV.contract contract, SCH_STBV.contract_edit_history contract_edit_history
where balance.contract_id = contract.contract_id
and balance.effective_date >= to_date('<%= start %>', 'MM/DD/YYYY')
and balance.effective_date <= to_date('<%= end %>', 'MM/DD/YYYY')
and contract.contract_id = contract_edit_history.contract_id
and contract_edit_history.effective_date = (
select max(effective_date) from SCH_STBV.contract_edit_history where contract.contract_id = contract_edit_history.contract_id
and contract_edit_history.effective_date <= to_date('<%= end %>', 'MM/DD/YYYY')
)
group by balance.contract_id, contract.contract_number,contract.short_plan_name, contract_edit_history.jurisdiction, contract_edit_history.assignment_State
order by contract.contract_number ASC