const validateSchema = require('../validateSchema')
const target = require('../../../api/balances/balancesSchema')
const {dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

describe('balance schema', () => {
  validateSchema(target, {
    tableShort: 'balance',
    table: 'sch_stbv.balance',
    tableId: 'balance_id',
    sequence: 'sch_stbv.balance_seq.NEXTVAL',
    orderBy: 'contract_id',
    columns: [
      {name: 'balanceId', column: 'balance_id', sequence: 'sch_stbv.balance_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'beginningBookValue', column: 'beginning_book_value', converter: numberConverter},
      {name: 'deposits', converter: numberConverter},
      {name: 'withdrawals', converter: numberConverter},
      {name: 'adjustments', converter: numberConverter},
      {name: 'interest', converter: numberConverter},
      {name: 'rate', converter: percentConverter},
      {name: 'endingBookValue', column: 'ending_book_value', converter: numberConverter},
      {name: 'accruedFee', column: 'accrued_fee', converter: numberConverter},
      {name: 'accruedFeeMtd', column: 'accrued_fee_mtd', converter: numberConverter},
      {name: 'feeReceived', column: 'fee_received', converter: numberConverter},
      {name: 'endingFee', column: 'ending_fee', converter: numberConverter},
      {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
      {name: 'rateEffectiveDate', column: 'rate_effective_date', converter: dateConverter},
      {name: 'beginningFee', column: 'beginning_fee', converter: numberConverter},
      {name: 'feeAdjustments', column: 'fee_adjustments', converter: numberConverter}
    ],
    required: {
      new: ['contractId', 'beginningBookValue', 'deposits', 'withdrawals', 'adjustments', 'interest', 'rate', 'endingBookValue', 'accruedFee', 'accruedFeeMtd', 'feeReceived', 'endingFee', 'effectiveDate', 'rateEffectiveDate', 'beginningFee', 'feeAdjustments'],
      update: ['contractId', 'beginningBookValue', 'deposits', 'withdrawals', 'adjustments', 'interest', 'rate', 'endingBookValue', 'accruedFee', 'accruedFeeMtd', 'feeReceived', 'endingFee', 'effectiveDate', 'rateEffectiveDate', 'beginningFee', 'feeAdjustments']
    }
  })
})
