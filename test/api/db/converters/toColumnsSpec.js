const toColumns = require('../../../../api/db/converters/toColumns')

const AN_EXPECTED_VALUE = 'an expected value'

describe('to columns', () => {
  let columns, params

  beforeEach(() => {
    columns = [
      {
        name: 'columnWithJustName'
      },
      {
        name: 'columnWithColumnSpecified',
        column: 'column_with_column_specified'
      }
    ]
  })

  it('handles column with just name', () =>
    toColumns(columns, {columnWithJustName: AN_EXPECTED_VALUE}).should.eql([{key: columns[0].name, column: columns[0].name, value: AN_EXPECTED_VALUE}])
  )

  it('handles column with column specified', () =>
    toColumns(columns, {columnWithColumnSpecified: AN_EXPECTED_VALUE}).should.eql([{key: columns[1].name, column: columns[1].column, value: AN_EXPECTED_VALUE}])
  )

  it('handles column with column specified', () =>
    toColumns(columns, {columnWithColumnSpecified: AN_EXPECTED_VALUE}).should.eql([{key: columns[1].name, column: columns[1].column, value: AN_EXPECTED_VALUE}])
  )
})
