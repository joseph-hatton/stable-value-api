const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/stableValueFunds/stableValueFundsSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

describe('stable value funds schema', () => {
  validateSchema(target, {
    tableShort: 'stable_value_fund',
    table: 'sch_stbv.stable_value_fund',
    tableId: 'stable_value_fund_id',
    sequence: 'sch_stbv.stable_value_fund_seq.NEXTVAL',
    orderBy: 'contract_id',
    columns: [
      {name: 'stableValueFundId', column: 'stable_value_fund_id', sequence: 'sch_stbv.stable_value_fund_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      version,
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
      {name: 'svBookValue', column: 'sv_book_value', converter: numberConverter},
      {name: 'svMarketValue', column: 'sv_market_value', converter: numberConverter},
      {name: 'svDuration', column: 'sv_duration', converter: numberConverter},
      {name: 'svCreditedRate', column: 'sv_credited_rate', converter: percentConverter},
      {name: 'svCashBuffer', column: 'sv_cash_buffer', converter: percentConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId
    ],
    required: {
      new: ['contractId', 'effectiveDate', 'svBookValue', 'svMarketValue', 'svDuration', 'svCreditedRate', 'svCashBuffer'],
      update: []
    }
  })
})
