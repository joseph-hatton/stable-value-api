const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const createApi = require('../common/createApi')
const getByIdApi = require('../common/getByIdApi')
const updateByIdApi = require('../common/updateByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../contacts/contactSchema')
const buildContactsQueryWhereClause = require('../contacts/buildContactsQueryWhereClause')

module.exports = createController({
  getMany: getManyApi(schema, buildContactsQueryWhereClause),
  getOne: getByIdApi(schema),
  create: createApi(schema),
  update: updateByIdApi(schema),
  delete: deleteByIdApi(schema)
})
