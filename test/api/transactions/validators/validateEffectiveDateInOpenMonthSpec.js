/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/transactions/transactionSchema')

describe('validate effective date in open month', () => {
  let target, isFutureMonth, isMonthOpen, transaction, contract
  beforeEach(() => {
    transaction = {effectiveDate: '2018-06-20T00:00:00.000Z'}
    contract = {effectiveDate: '2018-06-20T00:00:00.000Z'}
    isFutureMonth = sinon.stub()
    isMonthOpen = sinon.stub()
    target = proxyquire('api/transactions/validators/validateEffectiveDateInOpenMonth', {
      '../../monthlyCycles/isFutureMonth': isFutureMonth,
      '../../monthlyCycles/isMonthOpen': isMonthOpen
    })
  })

  const mockMonthOpenAndFuture = (monthOpen, futureMonth) => {
    isMonthOpen.resolves(monthOpen)
    isFutureMonth.resolves(futureMonth)
  }

  it('returns error message if no effective date', async () => {
    const result = await target({}, contract)

    result.should.eql('The effective date is required.')
  })

  it('returns error message if transaction effective date is before contract\'s', async () => {
    mockMonthOpenAndFuture(false, false)
    const result = await target(transaction, {effectiveDate: '2018-06-21T00:00:00.000Z'})

    result.should.eql('The effective date cannot be before the contract effective date.')
  })

  it('returns error message if month not open or future', async () => {
    mockMonthOpenAndFuture(false, false)
    const result = await target(transaction, contract)

    result.should.eql('The effective date must be for an open month.')
  })

  it('returns no error message if transation is termination', async () => {
    mockMonthOpenAndFuture(true, false)

    const result = await target(Object.assign({transactionType: 'TERMINATION'}, transaction), contract)

    should.not.exist(result)
  })

  it('returns no error message if month is open', async () => {
    mockMonthOpenAndFuture(true, false)
    const result = await target(transaction, contract)

    should.not.exist(result)
  })

  it('returns no error message if future month', async () => {
    mockMonthOpenAndFuture(false, true)
    const result = await target(transaction, contract)

    should.not.exist(result)
  })

  it('returns no error message if month is open and is future month', async () => {
    mockMonthOpenAndFuture(true, true)
    const result = await target(transaction, contract)

    should.not.exist(result)
  })
})
