const validateSchema = require('../validateSchema')
const target = require('../../../api/watchLists/reasonWatchListSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

describe('reason watch list schema', () => {
  validateSchema(target, {
    tableShort: 'reason_watch_list',
    table: 'sch_stbv.reason_watch_list',
    tableId: 'watch_list_id',
    orderBy: 'watch_list_id',
    columns: [
      {name: 'watchListId', column: 'watch_list_id', converter: numberConverter},
      {name: 'reasonId', column: 'reason_id', converter: numberConverter}
    ],
    required: {
      new: ['watchListId', 'reasonId'],
      update: ['watchListId', 'reasonId']
    }
  })
})
