const log = require('../../../log')
const schema = require('../../feeDistributions/feeDistributionsSchema')
const updateDistribution = require('./updateDistribution')
const applyPaymentsForFeeDistributionsWithDifferentInvoiceIds = require('./applyPaymentsForFeeDistributionsWithDifferentInvoiceIds')

module.exports = async (dbTransaction, feeDistributions = [], clientAmount, userId) => {
  log.info({domain: schema.tableShort, transactionId: dbTransaction.transactionId, feeDistributions, clientAmount, userId}, 'update fee distributions')

  for (const clientFeeDistribution of feeDistributions) {
    await updateDistribution(dbTransaction, clientFeeDistribution, clientAmount, userId)
  }

  await applyPaymentsForFeeDistributionsWithDifferentInvoiceIds(dbTransaction, feeDistributions, userId)
}
