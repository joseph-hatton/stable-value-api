const log = require('../../../log')
const selectFromDb = require('../../db/selectFromDb')
const insertIntoDb = require('../../db/insertIntoDb')
const buildDefaultParams = require('../../common/buildDefaultParams')
const schema = require('../creditingRatesSchema')

const selectNonForecastCreditingRates = async ({contractId}) => {
  return await selectFromDb(schema, { whereClause: `WHERE ${schema.table}.contract_id = :contractId and ${schema.table}.status != 'FORECAST'`, bindParams: {contractId} })
}

const buildInsertParams = (columns, userId, contract) =>
  buildDefaultParams({columns},
  {
    contractId: contract.contractId,
    effectiveDate: contract.effectiveDate,
    rate: contract.initialCreditingRate,
    status: 'APPROVED',
    referenceDate: contract.effectiveDate,
    locked: true,
    marketValue: contract.initialCoveredMarketValue || 0.0,
    annualEffectiveYield: 1,
    duration: 1.0,
    bookValue: contract.initialCoveredBookValue,
    modifiedId: userId
  })

module.exports = async (contract, effectiveDate, userId) => {
  log.info({domain: schema.tableShort, id: contract.contractId, effectiveDate, userId}, 'adding initial crediting rate')

  const existingNonForecasts = await selectNonForecastCreditingRates(contract)
  if (existingNonForecasts.length === 0) {
    const insertParams = buildInsertParams(schema.columns, userId, contract)
    return await insertIntoDb(schema, insertParams, userId)
  }
  return null
}
