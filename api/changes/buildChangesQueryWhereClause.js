const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const whereObjectClassEquals = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.objectClass) {
    whereClauseBits.push(`lower(object_class) = lower(:objectClass)`)
    response.bindParams.objectClass = req.query.objectClass
  }
}

const whereObjectIdEquals = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.objectId) {
    whereClauseBits.push(`object_id = :objectId`)
    response.bindParams.objectId = req.query.objectId
  }
}

module.exports = (schema, req) =>
  buildWhereClauseAndParamsWithPaging(schema, req, [whereObjectClassEquals, whereObjectIdEquals])
// { whereClause, bindParams, executeOptions, pagination }
