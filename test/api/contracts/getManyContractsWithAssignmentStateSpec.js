/* eslint-disable no-unused-expressions */
require('require-sql')
const sql = require('../../../api/contracts/getManyContractsWithAssignmentState.sql')
const target = require('../../../api/contracts/getManyContractsWithAssignmentState')

const contractId = 5

const buildCehRecord = (overrides) => Object.assign({
    contractEditHistoryId: undefined,
    effectiveDate: null,
    assignmentState: undefined,
    managerId: undefined
  },
  overrides)

describe('get contracts with assignment state', () => {
  it('has sql', async () => {
    target.sql.should.eql(sql)
  })

  describe('builds a mapper', () => {
    it('has a mapper', async () => {
      target.mapper.should.exist
    })

    it('copies all non ceh keys directly', async () => {
      target.mapper([{key: 'ok', contractId}]).should.eql([{editHistories: [], key: 'ok', contractId}])
    })

    it('strips ceh keys if cehId exists', async () => {
      target.mapper([{contractId, cehId: 42}]).should.eql([{editHistories: [buildCehRecord({contractEditHistoryId: 42})], contractId}])
    })

    it('maps all ceh keys', async () => {
      const row = {
        contractId,
        cehId: 42,
        cehEffectiveDate: (new Date()).toISOString(),
        cehAssignmentState: 'MO',
        cehManagerId: 88
      }
      target.mapper([row]).should.eql([
        {
          editHistories: [
            buildCehRecord({
              contractEditHistoryId: row.cehId,
              effectiveDate: new Date(row.cehEffectiveDate),
              assignmentState: row.cehAssignmentState,
              managerId: 88
            })],
            contractId
      }])
    })
  })
})
