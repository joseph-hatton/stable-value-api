SELECT  company.*,
        contact.contact_id,
        contact.first_name,
        contact.last_name,
        contact.middle_name
FROM sch_stbv.company 
LEFT OUTER JOIN sch_stbv.contact 
  ON company.company_id = contact.company_id