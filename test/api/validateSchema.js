const omit = require('lodash/omit')
const union = require('lodash/union')

const toName = (column) => column.name

module.exports = (actualSchema, expectedSchema) => {
  it('validate the tableShort', () => {
    actualSchema.tableShort.should.eql(expectedSchema.tableShort)
  })

  it('validate the table', () => {
    actualSchema.table.should.eql(expectedSchema.table)
  })

  it('validate the tableId', () => {
    actualSchema.tableId.should.eql(expectedSchema.tableId)
  })

  if (actualSchema.tableIdType || expectedSchema.tableIdType) {
    it('validate the tableIdType', () => {
      actualSchema.tableIdType.should.eql(expectedSchema.tableIdType)
    })
  }
  if (actualSchema.sequence || expectedSchema.sequence) {
    it('validate the sequence', () => {
      actualSchema.sequence.should.eql(expectedSchema.sequence)
    })
  }

  it('validate the orderBy', () => {
    actualSchema.orderBy.should.eql(expectedSchema.orderBy)
  })

  describe('validate the columns', () => {
    it('same number of columns', () => {
      actualSchema.columns.length.should.eql(expectedSchema.columns.length)
    })

    it('columns are in the exact expected order, required for inserts to work', () => {
      actualSchema.columns.map(toName).should.eql(expectedSchema.columns.map(toName))
    })

    actualSchema.columns.forEach((actualColumn, i) => {
      describe(`${actualColumn.name}`, () => {
        const expectedColumn = expectedSchema.columns[i]
        const keys = union(Object.keys(expectedColumn), Object.keys(actualColumn))
        keys.filter((key) => key !== 'defaultValue').forEach((key) => {
          if (actualColumn.column || expectedColumn.column) {
            it(`validate property [${key}] (${actualColumn[key]} === ${expectedColumn[key]})`, () => {
              actualColumn[key].should.eql(expectedColumn[key])
            })
          }
        })
        if (keys.indexOf('defaultValue') >= 0) {
          it('validate defaultValue result', () => {
            should.exist(actualColumn.defaultValue)
            should.exist(expectedColumn.defaultValue)
            const actualValue = actualColumn.defaultValue()
            const expectedValue = expectedColumn.defaultValue()
            if (Object.prototype.toString.call(actualValue) === '[object Date]') {
              Object.prototype.toString.call(actualValue).should.eql('[object Date]')
              Object.prototype.toString.call(expectedValue).should.eql('[object Date]')
            } else {
              actualColumn.defaultValue().should.eql(expectedColumn.defaultValue())
            }
          })
        }
      })
    })
  })

  if (actualSchema.required || expectedSchema.required) {
    it('validate the required', () => {
      actualSchema.required.should.eql(expectedSchema.required)
    })
  }
}
