const proxyquire = require('../../proxyquire')
const dbRejected = 'could not connect to the db'

describe('healthcheck getStatus', () => {
  let getStatus, selectFromDb, schema

  beforeEach(() => {
    schema = sinon.spy()
    selectFromDb = sinon.stub().withArgs(schema, { executeOptions: { maxRows: 2 } }).resolves([1, 2])
    getStatus = proxyquire('api/health/getStatus', {
      '../db/selectFromDb': selectFromDb,
      '../companies/companySchema': schema
    })
  })

  const apply = () => getStatus(100)

  it('resolves to error if a db connection cannot be established', () => {
    const error = new Error(dbRejected)
    selectFromDb.rejects(error)

    return apply().should.become({error})
  })

  it('resolves to empty if service is healthy', () => apply().should.become({}))
})
