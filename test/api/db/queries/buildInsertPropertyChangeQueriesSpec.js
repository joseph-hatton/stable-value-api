const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/changes/propertyChangeSchema')

const auditedSchema = {columns: [{audit: true, name: 'hat'}]}
const notAuditedSchema = {columns: [{audit: false}]}
const changeId = 55
const newObject = {hat: 'Bowler'}
const bindParams = {bindParams: true}

describe('build insert property change queries', () => {
  let target, buildInsertQuery, buildPropertyChangeBindParams

  beforeEach(() => {
    const selectBind = {
      whereClause: `WHERE ${schema.table}.contract_id = :id`,
      bindParams: { id: 542 }
    }
    buildInsertQuery = sinon.stub().withArgs(schema, bindParams).returns('property change insert query')
    buildPropertyChangeBindParams = sinon.stub().returns(bindParams)
    target = proxyquire('api/db/queries/buildInsertPropertyChangeQueries', {
      './buildInsertQuery': buildInsertQuery,
      './buildPropertyChangeBindParams': buildPropertyChangeBindParams
    })
  })

  it('schema with no audit columns returns empty array', () => {
    target(notAuditedSchema, changeId, newObject).should.eql([])
  })

  it('schema with audit columns returns insert queries', () => {
    target(auditedSchema, changeId, newObject).should.eql(['property change insert query'])
  })

  it('null property value does not create an insert query', () => {
    target(auditedSchema, changeId, {hat: null}).should.eql([])
  })
})
