const target = require('../../../api/riskScorecards/flattenRiskScorecardForInsertOrUpdateParam')

describe('flatten risk scorecard', () => {
  const scorecard = { riskScorecardId: 'aId', name: 'a' }

  const baseBody = {contractId: 88, effectiveDate: (new Date()).toISOString()}
  const baseBodyWithContractType = Object.assign({contractType: 'SINGLE_PLAN'}, baseBody)

  it('returns shell with empty', () => {
    const result = target({})
    result.should.eql({contractId: undefined, effectiveDate: undefined})
  })

  it('returns contractId and effectiveDate', () => {
    const result = target(baseBody)
    result.should.eql(baseBody)
  })

  it('returns contractId and effectiveDate', () => {
    const result = target(Object.assign({}, baseBody, {cashFlow: {score: 52, comments: 'a comment'}}))
    result.should.eql(Object.assign({}, baseBody, {cashFlow: 52, cashFlowComments: 'a comment'}))
  })

  it('omits contractType', () => {
    const result = target(baseBodyWithContractType)
    result.should.eql(baseBody)
  })

  it('doesn\'t omit contractType when we tell it not to', () => {
    const result = target(baseBodyWithContractType, true)
    result.should.eql(baseBodyWithContractType)
  })
})
