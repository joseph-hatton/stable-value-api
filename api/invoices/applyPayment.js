const Decimal = require('decimal.js')
const log = require('../../log')
const buildUpdateParams = require('../common/buildUpdateParams')
const updateDb = require('../db/updateDb')
const schema = require('./invoicesSchema')

module.exports = async (invoice, amount, adjustment, userId) => {
  log.info({domain: schema.tableShort, invoiceId: invoice.invoiceId, amount, adjustment, userId}, 'applying payment')
  const paramsToUpdate = {
    feesReceived: Decimal(invoice.feesReceived).add(amount).toNumber(),
    adjustments: Decimal(invoice.adjustments).add(adjustment).toNumber(),
    outstandingBalance: Decimal(invoice.outstandingBalance).sub(amount).add(adjustment).toNumber()
  }
  const updateParams = buildUpdateParams(schema.columns, userId, paramsToUpdate)
  return await updateDb(schema, updateParams, invoice.invoiceId, userId)
}
