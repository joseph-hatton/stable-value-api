const validateSchema = require('../validateSchema')
const target = require('../../../api/contractEditHistories/contractEditHistorySchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const {booleanConverter, bpsConverter, beginningOfDayDateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')
const stateValidator = require('../../../api/states/stateValidator')
const contactTypeValidator = require('../../../api/contacts/contactTypeValidator')

describe('contract edit history schema', () => {
  validateSchema(target, {
    tableShort: 'contract_edit_history',
    table: 'sch_stbv.contract_edit_history',
    tableId: 'contract_edit_history_id',
    sequence: 'sch_stbv.edit_history_seq.NEXTVAL',
    orderBy: 'contract_edit_history_id',
    columns: [
      {name: 'contractEditHistoryId', column: 'contract_edit_history_id', sequence: 'sch_stbv.edit_history_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      version,
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'effectiveDate', column: 'effective_date', defaultValue: () => new Date(), converter: beginningOfDayDateConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId,
      {name: 'jurisdiction'},
      {name: 'premiumRate', column: 'premium_rate', converter: bpsConverter},
      {name: 'managerFeeRate', column: 'manager_fee_rate', converter: bpsConverter},
      {name: 'advisorFeeRate', column: 'advisor_fee_rate', converter: bpsConverter},
      {name: 'creditingRateCalcMethod', column: 'crediting_rate_calc_method'},
      {name: 'assignmentState', column: 'assignment_state'},
      {name: 'legalFormName', column: 'legal_form_name'},
      {name: 'managerId', column: 'manager_id', converter: numberConverter},
      {name: 'fundLevelUtilization', column: 'fund_level_utilization', converter: percentConverter},
      {name: 'planLevelUtilization', column: 'plan_level_utilization', converter: percentConverter},
      {name: 'fundActiveRate', column: 'fund_active_rate', converter: percentConverter},
      {name: 'fundSeniorRate', column: 'fund_senior_rate', converter: percentConverter},
      {name: 'spRating', column: 'sp_rating'},
      {name: 'moodyRating', column: 'moody_rating'},
      {name: 'fitchRating', column: 'fitch_rating'},
      {name: 'overallRating', column: 'overall_rating'},
      {name: 'accountDurationLimit', column: 'account_duration_limit', converter: numberConverter}
    ],
    required: {
      new: ['contractId', 'createdDate'],
      update: ['contractId', 'createdDate']
    }
  })
})
