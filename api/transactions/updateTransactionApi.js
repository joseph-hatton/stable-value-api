const createError = require('http-errors')
const log = require('../../log')
const buildUpdateParams = require('../common/buildUpdateParams')
const validateThenCallApi = require('../common/validateThenCallApi')
const contractSchema = require('../contracts/contractSchema')
const updateDb = require('../db/updateDb')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const getUserId = require('../getUserId')
const schema = require('./transactionSchema')
const validateEffectiveDateInOpenMonth = require('./validators/validateEffectiveDateInOpenMonth')
const updateAdjustments = require('./adjustments/updateAdjustments')
const updateDistributions = require('./distributions/updateDistributions')

const attemptUpdateApi = async (schema, body, res, next, {userId, transactionId, dbTransaction}) => {
  log.info({domain: schema.tableShort, transactionType: body.transactionType, id: transactionId}, 'attempt to update transaction')

  // const id = (body.transactionType === 'FEE_ADJUSTMENT') ? body.adjustmentTransactionId : transactionId
  // const dbTransaction = await selectByIdFromDb(schema, transactionId)
  if (body.transactionType === 'FEE_RECEIPT') {
    await updateDistributions(dbTransaction, body.feeDistributions, body.amount, userId)
    await updateAdjustments(dbTransaction, body, userId)
  } else if (body.transactionType === 'TERMINATION' && body.statedMaturityDate) {
    const updateParams = buildUpdateParams(contractSchema.columns, userId, {statedMaturityDate: body.statedMaturityDate})
    await updateDb(contractSchema, updateParams, dbTransaction.contractId, userId)
  }
  const transactionUpdateParams = buildUpdateParams(schema.columns, userId, body)
  await updateDb(schema, transactionUpdateParams, dbTransaction.transactionId, userId)

  const result = await selectByIdFromDb(schema, dbTransaction.transactionId)
  res.status(200).json(result)
}

module.exports = async ({ params, headers, body }, res, next) => {
  log.info({domain: schema.tableShort, id: params.id}, 'update transaction by id api')
  const userId = getUserId(headers)
  const contract = await selectByIdFromDb(contractSchema, body.contractId)
  const errMessage = await validateEffectiveDateInOpenMonth(body, contract)
  if (errMessage) {
    throw createError.BadRequest(errMessage)
  }
  const id = (body.transactionType === 'FEE_ADJUSTMENT') ? body.adjustmentTransactionId : params.id
  const dbTransaction = await selectByIdFromDb(schema, id)
  if (dbTransaction && dbTransaction.locked) {
    throw createError.BadRequest('This transaction is locked.')
  }
  return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptUpdateApi, {userId, transactionId: id, dbTransaction})
}
