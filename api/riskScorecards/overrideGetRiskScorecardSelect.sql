SELECT  risk_scorecard.*,
        contract.contract_type
FROM    sch_stbv.risk_scorecard 
INNER JOIN sch_stbv.contract 
ON risk_scorecard.contract_id = contract.contract_id