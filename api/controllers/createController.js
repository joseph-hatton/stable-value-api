const toPairs = require('lodash/toPairs')
const fromPairs = require('lodash/fromPairs')
// see https://github.com/apigee-127/sway/issues/65
// traditional express middleware won't work here because we have to insert ourselves in the middle of
// the swagger router and the endpoint

const setParameters = (req, location, property = location) => {
  const operation = req.swagger && req.swagger.operation
  if (operation) {
    req[property] = fromPairs(
      operation.getParameters()
      .filter(p => p.in === location)
      .map(p => [p.name, p.getValue(req).value])
    )
  }
}

const wrappedEndpoint = endpoint =>
  async (req, res, next) => {
    setParameters(req, 'path', 'params')
    setParameters(req, 'query')
    try {
      return await endpoint(req, res, next)
    } catch (e) {
      next(e)
    }
  }

module.exports = endpoints =>
  fromPairs(toPairs(endpoints).map(([name, endpoint]) => [name, wrappedEndpoint(endpoint)]))
