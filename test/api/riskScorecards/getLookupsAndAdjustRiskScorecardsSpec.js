const proxyquire = require('../../proxyquire')
const schema = require('../../../api/riskScorecards/riskLookupsSchema')

const riskScorecards = ['scorecard1', 'scorecard2']

describe('get lookups and adjust riskscorecards', () => {
  let target, selectFromDb

  beforeEach(() => {
    selectFromDb = sinon.stub().withArgs(schema, {}).resolves('risk lookups')
    target = proxyquire('api/riskScorecards/getLookupsAndAdjustRiskScorecards', {
      '../db/selectFromDb': selectFromDb,
      './adjustRiskScorecards': sinon.stub().withArgs('risk lookups', riskScorecards).resolves(['adjustedScorecard1', 'adjustedScorecard2'])
    })
  })

  it('returns adjusted scorecards', async () => {
    const result = await target(riskScorecards)
    result.should.eql(['adjustedScorecard1', 'adjustedScorecard2'])
  })
})
