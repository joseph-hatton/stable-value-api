const buildWhereClauseAndParams = require('../common/buildWhereClauseAndParams')

const whereContractIdEquals = (schema, req, whereClauseBits, response) => {
  if (req.params && req.params.contractId) {
    whereClauseBits.push(`${schema.table}.contract_id = :contractId`)
    response.bindParams.contractId = req.params.contractId * 1
  }
}

const whereOutstandingBalanceIsNotZero = (schema, req, whereClauseBits) => {
  whereClauseBits.push(`${schema.table}.outstanding_balance <> 0`)
}

const theModule = (schema, req) => {
  return buildWhereClauseAndParams(schema, req, [whereContractIdEquals, whereOutstandingBalanceIsNotZero])
}

theModule.whereContractIdEquals = whereContractIdEquals
theModule.whereOutstandingBalanceIsNotZero = whereOutstandingBalanceIsNotZero

module.exports = theModule
