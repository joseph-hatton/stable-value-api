/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')
const contractSchema = require('../../../../api/contracts/contractSchema')

const params = {params: true}
const headers = ['header']
const body = {effectiveDate: '2017-11-13T00:00:00.000Z'}
const cannotUpdateError = 'Only Awaiting OR Forecast Crediting Rates with referenceDate, marketValue, annualEffectiveYield and duration on Active Contracts can be calculated.'
const wrongDateError = 'Effective date must be after the contract effective date and in an open month.'

describe('calculate crediting rate api', () => {
  let target, res, resJson, next, updateDb, selectByIdFromDb, getUserId, buildUpdateParams, validateThenCallApi, getCreditingRatesByContract, isMonthOpen, isFutureMonth, getBalancesByContract, calculateCreditingRateBuildParamsOrErrors
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    updateDb = sinon.stub()
    selectByIdFromDb = sinon.stub()
    getUserId = sinon.stub().resolves('a user')
    buildUpdateParams = sinon.stub()
    validateThenCallApi = sinon.stub().resolves('done')
    getCreditingRatesByContract = sinon.stub().resolves([])
    isMonthOpen = sinon.stub()
    isFutureMonth = sinon.stub()
    getBalancesByContract = sinon.stub()
    calculateCreditingRateBuildParamsOrErrors = sinon.stub()
    target = proxyquire('api/creditingRates/creditingRate/calculateCreditingRateApi', {
      '../../db/updateDb': updateDb,
      '../../db/selectByIdFromDb': selectByIdFromDb,
      '../../getUserId': getUserId,
      '../../common/buildUpdateParams': buildUpdateParams,
      '../../common/validateThenCallApi': validateThenCallApi,
      '../../monthlyCycles/isMonthOpen': isMonthOpen,
      '../../monthlyCycles/isFutureMonth': isFutureMonth,
      '../../balances/getBalancesByContract': getBalancesByContract,
      './calculateCreditingRateBuildParamsOrErrors': calculateCreditingRateBuildParamsOrErrors,
      './getCreditingRatesByContract': getCreditingRatesByContract
    })
  })

  it('validateThenCallApi was called', async () => {
    const result = await target({params, headers, body}, res, next)
    validateThenCallApi.should.have.been.called
  })

  describe('attempt to calculate', () => {
    let attemptToCalculate, contractId, creditingRateId, apiParams, creditingRateParams, updatedCreditingRate
    beforeEach(async () => {
      contractId = 54
      creditingRateId = 552
      updatedCreditingRate = {updatedCreditingRate: true}
      selectByIdFromDb.withArgs(contractSchema, contractId).resolves({status: 'ACTIVE', effectiveDate: new Date('2017-11-12T00:00:00.000Z')})
      isMonthOpen.withArgs(body.effectiveDate).resolves(true)
      isFutureMonth.withArgs(body.effectiveDate).resolves(true)
      selectByIdFromDb.withArgs(schema, creditingRateId).resolves({status: 'AWAITING', referenceDate: new Date(), marketValue: 2, annualEffectiveYield: 3, duration: 4, bookValue: 3})
      getCreditingRatesByContract.resolves([])
      calculateCreditingRateBuildParamsOrErrors.returns({errors: [], params: creditingRateParams})
      updateDb.resolves(updatedCreditingRate)
      await target({params, headers, body}, res, next)
      apiParams = {contractId, userId: 'a user', creditingRateId}
      attemptToCalculate = validateThenCallApi.getCall(0).args[5]
    })

    it('null contract throws error', async () => {
      selectByIdFromDb.withArgs(schema, creditingRateId).resolves(null)
      try {
        await attemptToCalculate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql(cannotUpdateError)
      }
    })

    it('pending contract throws error', async () => {
      selectByIdFromDb.withArgs(contractSchema, contractId).resolves({status: 'PENDING'})
      try {
        await attemptToCalculate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql(cannotUpdateError)
      }
    })

    it('calculation effective date after contract effective date', async () => {
      try {
        await attemptToCalculate(schema, {effectiveDate: '2017-11-15T00:00:00.000Z'}, res, next, apiParams)
      } catch (err) {
        err.message.should.eql(wrongDateError)
      }
    })

    it('throws error if crediting rate invalid', async () => {
      calculateCreditingRateBuildParamsOrErrors.returns({errors: ['error1'], params: creditingRateParams})
      try {
        await attemptToCalculate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql({messages: ['error1']})
      }
    })

    it('month is not open or future', async () => {
      isMonthOpen.withArgs(body.effectiveDate).resolves(false)
      isFutureMonth.withArgs(body.effectiveDate).resolves(false)
      try {
        await attemptToCalculate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql(wrongDateError)
      }
    })

    it('too many crediting rates with same reference data throws error', async () => {
      getCreditingRatesByContract.resolves(['one', 'two'])
      try {
        await attemptToCalculate(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('Effective date is not unique for contract.')
      }
    })

    it('sends insert result to response', async () => {
      await attemptToCalculate(schema, body, res, next, apiParams)
      resJson.should.have.been.calledWithExactly(updatedCreditingRate)
    })
  })
})
