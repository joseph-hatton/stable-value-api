const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {numberConverter} = require('../db/converters')

const table = 'fee_distribution'
const tableId = 'fee_distribution_id'
const sequence = `${dbSchemaName}.fee_distribution_seq.NEXTVAL`

const zeroDefault = () => 0

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'transaction_id',
  columns: [
    {name: 'feeDistributionId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'transactionId', column: 'transaction_id', converter: numberConverter},
    {name: 'invoiceId', column: 'invoice_id', converter: numberConverter},
    {name: 'amount', converter: numberConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId,
    version,
    {name: 'beginningBalance', column: 'beginning_balance', converter: numberConverter},
    {name: 'adjustment', converter: numberConverter},
    {name: 'endingBalance', column: 'ending_balance', converter: numberConverter}
  ],
  required: {
    new: ['transactionId'],
    update: ['transactionId']
  }
}
