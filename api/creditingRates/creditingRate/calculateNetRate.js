const mathjs = require('mathjs')
const log = require('../../../log')

module.exports = ({grossRate, premiumRate = 0, managerFeeRate = 0, advisorFeeRate = 0, creditingRateType, netOfThirdPartyFees, creditingRateManagerFee, creditingRateAdvisorFee, creditingRateRoundDecimals = 2}) => {
  log.info({domain: 'crediting_rate', grossRate, premiumRate, managerFeeRate, advisorFeeRate, creditingRateType, creditingRateManagerFee, creditingRateAdvisorFee, creditingRateRoundDecimals}, 'calculating net rate')

  let rate = grossRate
  if (creditingRateType === 'NET') {
    rate = rate - (premiumRate / 100)
    if (netOfThirdPartyFees && creditingRateManagerFee) {
      rate = rate - (managerFeeRate / 100)
    }
    if (netOfThirdPartyFees && creditingRateAdvisorFee) {
      rate = rate - (advisorFeeRate / 100)
    }
  }
  const calculatedNetRate = mathjs.round(rate, creditingRateRoundDecimals + 2)
  log.info({domain: 'crediting_rate', grossRate, premiumRate, managerFeeRate, advisorFeeRate, creditingRateType, creditingRateManagerFee, creditingRateAdvisorFee, creditingRateRoundDecimals, calculatedNetRate, beforeRounding: rate}, 'calculated net rate')
  return calculatedNetRate
}
