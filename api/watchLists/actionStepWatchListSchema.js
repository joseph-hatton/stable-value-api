const {dbSchemaName} = require('../config')
const {numberConverter} = require('../db/converters')

const table = 'action_step_watch_list'

const zeroDefault = () => 0
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: 'watch_list_id',
  orderBy: 'watch_list_id',
  columns: [
    {name: 'watchListId', column: 'watch_list_id', converter: numberConverter},
    {name: 'actionStepId', column: 'action_step_id', converter: numberConverter}
  ],
  required: {
    new: ['watchListId', 'actionStepId'],
    update: ['watchListId', 'actionStepId']
  }
}
