SELECT 
    watch_list.*, 
    contract.short_plan_name as short_plan_name, 
    contract.contract_number as contract_number, 
    reason.description as reason_description, 
    reason.reason_id as reason_id, 
    action_step.description as action_step_description,
    action_step.action_step_id as action_step_id 
FROM SCH_STBV.WATCH_LIST watch_list
LEFT OUTER JOIN sch_stbv.contract contract
    ON contract.contract_id = watch_list.contract_id
LEFT OUTER JOIN sch_stbv.reason_watch_list reason_watch_list
    ON reason_watch_list.watch_list_id = watch_list.watch_list_id
LEFT OUTER JOIN sch_stbv.reason reason
    ON reason.reason_id = reason_watch_list.reason_id
LEFT OUTER JOIN sch_stbv.action_step_watch_list action_step_watch_list
    ON action_step_watch_list.watch_list_id = watch_list.watch_list_id
LEFT OUTER JOIN sch_stbv.action_step action_step
    ON action_step.action_step_id = action_step_watch_list.action_step_id