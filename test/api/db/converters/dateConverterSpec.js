
const {toDb, fromDb} = require('../../../../api/db/converters/dateConverter')

describe('date converter', () => {
  it('handle to db with string', () => {
    const theDate = new Date()
    toDb(theDate.toISOString()).should.eql(theDate)
  })

  it('handle to db with date', () => {
    const theDate = new Date()
    toDb(theDate).should.eql(theDate)
  })

  it('handle to db with null', () => {
    should.not.exist(toDb(null))
  })

  it('handle to db with some other type', () => {
    const theDate = 5
    toDb(5).should.eql(5)
  })

  it('handle from db with string', () => {
    const theDate = new Date()
    fromDb(theDate.toISOString()).should.eql(theDate)
  })

  it('handle from db with null', () => {
    should.not.exist(fromDb(null))
  })
})
