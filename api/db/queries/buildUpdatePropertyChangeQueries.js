const log = require('../../../log')
const propertyChangeSchema = require('../../changes/propertyChangeSchema')
const buildInsertQuery = require('./buildInsertQuery')
const buildPropertyChangeBindParams = require('./buildPropertyChangeBindParams')

module.exports = (schema, changeId, oldObject, newObject) => {
  const auditableColumns = schema.columns.filter((column) => column.audit === true)
  if (auditableColumns.length > 0) {
    const propertyChangeQueries = []
    log.info({domain: schema.tableShort, auditableColumnsLength: auditableColumns.length}, 'should enable update properties audit')
    auditableColumns.forEach((column) => {
      const oldValue = (oldObject[column.name]) ? oldObject[column.name].toString() : null
      const newValue = (newObject[column.name]) ? newObject[column.name].toString() : null
      log.info({domain: schema.tableShort, auditableColumn: column.name, oldValue, newValue}, 'should enable update properties audit for column')
      if (oldValue !== newValue) {
        propertyChangeQueries.push(buildInsertQuery(propertyChangeSchema, buildPropertyChangeBindParams(schema, changeId, column.name, oldValue, newValue)))
      }
    })
    return propertyChangeQueries
  } else {
    log.info({domain: schema.tableShort}, 'should NOT enable update properties audit')
    return []
  }
}
