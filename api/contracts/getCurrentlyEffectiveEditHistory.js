const log = require('../../log')
const sortBy = require('lodash/sortBy')

module.exports = (contractStatus, editHistories, field) => {
  const currentDate = new Date()
  let currentlyEffective = (editHistories.length > 0) ? editHistories[0] : null
  const sorted = sortBy(editHistories, (editHistory) => new Date(editHistory.effectiveDate))
    .filter((editHistory) => editHistory[field])
  sorted.forEach((editHistory) => {
    if (currentDate.getTime() > (new Date(editHistory.effectiveDate)).getTime() || contractStatus === 'PENDING' || contractStatus === 'PENDING_JASPER' || contractStatus === 'SUBMITTED') {
      currentlyEffective = editHistory
    }
  })
  return currentlyEffective[field]
}
