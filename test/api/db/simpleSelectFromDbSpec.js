/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'

describe('simple select from db', () => {
  let target, connection, dbExecutePromise, dbExecuteOne, buildSimpleSqlQuery, mapResult

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    dbExecuteOne = sinon.stub().resolves('yay')
    buildSimpleSqlQuery = sinon.stub().returns('simple sql result')
    connection = sinon.spy()
    mapResult = sinon.stub().returns('mapped')
    target = proxyquire('api/db/simpleSelectFromDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/dbExecuteOne': dbExecuteOne,
      './queries/buildSimpleSqlQuery': buildSimpleSqlQuery
    })
  })

  const executeTarget = () => {
    return target('some sql', mapResult)
    .then((result) =>
      dbExecutePromise.getCall(0).args[0](connection)
      .then(() => result))
  }

  describe('execute promise', () => {
    it('called once', () =>
      executeTarget()
      .then(() => dbExecutePromise.callCount.should.eql(1)))

    it('returns result', () =>
      executeTarget()
      .then((result) => result.should.eql(DB_EXECUTE_PROMISE_RESULT)))
  })

  describe('execute one', () => {
    it('called once', () =>
      executeTarget()
      .then(() => dbExecuteOne.callCount.should.eql(1)))

    it('called with connection', () =>
      executeTarget()
      .then(() => dbExecuteOne.getCall(0).args[0].should.eql(connection)))

    it('called with connection', () =>
      executeTarget()
      .then(() => dbExecuteOne.getCall(0).args[1].should.eql('simple sql result')))
  })

  describe('build simple sql', () => {
    it('called once', () =>
      executeTarget()
      .then(() => buildSimpleSqlQuery.callCount.should.eql(1)))

    it('called with sql', () =>
      executeTarget()
      .then(() => buildSimpleSqlQuery.getCall(0).args[0].should.eql('some sql')))

    it('called with connection', () =>
      executeTarget()
      .then(() => buildSimpleSqlQuery.getCall(0).args[1].should.eql(mapResult)))
  })
})
