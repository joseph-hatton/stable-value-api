const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contractEditHistories/contractEditHistorySchema')

const EDIT_HISTORY_ID = 888
const BODY = {name: 'hi'}
const USER_ID = 'me'

describe('update contract edit history', () => {
  let target, updateDb, buildUpdateParams
  beforeEach(() => {
    updateDb = sinon.stub().resolves('done')
    buildUpdateParams = sinon.stub().returns('params ready to go')
    target = proxyquire('api/contractEditHistories/updateContractEditHistory', {
      '../db/updateDb': updateDb,
      '../common/buildUpdateParams': buildUpdateParams
    })
  })

  it('returns insert into db response', async () => {
    const result = await target(BODY, EDIT_HISTORY_ID, USER_ID)
    result.should.eql('done')
  })
})
