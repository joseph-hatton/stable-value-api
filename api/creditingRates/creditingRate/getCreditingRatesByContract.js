const moment = require('moment')
const log = require('../../../log')
const selectFromDb = require('../../db/selectFromDb')
const schema = require('../creditingRatesSchema')

module.exports = async (contractId, {referenceDate = null, effectiveDate = null}) => {
  log.info({domain: schema.tableShort, id: contractId, referenceDate}, 'get crediting rates by contract id and optionally reference date')

  let whereClauseBits = [`${schema.table}.contract_id = :id`]
  const bindParams = { id: contractId }

  if (referenceDate) {
    whereClauseBits.push(`${schema.table}.reference_date = :referenceDate`)
    bindParams.referenceDate = moment(referenceDate).utc().startOf('day').toDate()
  }
  if (effectiveDate) {
    whereClauseBits.push(`${schema.table}.effective_date = :effectiveDate`)
    bindParams.effectiveDate = moment(effectiveDate).utc().startOf('day').toDate()
  }
  const whereClause = `WHERE ${whereClauseBits.join(' AND ')}`
  return await selectFromDb(schema, { whereClause, bindParams })
}
