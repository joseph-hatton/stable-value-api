const moment = require('moment')

module.exports = (year) => {
  return moment(`${year}-02-01`).endOf('month').date() === 29
}
