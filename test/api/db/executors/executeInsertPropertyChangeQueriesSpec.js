/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const OBJECT_BEFORE = {before: true}
const PROP_CHANGES = ['PROP_CHANGE']

describe('execute insert insert property change queries', () => {
  let target, dbExecuteInOrder, connection, buildInsertPropertyChangeQueries
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteInOrder = sinon.stub().resolves('ONE_EXECUTED')
    buildInsertPropertyChangeQueries = sinon.stub().returns(PROP_CHANGES)
    target = proxyquire('api/db/executors/executeInsertPropertyChangeQueries', {
      './dbExecuteInOrder': dbExecuteInOrder,
      '../queries/buildInsertPropertyChangeQueries': buildInsertPropertyChangeQueries
    })
  })

  it('returns execute result', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute in order should only be called once', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then(() => dbExecuteInOrder.callCount.should.eql(1)))

  it('execute in order should only be called with', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then(() => dbExecuteInOrder.should.have.been.calledWithExactly(connection, PROP_CHANGES)))

  it('build insert propery change queries should have been called with', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then(() => buildInsertPropertyChangeQueries.should.have.been.calledWithExactly(schema, ROW_ID, OBJECT_BEFORE)))

  it('execute in order not called if no change queries', () => {
    buildInsertPropertyChangeQueries.returns([])
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    dbExecuteInOrder.should.have.been.not.been.called
  })

  it('execute in order returns other message if no change queries', () => {
    buildInsertPropertyChangeQueries.returns([])
    target(connection, schema, ROW_ID, OBJECT_BEFORE).should.eql('no property change queries')
  })
})
