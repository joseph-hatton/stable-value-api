const target = require('../../../api/wrapPortfolios/buildPortfolioQueryWhereClause')

const nullPagination = {pageNumber: null, pageSize: null}
const DEFAULT_STATE = { bindParams: {}, whereClause: '', pagination: nullPagination }

describe('build portfolio query where clause', () => {
  it('handles empty schema and req', () => target({}, {}).should.eql(DEFAULT_STATE))

  it('handles portfolio manager id query parameter', () => {
    return target({table: 'table'}, {query: {portfolioManagerId: 12345}}).should.eql({ bindParams: {portfolioManagerId: 12345}, whereClause: 'WHERE PORTFOLIO_MANAGER_ID = :portfolioManagerId', pagination: nullPagination })
  })

  it('ignores other path parameters', () => {
    return target({table: 'table'}, {params: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
