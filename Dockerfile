FROM stlpartifact01.rgare.net:5443/node-oracledb:8
RUN apt-get update && \
    apt-get install -y bash curl vim
WORKDIR /app
COPY . /app
RUN npm install --production
EXPOSE 3000
CMD ["npm", "start"]
HEALTHCHECK --interval=30s --timeout=10s --retries=4 CMD /app/scripts/healthcheck.sh
