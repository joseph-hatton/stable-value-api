module.exports = (schema, req, customRequestParsers = []) => {
  const whereClauseBits = []
  const response = {
    bindParams: {},
    pagination: {
      pageNumber: null,
      pageSize: null
    }
  }
  customRequestParsers.forEach((customRequestParser) => customRequestParser(schema, req, whereClauseBits, response))

  response.whereClause = (whereClauseBits.length > 0) ? `WHERE ${whereClauseBits.join(' AND ')}` : ''
  return response
}
// { whereClause, bindParams, executeOptions, pagination }
