const moment = require('moment')
const log = require('../../log')
const selectFromDb = require('../db/selectFromDb')
const schema = require('./balancesSchema')
const whereContractIdAndEffectiveDate = require('./whereContractIdAndEffectiveDate')

module.exports = async (contractId, {effectiveDate = null}) => {
  log.info({domain: schema.tableShort, id: contractId, effectiveDate}, 'get balances by contract id and optionally an effective date')
  const whereAndBindParams = whereContractIdAndEffectiveDate(contractId, effectiveDate)
  return await selectFromDb(schema, whereAndBindParams)
}
