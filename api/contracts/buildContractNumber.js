require('require-sql')
const sql = require('./buildContractNumber.sql')
const simpleSelectFromDb = require('../db/simpleSelectFromDb')
const log = require('../../log')

module.exports = (connection) =>
  simpleSelectFromDb(sql, (result) => {
    const contractNumber = result.rows[0].MIN
    log.info({domain: 'contract', contractNumber: contractNumber}, 'get next contract number')
    return contractNumber
  })
