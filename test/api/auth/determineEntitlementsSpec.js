/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

describe('determineEntitlements', () => {
  let target, entitlements, getUserGroups, groups
  beforeEach(() => {
    entitlements = {
      'url': {
        'method': ['g1', 'g2']
      }
    }
    groups = []
    getUserGroups = sinon.stub().returns({groups})
    target = proxyquire('api/auth/determineEntitlements', {
      './entitlements': entitlements,
      './getUserGroups': getUserGroups
    })
  })
  it('should call the correct functions', async () => {
    const result = target('user')

    getUserGroups.should.have.been.called
  })
  it('should set group correctly', async () => {
    groups.push('g1')

    const result = await target('user')

    getUserGroups.should.have.been.called
    result['url']['method'].should.eql(true)
  })
  it('should set group correctly', async () => {
    groups.push('g2')

    const result = await target('user')

    getUserGroups.should.have.been.called
    result['url']['method'].should.eql(true)
  })
  it('should set group correctly', async () => {
    groups.push('g3')

    const result = await target('user')

    getUserGroups.should.have.been.called
    result['url']['method'].should.eql(false)
  })
})
