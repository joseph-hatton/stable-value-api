/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const {oldAppBaseUrl} = require('../../../api/config')

describe('get cycle status api', () => {
  let target, res, jsonStub, getCycleStatus

  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    getCycleStatus = sinon.stub().resolves('expected cycle data')
    target = proxyquire('api/cycles/getCycleStatusApi', {
      './getCycleStatus': getCycleStatus
    })
  })

  it('sends cycle data to json response', async () => {
    await target({}, res)

    jsonStub.should.have.been.calledWithExactly('expected cycle data')
  })

  it('calls get with correct url', async () => {
    await target({}, res)

    getCycleStatus.should.have.been.called
  })
})
