/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')
const contractSchema = require('../../../../api/contracts/contractSchema')

const userId = 'a user'
const headers = {headers: true}
const params = {contractId: 55, id: 42}
const updateParams = {updateParams: true}
const clearParams =

describe('approve crediting rate', () => {
  let target, body, res, resJson, updateDb, selectByIdFromDb, getUserId, buildUpdateParams
  beforeEach(() => {
    body = {}
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    updateDb = sinon.stub().resolves('update db response')
    selectByIdFromDb = sinon.stub()
    selectByIdFromDb.withArgs(contractSchema, 55).resolves({status: 'ACTIVE'})
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'PENDING', modifiedId: 'somebody else'})
    getUserId = sinon.stub().withArgs(headers).returns(userId)
    buildUpdateParams = sinon.stub().withArgs(schema.columns, userId, {status: 'APPROVED'}).returns(updateParams)
    target = proxyquire('api/creditingRates/creditingRate/approveCreditingRateApi', {
      '../../db/updateDb': updateDb,
      '../../db/selectByIdFromDb': selectByIdFromDb,
      '../../getUserId': getUserId,
      '../../common/buildUpdateParams': buildUpdateParams
    })
  })

  it('tests update db called', async () => {
    await target({params, headers, body}, res)
    updateDb.should.have.been.calledWithExactly(schema, updateParams, 42, userId)
    resJson.should.have.been.calledWithExactly('update db response')
  })

  it('throws error if contract not active', async () => {
    selectByIdFromDb.withArgs(contractSchema, 55).resolves({status: 'PENDING'})
    try {
      await target({params, headers, body}, res)
    } catch (err) {
      err.message.should.eql('Only Pending Crediting Rates (not last modified by you) on Active Contracts can be approved.')
    }
  })

  it('throws error if crediting rate is not pending', async () => {
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'APPROVED', modifiedId: 'somebody else'})
    try {
      await target({params, headers, body}, res)
    } catch (err) {
      err.message.should.eql('Only Pending Crediting Rates (not last modified by you) on Active Contracts can be approved.')
    }
  })

  it('throws error if crediting rate was last modified by same user', async () => {
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'PENDING', modifiedId: userId})
    try {
      await target({params, headers, body}, res)
    } catch (err) {
      err.message.should.eql('Only Pending Crediting Rates (not last modified by you) on Active Contracts can be approved.')
    }
  })
})
