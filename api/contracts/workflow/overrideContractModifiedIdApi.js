const log = require('../../../log')
const updateDb = require('../../db/updateDb')
const getUserId = require('../../getUserId')
const schema = require('../contractSchema')

module.exports = async ({ params, headers }, res) => {
  log.info({domain: schema.tableShort, id: params.id}, 'override contract modifiedId api')
  const userId = getUserId(headers)
  const response = await updateDb(schema, {modifiedId: 'testing-override'}, params.id, userId, true)
  res.status(200).json(response)
}
