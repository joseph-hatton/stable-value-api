const log = require('../../log')
const insertIntoDb = require('../db/insertIntoDb')
const getUserId = require('../getUserId')
const buildDefaultParams = require('./buildDefaultParams')
const validateThenCallApi = require('./validateThenCallApi')

const buildInsertParams = (columns, userId, body) =>
  buildDefaultParams({columns}, body,
  {
    createdId: userId,
    modifiedId: userId
  })

const attemptInsertApi = async (schema, body, res, next, {userId}) => {
  const insertParams = buildInsertParams(schema.columns, userId, body)
  const result = await insertIntoDb(schema, insertParams, userId)
  res.status(201).json(result)
}

module.exports = (schema) =>
  async ({ headers, body }, res, next) => {
    log.info({domain: schema.tableShort}, 'create api')
    const userId = getUserId(headers)
    const requiredColumns = (schema && schema.required && schema.required.new) ? schema.required.new : []
    return await validateThenCallApi(schema, body, requiredColumns, res, next, attemptInsertApi, {userId})
  }
