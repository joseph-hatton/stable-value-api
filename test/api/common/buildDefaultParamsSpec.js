const buildDefaultParams = require('../../../api/common/buildDefaultParams')

describe('build default schema params', () => {
  let schema, userData, overridingData

  beforeEach(() => {
    schema = {
      columns: [
        {
          name: 'alwaysExpected'
        }
      ]
    }
  })

  describe('with no userData or overridingData', () => {
    beforeEach(() => {
      userData = {}
      overridingData = {}
    })
    it('ignores sequence columns', () => {
      schema.columns.push({name: 'skipped', sequence: true})
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: null})
    })

    it('if no default then null', () => {
      schema.columns.push({name: 'findMe'})
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: null, findMe: null})
    })

    it('calls default function if exists', () => {
      schema.columns.push({name: 'findMe', defaultValue: () => 'hi'})
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: null, findMe: 'hi'})
    })
  })

  describe('with userData and no overridingData', () => {
    beforeEach(() => {
      userData = {alwaysExpected: 'donkey'}
      overridingData = {}
    })
    it('uses known userData value', () => {
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: 'donkey'})
    })

    it('strips unknown user data value', () => {
      userData = {alwaysExpected: 'donkey', notExpected: 'strip this one'}
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: 'donkey'})
    })
  })

  describe('with userData and overridingData', () => {
    beforeEach(() => {
      userData = {alwaysExpected: 'donkey'}
      overridingData = {alwaysExpected: 'two donkeys'}
    })
    it('uses known userData value', () => {
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: 'two donkeys'})
    })

    it('strips unknown user data value', () => {
      userData = {alwaysExpected: 'donkey'}
      overridingData = {alwaysExpected: 'two donkeys', notExpected: 'strip this one'}
      buildDefaultParams(schema, userData, overridingData).should.eql({alwaysExpected: 'two donkeys'})
    })
  })
})
