const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const createReasonsAndActionStepsinWatchlist = require('../watchLists/createReasonsAndActionStepsinWatchlist')
const updateReasonsAndActionStepsinWatchlist = require('../watchLists/updateReasonsAndActionStepsinWatchlist')
const deleteReasonsAndActionStepsinWatchlist = require('../watchLists/deleteReasonsAndActionStepsinWatchlist')
const getByIdApi = require('../common/getByIdApi')
const updateByIdApi = require('../common/updateByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../watchLists/watchListSchema')

module.exports = createController({
  getMany: getManyApi(schema),
  getOne: getByIdApi(schema),
  create: createReasonsAndActionStepsinWatchlist,
  update: updateReasonsAndActionStepsinWatchlist,
  delete: deleteReasonsAndActionStepsinWatchlist
})
