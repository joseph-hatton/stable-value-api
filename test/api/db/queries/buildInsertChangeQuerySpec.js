/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/changes/changeSchema')

const auditedSchema = {columns: [{audit: true, name: 'hat'}], audit: {objectClass: 'testingClass', buildDescription: () => 'some description'}}
const rowId = 55
const userId = 'a user'
const objectBeingChanged = {hat: 'Bowler'}
const operation = 'AN OPERATION'
const bindParams = {bindParams: true}

describe('build insert change query', () => {
  let target, buildInsertQuery

  beforeEach(() => {
    const selectBind = {
      whereClause: `WHERE ${schema.table}.contract_id = :id`,
      bindParams: { id: 542 }
    }
    buildInsertQuery = sinon.stub().returns('insert query')
    target = proxyquire('api/db/queries/buildInsertChangeQuery', {
      './buildInsertQuery': buildInsertQuery
    })
  })

  it('returns insert query', () => {
    const result = target(auditedSchema, rowId, userId, objectBeingChanged, operation).should.eql('insert query')
  })

  it('build change bind params called', () => {
    target(auditedSchema, rowId, userId, objectBeingChanged, operation)
    buildInsertQuery.should.have.been.called
  })

  it('build change bind params called with schema', () => {
    target(auditedSchema, rowId, userId, objectBeingChanged, operation)
    buildInsertQuery.getCall(0).args[0].should.eql(schema)
  })

  it('build change bind params called with bind params', () => {
    target(auditedSchema, rowId, userId, objectBeingChanged, operation)
    const bindParams = buildInsertQuery.getCall(0).args[1]

    bindParams.should.eql({
      objectId: 55,
      operation,
      objectClass: 'testingClass',
      createdDate: bindParams.createdDate,
      createdId: userId,
      effectiveDate: null,
      contractId: null,
      description: 'some description'
    })
  })
})
