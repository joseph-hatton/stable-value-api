/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

describe('db execute promise', () => {
  let target, db, close, dbPromiseChainCreator, connection
  beforeEach(() => {
    close = sinon.stub().resolves('closed')
    connection = {close}
    db = sinon.stub().resolves(connection)
    dbPromiseChainCreator = sinon.stub().resolves('done')
    target = proxyquire('api/db/executors/dbExecutePromise', {
      '../../db': db
    })
  })

  it('returns error from db', () => {
    db.rejects(new Error('ow'))
    return target([])
    .catch((error) => error.message.should.eql('ow'))
  })

  it('returns simple execute result', () =>
    target(dbPromiseChainCreator)
    .then((result) => result.should.eql('done')))

  it('promise chain called once', () =>
    target(dbPromiseChainCreator)
    .then(() => dbPromiseChainCreator.should.have.been.called))

  it('promise chain called with connection', () =>
    target(dbPromiseChainCreator)
    .then(() => dbPromiseChainCreator.should.have.been.calledWithExactly(connection)))

  it('close should only be called once', () =>
    target(dbPromiseChainCreator)
    .then(() => close.callCount.should.eql(1)))

  it('execute error bubbles up', () => {
    dbPromiseChainCreator.rejects(new Error('execute error'))
    return target(dbPromiseChainCreator)
    .catch((error) => error.message.should.eql('execute error'))
  })
})
