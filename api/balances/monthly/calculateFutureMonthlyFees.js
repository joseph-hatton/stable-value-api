const moment = require('moment')
const isLeapYear = require('../isLeapYear')

module.exports = async (balance, date, filterOnDay) => {
  date = filterOnDay ? date : date.endOf('month')
  balance.deposits = 0
  balance.withdrawals = 0
  balance.adjustments = 0
  balance.rate = (balance.balanceRate * 1)
  balance.accruedFeeMtd = 0
  balance.feeReceived = 0
  balance.feeAdjustments = 0

  const numberOfDaysInAYear = isLeapYear(date.year()) ? 366 : 365

  const dailyRate = Math.pow(1 + balance.rate, 1 / numberOfDaysInAYear) - 1

  const premiumRate = balance.latestPremiumRate * 1
  const feeRate = premiumRate > 0 ? premiumRate : 0
  const dailyFeeRate = Math.pow(1 + feeRate, 1 / numberOfDaysInAYear) - 1

  const day = date.date()

  const beginningBookValue = balance.endingBookValue
  for (let i = 0; i < day; i++) {
    balance.beginningBookValue = balance.endingBookValue * 1
    balance.beginningFee = balance.endingFee * 1
    balance.interest = dailyRate * (balance.beginningBookValue + balance.deposits + balance.withdrawals + balance.adjustments)
    balance.endingBookValue = balance.beginningBookValue + balance.deposits + balance.withdrawals + balance.adjustments + balance.interest
    balance.accruedFee = dailyFeeRate * (balance.beginningBookValue + balance.deposits + balance.withdrawals + balance.adjustments + balance.interest)
    balance.accruedFeeMtd = balance.accruedFeeMtd + balance.accruedFee
    balance.endingFee = balance.endingFee - balance.feeReceived + balance.feeAdjustments + balance.accruedFee
  }

  if (!filterOnDay) {
    balance.beginningBookValue = beginningBookValue
  }

  return Promise.resolve()
}
