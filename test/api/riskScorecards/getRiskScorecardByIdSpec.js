const proxyquire = require('../../proxyquire')
const THE_ID = 8675309

describe('get risk scorecard by id api', () => {
  const scorecard = { riskScorecardId: 'aId', name: 'a' }
  const request = { headers: 'abc', params: {id: THE_ID} }

  let response, target, responseJson, getAdjustedRiskScorecardById

  beforeEach(() => {
    responseJson = sinon.spy()
    response = {
      status: sinon.stub().withArgs(200).returns({ json: responseJson })
    }
    getAdjustedRiskScorecardById = sinon.stub().withArgs(THE_ID).resolves(scorecard)
    target = proxyquire('api/riskScorecards/getRiskScorecardByIdApi', {
      './getAdjustedRiskScorecardById': getAdjustedRiskScorecardById
    })
  })

  it('sends get result to response', async () => {
    const result = await target(request, response)
    responseJson.should.have.been.calledWithExactly(scorecard)
  })

  it('throws error if not found', async () => {
    getAdjustedRiskScorecardById.resolves(null)
    try {
      await target(request, response)
    } catch (err) {
      err.message.should.eql('Requested resource does not exist.')
    }
  })
})
