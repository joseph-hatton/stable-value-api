require('require-sql')
const sql = require('./getContractsWithEditHistories.sql')
const omit = require('lodash/omit')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')
const applySchemaConverters = require('../db/converters/applySchemaConverters')
const contractEditHistorySchema = require('../contractEditHistories/contractEditHistorySchema')

const uniqueRowProperty = 'contractId'

const buildNewTopLevelObjectFromRow = (row) => {
  const editHistoryFields = Object.keys(row).filter((row) => row.startsWith('ceh'))
  return Object.assign({editHistories: [], derivativeTypes: []}, omit(row, editHistoryFields))
}

const appendChildToTopLevel = (topLevel, row) => {
  if (row.cehId) {
    const matchingEditHistories = topLevel.editHistories.filter((editHistory) => editHistory.contractEditHistoryId === row.cehId * 1)
    if (matchingEditHistories.length === 0) {
      topLevel.editHistories.push(applySchemaConverters(contractEditHistorySchema, {
        contractEditHistoryId: row.cehId,
        version: row.cehVersion,
        effectiveDate: row.cehEffectiveDate,
        createdDate: row.cehCreatedDate,
        createdId: row.cehCreatedId,
        modifiedDate: row.cehModifiedDate,
        modifiedId: row.cehModifiedId,
        jurisdiction: row.cehJurisdiction,
        premiumRate: row.cehPremiumRate,
        managerFeeRate: row.cehManagerFeeRate,
        advisorFeeRate: row.cehAdvisorFeeRate,
        creditingRateCalcMethod: row.cehCreditingRateCalcMethod,
        assignmentState: row.cehAssignmentState,
        legalFormName: row.cehLegalFormName,
        managerId: row.cehManagerId,
        fundLevelUtilization: row.cehFundLevelUtilization,
        planLevelUtilization: row.cehPlanLevelUtilization,
        fundActiveRate: row.cehFundActiveRate,
        fundSeniorRate: row.cehFundSeniorRate,
        spRating: row.cehSpRating,
        moodyRating: row.cehMoodyRating,
        fitchRating: row.cehFitchRating,
        overallRating: row.cehOverallRating,
        accountDurationLimit: row.cehAccountDurationLimit
      }))
    }
  }

  if (row.derivativeTypeId) {
    const matchingDerivates = topLevel.derivativeTypes.filter((derivativeType) => derivativeType.derivativeTypeId === row.derivativeTypeId)
    if (matchingDerivates.length === 0) {
      topLevel.derivativeTypes.push({derivativeTypeId: row.derivativeTypeId, description: row.description})
    }
  }
}

module.exports = {
  sql,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
