
const target = require('../../../../api/db/converters/numberConverter')

describe('number converter', () => {
  it('to db', () => target.toDb(1).should.eql(1))

  it('from db', () => target.fromDb('1').should.eql(1))
})
