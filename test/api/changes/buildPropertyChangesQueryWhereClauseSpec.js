const target = require('../../../api/changes/buildPropertyChangesQueryWhereClause')
const {schema} = require('../../data/testSchema')

const nullPagination = {pageNumber: null, pageSize: null}
const DEFAULT_STATE = { bindParams: {}, whereClause: '', pagination: nullPagination }

describe('build property changes query where clause', () => {
  it('handles empty schema and req', () => target(schema, {}).should.eql(DEFAULT_STATE))

  it('handles id params parameter', () => {
    return target(schema, {params: {id: 'hi'}}).should.eql({ bindParams: {id: 'hi'}, whereClause: `WHERE ${schema.table}.change_id = :id`, pagination: nullPagination })
  })

  it('ignores other query parameters', () => {
    return target(schema, {path: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
