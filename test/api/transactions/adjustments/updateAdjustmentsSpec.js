const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/transactions/transactionSchema')

const userId = 'somebody'

describe('update adjustments', () => {
  let target, dbTransaction, createAndInsertFeeAdjustmentTransaction, updateFeeReceiptTransactionWithAdjustmentTransactionId, updateExistingFeeAdjustmentTransaction, deleteExistingAdjustmentTransaction
  beforeEach(() => {
    createAndInsertFeeAdjustmentTransaction = sinon.stub()
    updateFeeReceiptTransactionWithAdjustmentTransactionId = sinon.stub()
    updateExistingFeeAdjustmentTransaction = sinon.stub()
    deleteExistingAdjustmentTransaction = sinon.stub()
    target = proxyquire('api/transactions/adjustments/updateAdjustments', {
      './createAndInsertFeeAdjustmentTransaction': createAndInsertFeeAdjustmentTransaction,
      './updateFeeReceiptTransactionWithAdjustmentTransactionId': updateFeeReceiptTransactionWithAdjustmentTransactionId,
      './updateExistingFeeAdjustmentTransaction': updateExistingFeeAdjustmentTransaction,
      './deleteExistingAdjustmentTransaction': deleteExistingAdjustmentTransaction
    })
  })

  describe('with an adjustment transaction id', () => {
    beforeEach(() => {
      dbTransaction = {
        adjustmentTransactionId: 5,
        contractId: 88,
        effectiveDate: 'yesterday'
      }
    })

    it('deletes existing adjustment transactions if totalAmount is 0', async () => {
      await target(dbTransaction, {feeDistributions: []}, userId)
      deleteExistingAdjustmentTransaction.should.have.been.calledWithExactly(dbTransaction, userId)
    })

    it('updates existing fee adjustment transactin if there is one', async () => {
      const clientBody = {feeDistributions: [{adjustment: 11}]}
      await target(dbTransaction, clientBody, userId)
      updateExistingFeeAdjustmentTransaction.should.have.been.calledWithExactly(dbTransaction.adjustmentTransactionId, clientBody, 11, userId)
    })
  })

  describe('with no adjustment transaction id', () => {
    beforeEach(() => {
      dbTransaction = {
        transactionId: 7,
        contractId: 88,
        effectiveDate: 'yesterday'
      }
    })

    it('updates existing fee adjustment transactin if there is one', async () => {
      const clientBody = {feeDistributions: [{adjustment: 11}]}
      createAndInsertFeeAdjustmentTransaction.withArgs(dbTransaction, 11, userId).resolves({transactionId: 8000})
      await target(dbTransaction, clientBody, userId)
      updateFeeReceiptTransactionWithAdjustmentTransactionId.should.have.been.calledWithExactly(dbTransaction.transactionId, 8000, userId)
    })
  })
})
