const dbExecuteOne = require('./dbExecuteOne')
const buildInsertQuery = require('../queries/buildInsertQuery')

module.exports = (connection, schema, insertBindParams) =>
  dbExecuteOne(connection, buildInsertQuery(schema, insertBindParams))
