module.exports = {
  toDb: (something) => (something) ? parseFloat((parseFloat(something) / 100).toFixed(4)) : something,
  fromDb: (something) => (something) ? parseFloat((parseFloat(something) * 100).toFixed(4)) : something
}
