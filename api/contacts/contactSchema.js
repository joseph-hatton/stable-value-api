const omit = require('lodash/omit')
const {dbSchemaName} = require('../config')
const overrideGetContactsSelect = require('./overrideGetContactsSelect')
const {createdDate, createdId, modifiedDate, modifiedId} = require('../common/auditColumns')
const {booleanConverter, numberConverter} = require('../db/converters')
const stateValidator = require('../states/stateValidator')
const contactTypeValidator = require('./contactTypeValidator')

const table = 'contact'
const tableId = 'contact_id'
const sequence = `${dbSchemaName}.contact_seq.NEXTVAL`

const falseDefault = () => false
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'contact.last_name, contact.first_name, contact.contact_id',
  columns: [
    {name: 'contactId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'version', defaultValue: () => 0, converter: numberConverter},
    {name: 'address1', audit: true},
    {name: 'address2', audit: true}, // NULLABLE
    {name: 'city', audit: true},
    {name: 'state', audit: true, validators: [stateValidator]},
    {name: 'zipCode', column: 'zip_code', audit: true},
    {name: 'companyId', column: 'company_id', audit: true, converter: numberConverter},
    {name: 'receiveInvoice', column: 'receive_invoice', defaultValue: falseDefault, converter: booleanConverter, audit: true},
    {name: 'receiveStatement', column: 'receive_statement', defaultValue: falseDefault, converter: booleanConverter, audit: true},
    {name: 'receiveScheduleA', column: 'receive_schedule_a', defaultValue: falseDefault, converter: booleanConverter, audit: true},
    {name: 'contactType', column: 'contact_type', audit: true, validators: [contactTypeValidator]},
    {name: 'emailAddress', column: 'email_address', audit: true},
    {name: 'fax1', audit: true}, // NULLABLE
    {name: 'cellNumber', column: 'cell_number', audit: true}, // NULLABLE
    {name: 'firstName', column: 'first_name', audit: true}, // NULLABLE
    {name: 'lastName', column: 'last_name', audit: true},
    {name: 'middleName', column: 'middle_name', audit: true}, // NULLABLE
    {name: 'primaryPhone', column: 'primary_phone', audit: true},
    {name: 'salutation', audit: true}, // NULLABLE
    createdDate,
    createdId,
    modifiedDate,
    modifiedId
  ],
  overrideSelectClause: {
    selectOne: overrideGetContactsSelect,
    selectMany: overrideGetContactsSelect
  },
  audit: {
    objectClass: 'Contact',
    buildDescription: (contact) => (contact.firstName) ? `Name: ${contact.firstName} ${contact.lastName}` : `Name: ${contact.lastName}`
  }
}
