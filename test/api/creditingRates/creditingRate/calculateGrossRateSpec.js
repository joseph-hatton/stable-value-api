const target = require('../../../../api/creditingRates/creditingRate/calculateGrossRate')

const createArgs = (creditingRateCalcMethod, marketValue, bookValue, duration, annualEffectiveYield, creditingRateRoundDecimals) => ({creditingRateCalcMethod, marketValue, bookValue, duration, annualEffectiveYield, creditingRateRoundDecimals})

describe('calculate crediting rate gross rate', () => {
  it('compound amortization', () => {
    const grossRate = target({
      creditingRateCalcMethod: 'COMPOUND_AMORTIZATION_METHOD',
      marketValue: 200,
      bookValue: 100,
      duration: 2,
      annualEffectiveYield: 400,
      creditingRateRoundDecimals: 2
    })
    grossRate.should.eql(6.0711)
  })

  it('compound amortization test from user session', () => {
    const grossRate = target({
      creditingRateCalcMethod: 'COMPOUND_AMORTIZATION_METHOD',
      marketValue: 77777777.77,
      bookValue: 76102878.66,
      duration: 3.77,
      annualEffectiveYield: 2.77,
      creditingRateRoundDecimals: 2
    })
    grossRate.should.eql(0.0337)
  })

  it('compound amortization live test', () => {
    const grossRate = target({
      creditingRateCalcMethod: 'COMPOUND_AMORTIZATION_METHOD',
      marketValue: 108666134.66,
      bookValue: 104204796.98,
      duration: 3.32,
      annualEffectiveYield: 1.39,
      creditingRateRoundDecimals: 2
    })
    grossRate.should.eql(0.0268)

    // 3.19 net
  })

  it('book value dollar duration', () => {
    const grossRate = target({
      creditingRateCalcMethod: 'BOOK_VALUE_DOLLAR_DURATION',
      marketValue: 100,
      bookValue: 200,
      duration: 2,
      annualEffectiveYield: 400,
      creditingRateRoundDecimals: 2
    })
    grossRate.should.eql(3.75)
  })

  it('market value dollar duration', () => {
    const grossRate = target({
      creditingRateCalcMethod: 'MARKET_VALUE_DOLLAR_DURATION',
      marketValue: 100,
      bookValue: 200,
      duration: 2,
      annualEffectiveYield: 400,
      creditingRateRoundDecimals: 2
    })
    grossRate.should.eql(3.5)
  })

  it('other', () => {
    const grossRate = target({
      creditingRateCalcMethod: 'IRR',
      marketValue: 100,
      bookValue: 200,
      duration: 3,
      annualEffectiveYield: 400,
      creditingRateRoundDecimals: 2
    })
    grossRate.should.eql(0.0)
  })
})
