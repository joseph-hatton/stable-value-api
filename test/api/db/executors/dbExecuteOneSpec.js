const target = require('../../../../api/db/executors/dbExecuteOne')

describe('db execute one', () => {
  let connection, query
  beforeEach(() => {
    query = {
      sql: 'select *',
      bindParams: {someParams: true},
      executeOptions: {doStuff: 'yep'},
      mapResult: sinon.stub().returns('MAPPED_RESULT')
    }
    connection = { execute: sinon.stub().resolves('EXECUTE_RESULT') }
  })

  it('returns mapped result result', () =>
    target(connection, query)
    .then((result) => result.should.eql('MAPPED_RESULT')))

  it('passes query to execute', () =>
    target(connection, query)
    .then((result) => connection.execute.should.have.been.calledWithExactly(query.sql, query.bindParams, query.executeOptions)))

  it('map result called with', () =>
    target(connection, query)
    .then((result) => query.mapResult.should.have.been.calledWithExactly('EXECUTE_RESULT')))
})
