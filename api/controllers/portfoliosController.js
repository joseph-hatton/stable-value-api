const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const createApi = require('../common/createApi')
const getByIdApi = require('../common/getByIdApi')
const updateByIdApi = require('../common/updateByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../wrapPortfolios/portfolioSchema')
const queryWhereClauseBuilder = require('../wrapPortfolios/buildPortfolioQueryWhereClause')

module.exports = createController({
  getMany: getManyApi(schema, queryWhereClauseBuilder),
  getOne: getByIdApi(schema),
  create: createApi(schema),
  update: updateByIdApi(schema),
  delete: deleteByIdApi(schema)
})
