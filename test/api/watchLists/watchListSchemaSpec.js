const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/watchLists/watchListSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')
const overrideGetWatchListSelect = require('../../../api/watchLists/overrideGetWatchListSelect')

describe('watch list schema', () => {
  validateSchema(target, {
    tableShort: 'watch_list',
    table: 'sch_stbv.watch_list',
    tableId: 'watch_list_id',
    sequence: 'sch_stbv.watch_list_seq.NEXTVAL',
    orderBy: 'watch_list.contract_id',
    columns: [
      {name: 'watchListId', column: 'watch_list_id', sequence: 'sch_stbv.watch_list_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      version,
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId,
      {name: 'comments'}
    ],
    required: {
      new: ['watchListId', 'version', 'contractId', 'effectiveDate', 'createdDate'],
      update: ['watchListId', 'version', 'contractId', 'effectiveDate', 'createdDate']
    },
    overrideSelectClause: {
      selectOne: overrideGetWatchListSelect,
      selectMany: overrideGetWatchListSelect
    }
  })
})
