SELECT ('RGA'||to_char(MIN (n),'FM00009')) as MIN
  FROM (SELECT (ROWNUM + 50) n
              FROM DUAL
        CONNECT BY LEVEL <= 99999)
 WHERE NOT (n IN
               (SELECT TO_NUMBER (replace (contract_number, 'RGA'), '999999')
                  FROM SCH_STBV.contract c where C.CONTRACT_NUMBER like 'RGA%'))