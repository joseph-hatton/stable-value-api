const cloneDeep = require('lodash/cloneDeep')
const log = require('../../log')

const setKeyToPropertyIfNotNull = (properties, history, key) => {
  properties[key] = history[key] != null ? history[key] : properties[key]
  if (key === 'premiumRate') {
    log.info({domain: 'edit_history', id: history.contractEditHistoryId, historyEffectiveDate: history.effectiveDate, key, val: properties[key], historyKey: history[key]}, 'setting a value')
  }
}

const keysToCopy = [
  'legalFormName',
  'jurisdiction',
  'premiumRate',
  'managerFeeRate',
  'advisorFeeRate',
  'creditingRateCalcMethod',
  'managerId',
  'fundLevelUtilization',
  'planLevelUtilization',
  'fundActiveRate',
  'fundSeniorRate',
  'assignmentState',
  'spRating',
  'moodyRating',
  'fitchRating',
  'overallRating',
  'accountDurationLimit'
]
module.exports = (contract, effectiveDate) => {
  log.info({domain: 'edit_history', effectiveDate}, 'determining edit history data for effective date')

  const properties = cloneDeep(contract)

  contract.editHistories
  .filter((history) => (new Date(history.effectiveDate)).getTime() <= (new Date(effectiveDate)).getTime())
  .sort((a, b) => new Date(a.effectiveDate) - new Date(b.effectiveDate))
  .forEach((history) => {
    keysToCopy.forEach((key) => setKeyToPropertyIfNotNull(properties, history, key))
  })

  log.info({domain: 'edit_history', effectiveDate, properties}, 'determined edit history data for effective date')
  return properties
}
