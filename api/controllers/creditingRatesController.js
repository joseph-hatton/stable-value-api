const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const getByIdApi = require('../common/getByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../creditingRates/creditingRatesSchema')
const buildContractIdWhereClause = require('../contracts/buildContractIdWhereClause')
const overrideGetManyCreditingRateWithBalance = require('../creditingRates/overrideGetManyCreditingRateWithBalance')

const creditingRateSchemaWithBalance = {
  ...schema,
  overrideSelectClause: {
    selectMany: overrideGetManyCreditingRateWithBalance
  }
}

module.exports = createController({
  getManyCreditingRates: getManyApi(creditingRateSchemaWithBalance, buildContractIdWhereClause),
  getCreditingRatesForContracts: require('../creditingRates/queryCreditingRatesApi'),
  getOneCreditingRate: getByIdApi(schema),
  createMarketValue: require('../creditingRates/marketValue/createMarketValueApi'),
  updateMarketValue: require('../creditingRates/marketValue/updateMarketValueApi'),
  deleteMarketValue: require('../creditingRates/marketValue/deleteMarketValueApi'),
  calculateCreditingRate: require('../creditingRates/creditingRate/calculateCreditingRateApi'),
  updateCreditingRate: require('../creditingRates/creditingRate/updateCreditingRateApi'),
  clearCreditingRate: require('../creditingRates/creditingRate/clearCreditingRateApi'),
  overrideModifiedId: require('../creditingRates/creditingRate/overrideCreditingRateModifiedIdApi'),
  approveCreditingRate: require('../creditingRates/creditingRate/approveCreditingRateApi')
})
