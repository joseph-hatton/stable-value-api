const validateSchema = require('../validateSchema')
const target = require('../../../api/derivativeTypes/derivativeTypeSchema')

describe('derivative type schema', () => {
  validateSchema(target, {
    tableShort: 'derivative_type',
    table: 'sch_stbv.derivative_type',
    tableId: 'derivative_type_id',
    tableIdType: 'STRING',
    sequence: null,
    orderBy: 'derivative_type_id',
    columns: [
      {name: 'derivativeTypeId', column: 'derivative_type_id', pk: true, immutable: true},
      {name: 'description', column: 'description'}
    ],
    required: {
      new: ['derivativeTypeId', 'description'],
      update: ['description']
    }
  })
})
