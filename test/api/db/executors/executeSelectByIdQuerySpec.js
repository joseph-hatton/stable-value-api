/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const SELECT_QUERY = 'SELECT_QUERY'

describe('execute select by id query', () => {
  let target, dbExecuteOne, connection, buildSelectByIdQuery
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves(['ONE_EXECUTED'])
    buildSelectByIdQuery = sinon.stub().returns(SELECT_QUERY)
    target = proxyquire('api/db/executors/executeSelectByIdQuery', {
      './dbExecuteOne': dbExecuteOne,
      '../queries/buildSelectByIdQuery': buildSelectByIdQuery
    })
  })

  it('returns execute result', () =>
    target(connection, schema, ROW_ID)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute one should only be called once', () =>
    target(connection, schema, ROW_ID)
    .then(() => dbExecuteOne.callCount.should.eql(1)))

  it('execute one  should only be called with', () =>
    target(connection, schema, ROW_ID)
    .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, SELECT_QUERY)))

  it('build select query should have been called with', () =>
    target(connection, schema, ROW_ID)
    .then(() => buildSelectByIdQuery.should.have.been.calledWithExactly(schema, ROW_ID)))
})
