/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('healthcheck ping', () => {
  let getStatus, ping, res, next, log

  beforeEach(() => {
    res = mockResponse()
    next = sinon.stub()
    getStatus = sinon.stub().resolves({})
    log = {
      info: sinon.stub()
    }

    ping = proxyquire('api/health/ping', {
      './getStatus': getStatus
    })
    ping = proxyquire('api/health/ping', {
      './getStatus': getStatus,
      '../../log': log
    })
  })

  const apply = () => ping({}, res, next)

  it('responds with 500 if an error occurs', async () => {
    const error = new Error('crap')
    getStatus.resolves({error})

    await apply()

    log.info.should.have.been.calledWith(error, 'healthcheck ping failed')
    res.status.should.have.been.calledWith(500)
  })

  it('responds with 200 if the service is healthy', async () => {
    await apply()

    log.info.should.not.have.been.called
    res.status.should.not.have.been.called
    res.send.should.have.been.calledWith()
  })

  it('invokes next if something goes wrong', async () => {
    const error = new Error('!')
    getStatus.rejects(error)

    await apply()

    next.should.have.been.calledWithExactly(error)
  })
})
