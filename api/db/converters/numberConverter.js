// to fix a bug where oracledb() returns decimal points with extra numbers, all numbers are returned as strings, then we convert here

module.exports = {
  toDb: (something) => something,
  fromDb: (something) => (something) ? something * 1 : something
}
