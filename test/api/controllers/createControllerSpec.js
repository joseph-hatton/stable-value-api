const createController = require('../../../api/controllers/createController')

describe('createController', () => {
  let endpoint, controller, parameters, req, res, next

  beforeEach(() => {
    endpoint = sinon.stub()
    controller = createController({endpoint})
    parameters = [
    ]
    req = {
      swagger: {
        operation: {
          getParameters: () => parameters
        }
      }
    }
    next = sinon.stub()
    res = {}
  })

  const apply = () => controller.endpoint(req, res, next)

  const createParameter = (inTarget, name, value) => ({
    in: inTarget,
    name,
    getValue: sinon.stub().returns({value})
  })

  it('sets path params as params', () => {
    parameters = [createParameter('path', 'id', 42)]
    apply()

    req.params.should.eql({id: 42})
    parameters[0].getValue.should.have.been.calledWithExactly(req)
  })

  it('sets query params as query', () => {
    parameters = [createParameter('query', 'q', 'foobar')]
    apply()

    req.query.should.eql({q: 'foobar'})
  })

  it('does nothing if operation is missing', () => {
    delete req.swagger.operation
    apply()
  })

  it('does nothing if swagger object is missing', () => {
    delete req.swagger
    apply()
  })
})
