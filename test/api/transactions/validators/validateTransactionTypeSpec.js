const target = require('../../../../api/transactions/validators/validateTransactionType')

describe('validate transaction type', () => {
  const validCases = [
    'DEPOSIT',
    'WITHDRAWAL',
    'ADJUSTMENT',
    'FEE_RECEIPT',
    'FEE_ADJUSTMENT',
    'TERMINATION'
  ]

  describe('test valid cases', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Transaction Type')
   })
})
