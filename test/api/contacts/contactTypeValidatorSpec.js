const target = require('../../../api/contacts/contactTypeValidator')

describe('contact type validator', () => {
  const validCases = [
    'ADVISOR', 'CONSULTANT', 'CUSTODIAN', 'INTERMEDIARY', 'INVESTMENT_MANAGER', 'LEGAL', 'MARKETING',
    'OPERATIONS', 'OTHER', 'OWNER', 'PLAN_SPONSOR', 'PROVIDER', 'RECORD_KEEPER', 'RISK', 'TRUSTEE'
  ]

  describe('test valid contact types', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Contact Type')
   })
})
