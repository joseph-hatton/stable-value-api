const target = require('../../../api/contractEditHistories/determineEditHistoryDataForEffectiveDate')

const date0 = '2017-09-25T00:00:00.000Z'
const date1 = '2017-10-25T00:00:00.000Z'
const date2 = '2017-11-25T00:00:00.000Z'

const createEditHistory = (effectiveDateString, params) => (Object.assign({
  effectiveDate: effectiveDateString,
  legalFormName: null,
  jurisdiction: null,
  premiumRate: null,
  managerFeeRate: null,
  advisorFeeRate: null,
  creditingRateCalcMethod: null,
  managerId: null,
  fundLevelUtilization: null,
  planLevelUtilization: null,
  fundActiveRate: null,
  fundSeniorRate: null,
  assignmentState: null,
  spRating: null,
  moodyRating: null,
  fitchRating: null,
  overallRating: null,
  accountDurationLimit: null
}, params))

const eh1 = createEditHistory(date1, {legalFormName: 'some form'})
const eh2 = createEditHistory(date2, {legalFormName: 'some other form', premiumRate: 5.2})

describe('determine edit history data for effective date', () => {
  it('finds form name with same effective date', () => {
    const result = target({editHistories: [eh1]}, date1)

    result.legalFormName.should.eql('some form')
  })

  it('does not find form name with earlier effective date', () => {
    const result = target({editHistories: [eh1]}, date0)

    should.not.exist(result.legalFormName)
  })

  it('finds a newer form name', () => {
    const result = target({editHistories: [eh1, eh2]}, date2)

    result.legalFormName.should.eql('some other form')
  })

  it('finds a only premium rate', () => {
    const result = target({editHistories: [eh1, eh2]}, date2)

    result.premiumRate.should.eql(5.2)
  })

  it('dates sorting works', () => {
    const editHistories = [
      createEditHistory('2017-12-01T00:00:00.000Z', {premiumRate: 18}),
      createEditHistory('2013-05-10T00:00:00.000Z', {premiumRate: 21}),
      createEditHistory('2016-01-31T00:00:00.000Z'),
      createEditHistory('2016-03-31T00:00:00.000Z'),
      createEditHistory('2016-07-01T00:00:00.000Z', {premiumRate: 20}),
      createEditHistory('2017-03-31T00:00:00.000Z'),
      createEditHistory('2017-07-25T00:00:00.000Z')
    ]

    const result = target({editHistories}, '2017-12-02T00:00:00.000Z')

    result.premiumRate.should.eql(18)
  })
})
