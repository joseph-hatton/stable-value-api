const moment = require('moment')
const createError = require('http-errors')
const log = require('../../../log')
const getUserId = require('../../getUserId')
const contractSchema = require('../../contracts/contractSchema')
const deleteByIdFromDb = require('../../db/deleteByIdFromDb')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const schema = require('../creditingRatesSchema')

const canDeleteMarketValue = (contract, creditingRate) => {
  const isActiveContract = contract && contract.status === 'ACTIVE'
  const isCreditingRateNotApprovedOrCurrentMonth = creditingRate
    && (creditingRate.status !== 'APPROVED' || moment(creditingRate.effectiveDate).utc().isSame(moment(), 'month'))
  const deletable = isActiveContract
    && isCreditingRateNotApprovedOrCurrentMonth

  log.info({domain: schema.tableShort, contract, creditingRate, deletable}, 'can we delete market value')
  return deletable
}

module.exports = async ({params: {contractId, id}, headers}, res) => {
  const userId = getUserId(headers)
  log.info({domain: schema.tableShort, contractId, id: id, userId}, 'delete market value by id api')

  const dbContract = await selectByIdFromDb(contractSchema, contractId)
  const dbCreditingRate = await selectByIdFromDb(schema, id)

  if (canDeleteMarketValue(dbContract, dbCreditingRate)) {
    const result = await deleteByIdFromDb(schema, id, userId)
    res.status(200).json(result)
  } else {
    throw createError.BadRequest('Forecasted, Awaiting Calc, Pending or Crediting Rates approved in current open month can be deleted.')
  }
}
