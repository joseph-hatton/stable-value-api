/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

const userId = 'a user'
const contractId = 5

describe('reactivate contract', () => {
  let target, transitionContractInWorkflow
  beforeEach(() => {
    transitionContractInWorkflow = sinon.stub().returns('base returned')
    target = proxyquire('api/contracts/workflow/reactivateContract', {
      './transitionContractInWorkflow': transitionContractInWorkflow
    })
  })

  it('returns the base result', () => {
    target.should.eql('base returned')
  })

  it('base should have been called once', () => {
    transitionContractInWorkflow.should.have.been.called
  })

  it('sets action verb', () => {
    transitionContractInWorkflow.getCall(0).args[0].actionVerb.should.eql('reactivate')
  })

  it('sets next contract status', () => {
    transitionContractInWorkflow.getCall(0).args[0].nextContractStatus.should.eql('PENDING')
  })

  it('does not set a callBeforeChangingState', () => {
    should.not.exist(transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState)
  })

  describe('validate can transition', () => {
    let asyncValidate
    beforeEach(() => {
      asyncValidate = transitionContractInWorkflow.getCall(0).args[0].validateCanTransition
    })

    it('fails with ACTIVE contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'ACTIVE'}})

      result.should.eql('Only INACTIVE Contracts may be reactivated.')
    })

    it('passes with INACTIVE contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'INACTIVE'}})

      should.not.exist(result)
    })
  })
})
