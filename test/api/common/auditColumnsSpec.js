const auditColumns = require('../../../api/common/auditColumns')

describe('audit columns', () => {
  it('should be 5',
    () => Object.keys(auditColumns).length.should.eql(5))

  it('should contain createdDated',
    () => auditColumns.should.have.property('createdDate'))

  it('should contain createdId',
    () => auditColumns.should.have.property('createdId'))

  it('should contain modifiedDate',
    () => auditColumns.should.have.property('modifiedDate'))

  it('should contain modifiedId',
    () => auditColumns.should.have.property('modifiedId'))

  it('should contain version',
    () => auditColumns.should.have.property('version'))
})
