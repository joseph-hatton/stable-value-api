const target = require('../../../api/riskScorecards/adjustRiskScorecards')
const riskLookups = require('./riskLookupsHardCodedFromDb.json')

describe('adjust risk scorecards', () => {
  let riskScoreCards, adjustedRiskScorecards

  const testScoreWithGrade = (fieldName, score, grade) => {
    it(`${fieldName} should exist `, () => adjustedRiskScorecards[0][fieldName].should.exist)
    it(`${fieldName} score should equal`, () => adjustedRiskScorecards[0][fieldName].score.should.eql(score))
    it(`${fieldName} grade should equal`, () => adjustedRiskScorecards[0][fieldName].grade.should.eql(grade))
  }

  const testAdjustedGrade = (fieldName, score, weightPercentage, adjustedWeightPercentage, grade, comments = null) => {
    testScoreWithGrade(fieldName, score, grade)
    it(`${fieldName} categoryWeight should equal`, () => adjustedRiskScorecards[0][fieldName].categoryWeight.should.eql(weightPercentage))
    it(`${fieldName} adjustedWeight toFixed(1) should equal`, () => adjustedRiskScorecards[0][fieldName].adjustedWeight.toFixed(1).should.eql(adjustedWeightPercentage.toFixed(1)))
    if (comments) {
      it(`${fieldName} comments should equal`, () => adjustedRiskScorecards[0][fieldName].comments.should.eql(comments))
    } else {
      it(`${fieldName} comments should be null`, () => should.not.exist(adjustedRiskScorecards[0][fieldName].comments))
    }
  }

  describe('with first row from contract 530', () => {
    beforeEach(() => {
      riskScoreCards = require('./riskScorecardsFor459.json')
      adjustedRiskScorecards = target(riskLookups, [riskScoreCards[0]])
    })
    testAdjustedGrade('cashFlow', 94.0, 20.0, 17.1, 'A', 'migration data')
    testAdjustedGrade('demographics', 91.0, 20.0, 22.9, 'A-')
    testAdjustedGrade('competingFunds', 95.0, 10.0, 8.6, 'A')
    testAdjustedGrade('planSponsor', 95.0, 10.0, 8.6, 'A')
    testAdjustedGrade('planSvFundBalances', 100.0, 5.0, 2.9, 'A+')
    testAdjustedGrade('planDesign', 95.0, 10.0, 8.6, 'A')
    testAdjustedGrade('fundManagement', 95.0, 10.0, 8.6, 'A')
    testAdjustedGrade('wrappedPortfolio', 97.0, 10.0, 5.7, 'A+')
    testAdjustedGrade('marketBookRatio', 86.0, 5.0, 17.1, 'B')
    testScoreWithGrade('overallUnadjusted', 94.0, 'A')
    testScoreWithGrade('overallAdjusted', 92.6, 'A-')
  })

  describe('with second row from contract 530', () => {
    beforeEach(() => {
      riskScoreCards = require('./riskScorecardsFor459.json')
      adjustedRiskScorecards = target(riskLookups, [riskScoreCards[1]])
    })
    testAdjustedGrade('cashFlow', 95.0, 20.0, 18.2, 'A', 'migration data')
    testAdjustedGrade('demographics', 93.0, 20.0, 18.2, 'A')
    testAdjustedGrade('competingFunds', 95.0, 10.0, 9.1, 'A')
    testAdjustedGrade('planSponsor', 95.0, 10.0, 9.1, 'A')
    testAdjustedGrade('planSvFundBalances', 97.0, 5.0, 3.0, 'A+')
    testAdjustedGrade('planDesign', 95.0, 10.0, 9.1, 'A')
    testAdjustedGrade('fundManagement', 95.0, 10.0, 9.1, 'A')
    testAdjustedGrade('wrappedPortfolio', 99.0, 10.0, 6.1, 'A+')
    testAdjustedGrade('marketBookRatio', 84.0, 5.0, 18.2, 'B')
    testScoreWithGrade('overallUnadjusted', 94.5, 'A')
    testScoreWithGrade('overallAdjusted', 92.9, 'A-')
  })
})
