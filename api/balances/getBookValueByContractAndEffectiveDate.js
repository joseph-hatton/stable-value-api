const log = require('../../log')
const dbExecutePromise = require('../db/executors/dbExecutePromise')
const dbExecuteOne = require('../db/executors/dbExecuteOne')

const schema = require('./balancesSchema')

const bookValueQuery = `select ending_book_value from ${schema.table} where contract_id = :contractId and effective_date <= :effectiveDate and rownum = 1 order by effective_date desc`

module.exports = async (contractId, effectiveDate) => {
  log.info({
    domain: schema.tableShort,
    id: contractId,
    effectiveDate
  }, 'get book value by contract id and an effective date')

  const dbRequest = {
    sql: bookValueQuery,
    bindParams: {
      contractId,
      effectiveDate
    },
    mapResult: result => result.rows && result.rows.length > 0 ? result.rows[0]['ENDING_BOOK_VALUE'] : null,
    executeOptions: {}
  }

  return await dbExecutePromise(connection => dbExecuteOne(connection, dbRequest))
}
