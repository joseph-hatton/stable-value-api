const moment = require('moment')
const log = require('../../../log')
const schema = require('../creditingRatesSchema')
const getLatestCycle = require('../../monthlyCycles/getLatestCycle')
const deleteForecasts = require('./deleteForecasts')
const calculateAndSaveCreditingRateForecast = require('./calculateAndSaveCreditingRateForecast')

module.exports = async (contract, userId) => {
  const contractId = contract.contractId
  log.info({domain: schema.tableShort, id: contractId, userId}, 'recalculate crediting rate forecasts')

  const latestCycle = await getLatestCycle()

  const firstDayOfMonthOfStartDate = moment(latestCycle.startDate).utc().startOf('day').startOf('month').toISOString()

  log.info({domain: schema.tableShort, id: contractId, userId, firstDayOfMonthOfStartDate}, 'recalculate crediting rate forecasts by date')

  await deleteForecasts(contractId, firstDayOfMonthOfStartDate, userId)

  await calculateAndSaveCreditingRateForecast(contract, firstDayOfMonthOfStartDate)
}
