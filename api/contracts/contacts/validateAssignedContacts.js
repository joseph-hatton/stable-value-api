const log = require('../.././../log')
const selectFromDb = require('../../db/selectFromDb')
const schema = require('./contactContractSchema')

const getContractContactsByContractId = async (contractId) =>
 await selectFromDb(schema, {bindParams: {contractId}, whereClause: `WHERE ${schema.table}.contract_id = :contractId`})

const validateContacts = async (contactContracts) => {
  console.log(contactContracts, 'WWWW')
  const errors = []
  if (contactContracts.length > 0) {
    const receivesScheduleA = contactContracts.filter((contact) => contact.receiveScheduleA)
    const receivesStatement = contactContracts.filter((contact) => contact.receiveStatement)
    const receivesInvoice = contactContracts.filter((contact) => contact.receiveInvoice)
    const investmentManagers = contactContracts.filter((contact) => contact.contactType === 'INVESTMENT_MANAGER')
    const planSponsors = contactContracts.filter((contact) => contact.contactType === 'PLAN_SPONSOR')
    if (receivesScheduleA.length === 0 || receivesStatement.length === 0 || receivesInvoice.length === 0) {
      errors.push('Contract should be assigned at least 1 Contact to receive each report.')
    }
    if (planSponsors.length === 0 || investmentManagers.length === 0) {
      errors.push('Contract should be assigned a Plan Sponsor and an Investment Manager.')
    }
  }
  console.log(errors)
    return errors
  }
const validateCurrentlyAssigned = async (contractId) => {
  log.info({domain: schema.tableShort, id: contractId}, 'validate assigned contacts')
  const contactContracts = await getContractContactsByContractId(contractId)
  return validateContacts(contactContracts)
}
module.exports = {
  getContractContactsByContractId: getContractContactsByContractId,
  validateContractContacts: validateContacts,
  validateCurrentlyAssigned: validateCurrentlyAssigned
}
