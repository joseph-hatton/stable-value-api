const proxyquire = require('../../proxyquire')
const schema = {
  table: 'atable',
  tableId: 'id',
  orderBy: 'name'
}

describe('get many api', () => {
  const companies = [{ id: 'aId', name: 'a' }, { id: 'bId', name: 'b' }]
  const request = { headers: 'abc' }

  let response, target, next, getUserId, responseStatusSend, selectFromDbWithPagination

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    response = {
      json: sinon.spy(),
      status: sinon.stub().returns({ send: responseStatusSend })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    selectFromDbWithPagination = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)

    target = proxyquire('api/common/getManyApi', {
      '../getUserId': getUserId,
      '../db/selectFromDbWithPagination': selectFromDbWithPagination
    })
  })

  it('with valid user', async () => {
    getUserId.withArgs(request.headers).returns('joe')

    const result = await target(schema)(request, response, next)
    response.json.should.have.been.calledWithExactly(companies)
  })

  it('with custom schema mapper', async () => {
    getUserId.withArgs(request.headers).returns('joe')

    const buildWhereClauseAndParams = (tableSchema, req) => ({hi: 'there'})

    await target(schema, buildWhereClauseAndParams)(request, response, next)
    selectFromDbWithPagination.should.have.been.calledWithExactly(schema, {hi: 'there'})
  })

  it('with pagination info', async () => {
    getUserId.withArgs(request.headers).returns('joe')

    const buildWhereClauseAndParams = (tableSchema, req) => ({ hi: 'there', pagination: {pageNumber: 8, pageSize: 10} })

    const result = await target(schema, buildWhereClauseAndParams)(request, response, next)
    response.json.should.have.been.calledWithExactly(companies)
  })
})
