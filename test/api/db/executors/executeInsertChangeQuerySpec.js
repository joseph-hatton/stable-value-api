/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const USER_ID = 'person'
const OBJECT_BEING_CHANGED = {before: true}
const INSERT_CHANGE_QUERY = 'INSERT_CHANGE_QUERY'

describe('execute insert change query', () => {
  let target, dbExecuteOne, connection, buildInsertChangeQuery
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves('ONE_EXECUTED')
    buildInsertChangeQuery = sinon.stub().returns(INSERT_CHANGE_QUERY)
    target = proxyquire('api/db/executors/executeInsertChangeQuery', {
      './dbExecuteOne': dbExecuteOne,
      '../queries/buildInsertChangeQuery': buildInsertChangeQuery
    })
  })

  it('returns execute result', () =>
    target(connection, schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'CREATE')
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute one should only be called once', () =>
    target(connection, schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'CREATE')
    .then(() => dbExecuteOne.callCount.should.eql(1)))

  it('execute one  should only be called with', () =>
    target(connection, schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'CREATE')
    .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, INSERT_CHANGE_QUERY)))

  it('build insert change query should have been called with', () =>
    target(connection, schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'CREATE')
    .then(() => buildInsertChangeQuery.should.have.been.calledWithExactly(schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'CREATE')))

  it('build insert change query with different operation should have been called with', () =>
    target(connection, schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'DIFFERENT')
    .then(() => buildInsertChangeQuery.should.have.been.calledWithExactly(schema, ROW_ID, USER_ID, OBJECT_BEING_CHANGED, 'DIFFERENT')))
})
