/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const BIND_PARAMS = {binding: true}
const UPDATE_QUERY = 'UPDATE_QUERY'

describe('execute update query', () => {
  let target, dbExecuteOne, connection, buildUpdateQuery
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves('ONE_EXECUTED')
    buildUpdateQuery = sinon.stub().returns(UPDATE_QUERY)
    target = proxyquire('api/db/executors/executeUpdateQuery', {
      './dbExecuteOne': dbExecuteOne,
      '../queries/buildUpdateQuery': buildUpdateQuery
    })
  })

  it('returns execute result', () =>
    target(connection, schema, BIND_PARAMS, ROW_ID)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute one should only be called once', () =>
    target(connection, schema, BIND_PARAMS, ROW_ID)
    .then(() => dbExecuteOne.callCount.should.eql(1)))

  it('execute one should only be called with', () =>
    target(connection, schema, BIND_PARAMS, ROW_ID)
    .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, UPDATE_QUERY)))

  it('build update query should have been called with', () =>
    target(connection, schema, BIND_PARAMS, ROW_ID)
    .then(() => buildUpdateQuery.should.have.been.calledWithExactly(schema, BIND_PARAMS, ROW_ID)))
})
