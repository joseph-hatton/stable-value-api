const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {numberConverter, percentConverter} = require('../db/converters')

const table = 'risk_lookup'
const tableId = 'risk_lookup_id'
const sequence = null

const zeroDefault = () => 0

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: `${table}.${tableId}`,
  columns: [
    {name: 'riskLookupId', column: tableId, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'scoreType', column: 'score_type'},
    {name: 'description'},
    {name: 'lowLimit', column: 'low_limit', converter: numberConverter},
    {name: 'highLimit', column: 'high_limit', converter: numberConverter},
    {name: 'singlePlanWeight', column: 'single_plan_weight', converter: percentConverter},
    {name: 'pooledPlanWeight', column: 'pooled_plan_weight', converter: percentConverter},
    {name: 'savings529PlanWeight', column: 'savings_529_plan_weight', converter: percentConverter}
  ],
  required: {
    new: ['riskLookupId', 'scoreType', 'description', 'singlePlanWeight', 'pooledPlanWeight', 'savings529PlanWeight'],
    update: []
  }
}
