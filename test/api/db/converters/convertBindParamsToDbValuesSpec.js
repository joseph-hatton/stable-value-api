const convert = require('../../../../api/db/converters/convertBindParamsToDbValues')

describe('convert bind params to db values', () => {
  let bindParams, columns

  beforeEach(() => {
    bindParams = null
    columns = [
      {name: 'aColumn'},
      {name: 'bColumn'},
      {name: 'cColumn', converter: { toDb: (value) => `${value}_converted` }}
    ]
  })

  it('returns empty when passed empty', () => {
    bindParams = {}
    convert(columns, bindParams).should.eql(bindParams)
  })

  it('returns bindParams when no converter found', () => {
    bindParams = {aColumn: 'value one', bColumn: 'value two'}
    convert(columns, bindParams).should.eql(bindParams)
  })

  it('returns modified bindParams when converter found', () => {
    bindParams = {aColumn: 'value one', cColumn: 'value two'}
    convert(columns, bindParams).should.eql({aColumn: 'value one', cColumn: 'value two_converted'})
  })

  it('converter is not hard coded', () => {
    columns[2].converter.toDb = (value) => 'a different value'
    bindParams = {aColumn: 'value one', cColumn: 'value two'}
    convert(columns, bindParams).should.eql({aColumn: 'value one', cColumn: 'a different value'})
  })
})
