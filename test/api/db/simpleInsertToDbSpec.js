/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'

describe('simple insert to db', () => {
  let target, connection, dbExecutePromise, dbExecuteOne, buildInsertQuery, mapResult

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    dbExecuteOne = sinon.stub().resolves('yay')
    buildInsertQuery = sinon.stub().returns('simple insert result')
    connection = sinon.spy()
    mapResult = sinon.stub().returns('mapped')
    target = proxyquire('api/db/simpleInsertToDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/dbExecuteOne': dbExecuteOne,
      './queries/buildInsertQuery': buildInsertQuery
    })
  })

  const executeTarget = () => {
    return target('some sql', mapResult)
    .then((result) =>
      dbExecutePromise.getCall(0).args[0](connection)
      .then(() => result))
  }

  describe('execute promise', () => {
    it('called once', () =>
      executeTarget()
      .then(() => dbExecutePromise.callCount.should.eql(1)))

    it('returns result', () =>
      executeTarget()
      .then((result) => result.should.eql(DB_EXECUTE_PROMISE_RESULT)))
  })

  describe('execute one', () => {
    it('called once', () =>
      executeTarget()
      .then(() => dbExecuteOne.callCount.should.eql(1)))

    it('called with connection', () =>
      executeTarget()
      .then(() => dbExecuteOne.getCall(0).args[0].should.eql(connection)))

    it('called with connection', () =>
      executeTarget()
      .then(() => dbExecuteOne.getCall(0).args[1].should.eql('simple insert result')))
  })

  describe('build simple sql', () => {
    it('called once', () =>
      executeTarget()
      .then(() => buildInsertQuery.callCount.should.eql(1)))

    it('called with sql', () =>
      executeTarget()
      .then(() => buildInsertQuery.getCall(0).args[0].should.eql('some sql')))

    it('called with connection', () =>
      executeTarget()
      .then(() => buildInsertQuery.getCall(0).args[1].should.eql(mapResult)))
  })
})
