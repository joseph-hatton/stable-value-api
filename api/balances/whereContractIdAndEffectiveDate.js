const moment = require('moment')
const log = require('../../log')
const schema = require('./balancesSchema')

module.exports = (contractId, {effectiveDate = null} = {}) => {
  log.info({domain: schema.tableShort, id: contractId, effectiveDate}, 'build balances by contract id and optionally an effective date where clause')

  let whereClauseBits = [`${schema.table}.contract_id = :id`]
  const bindParams = { id: contractId }

  if (effectiveDate) {
    whereClauseBits.push(`${schema.table}.effective_date = :effectiveDate`)
    bindParams.effectiveDate = moment(effectiveDate).utc().startOf('day').toDate()
  }
  const whereClause = `WHERE ${whereClauseBits.join(' AND ')}`
  return { whereClause, bindParams }
}
