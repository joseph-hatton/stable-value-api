const db = require('../../db')
const convertBindParamsToDbValues = require('../converters/convertBindParamsToDbValues')

const buildInsertValues = (columns) =>
  columns
    .map((column) => (column.sequence) ? column.sequence : `:${column.name}`)
    .join(', ')

module.exports = (schema, bindParams, byId = true) => {
  const bindParamsToUse = (byId)
    ? Object.assign({id: {type: (schema.tableIdType) ? db.TYPES[schema.tableIdType] : db.TYPES.NUMBER, dir: db.BIND_OUT}}, convertBindParamsToDbValues(schema.columns, bindParams))
    : convertBindParamsToDbValues(schema.columns, bindParams)
  const intoIdSql = (byId) ? `RETURNING ${schema.tableId}  INTO :id` : ''
  return {
    sql: `INSERT INTO ${schema.table} VALUES (${buildInsertValues(schema.columns)}) ${intoIdSql}`,
    bindParams: bindParamsToUse,
    executeOptions: { autoCommit: true },
    mapResult: (result) => {
      if (result && result.error) {
        throw new Error(result.error)
      } else {
        return (byId) ? { id: result.outBinds.id[0] } : result
      }
    }
  }
}
