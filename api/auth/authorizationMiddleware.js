const log = require('../../log')
const {responses: {unauthorized}, isDeployed} = require('../config')
const getUserId = require('../getUserId')
const isAuthorized = require('./isAuthorized')
const ENTITLEMENTS = require('./entitlements')
const _ = require('lodash')
const mem = require('mem')
const determineEntitlements = require('./determineEntitlements')

const wrappedDetermineEntitlements = mem(determineEntitlements, {maxAge: 30 * 60 * 1000})

const useRegExIfNeeded = (urlKey) => urlKey[0] === '^' ? RegExp(urlKey) : urlKey

const setupMiddlewaresForEntitlementAndMethod = (urlKey, method) => async (req, res, next) => {
  const {headers} = req
  const user = getUserId(headers)
  const notAuthorized = isDeployed && user && !(await isAuthorized({urlKey, method, user, wrappedDetermineEntitlements}))
  log.info({urlKey, method, user, isDeployed: isDeployed, notAuthorized}, 'authorization middleware')

  return notAuthorized ?
    res.status(unauthorized.status).send(unauthorized.message) :
    next()
}

module.exports = (app) => {
  _.forEach(ENTITLEMENTS, (entitlements, urlKey) => {
    _.forEach(entitlements, (entitlement, method) => {
      app[method](useRegExIfNeeded(urlKey), setupMiddlewaresForEntitlementAndMethod(urlKey, method))
    })
  })
}
