const createError = require('http-errors')
const log = require('../../log')
const getUserId = require('../getUserId')
const insertIntoDb = require('../db/insertIntoDb')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const validateThenCallApi = require('../common/validateThenCallApi')
const watchListSchema = require('./watchListSchema')
const actionStepWatchListSchema = require('./actionStepWatchListSchema')
const reasonWatchListSchema = require('./reasonWatchListSchema')
const buildWatchListInsertParams = require('./buildWatchListInsertParams')
const buildActionStepWatchListInsertParams = require('./buildActionStepWatchListInsertParams')
const buildReasonWatchListInsertParams = require('./buildReasonWatchListInsertParams')
const moment = require('moment')
const SKIP_AUDIT = true

const attemptInsertApi = async (schema, body, res, next, {userId}) => {
  if (!body) {
    throw createError.BadRequest('Body isn\'t set!')
  }

  const watchListInsertParams = buildWatchListInsertParams(userId, body)
  const watchListInsertResponse = await insertIntoDb(watchListSchema, watchListInsertParams, userId, SKIP_AUDIT)

  const watchListId = watchListInsertResponse.watchListId

  await Promise.all(body.actionSteps.map(async (actionStep) => {
    const actionStepWatchListInsertParams = buildActionStepWatchListInsertParams(userId, body, watchListId, actionStep.actionStepId)
    return await insertIntoDb(actionStepWatchListSchema, actionStepWatchListInsertParams, userId, SKIP_AUDIT)
  }))

  await Promise.all(body.reasons.map(async (reason) => {
    const reasonWatchListInsertParams = buildReasonWatchListInsertParams(userId, body, watchListId, reason.reasonId)
    await insertIntoDb(reasonWatchListSchema, reasonWatchListInsertParams, userId, SKIP_AUDIT)
  }))

  const selectResponse = await selectByIdFromDb(watchListSchema, watchListId)
  return res.status(201).json(selectResponse)
}

module.exports = async ({ headers, body }, res, next) => {
  log.info({domain: watchListSchema.tableShort}, 'create watchlist api')
  const userId = getUserId(headers)
  return await validateThenCallApi(watchListSchema, body, watchListSchema.required.new, res, next, attemptInsertApi, {userId})
}
