/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

describe('query contracts', () => {
  let target, simpleSelectFromDb, convertRowResultFromDbValue, getManyContractsWithAssignmentState, getCurrentlyEffectiveEditHistory, getContractsWithContactCompanyNames, req, res

  beforeEach(() => {
    simpleSelectFromDb = sinon.stub().resolves('database rows')
    convertRowResultFromDbValue = sinon.stub()
    getManyContractsWithAssignmentState = { sql: sinon.spy, mapper: sinon.stub().withArgs('database rows').returns([]) }
    getCurrentlyEffectiveEditHistory = sinon.stub()
    getContractsWithContactCompanyNames = sinon.stub()
    req = {query: {}}
    res = { json: sinon.spy() }
    target = proxyquire('api/contracts/queryContractsApi', {
      '../db/simpleSelectFromDb': simpleSelectFromDb,
      '../db/converters/convertRowResultFromDbValue': convertRowResultFromDbValue,
      './getManyContractsWithAssignmentState': getManyContractsWithAssignmentState,
      './getCurrentlyEffectiveEditHistory': getCurrentlyEffectiveEditHistory,
      './getContractsWithContactCompanyNames': getContractsWithContactCompanyNames
    })
  })

  describe('with empty results', () => {
    it('simple select called once', async () => {
     await target(req, res)
     simpleSelectFromDb.callCount.should.equal(1)
    })

    it('res.json sent empty', async () => {
      await target(req, res)
      res.json.should.have.been.calledWithExactly({ pageSize: null, results: [], pageNumber: null, total: 0 })
     })
  })

  describe('with results', () => {
    let contract1, contract2
    beforeEach(() => {
      contract1 = {contractId: 1, editHistories: ['history 1'], status: 'pending'}
      contract2 = {contractId: 2, editHistories: ['history 2'], status: 'something else'}
      getManyContractsWithAssignmentState.mapper.returns([contract1, contract2])
      getCurrentlyEffectiveEditHistory.withArgs(contract1.status, contract1.editHistories, 'assignmentState').returns('contract1 effective assignment state')
      getCurrentlyEffectiveEditHistory.withArgs(contract2.status, contract2.editHistories, 'assignmentState').returns(null)
      getCurrentlyEffectiveEditHistory.withArgs(contract1.status, contract1.editHistories, 'managerId').returns(17)
      getCurrentlyEffectiveEditHistory.withArgs(contract2.status, contract2.editHistories, 'managerId').returns(null)
    })

    it('simple select called once', async () => {
     await target(req, res)
     simpleSelectFromDb.callCount.should.equal(1)
    })

    it('res.json called with contracts', async () => {
      await target(req, res)
      res.json.should.have.been.calledWithExactly({
        results: [
          {
            assignmentState: 'contract1 effective assignment state',
            contractId: 1,
            managerId: 17,
            status: 'pending'
          },
          {
            assignmentState: null,
            contractId: 2,
            managerId: null,
            status: 'something else'
          }
        ],
        pageNumber: null,
        pageSize: null,
        total: 2
      })
    })
    it('should call getContractsWithContactCompanyNames if contactCompanies is passed in through query', async () => {
      req.query.contactCompanies = true
      getContractsWithContactCompanyNames.returns([{test: 'test'}])
      await target(req, res)
      getContractsWithContactCompanyNames.should.have.been.called
      res.json.should.have.been.calledWithExactly({
        total: 1,
        results: [{test: 'test'}],
        pageNumber: null,
        pageSize: null
      })
    })
  })
})
