const selectFromDb = require('../db/selectFromDb')
const schema = require('../companies/companySchema')

module.exports = () =>
  Promise.all([
    selectFromDb(schema, {executeOptions: {maxRows: 2}})
  ])
  .then(() => ({}))
  .catch(error => ({error}))
