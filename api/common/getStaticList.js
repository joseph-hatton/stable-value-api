module.exports = (referenceData) =>
  (req, res, next) =>
    Promise.resolve()
      .then(() => res.json(referenceData))
