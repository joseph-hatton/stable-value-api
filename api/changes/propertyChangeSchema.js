const {dbSchemaName} = require('../config')
const {numberConverter} = require('../db/converters')
const table = 'property_change'
const tableId = 'property_change_id'
const sequence = `${dbSchemaName}.property_change_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'property_change.property_change_id',
  columns: [
    {name: 'propertyChangeId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'changeId', column: 'change_id', immutable: true, converter: numberConverter},
    {name: 'name', immutable: true},
    {name: 'oldValue', column: 'old_value', immutable: true},
    {name: 'newValue', column: 'new_value', immutable: true},
    {name: 'oldId', column: 'old_id', immutable: true, converter: numberConverter},
    {name: 'newId', column: 'new_id', immutable: true, converter: numberConverter}
  ]
}
