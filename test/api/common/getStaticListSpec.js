const target = require('../../../api/common/getStaticList')
const user = 'THE USER'
const THE_ID = 8675309

describe('get static list api', () => {
  const REFERENCE_DATA = ['a', 'c', 'f']
  const request = { headers: 'abc', params: {id: THE_ID} }

  let response, next, responseStatusSend, responseJson

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    responseJson = sinon.spy()
    response = {
      json: responseJson,
      status: sinon.stub().returns({ send: responseStatusSend, json: responseJson })
    }
    next = sinon.spy()
  })

  it('sends get result to response', () =>
    target(REFERENCE_DATA)(request, response, next)
      .then((result) => {
        response.json.should.have.been.calledWithExactly(REFERENCE_DATA)
      })
  )
})
