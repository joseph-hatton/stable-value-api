const createError = require('http-errors')
const log = require('../../../log')
const updateDb = require('../../db/updateDb')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const getUserId = require('../../getUserId')
const buildUpdateParams = require('../../common/buildUpdateParams')
const contractSchema = require('../../contracts/contractSchema')
const schema = require('../creditingRatesSchema')

const canApproveCreditingRate = (contract, creditingRate, userId) => {
  const isActiveContract = contract && contract.status === 'ACTIVE'
  const isPendingCreditingRateNotModifiedByYou = creditingRate && creditingRate.status === 'PENDING' && creditingRate.modifiedId !== userId
  return isActiveContract && isPendingCreditingRateNotModifiedByYou
}

module.exports = async ({ params, headers, body }, res) => {
  const contractId = params.contractId
  const creditingRateId = params.id
  const userId = getUserId(headers)
  log.info({domain: schema.tableShort, contractId, creditingRateId, userId}, 'approve crediting rate api')

  const dbContract = await selectByIdFromDb(contractSchema, contractId)
  const dbCreditingRate = await selectByIdFromDb(schema, creditingRateId)

  if (canApproveCreditingRate(dbContract, dbCreditingRate, userId)) {
    const params = {
      status: 'APPROVED'
    }
    const updateParams = buildUpdateParams(schema.columns, userId, params)
    const result = await updateDb(schema, updateParams, creditingRateId, userId)
    return res.status(200).json(result)
  } else {
    throw createError.BadRequest('Only Pending Crediting Rates (not last modified by you) on Active Contracts can be approved.')
  }
}
