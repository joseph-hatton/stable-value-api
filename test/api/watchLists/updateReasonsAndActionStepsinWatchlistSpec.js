/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const SKIP_AUDIT = true
const params = {params: true}
const headers = ['header']
const body = {actionSteps: [{actionStepId: 1}, {actionStepId: 2}], reasons: [{reasonId: 1}, {reasonId: 2}, {reasonId: 3}]}
const schema = {}
const userId = 'user'
const selectResponse = 'selectResponse'

describe('update reasons and action steps in watchlist', () => {
  let target, validateThenCallApi, res, next, resJson, getUserId, selectByIdFromDb, buildUpdateParams, updateDb, updateActionSteps, updateReasons
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    validateThenCallApi = sinon.stub().resolves('done')
    getUserId = sinon.stub().returns(userId)
    selectByIdFromDb = sinon.stub().returns(selectResponse)
    buildUpdateParams = sinon.stub()
    updateDb = sinon.stub().resolves('')
    updateActionSteps = sinon.stub().resolves('')
    updateReasons = sinon.stub().resolves('')

    target = proxyquire('api/watchLists/updateReasonsAndActionStepsinWatchlist', {
      '../common/validateThenCallApi': validateThenCallApi,
      '../getUserId': getUserId,
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../common/buildUpdateParams': buildUpdateParams,
      '../db/updateDb': updateDb,
      './updateActionSteps': updateActionSteps,
      './updateReasons': updateReasons
    })
  })
  it('calls validateThenCallApi', async () => {
    const result = await target(params, headers, body, res, next)
    validateThenCallApi.should.have.been.called
    result.should.eql('done')
  })
  describe('calling attemptUpdateApi', () => {
    let attemptUpdateApi
    beforeEach(async () => {
      await target(params, headers, body, res, next)
      attemptUpdateApi = validateThenCallApi.getCall(0).args[5]
    })
    it('should throw error if body isn\'t set', async () => {
      try {
        await attemptUpdateApi(schema, null, res, next, {userId})
      } catch (err) {
        err.message.should.eql("Body isn't set!")
      }
    })
    describe('should call', () => {
      beforeEach(async () => {
        await attemptUpdateApi(schema, body, res, next, {userId})
      })
      it('buildUpdateParams', () => buildUpdateParams.should.have.been.called)
      it('updateDb', () => updateDb.should.have.been.called)
      it('updateActionSteps', () => updateActionSteps.should.have.been.called)
      it('updateReasons', () => updateReasons.should.have.been.called)
      it('selectByIdFromDb', () => selectByIdFromDb.should.have.been.called)
    })
  })
})
