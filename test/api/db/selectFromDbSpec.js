/* eslint-disable no-unused-expressions */
const cloneDeep = require('lodash/cloneDeep')
const proxyquire = require('../../proxyquire')
const testSchema = require('../../data/testSchema')
const THE_ID = 8675309
const USER_ID = 'someUser'
const CHANGE_ID = 4443
const params = {
  bColumn: 'bValue',
  cColumn: 'cValue'
}
const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'
const DB_EXECUTE_ONE_RESULT = 'DB_EXECUTE_ONE_RESULT'
const BUILD_SELECT_QUERY_RESULT = 'BUILD_SELECT_QUERY_RESULT'

describe('select from db', () => {
  let target, schema, connection, dbExecutePromise, dbExecuteOne, buildSelectQuery

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves(DB_EXECUTE_ONE_RESULT)
    buildSelectQuery = sinon.stub().returns(BUILD_SELECT_QUERY_RESULT)
    target = proxyquire('api/db/selectFromDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/dbExecuteOne': dbExecuteOne,
      './queries/buildSelectQuery': buildSelectQuery
    })
  })

  const executeTarget = async (schemaOverides = {}) => {
    schema = Object.assign(cloneDeep(testSchema.schema), schemaOverides)
    const result = await target(schema, params)
    await dbExecutePromise.getCall(0).args[0](connection)
    return result
  }

  describe('execute promise', () => {
    it('called once', () =>
      executeTarget()
      .then(() => dbExecutePromise.callCount.should.eql(1)))

    it('returns result', () =>
      executeTarget()
      .then((result) => result.should.eql(DB_EXECUTE_PROMISE_RESULT)))
  })

  it('build select query called once', () =>
    executeTarget()
    .then(() => buildSelectQuery.callCount.should.eql(1)))

  it('build select query called with', () =>
    executeTarget()
    .then(() => buildSelectQuery.should.have.been.calledWithExactly(schema, true, params)))

  it('execute one called once', () =>
    executeTarget()
    .then(() => dbExecuteOne.callCount.should.eql(1)))

  it('execute one called with', () =>
    executeTarget()
    .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, BUILD_SELECT_QUERY_RESULT)))
})
