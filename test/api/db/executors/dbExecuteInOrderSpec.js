/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

const EXECUTION_1 = {sql: '1 sql', bindParams: {1: true}, executeOptions: {}, mapResult: (result) => result}
const EXECUTION_2 = {sql: '2 sql', bindParams: {2: true}, executeOptions: {}, mapResult: (result) => 'TWOs RESULT'}

describe('db execute in order', () => {
  let target, dbExecuteOne, connection
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves('ONE_EXECUTED')
    target = proxyquire('api/db/executors/dbExecuteInOrder', {
      './dbExecuteOne': dbExecuteOne
    })
  })

  describe('with only a single execution', () => {
    let exectutionArray
    beforeEach(() => {
      exectutionArray = [EXECUTION_1]
    })

    it('returns execute result', () =>
      target(connection, exectutionArray)
      .then((result) => result.should.eql(['ONE_EXECUTED'])))

    it('execute one should only be called once', () =>
      target(connection, exectutionArray)
      .then(() => dbExecuteOne.callCount.should.eql(1)))

    it('execute one should only be called with', () =>
      target(connection, exectutionArray)
      .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, EXECUTION_1)))

    it('execute one error bubbles up', () => {
      dbExecuteOne.rejects(new Error('execute error'))
      return target(connection, exectutionArray)
      .catch((error) => error.message.should.eql('execute error'))
    })
  })

  describe('with only two executions', () => {
    let exectutionArray
    beforeEach(() => {
      exectutionArray = [EXECUTION_1, EXECUTION_2]
    })

    it('returns execute result', () =>
      target(connection, exectutionArray)
      .then((result) => result.should.eql(['ONE_EXECUTED', 'ONE_EXECUTED'])))

    it('execute one should only be called twice', () =>
      target(connection, exectutionArray)
      .then(() => dbExecuteOne.callCount.should.eql(2)))

    it('execute one first call should only be called with', () =>
      target(connection, exectutionArray)
      .then(() => dbExecuteOne.getCall(0).should.have.been.calledWithExactly(connection, EXECUTION_1)))

    it('execute one second call should only be called with', () =>
      target(connection, exectutionArray)
      .then(() => dbExecuteOne.getCall(1).should.have.been.calledWithExactly(connection, EXECUTION_2)))
  })
})
