const createError = require('http-errors')
const log = require('../../log')
const buildUpdateParams = require('../common/buildUpdateParams')
const contractSchema = require('../contracts/contractSchema')
const undoPendingTerminationContract = require('../contracts/workflow/undoPendingTerminationContract')
const deleteByIdFromDb = require('../db/deleteByIdFromDb')
const updateDb = require('../db/updateDb')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const selectFromDb = require('../db/selectFromDb')
const deleteFeeDistributionsByTransactionId = require('../feeDistributions/deleteFeeDistributionsByTransactionId')
const getUserId = require('../getUserId')
const schema = require('./transactionSchema')

const handleFeeReceipt = async (transactionId, dbTransaction, userId) => {
  if (dbTransaction.adjustmentTransactionId) {
    const updateParams = buildUpdateParams(schema.columns, userId, {adjustmentTransactionId: null})
    await updateDb(schema, updateParams, transactionId, userId)
    await deleteByIdFromDb(schema, dbTransaction.adjustmentTransactionId)
  }
  await deleteFeeDistributionsByTransactionId(transactionId, userId)
}

const handleFeeAdjustment = async (transactionId, userId) => {
  const parentTransactions = await selectFromDb(schema, {whereClause: 'where transaction.adjustment_transaction_id = :adjustmentTransactionId', bindParams: {adjustmentTransactionId: transactionId}})
  if (parentTransactions && parentTransactions.length > 0) {
    await deleteFeeDistributionsByTransactionId(parentTransactions[0].transactionId)
    await deleteByIdFromDb(schema, parentTransactions[0].transactionId, userId)
  }
}

const handleTermination = async (transactionId, dbTransaction, userId) => {
  const contract = await selectByIdFromDb(contractSchema, dbTransaction.contractId)
  await undoPendingTerminationContract({contract, userId})
}

module.exports = async ({ params, headers }, res) => {
  log.info({domain: schema.tableShort, id: params.id}, 'delete transaction by id api')
  const transactionId = params.id
  const userId = getUserId(headers)
  const dbTransaction = await selectByIdFromDb(schema, transactionId)
  if (dbTransaction.locked) {
    throw createError.BadRequest('This transaction is locked.')
  }

  if (dbTransaction.transactionType === 'FEE_RECEIPT') {
    await handleFeeReceipt(transactionId, dbTransaction, userId)
  } else if (dbTransaction.transactionType === 'FEE_ADJUSTMENT') {
    await handleFeeAdjustment(transactionId, userId)
  }
  const result = await deleteByIdFromDb(schema, transactionId, userId)

  if (dbTransaction.transactionType === 'TERMINATION') {
    await handleTermination(transactionId, dbTransaction, userId)
  }
  res.status(200).json(result)
}
