const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/feeDistributions/feeDistributionsSchema')
const {numberConverter} = require('../../../api/db/converters')

describe('fee distribution schema', () => {
  validateSchema(target, {
    tableShort: 'fee_distribution',
    table: 'sch_stbv.fee_distribution',
    tableId: 'fee_distribution_id',
    sequence: 'sch_stbv.fee_distribution_seq.NEXTVAL',
    orderBy: 'transaction_id',
    columns: [
      {name: 'feeDistributionId', column: 'fee_distribution_id', sequence: 'sch_stbv.fee_distribution_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'transactionId', column: 'transaction_id', converter: numberConverter},
      {name: 'invoiceId', column: 'invoice_id', converter: numberConverter},
      {name: 'amount', converter: numberConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId,
      version,
      {name: 'beginningBalance', column: 'beginning_balance', converter: numberConverter},
      {name: 'adjustment', converter: numberConverter},
      {name: 'endingBalance', column: 'ending_balance', converter: numberConverter}
    ],
    required: {
      new: ['transactionId'],
      update: ['transactionId']
    }
  })
})
