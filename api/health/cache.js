const cacheManager = require('cache-manager')

module.exports = cacheManager.caching({store: 'memory', max: 100, ttl: 5 * 60})
