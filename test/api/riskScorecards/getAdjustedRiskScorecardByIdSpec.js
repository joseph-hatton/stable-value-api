const proxyquire = require('../../proxyquire')
const schema = require('../../../api/riskScorecards/riskScorecardsSchema')

describe('get adjusted riskscorecard by id', () => {
  let target, selectByIdFromDb

  beforeEach(() => {
    selectByIdFromDb = sinon.stub().withArgs(schema, 5).resolves('unadjusted risk scorecard')
    target = proxyquire('api/riskScorecards/getAdjustedRiskScorecardById', {
      '../db/selectByIdFromDb': selectByIdFromDb,
      './getLookupsAndAdjustRiskScorecards': sinon.stub().resolves(['adjusted risk scorecard'])
    })
  })

  it('returns adjusted scorecard if found', async () => {
    const result = await target(5)
    result.should.eql('adjusted risk scorecard')
  })

  it('returns empty if not found', async () => {
    selectByIdFromDb.resolves(null)
    const result = await target(5)
    should.not.exist(result)
  })
})
