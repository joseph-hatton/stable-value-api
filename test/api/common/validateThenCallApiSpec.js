const proxyquire = require('../../proxyquire')
const {schema} = require('../../data/testSchema')

const RESPONSE_JSON_RESULT = 'RESPONSE_JSON_RESULT'
const AN_API_CALL_RESULT = 'AN_API_CALL_RESULT'
const REQUIRED_FIELDS = ['a', 'b']

describe('validate then call api', () => {
  let target, body, res, responseJson, next, anApiCall, apiCallOpts, schemaValidator

  beforeEach(() => {
    body = {some: 'body'}
    responseJson = sinon.stub().returns(RESPONSE_JSON_RESULT)
    res = { status: sinon.stub().withArgs(400).returns({json: responseJson}) }
    next = sinon.spy()
    anApiCall = sinon.stub().returns(AN_API_CALL_RESULT)
    apiCallOpts = sinon.spy()
    schemaValidator = sinon.stub().withArgs(schema, body).returns([])
    target = proxyquire('api/common/validateThenCallApi', {
      './schemaValidator': schemaValidator
    })
  })

  it('returns api call result with no validation errors', () => {
    target(schema, body, REQUIRED_FIELDS, res, next, anApiCall, apiCallOpts).should.eql(AN_API_CALL_RESULT)
  })

  it('res status result with validation error', () => {
    schemaValidator.returns(['an error'])
    target(schema, body, REQUIRED_FIELDS, res, next, anApiCall, apiCallOpts).should.eql(RESPONSE_JSON_RESULT)
  })

  it('res status result with validation sends error to responseJson', () => {
    schemaValidator.returns(['an error'])
    target(schema, body, REQUIRED_FIELDS, res, next, anApiCall, apiCallOpts)
    responseJson.should.have.been.calledWithExactly({message: 'an error'})
  })
})
