const toPairs = require('lodash/toPairs')
const fromPairs = require('lodash/fromPairs')
const log = require('../../log')

const columnsToDefault = (columns) => {
  const defaultParams = {}
  columns.forEach((column) => {
    if (!column.sequence) {
      defaultParams[column.name] = (column.defaultValue) ? column.defaultValue() : null
    }
  })
  return defaultParams
}

const logUnRegisteredPairs = (columns, pairs) => {
  const unRegisteredPairs = pairs.filter(pair => columns.filter((column) => column.name === pair[0]).length === 0)
  if (unRegisteredPairs.length > 0) {
    log.warn({ pairs: unRegisteredPairs }, 'pairs not registered!')
  }
}

const removeFieldsNotInSchema = (columns, someObject) => {
  const pairs = toPairs(someObject)
  logUnRegisteredPairs(columns, pairs)
  return fromPairs(pairs.filter(pair => columns.filter((column) => column.name === pair[0]).length > 0))
}

module.exports = ({columns}, userData, overridingData) => {
  const defaultParams = columnsToDefault(columns)
  return Object.assign(defaultParams, removeFieldsNotInSchema(columns, userData), removeFieldsNotInSchema(columns, overridingData))
}
