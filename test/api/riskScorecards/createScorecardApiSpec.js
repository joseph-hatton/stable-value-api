/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/riskScorecards/riskScorecardsSchema')
const defaultParams = {a: true}
const insertResult = {riskScorecardId: 42}
const user = 'THE USER'
const body = {b: 'from request body'}
const VALIDATE_THEN_CALL_API_RESULT = 'VALIDATE_THEN_CALL_API_RESULT'

describe('create risk scorecard api', () => {
  const scorecards = [{ riskScorecardId: 'aId', name: 'a' }, { riskScorecardId: 'bId', name: 'b' }]
  const adjustedScorecards = scorecards.map((scorecard) => {
    scorecard.adjusted = true
    return scorecard
  })
  const request = { headers: 'abc' }

  let response, target, next, getUserId, responseJson, insertInto, validateThenCallApi, getAdjustedRiskScorecardById, flattenRiskScorecardForInsertOrUpdateParam

  beforeEach(() => {
    responseJson = sinon.spy()
    response = {
      status: sinon.stub().withArgs(201).returns({ json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    insertInto = sinon.stub().resolves(insertResult)
    const buildDefaultParams = sinon.stub().returns(defaultParams)
    validateThenCallApi = sinon.stub().returns(VALIDATE_THEN_CALL_API_RESULT)
    getAdjustedRiskScorecardById = sinon.stub().withArgs(insertResult.riskScorecardId).resolves('adjusted scorecard')
    flattenRiskScorecardForInsertOrUpdateParam = sinon.stub().withArgs(body).returns('flattenedBody')
    target = proxyquire('api/riskScorecards/createScorecardApi', {
      '../db/insertIntoDb': insertInto,
      '../getUserId': getUserId,
      '../common/buildDefaultParams': buildDefaultParams,
      '../common/validateThenCallApi': validateThenCallApi,
      './getAdjustedRiskScorecardById': getAdjustedRiskScorecardById,
      './flattenRiskScorecardForInsertOrUpdateParam': flattenRiskScorecardForInsertOrUpdateParam
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
      request.body = body
    })

    it('returns validateThenCallApiResult', async () => {
      const result = await target(request, response, next)

      result.should.eql(VALIDATE_THEN_CALL_API_RESULT)
    })

    it('validateThenCallApiResult should be called with schema', async () => {
      await target(request, response, next)

      validateThenCallApi.lastCall.args[0].should.eql
    })

    it('validateThenCallApiResult should be called with body', async () => {
      await target(request, response, next)

      validateThenCallApi.lastCall.args[1].should.eql(body)
    })

    it('validateThenCallApiResult should be called with empty required fields', async () => {
      await target(request, response, next)

      validateThenCallApi.lastCall.args[2].should.eql(schema.required.new)
    })

    it('validateThenCallApiResult should be called with res', async () => {
      await target(request, response, next)

      validateThenCallApi.lastCall.args[3].should.eql(response)
    })

    it('validateThenCallApiResult should be called with next', async () => {
      await target(request, response, next)

      validateThenCallApi.lastCall.args[4].should.eql(next)
    })

    it('validateThenCallApiResult should be called with userId in options', async () => {
      await target(request, response, next)

      validateThenCallApi.lastCall.args[6].should.eql({userId: user})
    })

    it('sends expected data into insert into', async () => {
      await target(request, response, next)
      await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user})

      insertInto.should.have.been.calledWithExactly(schema, defaultParams, user)
    })

    it('sends insert results to response', async () => {
      await target(request, response, next)
      await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user})

      responseJson.should.have.been.calledWithExactly('adjusted scorecard')
    })
  })
})
