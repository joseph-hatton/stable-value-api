const {toDb, fromDb} = require('../../../../api/db/converters/beginningOfDayDateConverter')

describe('beginning of day date converter', () => {
  it('handle to db with string', () => {
    const theDate = new Date('2017-11-13T16:57:22.231Z')
    toDb(theDate.toISOString()).should.eql(new Date('2017-11-13T00:00:00.000Z'))
  })

  it('handle to db with date', () => {
    const theDate = new Date('2017-11-13T16:57:22.231Z')
    toDb(theDate).should.eql(new Date('2017-11-13T00:00:00.000Z'))
  })

  it('handle to db with null', () => {
    should.not.exist(toDb(null))
  })

  it('handle from db with string', () => {
    const theDate = new Date()
    fromDb(theDate.toISOString()).should.eql(theDate)
  })

  it('handle from db with null', () => {
    should.not.exist(fromDb(null))
  })
})
