const flatten = require('lodash/flatten')
const uniq = require('lodash/uniq')

module.exports = (schema, requiredFields = [], json) =>
  uniq(flatten(schema.columns.map((column) => {
    const isRequiredField = (requiredFields && requiredFields.includes(column.name))
    const hasColumnValidators = (column.validators && column.validators.length > 0)
    const value = json[column.name]
    return (value && hasColumnValidators)
      ? column.validators.map((validator) => validator(json[column.name], json))
      : undefined

    // VALUE is required has validators = yes
    // value is required no validators = no
    // value, not required, has validators = yes
    // no value, not required, == no
    // no value, required, validators = no?  it shouldn't have gotten here then
  }
  ))).filter((result) => result !== undefined)
