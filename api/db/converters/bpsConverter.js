const Decimal = require('decimal.js')
const BPS = 10000

module.exports = {
  toDb: (something) => (something) ? Decimal(something).div(BPS).toNumber() : something,
  fromDb: (something) => (something) ? Decimal(something).mul(BPS).toNumber() : something
}
