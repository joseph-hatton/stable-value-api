const { states } = require('../referenceData.json')
const validateIdAndText = require('../common/validateIdTextField')

module.exports = validateIdAndText(states, 'Invalid State')
