const target = require('../../../api/common/validateIdTextField')
const testSchema = require('../../data/testSchema')
const cloneDeep = require('lodash/cloneDeep')

describe('id and text field validator', () => {
  let idTextArray, message
  beforeEach(() => {
    idTextArray = [{id: 1, text: 'one'}, {id: 2, text: 'two'}]
    message = 'an error message'
  })

  it('returns error message if id not found',
    () => target(idTextArray, message)(8).should.eql(message))

  it('returns undefined if id found',
    () => should.not.exist(target(idTextArray, message)(1)))
})
