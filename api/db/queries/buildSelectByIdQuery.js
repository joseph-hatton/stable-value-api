const buildSelectQuery = require('./buildSelectQuery')

module.exports = (schema, id) =>
  buildSelectQuery(schema,
    false,
    {
      whereClause: `WHERE ${schema.table}.${schema.tableId} = :id`,
      bindParams: { id },
      executeOptions: {}
    })
