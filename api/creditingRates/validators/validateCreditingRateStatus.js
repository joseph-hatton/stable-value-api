const validateIdAndText = require('../../common/validateIdTextField')
const {creditingRateStatuses} = require('../../referenceData.json')
module.exports = validateIdAndText(creditingRateStatuses, 'Invalid Crediting Rate Status')
