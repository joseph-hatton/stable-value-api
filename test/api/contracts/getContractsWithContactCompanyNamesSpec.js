/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const SQL = 'SOME SQL'

describe('get contracts with contact company names', () => {
  let target, dbExecuteOne, dbExecutePromise, connection, contractSchema, convertRowResultFromDbValue
  beforeEach(() => {
    dbExecutePromise = sinon.stub().callsFake((connection) => Promise.resolve(connection))
    dbExecuteOne = sinon.stub().callsFake((connection, dbRequest) => ({connection, dbRequest}))
    connection = sinon.stub()
    convertRowResultFromDbValue = sinon.stub()

    target = proxyquire('api/contracts/getContractsWithContactCompanyNames', {
      '../db/executors/dbExecutePromise': dbExecutePromise,
      '../db/executors/dbExecuteOne': dbExecuteOne,
      './contactCompanyQuery.sql': SQL
    })
  })

  it('should call dbExecutePromise', async () => {
    await target()
    dbExecutePromise.should.have.been.called
  })
  it('should call dbExecute one when the function passed into dbExecutePromise is called', async () => {
    (await target())('connection')
    dbExecuteOne.should.have.been.called
    dbExecuteOne.args[0][0].should.eql('connection')
  })
  it('dbRequest should be setup correctly', async () => {
    (await target())()
    dbExecuteOne.args.length.should.eql(1)
    const dbRequest = dbExecuteOne.args[0][1]
    dbRequest.sql.should.eql(SQL)
    dbRequest.bindParams.should.eql({})
    dbRequest.executeOptions.should.eql({maxRows: 1000})
  })
  it('getContactCompanyQuery should map correctly', async () => {
    (await target())()
    const dbRequest = dbExecuteOne.args[0][1]
    dbRequest.mapResult({rows: [
      {
        CONTRACT_ID: 123,
        NAME: 'name1',
        CONTRACT_TYPE: 'contractType'
      },
      {
        CONTRACT_ID: 123,
        NAME: 'name2',
        CONTRACT_TYPE: 'contractType'
      }
    ]}).should.eql([{
      contractId: 123,
      contractType: 'contractType',
      contactCompanyNames: [
        'name1',
        'name2'
      ]
    }])
  })
})
