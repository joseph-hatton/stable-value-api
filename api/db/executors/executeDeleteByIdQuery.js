const dbExecuteOne = require('./dbExecuteOne')
const buildDeleteQuery = require('../queries/buildDeleteQuery')

module.exports = (connection, schema, rowId) =>
  dbExecuteOne(connection, buildDeleteQuery(schema, rowId))
