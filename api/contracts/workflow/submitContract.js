const log = require('../../../log')
const { validateCurrentlyAssigned } = require('../contacts/validateAssignedContacts')
const transitionContractInWorkflow = require('./transitionContractInWorkflow')

const PENDING_JASPER = 'PENDING_JASPER'

const validateCanTransition = async ({contract, userId}) => {
  if (!['PENDING', PENDING_JASPER].includes(contract.status)) {
    return 'Only PENDING or PENDING_JASPER Contracts may be submitted.'
  }
  const assignedContactsErrors = await validateCurrentlyAssigned(contract.contractId)

  if (contract.status !== PENDING_JASPER && assignedContactsErrors.length > 0) {
    return assignedContactsErrors
  }
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'submit',
  nextContractStatus: 'SUBMITTED',
  validateCanTransition
})
