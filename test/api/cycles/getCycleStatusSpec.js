/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const {oldAppBaseUrl} = require('../../../api/config')

describe('get cycle status', () => {
  let target, res, jsonStub, httpGet

  beforeEach(() => {
    httpGet = sinon.stub().resolves({body: 'expected cycle data'})
    target = proxyquire('api/cycles/getCycleStatus', {
      'superagent': {get: httpGet}
    })
  })

  it('sends cycle data to json response', async () => {
    const result = await target({}, res)

    result.should.be.eql('expected cycle data')
  })

  it('calls get with correct url', async () => {
    await target({}, res)

    httpGet.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/cycle`)
  })

  it('caches call', async () => {
    await target({}, res)
    await target({}, res)

    httpGet.callCount.should.eql(1)
  })
})
