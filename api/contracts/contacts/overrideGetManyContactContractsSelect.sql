SELECT 	contact_contract.*,
        contact.first_name,
        contact.last_name,
        contact.company_id,
        contact.contact_type,
        company.name AS company_name,
        contract.status AS contract_status
FROM sch_stbv.contact_contract
INNER JOIN sch_stbv.contact
ON contact_contract.contact_id = contact.contact_id
INNER JOIN sch_stbv.company
ON contact.company_id = company.company_id
INNER join sch_stbv.contract
ON contract.CONTRACT_ID = contact_contract.CONTRACT_ID