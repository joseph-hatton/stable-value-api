const log = require('../../log')

module.exports = async ({urlKey, method, user, wrappedDetermineEntitlements}) => {
  const entitlements = await wrappedDetermineEntitlements(user)
  log.info({authorized: entitlements[urlKey][method], user, urlKey, method}, 'is authorized')
  return entitlements[urlKey][method]
}
