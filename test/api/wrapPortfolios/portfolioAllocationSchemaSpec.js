require('require-sql')
const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/wrapPortfolios/portfolioAllocationSchema')
const sql = require('../../../api/wrapPortfolios/getPorfolioAllocationFull.sql')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

const zeroDefault = () => 0

describe('portfolio allocation schema', () => {
  validateSchema(target, {
    tableShort: 'portfolio_allocation',
    table: 'sch_stbv.portfolio_allocation',
    tableId: 'portfolio_allocation_id',
    sequence: 'sch_stbv.portfolio_allocation_seq.NEXTVAL',
    orderBy: 'portfolio_allocation_id', //
    columns: [  //  order by the column ids in the table
      {name: 'portfolioAllocationId', column: 'portfolio_allocation_id', sequence: 'sch_stbv.portfolio_allocation_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'version', converter: numberConverter},
      {name: 'effectiveDate', column: 'effective_date', audit: true, converter: dateConverter},
      {name: 'contractId', column: 'contract_id', immutable: true, converter: numberConverter},
      {name: 'portfolioOneId', column: 'portfolio_one_id', audit: true, converter: numberConverter},
      {name: 'portfolioOneRate', column: 'portfolio_one_rate', audit: true, converter: percentConverter},
      {name: 'portfolioTwoId', column: 'portfolio_two_id', audit: true, converter: numberConverter},
      {name: 'portfolioTwoRate', column: 'portfolio_two_rate', audit: true, converter: percentConverter},
      {name: 'portfolioThreeId', column: 'portfolio_three_id', audit: true, converter: numberConverter},
      {name: 'portfolioThreeRate', column: 'portfolio_three_rate', audit: true, converter: percentConverter},
      {name: 'portfolioFourId', column: 'portfolio_four_id', audit: true, converter: numberConverter},
      {name: 'portfolioFourRate', column: 'portfolio_four_rate', audit: true, converter: percentConverter},
      {name: 'portfolioFiveId', column: 'portfolio_five_id', audit: true, converter: numberConverter},
      {name: 'portfolioFiveRate', column: 'portfolio_five_rate', audit: true, converter: percentConverter},
      {name: 'portfolioSixId', column: 'portfolio_six_id', audit: true, converter: numberConverter},
      {name: 'portfolioSixRate', column: 'portfolio_six_rate', audit: true, converter: percentConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId,
      {name: 'portfolioSevenId', column: 'portfolio_seven_id', audit: true, converter: numberConverter},
      {name: 'portfolioSevenRate', column: 'portfolio_seven_rate', audit: true, converter: percentConverter},
      {name: 'portfolioEightId', column: 'portfolio_eight_id', audit: true, converter: numberConverter},
      {name: 'portfolioEightRate', column: 'portfolio_eight_rate', audit: true, converter: percentConverter}
    ],
    overrideSelectClause: {
      selectOne: {sql: sql},
      selectMany: {sql: sql}
    },
    required: {
      new: ['version', 'effectiveDate', 'contractId']
    }
  })
})
