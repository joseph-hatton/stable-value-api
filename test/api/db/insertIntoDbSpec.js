/* eslint-disable no-unused-expressions */
const cloneDeep = require('lodash/cloneDeep')
const proxyquire = require('../../proxyquire')
const testSchema = require('../../data/testSchema')
const THE_ID = 8675309
const USER_ID = 'someUser'
const CHANGE_ID = 4443
const params = {
  bColumn: 'bValue',
  cColumn: 'cValue'
}
const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'
const EXECUTE_SELECT_BY_ID_RESULT = 'EXECUTE_SELECT_BY_ID_RESULT'

describe('insert into db', () => {
  let target, schema, connection, dbExecutePromise, executeInsertQuery, executeSelectByIdQuery, executeInsertChangeQuery, executePropertyChanges

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    connection = sinon.spy()
    executeInsertQuery = sinon.stub().resolves({id: THE_ID})
    executeSelectByIdQuery = sinon.stub().resolves(EXECUTE_SELECT_BY_ID_RESULT)
    executeInsertChangeQuery = sinon.stub().resolves({id: CHANGE_ID})
    executePropertyChanges = sinon.stub()
    target = proxyquire('api/db/insertIntoDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/executeInsertQuery': executeInsertQuery,
      './executors/executeSelectByIdQuery': executeSelectByIdQuery,
      './executors/executeInsertChangeQuery': executeInsertChangeQuery,
      './executors/executeInsertPropertyChangeQueries': executePropertyChanges
    })
  })

  const executeTarget = async (schemaOverides = {}, skipAudit = false, skipSelect = false) => {
    schema = Object.assign(cloneDeep(testSchema.schema), schemaOverides)
    const targetResult = await target(schema, params, USER_ID, skipAudit, skipSelect)
    await dbExecutePromise.getCall(0).args[0](connection)
    return targetResult
  }

  describe('execute promise', () => {
    it('called once', async () => {
      await executeTarget()
      dbExecutePromise.callCount.should.eql(1)
    })

    it('returns result', async () => {
      const result = await executeTarget()
      result.should.eql(DB_EXECUTE_PROMISE_RESULT)
    })
  })

  describe('with no audit columns', () => {
    it('execute insert query called once', async () => {
      await executeTarget()
      executeInsertQuery.callCount.should.eql(1)
    })

    it('execute insert query called with', async () => {
      await executeTarget()
      executeInsertQuery.should.have.been.calledWithExactly(connection, schema, params)
    })

    it('execute select by id query called once', async () => {
      await executeTarget()
      executeSelectByIdQuery.callCount.should.eql(1)
    })

    it('execute select id query called with', async () => {
      await executeTarget()
      executeSelectByIdQuery.should.have.been.calledWithExactly(connection, schema, THE_ID)
    })

    it('execute select id query called after execute insert query', async () => {
      await executeTarget()
      executeSelectByIdQuery.should.have.been.calledAfter(executeInsertQuery)
    })
  })

  describe('with audit columns', () => {
    it('execute insert query called once', async () => {
      await executeTarget()
      executeInsertQuery.callCount.should.eql(1)
    })

    it('execute insert query called with', async () => {
      await executeTarget()
      executeInsertQuery.should.have.been.calledWithExactly(connection, schema, params)
    })

    it('execute insert change query called once', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      executeInsertChangeQuery.callCount.should.eql(1)
    })

    it('execute insert change called with', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      executeInsertChangeQuery.should.have.been.calledWithExactly(connection, schema, THE_ID, USER_ID, params, 'CREATE')
    })

    it('execute property changes called once', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      executePropertyChanges.callCount.should.eql(1)
    })

    it('execute property changes called with', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      executePropertyChanges.should.have.been.calledWithExactly(connection, schema, CHANGE_ID, params)
    })

    it('execute select by id query called once', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      executeSelectByIdQuery.callCount.should.eql(1)
    })

    it('execute select id query called with', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      executeSelectByIdQuery.should.have.been.calledWithExactly(connection, schema, THE_ID)
    })

    it('skip audit with flag', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]}, true)
      executePropertyChanges.should.not.have.been.called
    })

    it('select by id query not executed with skipSelect', async () => {
      await executeTarget({columns: [{name: 'auditedColumn', audit: true}]}, true, true)
      executeSelectByIdQuery.should.have.been.not.been.called
    })
  })
})
