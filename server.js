const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const swaggerExpress = require('swagger-express-mw')
const util = require('util')
const log = require('./log')
const authenticateMiddleware = require('./api/auth/authenticateMiddleware')
const authorizationMiddleware = require('./api/auth/authorizationMiddleware')
const cycleRunningMiddleware = require('./api/cycles/cycleRunningMiddleware')
const {port, maxUploadChunkSize, isDeployed} = require('./api/config')

const app = express()
app.use(compression())
// app.use('/search', require('./api/search'))

if (!isDeployed) {
  const cors = require('cors')
  app.use(cors())
}

const errorHandler = (error, req, res, next) => {
  if (error) {
    error.stack && log.error(error.stack)
    res.status(500).json(error)
  } else {
    next()
  }
}

const apiRoot = '/api'

const indexHtml = require('./index.html')

// 404 on id route... no such batch
// app.use(`${apiRoot}/v1/batches/:id`, require('./api/batches/batchIdMiddleware'))

swaggerExpress.create({appRoot: __dirname}, (error, swagger) => {
  if (error) {
    log.error('failed to initialize swagger', util.inspect(error, {depth: null, color: true}))
  } else {
    app.get(`${apiRoot}/swagger.json`, require('./api/swagger/jsonEndpoint'))
    app.get('/', (req, res) => res.send(indexHtml))
    // noinspection JSUnusedGlobalSymbols
    app.use(`${apiRoot}/v1/action-steps`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/changes`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/companies`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/contacts`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/contracts`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/crediting-rates`, authenticateMiddleware)
    // app.use(`${apiRoot}/v1/cycles`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/derivative-types`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/monthly-cycles`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/portfolios`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/portfolio-allocations`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/profiles`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/reasons`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/reference`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/transactions`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/prior-withdrawals`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/watch-lists`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/balances`, authenticateMiddleware)
    app.use(`${apiRoot}/v1/book-values`, authenticateMiddleware)
    app.put('*', cycleRunningMiddleware)
    app.post('*', cycleRunningMiddleware)
    app.delete('*', cycleRunningMiddleware)
    app.use(apiRoot, bodyParser.json({
      limit: '5mb'
    }))
    app.use(apiRoot, bodyParser.raw({
      limit: maxUploadChunkSize,
      type: 'application/octet-stream'
    }))
    authorizationMiddleware(app)
    // BUG WORKAROUND: https://github.com/apigee-127/sway/pull/114
    // app.put(
    //   `${apiRoot}/v1/batches/:id/business-units`,
    //   require('./api/batch-security/putBatchBusinessUnits')
    // )
    swagger.register(app)
    app.use(errorHandler)
    app.listen(port, () => log.info(`running: http://localhost:${port}`))
  }
})
