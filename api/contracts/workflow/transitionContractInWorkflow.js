const log = require('../../../log')
const schema = require('../contractSchema')
const updateContractStatus = require('./updateContractStatus')

module.exports = ({actionVerb, nextContractStatus, validateCanTransition, callBeforeChangingState = null}) => async (contractUserIdAndOptional) => {
  const {contract, userId} = contractUserIdAndOptional
  const contractId = contract.contractId
  const errorMessages = await validateCanTransition(contractUserIdAndOptional)
  if (!errorMessages || (Array.isArray(errorMessages) && errorMessages.length === 0)) {
    if (callBeforeChangingState) {
      await (callBeforeChangingState(contractUserIdAndOptional))
    }

    await updateContractStatus(contractId, userId, nextContractStatus, true)
    log.info({domain: schema.tableShort, id: contractId, userId, contractStatus: nextContractStatus}, 'contract status set')
  }
  return errorMessages
}
