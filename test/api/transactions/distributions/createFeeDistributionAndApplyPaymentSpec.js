/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/feeDistributions/feeDistributionsSchema')

const userId = 'somebody'

const dbFeeDistributionWithInvoiceId = {base: true}
const invoice = { invoiceId: 11 }
const dbTransaction = { transactionId: 55 }

describe('create fee distribution and apply payment', () => {
  let target, buildDefaultParams, insertIntoDb, applyPayment
  beforeEach(() => {
    buildDefaultParams = sinon.stub().returns('fd params')
    insertIntoDb = sinon.stub()
    applyPayment = sinon.stub()
    target = proxyquire('api/transactions/distributions/createFeeDistributionAndApplyPayment', {
      '../../common/buildDefaultParams': buildDefaultParams,
      '../../db/insertIntoDb': insertIntoDb,
      '../../invoices/applyPayment': applyPayment
    })
  })

  it('inserts fee distribution', async () => {
    const clientFeeDistribution = {feeDistributionId: 878}
    await target(99, clientFeeDistribution, dbFeeDistributionWithInvoiceId, null, dbTransaction, userId)
    insertIntoDb.should.have.been.calledWithExactly(schema, 'fd params')
  })

  it('does not apply payment if no invoice', async () => {
    const clientFeeDistribution = {feeDistributionId: 878}
    await target(99, clientFeeDistribution, dbFeeDistributionWithInvoiceId, null, dbTransaction, userId)
    applyPayment.should.have.not.been.called
  })

  it('applies payment with invoice', async () => {
    const clientFeeDistribution = {feeDistributionId: 878}
    await target(99, clientFeeDistribution, dbFeeDistributionWithInvoiceId, invoice, dbTransaction, userId)
    applyPayment.should.have.been.calledWithExactly(invoice, 0, 0, userId)
  })
})
