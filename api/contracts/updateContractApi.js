const createError = require('http-errors')
const log = require('../../log')
const buildUpdateParams = require('../common/buildUpdateParams')
const validateThenCallApi = require('../common/validateThenCallApi')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const updateDb = require('../db/updateDb')
const getUserId = require('../getUserId')
const updateContractEditHistories = require('../contractEditHistories/updateContractEditHistories')
const updateContractDerivatives = require('./derivativeTypes/updateContractDerivatives')
const recalculateForecasts = require('../creditingRates/forecasts/recalculateForecasts')
const validateContractPlanTypeRates = require('./validateContractPlanTypeRates')
const schema = require('./contractSchema')

const possiblyRecalculateForecasts = async (dbContract, body, postUpdatesDbContract, userId) => {
  const isActive = dbContract.status === 'ACTIVE'
  const frequencyChanged = (dbContract.creditingRateFrequency || body.creditingRateFrequency) && dbContract.creditingRateFrequency !== body.creditingRateFrequency
  const monthChanged = (dbContract.creditingRateMonth || body.creditingRateMonth) && dbContract.creditingRateMonth !== body.creditingRateMonth
  const firstResetChanged = (dbContract.creditingRateFirstReset || body.creditingRateFirstReset) && dbContract.creditingRateFirstReset !== body.creditingRateFirstReset

  const didForecastMetadataChange = isActive && (frequencyChanged || monthChanged || firstResetChanged)

  const dbForecastFields = {creditingRateFrequency: dbContract.creditingRateFrequency, creditingRateMonth: dbContract.creditingRateMonth, creditingRateFirstReset: dbContract.creditingRateFirstReset}
  const bodyForecastFields = {creditingRateFrequency: body.creditingRateFrequency, creditingRateMonth: body.creditingRateMonth, creditingRateFirstReset: body.creditingRateFirstReset}
  log.info({domain: schema.tableShort, id: dbContract.contractId, isActive, status: dbContract.status, didForecastMetadataChange, dbForecastFields, bodyForecastFields}, 'are we going to recalculate forecasts')
  if (didForecastMetadataChange) {
    await recalculateForecasts(postUpdatesDbContract, userId)
  }
}

const attemptUpdateApi = async (schema, body, res, next, {userId, contractId}) => {
  log.info({domain: schema.tableShort, id: contractId, userId}, 'attempt to update contract api')
  if (body && body.status === 'PENDING' && body.editHistories && body.editHistories.length > 1) {
    throw createError.BadRequest('New contracts or contracts in pending state can only have one edit history.')
  }
  const planTypesError = validateContractPlanTypeRates(body)
  if (planTypesError) {
    throw createError.BadRequest(planTypesError)
  }
  const dbContract = await selectByIdFromDb(schema, contractId)
  const skipUpdate = !(body && (body.status === 'ACTIVE' || body.status === 'PENDING_TERMINATION'))
  const updateParams = buildUpdateParams(schema.columns, userId, body)
  await updateDb(schema, updateParams, contractId, userId, skipUpdate)
  await updateContractEditHistories(updateParams, body, contractId)
  await updateContractDerivatives(body, contractId)

  const postUpdatesDbContract = await selectByIdFromDb(schema, contractId)

  await possiblyRecalculateForecasts(dbContract, body, postUpdatesDbContract, userId)

  return res.status(200).json(postUpdatesDbContract)
}

module.exports = async ({ params, headers, body }, res, next) => {
  log.info({domain: schema.tableShort, id: params.id}, 'update contract by id api')
  const userId = getUserId(headers)
  return await validateThenCallApi(schema, body, schema.required.update, res, next, attemptUpdateApi, {userId, contractId: params.id})
}
