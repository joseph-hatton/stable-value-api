
const target = require('../../../../api/db/converters/percentConverter')

describe('percent converter', () => {
  describe('to db', () => {
    it('not number', () => target.toDb('donkey').should.eql(NaN))

    it('1.2', () => target.toDb(1.2).should.eql(0.012))

    it('0.81', () => target.toDb(0.81).should.eql(0.0081))

    it('null', () => should.not.exist(target.toDb(null)))
  })

  describe('from db', () => {
    it('0.012', () => target.fromDb('0.012').should.eql(1.2))

    it('0.0081', () => target.fromDb('0.0081').should.eql(0.81))

    it('null', () => should.not.exist(target.fromDb(null)))

    it('-0.0001', () => target.fromDb('-0.0001').should.eql(-0.01))
  })
})
