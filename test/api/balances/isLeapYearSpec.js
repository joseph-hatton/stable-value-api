/* eslint-disable no-unused-expressions */
const isLeapYear = require('../../../api/balances/isLeapYear')

describe('isLeapYear', () => {
  it('should return true when the year is a leap year', () => {
    const result = isLeapYear(2016)

    result.should.be.true
  })
  it('should return false when the year is not a leap year', () => {
    const result = isLeapYear(2018)

    result.should.be.false
  })
})
