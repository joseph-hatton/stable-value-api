const dbExecuteOne = require('./dbExecuteOne')
const buildInsertChangeQuery = require('../queries/buildInsertChangeQuery')

module.exports = (connection, schema, rowId, userId, objectBeingChanged, operation) =>
  dbExecuteOne(connection, buildInsertChangeQuery(schema, rowId, userId, objectBeingChanged, operation))
