const buildCompaniesQueryWhereClause = require('../../../api/companies/buildCompaniesQueryWhereClause')
const {schema} = require('../../data/testSchema')

const nullPagination = {pageNumber: null, pageSize: null}
const DEFAULT_STATE = { bindParams: {}, whereClause: '', pagination: nullPagination }

describe('build companies query where clause', () => {
  it('handles empty schema and req', () => buildCompaniesQueryWhereClause(schema, {}).should.eql(DEFAULT_STATE))

  it('handles id path parameter', () => {
    return buildCompaniesQueryWhereClause(schema, {query: {name: 'hi'}}).should.eql({bindParams: {name: 'hi%'}, whereClause: 'WHERE lower(name) LIKE lower(:name)', pagination: nullPagination})
  })

  it('ignores other query parameters', () => {
    return buildCompaniesQueryWhereClause(schema, {query: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
