const dbExecuteInOrder = require('./dbExecuteInOrder')
const buildUpdatePropertyChangeQueries = require('../queries/buildUpdatePropertyChangeQueries')

module.exports = (connection, schema, id, objectBefore, bindParams) => {
  const changeQueries = buildUpdatePropertyChangeQueries(schema, id, objectBefore, bindParams)
  return (changeQueries.length > 0) ? dbExecuteInOrder(connection, changeQueries) : 'no property change queries'
}
