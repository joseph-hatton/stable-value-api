const proxyquire = require('../../proxyquire')
const {schema} = require('../../data/testSchema')
const expectedResult = {a: true}
const user = 'THE USER'
const body = {b: 'from request body'}
const THE_ID = 8675309

describe('get by id api', () => {
  const companies = [{ id: 'aId', name: 'a' }, { id: 'bId', name: 'b' }]
  const request = { headers: 'abc', params: {id: THE_ID} }

  let response, target, next, getUserId, responseStatusSend, responseJson, selectByIdFromDb

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    responseJson = sinon.spy()
    response = {
      json: responseJson,
      status: sinon.stub().returns({ send: responseStatusSend, json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    const query = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)
    selectByIdFromDb = sinon.stub().resolves(expectedResult)
    target = proxyquire('api/common/getByIdApi', {
      '../getUserId': getUserId,
      '../db/selectByIdFromDb': selectByIdFromDb
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
      request.body = body
    })

    it('sends expected args to select from db', () =>
      target(schema)(request, response, next)
        .then(() => {
          selectByIdFromDb.should.have.been.calledWithExactly(schema, THE_ID)
        })
    )

    it('sends get result to response', () =>
      target(schema)(request, response, next)
        .then((result) => {
          response.json.should.have.been.calledWithExactly(expectedResult)
        })
    )
  })
})
