const moment = require('moment')
const log = require('../../../log')

const DAY = 'day'
const DAILY = 'DAILY'
const MONTH = 'month'
const MONTHLY = 'MONTHLY'
const QUARTERLY = 'QUARTERLY'

const isValidCreditingRateMonth = (creditingRateMonth) => creditingRateMonth && !['NOT_APPLICABLE', 'UNKNOWN'].includes(creditingRateMonth)

const adjustStartDate = (contract, start) => {
  const adjustedStartDate = start.clone()
  if (contract.creditingRateFrequency === MONTHLY) {
    adjustedStartDate.startOf(MONTH)
  } else if (contract.creditingRateFrequency === QUARTERLY) {
    adjustedStartDate.startOf(MONTH)
    const creditingRateMonth = contract.creditingRateMonth
    if (isValidCreditingRateMonth(creditingRateMonth)) {
      const creditingRateMonthNumber = moment(creditingRateMonth, 'MMMM').month()
      while (adjustedStartDate.month() % 3 !== creditingRateMonthNumber % 3) {
        adjustedStartDate.add(1, MONTH)
      }
    }
  }
  return adjustedStartDate
}

const getFirstForecastDate = (contract) => {
  log.info({domain: 'contract', id: contract.contractId, creditingRateFrequency: contract.creditingRateFrequency, creditingRateMonth: contract.creditingRateMonth, creditingRateFirstReset: contract.creditingRateFirstReset}, 'getFirstForecastDate')
  let firstForecastDate = null
  if (contract.creditingRateFrequency === DAILY) {
    firstForecastDate = moment(contract.creditingRateFirstReset).utc()
  } else if ([MONTHLY, QUARTERLY].includes(contract.creditingRateFrequency)) {
    const creditingRateMonth = contract.creditingRateMonth
    if (isValidCreditingRateMonth(creditingRateMonth)) {
      const start = moment(contract.effectiveDate).utc().startOf(MONTH).startOf(DAY)

      const creditingRateMonthNumber = moment(creditingRateMonth, 'MMMM').month()
      log.info({domain: 'contract', id: contract.contractId, startMonth: start.month(), creditingRateMonthNumber: creditingRateMonthNumber}, 'creditingRateMonthNumber')
      while (start.month() !== creditingRateMonthNumber) {
        start.add(1, MONTH)
      }
      firstForecastDate = start
    } else {
      firstForecastDate = moment(contract.effectiveDate).utc()
    }
  }
  return firstForecastDate
}

const getForecastDates = (contract, startingDate) => {
  const start = moment(startingDate).utc().startOf(DAY)
  const end = start.clone().add(1, 'year').subtract(1, DAY).endOf(DAY)
  const adjustedStartDate = adjustStartDate(contract, start)
  const firstForecastDate = getFirstForecastDate(contract)

  const forecastDates = []
  let keepWorking = true
  let currentForecastDate = adjustedStartDate.clone()
  while (keepWorking) {
    if (currentForecastDate.isBefore(end) && currentForecastDate.isSameOrAfter(firstForecastDate)) {
      forecastDates.push(currentForecastDate.clone())
    }
    log.info({domain: 'contract', id: contract.contractId, currentForecastDate, end, firstForecastDate, creditingRateFrequency: contract.creditingRateFrequency, beforeEnd: currentForecastDate.isBefore(end), isSameOrAfter: currentForecastDate.isSameOrAfter(firstForecastDate)}, 'in while loop')
    if (contract.creditingRateFrequency === MONTHLY) {
      currentForecastDate.add(1, MONTH)
    } else if (contract.creditingRateFrequency === DAILY) {
      currentForecastDate.add(1, DAY)
    } else if (contract.creditingRateFrequency === QUARTERLY) {
      currentForecastDate.add(1, 'quarter')
    }
    keepWorking = currentForecastDate.isBefore(end)
  }
  return forecastDates
}

module.exports = (contract, startingDate = new Date()) => {
  log.info({domain: 'contract', id: contract.contractId}, 'calculate crediting rate forecast')
  return getForecastDates(contract, startingDate)
}
