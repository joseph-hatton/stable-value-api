const planRates = ['plan401ARate', 'plan401KRate', 'plan403BRate', 'plan457Rate', 'plan529Rate', 'plan501CRate', 'planTaftRate']

module.exports = (contractBody) => {
  let errorMessage = null
  if (contractBody && contractBody.status !== 'PENDING') {
    const sum = planRates.map(rate => contractBody[rate]).filter(rate => rate).reduce((accumulator, currentValue) => accumulator + currentValue)
    if (sum !== 100) {
      errorMessage = 'Plan type rates must total 100%.'
    }
  }
  return errorMessage
}
