/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/contracts/contractSchema')

const userId = 'a user'
const contractId = 5
describe('update contract status', () => {
  let target, buildUpdateParams, updateDb

  buildUpdateParams = sinon.stub().withArgs(schema.columns, userId, {status: 'SUBMITTED'}).returns('update params')
  updateDb = sinon.spy()
  beforeEach(() => {
    target = proxyquire('api/contracts/workflow/updateContractStatus', {
      '../../common/buildUpdateParams': buildUpdateParams,
      '../../db/updateDb': updateDb
    })
  })

  it('updates status', async () => {
    const result = await target(contractId, userId, 'SUBMITTED', true)
    updateDb.should.have.been.calledWithExactly(schema, 'update params', contractId, userId, true)
  })
})
