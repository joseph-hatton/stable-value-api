const {isDeployed} = require('../api/config')

let localUserEnabled = !isDeployed

const localUser = process.env.USERNAME || process.env.USER || 'unknown'

const getUserId = headers =>
  headers['user-id'] || (localUserEnabled ? localUser : undefined)

// noinspection JSUnusedGlobalSymbols
module.exports = Object.assign(
  getUserId,
  {
    setLocalUserEnabled: enabled => {
      localUserEnabled = enabled
    },
    localUser
  }
)
