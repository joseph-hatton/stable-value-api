module.exports = (idTextArray, errorMessage) =>
  (expectedIdString) =>
    idTextArray.filter((idAndText) => idAndText.id === expectedIdString).length !== 1 ? errorMessage : undefined
