/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')
const NUMBER_TYPE = 'number'
const BIND_OUT = 'bind out'
const params = {
  bColumn: 'bValue',
  cColumn: 'cValue'
}
const expectedParams = Object.assign({id: {type: NUMBER_TYPE, dir: BIND_OUT}}, params)
const autoCommit = {autoCommit: true}
const expectedResult = { id: 88 }

const sql = `INSERT INTO ${schema.table} VALUES (${schema.columns[0].sequence}, :${schema.columns[1].name}, :${schema.columns[2].name}) RETURNING ${schema.tableId}  INTO :id`

describe('build insert query', () => {
  let target, dbExecuteMultiple

  beforeEach(() => {
    const db = {
      TYPES: {
        NUMBER: NUMBER_TYPE
      },
      BIND_OUT: BIND_OUT
    }
    target = proxyquire('api/db/queries/buildInsertQuery', {
      '../../db': db
    })
  })

 describe('by id', () => {
  it('returns object with insert sql', () =>
    target(schema, params).sql.should.eql(sql))

  it('returns object with bindParams', () =>
    target(schema, params).bindParams.should.eql(Object.assign({id: {dir: BIND_OUT, type: NUMBER_TYPE}}, params)))

  it('returns object with executeOptions', () =>
    target(schema, params).executeOptions.should.eql({ autoCommit: true }))

  it('returns object with mapResult', () =>
    target(schema, params).mapResult({outBinds: {id: [5]}}).should.eql({ id: 5 }))

  it('throws not found error', () => {
    try {
      target(schema, params).mapResult({rowsAffected: 0})
    } catch (error) {
      error.should.eql(new Error('not found'))
    }
  })

  it('throws other exceptions', () => {
    try {
      target(schema, params).mapResult({error: {message: 'some other message'}})
    } catch (error) {
      error.should.eql(new Error('some other message'))
    }
  })
 })

 describe('not by id', () => {
  it('returns object with insert sql', () =>
    target(schema, params, false).sql.trim().should.eql('INSERT INTO aTable VALUES (aSequence, :bColumn, :cColumn)'))

  it('returns object with bindParams', () =>
    target(schema, params, false).bindParams.should.eql(params))

  it('returns object with executeOptions', () =>
    target(schema, params, false).executeOptions.should.eql({ autoCommit: true }))

  it('returns object with mapResult', () =>
    target(schema, params, false).mapResult({outBinds: {id: [5]}}).should.eql({outBinds: {id: [5]}}))

  it('throws not found error', () => {
    try {
      target(schema, params, false).mapResult({rowsAffected: 0})
    } catch (error) {
      error.should.eql(new Error('not found'))
    }
  })
 })
})
