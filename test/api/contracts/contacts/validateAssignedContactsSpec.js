/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/contracts/contacts/contactContractSchema')

const contractId = 5
const reportReceiversMessage = 'Contract should be assigned at least 1 Contact to receive each report.'
const planSponsorAndInvestmentManagerMessage = 'Contract should be assigned a Plan Sponsor and an Investment Manager.'

describe('validate assigned contacts', () => {
  let target, selectFromDb

  selectFromDb = sinon.stub()
  beforeEach(() => {
    target = proxyquire('api/contracts/contacts/validateAssignedContacts', {
      '../../db/selectFromDb': selectFromDb
    })
  })

  it('no contacts', async () => {
    selectFromDb.resolves([])
    const result = await target.validateCurrentlyAssigned(contractId)
    result.should.eql([])
  })
 it('no report receivers', async () => {
    selectFromDb.resolves([{contactType: 'INVESTMENT_MANAGER'}, {contactType: 'PLAN_SPONSOR'}])
    const result = await target.validateCurrentlyAssigned(contractId)
    result.should.eql([reportReceiversMessage])
  })
  it('no plan sponsor', async () => {
    selectFromDb.resolves([{contactType: 'INVESTMENT_MANAGER', receiveScheduleA: true, receiveStatement: true, receiveInvoice: true}])
    const result = await target.validateCurrentlyAssigned(contractId)
    result.should.eql([planSponsorAndInvestmentManagerMessage])
  })

  it('no errors with one contact receiving all', async () => {
    selectFromDb.resolves([{contactType: 'INVESTMENT_MANAGER', receiveScheduleA: true, receiveStatement: true, receiveInvoice: true}, {contactType: 'PLAN_SPONSOR'}])
    const result = await target.validateCurrentlyAssigned(contractId)
    result.should.eql([])
  })

  it('no errors with two contacts split receiving reports', async () => {
    selectFromDb.resolves([{contactType: 'INVESTMENT_MANAGER', receiveScheduleA: true, receiveStatement: true}, {contactType: 'PLAN_SPONSOR', receiveInvoice: true}])
    const result = await target.validateCurrentlyAssigned(contractId)
    result.should.eql([])
  })
})
