/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const {oldAppBaseUrl} = require('../../../api/config')

describe('cycle running middleware', () => {
  let target, res, jsonStub, next, getCycleStatus

  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    next = sinon.stub()
    getCycleStatus = sinon.stub()
    target = proxyquire('api/cycles/cycleRunningMiddleware', {
      '../getUserId': sinon.stub().returns('a user'),
      './getCycleStatus': getCycleStatus
    })
  })

  it('calls next when no cycle is running', async () => {
    getCycleStatus.resolves({isRunning: false})

    await target({}, res, next)

    next.should.have.been.called
  })

  it('does not call next when cycle is running', async () => {
    getCycleStatus.resolves({isRunning: true})

    await target({}, res, next)

    next.should.have.not.been.called
    res.status.should.have.been.calledWithExactly(503)
    jsonStub.should.have.been.calledWithExactly({message: 'Please wait, cycle in progress...'})
  })
})
