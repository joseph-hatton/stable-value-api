const target = require('../../../../api/db/queries/buildPropertyChangeBindParams')
const {schema} = require('../../../data/testSchema')

const CHANGE_ID = 81

describe('build property change bind param', () => {
  it('returns result', () =>
    target(schema, CHANGE_ID, 'a name', 'old', 'new').should.eql({changeId: CHANGE_ID, name: 'a name', oldValue: 'old', newValue: 'new', oldId: null, newId: null}))

  it('defaults values to null', () =>
    target(schema, CHANGE_ID, 'a name').should.eql({changeId: CHANGE_ID, name: 'a name', oldValue: null, newValue: null, oldId: null, newId: null}))
})
