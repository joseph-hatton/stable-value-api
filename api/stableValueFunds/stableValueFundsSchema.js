const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {dateConverter, numberConverter, percentConverter} = require('../db/converters')
const validateIdAndText = require('../common/validateIdTextField')
const {contractTypes, contractStatuses} = require('../referenceData.json')
const contractTypeValidator = validateIdAndText(contractTypes, 'Invalid Contract Type')

const table = 'stable_value_fund'
const tableId = 'stable_value_fund_id'
const sequence = `${dbSchemaName}.stable_value_fund_seq.NEXTVAL`

const zeroDefault = () => 0

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'contract_id',
  columns: [
    {name: 'stableValueFundId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
    {name: 'svBookValue', column: 'sv_book_value', converter: numberConverter},
    {name: 'svMarketValue', column: 'sv_market_value', converter: numberConverter},
    {name: 'svDuration', column: 'sv_duration', converter: numberConverter},
    {name: 'svCreditedRate', column: 'sv_credited_rate', converter: percentConverter},
    {name: 'svCashBuffer', column: 'sv_cash_buffer', converter: percentConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId
  ],
  required: {
    new: ['contractId', 'effectiveDate', 'svBookValue', 'svMarketValue', 'svDuration', 'svCreditedRate', 'svCashBuffer'],
    update: []
  }
}
