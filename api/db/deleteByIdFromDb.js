const log = require('../../log')
const changeOperations = require('../changes/changeOperations')
const dbExecutePromise = require('./executors/dbExecutePromise')
const executeSelectByIdQuery = require('./executors/executeSelectByIdQuery')
const executeDeleteByIdQuery = require('./executors/executeDeleteByIdQuery')
const executeInsertChange = require('./executors/executeInsertChangeQuery')
const executePropertyChanges = require('./executors/executeDeletePropertyChangeQueries')

const attemptDelete = async (connection, schema, rowId, userId) => {
  const auditingRequired = schema.columns.filter((column) => column.audit === true).length > 0

  let objectBefore
  if (auditingRequired) {
    objectBefore = await executeSelectByIdQuery(connection, schema, rowId)
  }
  const deleteResult = await executeDeleteByIdQuery(connection, schema, rowId)

  if (auditingRequired) {
    const executeDeleteChangeResponse = await executeInsertChange(connection, schema, rowId, userId, objectBefore, changeOperations.delete)
    await executePropertyChanges(connection, schema, executeDeleteChangeResponse.id, objectBefore)
  }
  return deleteResult
}

module.exports = (schema, rowId, userId) => {
  const dbPromiseChainCreator = (connection) => attemptDelete(connection, schema, rowId, userId)
  return dbExecutePromise(dbPromiseChainCreator)
}
