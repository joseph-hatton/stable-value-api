const createController = require('./createController')
const getByIdApi = require('../common/getByIdApi')
const schema = require('../contracts/contractSchema')
const transitionContractInWorkflowApi = require('../contracts/workflow/transitionContractInWorkflowApi')

const buildTransitionContractInWorkflowApi = (actionVerb, serviceFunctionPath) =>
  transitionContractInWorkflowApi({actionVerb, transitionContractInWorkflow: require(serviceFunctionPath)})

module.exports = createController({
  getMany: require('../contracts/queryContractsApi'),
  getOne: getByIdApi(schema),
  create: require('../contracts/createContractApi'),
  update: require('../contracts/updateContractApi'),
  delete: require('../contracts/deleteContractApi'),
  overrideModifiedId: require('../contracts/workflow/overrideContractModifiedIdApi'),
  submit: buildTransitionContractInWorkflowApi('submit', '../contracts/workflow/submitContract'),
  approve: buildTransitionContractInWorkflowApi('approve', '../contracts/workflow/approveContract'),
  reject: buildTransitionContractInWorkflowApi('reject', '../contracts/workflow/rejectContract'),
  inactivate: buildTransitionContractInWorkflowApi('inactivate', '../contracts/workflow/inactivateContract'),
  reactivate: buildTransitionContractInWorkflowApi('reactivate', '../contracts/workflow/reactivateContract')
})
