const moment = require('moment')
const calculateFutureMonthlyFees = require('../../../../api/balances/monthly/calculateFutureMonthlyFees')

describe('calculateFutureMonthlyFees', () => {
  describe('should calculate appropiately', () => {
    it('with positive premium rate', async () => {
      const balance = {
        balanceRate: '0.0229',
        endingBookValue: '1000000.00',
        endingFee: '20000.00',
        latestPremiumRate: '0.0018'
      }
      const date = moment('01-01-2018')

      await calculateFutureMonthlyFees(balance, date, true)

      balance.beginningBookValue = balance.beginningBookValue * 1
      balance.beginningFee = balance.beginningFee * 1
      balance.interest = balance.interest.toFixed(3) * 1
      balance.endingBookValue = balance.endingBookValue.toFixed(3) * 1
      balance.accruedFee = balance.accruedFee.toFixed(3) * 1
      balance.accruedFeeMtd = balance.accruedFeeMtd.toFixed(3) * 1
      balance.endingFee = balance.endingFee.toFixed(3) * 1

      balance.beginningBookValue.should.eql(1000000)
      balance.beginningFee.should.eql(20000)
      balance.interest.should.eql(62.034)
      balance.endingBookValue.should.eql(1000062.034)
      balance.accruedFee.should.eql(4.927)
      balance.accruedFeeMtd.should.eql(4.927)
      balance.endingFee.should.eql(20004.927)
    })
    it('with negative premium rate', async () => {
      const balance = {
        balanceRate: '0.0229',
        endingBookValue: '1000000.00',
        endingFee: '20000.00',
        latestPremiumRate: '-0.0018'
      }
      const date = moment('01-01-2018')

      await calculateFutureMonthlyFees(balance, date, true)

      balance.beginningBookValue = balance.beginningBookValue * 1
      balance.beginningFee = balance.beginningFee * 1
      balance.interest = balance.interest.toFixed(3) * 1
      balance.endingBookValue = balance.endingBookValue.toFixed(3) * 1
      balance.accruedFee = balance.accruedFee.toFixed(3) * 1
      balance.accruedFeeMtd = balance.accruedFeeMtd.toFixed(3) * 1
      balance.endingFee = balance.endingFee.toFixed(3) * 1

      balance.beginningBookValue.should.eql(1000000)
      balance.beginningFee.should.eql(20000)
      balance.interest.should.eql(62.034)
      balance.endingBookValue.should.eql(1000062.034)
      balance.accruedFee.should.eql(0)
      balance.accruedFeeMtd.should.eql(0)
      balance.endingFee.should.eql(20000)
    })
    it('for a whole month with negative premium rate', async () => {
      const balance = {
        balanceRate: '0.0229',
        endingBookValue: '1000000.00',
        endingFee: '20000.00',
        latestPremiumRate: '-0.0018'
      }
      const date = moment('01-31-2018')

      await calculateFutureMonthlyFees(balance, date, true)

      balance.beginningBookValue = balance.beginningBookValue.toFixed(3) * 1
      balance.beginningFee = balance.beginningFee.toFixed(3) * 1
      balance.interest = balance.interest.toFixed(3) * 1
      balance.endingBookValue = balance.endingBookValue.toFixed(3) * 1
      balance.accruedFee = balance.accruedFee.toFixed(3) * 1
      balance.accruedFeeMtd = balance.accruedFeeMtd.toFixed(3) * 1
      balance.endingFee = balance.endingFee.toFixed(3) * 1

      balance.beginningBookValue.should.eql(1001862.697)
      balance.beginningFee.should.eql(20000)
      balance.interest.should.eql(62.15)
      balance.endingBookValue.should.eql(1001924.846)
      balance.accruedFee.should.eql(0)
      balance.accruedFeeMtd.should.eql(0)
      balance.endingFee.should.eql(20000)
    })
    it('for a whole month with positive premium rate', async () => {
      const balance = {
        balanceRate: '0.0229',
        endingBookValue: '1000000.00',
        endingFee: '20000.00',
        latestPremiumRate: '0.0018'
      }
      const date = moment('01-31-2018')

      await calculateFutureMonthlyFees(balance, date, true)

      balance.beginningBookValue = balance.beginningBookValue.toFixed(3) * 1
      balance.beginningFee = balance.beginningFee.toFixed(3) * 1
      balance.interest = balance.interest.toFixed(3) * 1
      balance.endingBookValue = balance.endingBookValue.toFixed(3) * 1
      balance.accruedFee = balance.accruedFee.toFixed(3) * 1
      balance.accruedFeeMtd = balance.accruedFeeMtd.toFixed(3) * 1
      balance.endingFee = balance.endingFee.toFixed(3) * 1

      balance.beginningBookValue.should.eql(1001862.697)
      balance.beginningFee.should.eql(20147.955)
      balance.interest.should.eql(62.15)
      balance.endingBookValue.should.eql(1001924.846)
      balance.accruedFee.should.eql(4.937)
      balance.accruedFeeMtd.should.eql(152.891)
      balance.endingFee.should.eql(20152.891)
    })
    it('should use 365 for days in year when not leap year', async () => {
      const balance = {
        balanceRate: '.01',
        endingBookValue: '1',
        endingFee: '20000.00',
        latestPremiumRate: '0'
      }
      const date = moment('01-1-2018')

      await calculateFutureMonthlyFees(balance, date, true)

      balance.interest.should.eql(Math.pow(1.01, 1 / 365) - 1)
    })
    it('should use 365 for days in year when not leap year', async () => {
      const balance = {
        balanceRate: '.01',
        endingBookValue: '1',
        endingFee: '20000.00',
        latestPremiumRate: '0'
      }
      const date = moment('01-1-2016')

      await calculateFutureMonthlyFees(balance, date, true)

      balance.interest.should.eql(Math.pow(1.01, 1 / 366) - 1)
    })
  })
})
