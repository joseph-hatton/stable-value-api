/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/transactions/transactionSchema')
const contractSchema = require('../../../api/contracts/contractSchema')
const cloneDeep = require('lodash/cloneDeep')

const dbTransaction = {transactionId: 542, effectiveDate: 'yesterday', contractId: 11}
const CONTRACT_INSERT_PARAMS = {stuff: true}
const userId = 'me'
const HEADERS = 'HEADERS'
const contract = {contractId: 11}

describe('update transaction api', () => {
  let target, res, jsonStub, next, body, buildUpdateParams, getUserId, updateDb, selectByIdFromDb, validateThenCallApi, validateEffectiveDateInOpenMonth, updateAdjustments, updateDistributions
  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    next = sinon.spy()
    buildUpdateParams = sinon.stub().returns('updateParams')
    getUserId = sinon.stub().returns(userId)
    updateDb = sinon.stub().resolves(dbTransaction)
    selectByIdFromDb = sinon.stub().resolves(dbTransaction)
    validateEffectiveDateInOpenMonth = sinon.stub()
    updateAdjustments = sinon.stub()
    updateDistributions = sinon.stub()
    validateThenCallApi = sinon.stub().resolves('VALIDATION RESULT')
    target = proxyquire('api/transactions/updateTransactionApi', {
      '../common/buildUpdateParams': buildUpdateParams,
      '../common/validateThenCallApi': validateThenCallApi,
      '../db/updateDb': updateDb,
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../getUserId': getUserId,
      './validators/validateEffectiveDateInOpenMonth': validateEffectiveDateInOpenMonth,
      './adjustments/updateAdjustments': updateAdjustments,
      './distributions/updateDistributions': updateDistributions
    })
  })

  describe('locked transaction', () => {
    it('throws locked message', async () => {
      selectByIdFromDb.resolves({...dbTransaction, locked: true})
      try {
        await target({headers: HEADERS, params: {id: dbTransaction.transactionId}, body: {}}, res, next)
      } catch (err) {
        err.message.should.eql('This transaction is locked.')
      }
    })
  })

  describe('invalid effective date in open month', () => {
    it('throws specific message', async () => {
      validateEffectiveDateInOpenMonth.resolves('some specific message')
      try {
        await target({headers: HEADERS, params: {id: dbTransaction.transactionId}, body: {}}, res, next)
      } catch (err) {
        err.message.should.eql('some specific message')
      }
    })
  })

  describe('throws error if locked', () => {
    let response
    beforeEach(async () => {
      body = {}
      response = await target({headers: HEADERS, params: {id: dbTransaction.transactionId}, body}, res, next)
    })

    it('returns validation result', () => {
      response.should.eql('VALIDATION RESULT')
    })

    it('then calls api', () => {
      validateThenCallApi.should.have.been.called
    })

    it('then calls api with schema', () => {
      validateThenCallApi.getCall(0).args[0].should.eql(schema)
    })

    it('then calls api with body', () => {
      validateThenCallApi.getCall(0).args[1].should.eql(body)
    })

    it('then calls with required fields', () => {
      validateThenCallApi.getCall(0).args[2].should.eql(schema.required.new)
    })

    it('then calls with response', () => {
      validateThenCallApi.getCall(0).args[3].should.eql(res)
    })

    it('then calls with next', () => {
      validateThenCallApi.getCall(0).args[4].should.eql(next)
    })

    it('then calls with user id', () => {
      validateThenCallApi.getCall(0).args[6].should.eql({userId, transactionId: dbTransaction.transactionId, dbTransaction})
    })
  })

  describe('attempts to update', () => {
    let response

    beforeEach(async () => {
      body = {transactionType: 'DEPOSIT', transactionId: dbTransaction.transactionId, feeDistributions: ['a', 'b'], amount: 555}
      response = await target({headers: HEADERS, params: {id: dbTransaction.transactionId}, body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId, transactionId: dbTransaction.transactionId, dbTransaction})
    })

    it('calls update db with', () => {
      updateDb.should.have.been.calledWithExactly(schema, 'updateParams', dbTransaction.transactionId, userId)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.transactionId)
    })

    it('sends select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly(dbTransaction)
    })

    it('update distributions not called', () => {
      updateDistributions.should.have.not.been.called
    })

    it('update adjustments not called', () => {
      updateAdjustments.should.have.not.been.called
    })
  })

  describe('attempts to update fee receipt', () => {
    let response

    beforeEach(async () => {
      body = {transactionType: 'FEE_RECEIPT', transactionId: dbTransaction.transactionId, feeDistributions: ['a', 'b'], amount: 555}
      response = await target({headers: HEADERS, params: {id: dbTransaction.transactionId}, body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId, transactionId: dbTransaction.transactionId, dbTransaction})
    })

    it('calls update db with', () => {
      updateDb.should.have.been.calledWithExactly(schema, 'updateParams', dbTransaction.transactionId, userId)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.transactionId)
    })

    it('sends select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly(dbTransaction)
    })

    it('update distributions called', () => {
      updateDistributions.should.have.been.calledWithExactly(dbTransaction, body.feeDistributions, body.amount, userId)
    })

    it('update adjustments called', () => {
      updateAdjustments.should.have.been.calledWithExactly(dbTransaction, body, userId)
    })
  })

  describe('attempts to update termination', () => {
    let response

    beforeEach(async () => {
      body = {transactionType: 'TERMINATION', transactionId: dbTransaction.transactionId, amount: 555, statedMaturityDate: (new Date()).toISOString()}
      response = await target({headers: HEADERS, params: {id: dbTransaction.transactionId}, body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId, transactionId: dbTransaction.transactionId, dbTransaction})
    })

    it('calls update db with', () => {
      updateDb.should.have.been.calledWithExactly(schema, 'updateParams', dbTransaction.transactionId, userId)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.transactionId)
    })

    it('calls update contract with stated maturity date', () => {
      updateDb.should.have.been.calledWithExactly(contractSchema, 'updateParams', dbTransaction.contractId, userId)
    })

    it('sends select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly(dbTransaction)
    })

    it('update distributions called', () => {
      updateDistributions.should.have.not.been.called
    })

    it('update adjustments called', () => {
      updateAdjustments.should.have.not.been.called
    })
  })
})
