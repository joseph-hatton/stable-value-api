const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const sinonChai = require('sinon-chai')
const sinon = require('sinon')
const Chance = require('chance')

global.should = chai.should()
chai.use(chaiAsPromised)
chai.use(sinonChai)

global.sinon = sinon
global.testAsync = (done, funct) => {
  try {
    funct()
    done()
  } catch (e) {
    done(e)
  }
}
global.chance = new Chance()

if (typeof Object.values === 'undefined') {
  Object.values = require('lodash/values')
}
