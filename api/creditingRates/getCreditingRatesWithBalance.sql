SELECT  crediting_rate.*,
        NVL(crediting_rate.book_value, balance.ending_book_value) as book_value
FROM sch_stbv.crediting_rate crediting_rate
LEFT JOIN sch_stbv.balance balance
  ON crediting_rate.contract_id = balance.contract_id
  and crediting_rate.reference_date = balance.effective_date