require('require-sql')
const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId} = require('../common/auditColumns')
const {numberConverter} = require('../db/converters')
const getPorfolioFull = require('./getPorfolioFull.sql')

const table = 'portfolio'
const tableId = 'portfolio_id'
const sequence = `${dbSchemaName}.portfolio_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: tableId, //
  columns: [  //  order by the column ids in the table
    {name: 'portfolioId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'version', converter: numberConverter},
    {name: 'portfolioName', column: 'portfolio_name', audit: true},
    {name: 'portfolioManagerId', column: 'portfolio_manager_id', audit: true, converter: numberConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId
  ],
  overrideSelectClause: {
    selectOne: {sql: getPorfolioFull},
    selectMany: {sql: getPorfolioFull}
  },
  required: {
    new: ['portfolioManagerId', 'portfolioName']
  },
  audit: {
    objectClass: 'Portfolio',
      buildDescription: (portfolio) => `Name: ${portfolio.name}`
  }
}
