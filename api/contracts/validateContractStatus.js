const validateIdAndText = require('../common/validateIdTextField')
const {contractStatuses} = require('../referenceData.json')
module.exports = validateIdAndText(contractStatuses, 'Invalid Contract Status')
