const log = require('../../log')
const selectFromDb = require('../db/selectFromDb')
const schema = require('./riskScorecardsSchema')
const buildContractIdWhereClause = require('../contracts/buildContractIdWhereClause')
const getLookupsAndAdjustRiskScorecards = require('./getLookupsAndAdjustRiskScorecards')

const buildResponse = (things) => ({
  total: things.length,
  startId: (things.length > 0) ? things[0].riskScorecardId : null,
  limit: null,
  results: things
})

module.exports = async (req, res) => {
    log.info({domain: schema.tableShort, params: req.params}, 'query risk scorecards api')
    const riskScorecardsByContract = await selectFromDb(schema, buildContractIdWhereClause(schema, req))
    const adjustedScorecards = await getLookupsAndAdjustRiskScorecards(riskScorecardsByContract)
    res.json(buildResponse(adjustedScorecards))
  }
