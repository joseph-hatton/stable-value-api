const createError = require('http-errors')
const log = require('../../../log')
const updateDb = require('../../db/updateDb')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const getUserId = require('../../getUserId')
const buildUpdateParams = require('../../common/buildUpdateParams')
const contractSchema = require('../../contracts/contractSchema')
const schema = require('../creditingRatesSchema')

const canUpdateCreditingRate = (contract, creditingRate, userId) => {
  const updatable = contract && creditingRate && contract.status === 'ACTIVE' &&
    ((creditingRate.status === 'AWAITING' || creditingRate.status === 'FORECAST') ||
      (creditingRate.status === 'PENDING' && creditingRate.modifiedId === userId))
  log.info({domain: schema.tableShort, contract, creditingRate}, 'can we update crediting rate')
  return updatable
}

module.exports = async ({params: {contractId, id}, headers, body}, res, next) => {
  const creditingRateId = id
  const userId = getUserId(headers)
  log.info({domain: schema.tableShort, contractId, creditingRateId, userId}, 'update crediting rate api')

  const dbContract = await selectByIdFromDb(contractSchema, contractId)
  const dbCreditingRate = await selectByIdFromDb(schema, creditingRateId)

  if (canUpdateCreditingRate(dbContract, dbCreditingRate, userId)) {
    const {adjustment, creditingRateRateComments, grossRate, rate} = body
    const params = {adjustment, creditingRateRateComments, grossRate, rate}
    const updateParams = buildUpdateParams(schema.columns, userId, params)
    const result = await updateDb(schema, updateParams, creditingRateId, userId)
    return res.status(200).json(result)
  } else {
    throw createError.BadRequest('Only Awaiting, Forecast or Pending last modified by you Crediting Rates on Active Contracts can be updated.')
  }
}
