const schema = {
  table: 'aTable',
  tableId: 'aTableId',
  columns: [
    {name: 'aColumn', sequence: 'aSequence'},
    {name: 'bColumn'},
    {name: 'cColumn'}
  ],
  orderBy: 'bColumn'
}

module.exports = {
  schema
}
