const proxyquire = require('../../proxyquire')
const schema = require('../../../api/invoices/invoicesSchema')

const invoiceId = 55

describe('get invoice by id', () => {
  let target, selectByIdFromDb
  beforeEach(() => {
    selectByIdFromDb = sinon.stub().withArgs(schema, invoiceId).resolves({theInvoice: true})
    target = proxyquire('api/invoices/getInvoiceById', {
      '../db/selectByIdFromDb': selectByIdFromDb
    })
  })

  it('returns invoice', async () => {
    const result = await target()
    result.should.have.been.eql({theInvoice: true})
  })
})
