const proxyquire = require('../../proxyquire')
const schema = require('../../../api/balances/balancesSchema')

describe('get book value by contract and effectiveDate', () => {
  let target, dbExecutePromise, dbExecuteOne, effectiveDate, connection
  beforeEach(() => {
    effectiveDate = '2017-11-13T00:00:00.000Z'
    dbExecutePromise = sinon.stub()
    dbExecuteOne = sinon.stub()
    connection = sinon.stub()
    target = proxyquire('api/balances/getBookValueByContractAndEffectiveDate', {
      '../db/executors/dbExecutePromise': dbExecutePromise,
      '../db/executors/dbExecuteOne': dbExecuteOne
    })
  })

  it('returns book value', async () => {
    await target(500, effectiveDate)

    const dbPromise = dbExecutePromise.getCall(0).args[0]
    dbPromise(connection)

    const actualConnection = dbExecuteOne.getCall(0).args[0]
    const dbRequest = dbExecuteOne.getCall(0).args[1]

    actualConnection.should.eql(connection)
    dbRequest.sql.should.eql(`select ending_book_value from ${schema.table} where contract_id = :contractId and effective_date <= :effectiveDate and rownum = 1 order by effective_date desc`)
    dbRequest.bindParams.effectiveDate.should.eql(effectiveDate)
    dbRequest.bindParams.contractId.should.eql(500)
    dbRequest.executeOptions.should.eql({})

    const expectedBookValue = 73723742.3222
    const bookValue = dbRequest.mapResult({rows: [{ENDING_BOOK_VALUE: expectedBookValue}]})
    bookValue.should.eql(expectedBookValue)
  })
})
