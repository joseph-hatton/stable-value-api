
const target = require('../../../../api/db/converters/booleanConverter')

describe('boolean converter', () => {
  describe('to db', () => {
    it('not boolean', () => should.not.exist(target.toDb(12.34)))

    it('true', () => target.toDb(true).should.eql('Y'))

    it('false', () => target.toDb(false).should.eql('N'))

    it('null', () => should.not.exist(target.toDb(null)))
  })

  describe('from db', () => {
    it('Y', () => target.fromDb('Y').should.eql(true))

    it('N', () => target.fromDb('N').should.eql(false))

    it('null', () => should.not.exist(target.fromDb(null)))
  })
})
