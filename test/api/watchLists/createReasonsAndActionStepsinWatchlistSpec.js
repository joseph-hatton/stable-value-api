/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const SKIP_AUDIT = true
const params = {params: true}
const headers = ['header']
const body = {actionSteps: [{actionStepId: 1}, {actionStepId: 2}], reasons: [{reasonId: 1}, {reasonId: 2}, {reasonId: 3}]}
const schema = {}
const userId = 'user'
const watchListInsertResponse = { watchListId: 123 }
const actionStepWatchListSchema = 'actionStepWatchListSchema'
const reasonWatchListSchema = 'reasonWatchListSchema'
const actionStepWatchListInsertParams = 'actionStepWatchListInsertParams'
const reasonWatchListInsertParams = 'reasonWatchListInsertParams'
const selectResponse = 'selectResponse'

describe('create reasons and action steps in watchlist', () => {
  let target, validateThenCallApi, res, next, resJson, getUserId, buildWatchListInsertParams, insertIntoDb, buildActionStepWatchListInsertParams, buildReasonWatchListInsertParams, selectByIdFromDb
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    validateThenCallApi = sinon.stub().resolves('done')
    getUserId = sinon.stub().returns(userId)
    buildWatchListInsertParams = sinon.stub().returns(params)
    insertIntoDb = sinon.stub().resolves(watchListInsertResponse)
    buildActionStepWatchListInsertParams = sinon.stub().returns(actionStepWatchListInsertParams)
    buildReasonWatchListInsertParams = sinon.stub().returns(reasonWatchListInsertParams)
    selectByIdFromDb = sinon.stub().resolves(selectResponse)
    target = proxyquire('api/watchLists/createReasonsAndActionStepsinWatchlist', {
      '../common/validateThenCallApi': validateThenCallApi,
      '../getUserId': getUserId,
      './buildWatchListInsertParams': buildWatchListInsertParams,
      '../db/insertIntoDb': insertIntoDb,
      './buildActionStepWatchListInsertParams': buildActionStepWatchListInsertParams,
      './actionStepWatchListSchema': actionStepWatchListSchema,
      './reasonWatchListSchema': reasonWatchListSchema,
      './buildReasonWatchListInsertParams': buildReasonWatchListInsertParams,
      '../db/selectByIdFromDb': selectByIdFromDb
    })
  })
  it('calls validateThenCallApi', async () => {
    const result = await target(params, headers, body, res, next)
    validateThenCallApi.should.have.been.called
    result.should.eql('done')
  })
  describe('calling attemptInsertApi', () => {
    let attemptInsertApi
    beforeEach(async () => {
      await target(params, headers, body, res, next)
      attemptInsertApi = validateThenCallApi.getCall(0).args[5]
    })
    it('should throw error if body isn\'t set', async () => {
      try {
        await attemptInsertApi(schema, null, res, next, {userId})
      } catch (err) {
        err.message.should.eql("Body isn't set!")
      }
    })
    it('should call buildWatchListInsertParams', async () => {
      await attemptInsertApi(schema, body, res, next, {userId})
      buildWatchListInsertParams.should.have.been.called
    })
    it('should call insertIntoDb', async () => {
      await attemptInsertApi(schema, body, res, next, {userId})
      insertIntoDb.should.have.been.called
    })
    describe('for all action steps', () => {
      it('should call buildReasonWatchListInsertParams with watchListId and actionStepId', async () => {
        await attemptInsertApi(schema, body, res, next, {userId})
        buildActionStepWatchListInsertParams.should.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 1)
        buildActionStepWatchListInsertParams.should.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 2)
        buildActionStepWatchListInsertParams.should.not.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 3)
      })
      it('should call insertIntoDb actionStepWatchListSchema', async () => {
        await attemptInsertApi(schema, body, res, next, {userId})
        insertIntoDb.should.have.been.calledWith(actionStepWatchListSchema, actionStepWatchListInsertParams, userId, SKIP_AUDIT)
      })
    })
    describe('for all reasons', () => {
      it('should call buildReasonWatchListInsertParams with watchListId and reasonId', async () => {
        await attemptInsertApi(schema, body, res, next, {userId})
        buildReasonWatchListInsertParams.should.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 1)
        buildReasonWatchListInsertParams.should.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 2)
        buildReasonWatchListInsertParams.should.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 3)
        buildReasonWatchListInsertParams.should.not.have.been.calledWith(userId, body, watchListInsertResponse.watchListId, 4)
      })
      it('should call insertIntoDb reasonWatchListSchema', async () => {
        await attemptInsertApi(schema, body, res, next, {userId})
        insertIntoDb.should.have.been.calledWith(reasonWatchListSchema, reasonWatchListInsertParams, userId, SKIP_AUDIT)
      })
    })
    it('should call selectByIdFromDb', async () => {
      await attemptInsertApi(schema, body, res, next, {userId})
      selectByIdFromDb.should.have.been.called
    })
    it('should should call status with 201', async () => {
      await attemptInsertApi(schema, body, res, next, {userId})
      resJson.should.have.been.calledWith(selectResponse)
    })
  })
})
