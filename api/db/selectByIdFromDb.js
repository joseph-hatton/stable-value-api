const dbExecutePromise = require('./executors/dbExecutePromise')
const executeSelectByIdQuery = require('./executors/executeSelectByIdQuery')

module.exports = (schema, id) =>
  dbExecutePromise((connection) =>
    executeSelectByIdQuery(connection, schema, id))
