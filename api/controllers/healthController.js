const createController = require('./createController')

// noinspection JSUnusedGlobalSymbols
module.exports = createController({
  ping: require('../health/ping'),
  status: require('../health/status')
})
