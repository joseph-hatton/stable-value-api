const isAuthorized = require('../../../api/auth/isAuthorized')

describe('isAuthorized', () => {
  it('should return appropriate access', async () => {
    const wrappedDetermineEntitlements = sinon.stub().resolves({urlKey: {method: true}})

    const props = {
      urlKey: 'urlKey',
      method: 'method',
      user: 'user',
      wrappedDetermineEntitlements
    }

    const result = await isAuthorized(props)

    result.should.eql(true)
  })
  it('should return appropriate access', async () => {
    const wrappedDetermineEntitlements = sinon.stub().resolves({urlKey: {method: false}})

    const props = {
      urlKey: 'urlKey',
      method: 'method',
      user: 'user',
      wrappedDetermineEntitlements
    }

    const result = await isAuthorized(props)

    result.should.eql(false)
  })
})
