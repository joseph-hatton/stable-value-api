const Decimal = require('decimal.js')
const PERCENT = 100

module.exports = {
  toDb: (something) => (something) ? isNaN(something) ? NaN : Decimal(something).div(PERCENT).toNumber() : something,
  fromDb: (something) => (something) ? Decimal(something).mul(PERCENT).toNumber() : something
}
