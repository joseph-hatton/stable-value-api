const dbExecutePromise = require('./executors/dbExecutePromise')
const dbExecuteOne = require('./executors/dbExecuteOne')
const buildSelectQuery = require('./queries/buildSelectQuery')

module.exports = (schema, buildWhereClauseAndParams) =>
  dbExecutePromise((connection) =>
    dbExecuteOne(connection, buildSelectQuery(schema, true, buildWhereClauseAndParams)))
