const target = require('../../../api/states/stateValidator')

describe('state validator', () => {
  it('returns error message if state not found',
    () => target('ZZ').should.eql('Invalid State'))

  it('returns undefined if state found',
    () => should.not.exist(target('MO')))
})
