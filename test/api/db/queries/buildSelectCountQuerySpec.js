/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const testSchema = require('../../../data/testSchema')

const overrideSelectMany = {
  sql: 'select many from aTable',
  mapper: (rows) => 'many mapped results'
}

const overrideSelectOne = {
  sql: 'select one from aTable',
  mapper: (rows) => 'one mapped result'
}

const expectedCountSql = 'select count(*) from aTable  order by bColumn'

const paginationArgs = {pageSize: 2, pageNumber: 2}

describe('build select count query', () => {
  let target, convertRowResultFromDbValue, schema, sql, params
  beforeEach(() => {
    schema = Object.assign({}, testSchema.schema)
    params = {
      bColumn: 'bValue',
      cColumn: 'cValue'
    }
    sql = `INSERT INTO ${schema.table} VALUES (${schema.columns[0].sequence}, :${schema.columns[1].name}, :${schema.columns[2].name}) RETURNING ${schema.tableId} INTO :id`

    convertRowResultFromDbValue = sinon.stub().returns('fromDb1')
    target = proxyquire('api/db/queries/buildSelectCountQuery', {
      '../converters/convertRowResultFromDbValue': convertRowResultFromDbValue
    })
  })

  it('returns object with insert sql', () =>
    target(schema, true, params).sql.should.eql(expectedCountSql))

  it('returns object with default bindParams', () =>
    target(schema, true, params).bindParams.should.eql({}))

  it('returns object with default executeOptions', () =>
    target(schema, true, params).executeOptions.should.eql({ }))

  it('returns object with mapResult', () =>
    target(schema, true, params).mapResult({rows: [{aColumn: 8}]}).should.eql(['fromDb1']))

  const assertOverrideSelectUsed = (overrideSelectClause, sql, mappedRowsResult, pagination) => {
    it('returns object with custom schema sql if exists', () => {
      schema.overrideSelectClause = overrideSelectClause
      if (pagination) {
        params.pagination = pagination
      }
      target(schema, params, pagination).sql.should.eql(sql)
    })

    it('returns object with custom mapResult mapped', () => {
      schema.overrideSelectClause = overrideSelectClause
      if (pagination) {
        params.pagination = pagination
      }
      target(schema, params).mapResult({rows: [{aColumn: 8}]}).should.eql(mappedRowsResult)
    })
  }

  const assertOverrideSelectManyUsed = (shouldSelectMany, overrideSelectClause) => {
    assertOverrideSelectUsed(overrideSelectClause, expectedCountSql, [ 'fromDb1' ])
  }

  const assertOverrideSelectOneUsed = (shouldSelectMany, overrideSelectClause) => {
    assertOverrideSelectUsed(overrideSelectClause, expectedCountSql, [ 'fromDb1' ])
  }

  describe('selecting many', () => {
    describe('with custom schema select many', () => {
      assertOverrideSelectManyUsed({ selectMany: overrideSelectMany })
    })

    describe('falls back on select one if select many doesnt exist', () => {
      assertOverrideSelectOneUsed({ selectOne: overrideSelectOne })
    })

    describe('with pagination ignores pagination', () => {
      assertOverrideSelectUsed({ selectMany: overrideSelectMany }, expectedCountSql, 'many mapped results', paginationArgs)
    })
  })
})
