const moment = require('moment')
const dateConverter = require('./dateConverter')

const converteringApiValue = (apiValue) => moment(apiValue).utc().startOf('day').toDate()

module.exports = {
  toDb: (apiValue) => (apiValue !== null)
    ? converteringApiValue(apiValue)
    : null,
  fromDb: dateConverter.fromDb
}
