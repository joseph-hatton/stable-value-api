/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const moment = require('moment')

describe('Crediting Rates Pagination Query builder', () => {
  let target, buildWhereClauseAndParamsWithPaging, schema, queryParams

  beforeEach(() => {
    queryParams = sinon.stub()
    buildWhereClauseAndParamsWithPaging = sinon.stub().returns(queryParams)
    schema = sinon.stub()

    target = proxyquire('api/creditingRates/buildCreditingRatesQueryWhereClause', {
      '../common/buildWhereClauseAndParamsWithPaging': buildWhereClauseAndParamsWithPaging
    })
  })

  const newRequest = (param) => ({query: param})

  it('builds where clause params', () => {
    const request = {query: {}}
    const params = target(schema, request)

    params.should.eql(queryParams)
    request.query.pageNumber.should.eql(1)
    request.query.pageSize.should.eql(50)

    const args = buildWhereClauseAndParamsWithPaging.getCalls(0)[0].args

    args[0].should.eql(schema)
    args[1].should.eql(request)

    const whereClauseBuilders = args[2]

    // Contract Status Param Builder
    let whereClauseBits = []
    let response = {bindParams: {}}
    const whereContractStatusIn = whereClauseBuilders[0]
    whereContractStatusIn(schema, request, whereClauseBits, response)

    whereClauseBits[0].should.eql(`contract.status in ('PENDING_TERMINATION', 'ACTIVE')`)

    // Contract Ids (for a manger) Param Builder
    whereClauseBits = []
    response = {bindParams: {}}
    const whereContractIdIn = whereClauseBuilders[1]
    whereContractIdIn(schema, newRequest({contractIds: [1, 2]}), whereClauseBits, response)

    whereClauseBits[0].should.eql(`crediting_rate.contract_id in (1, 2)`)

    // Contract Id Param Builder
    whereClauseBits = []
    response = {bindParams: {}}
    const whereCreditingRateContractIdEquals = whereClauseBuilders[2]
    whereCreditingRateContractIdEquals(schema, newRequest({contractId: 3}), whereClauseBits, response)

    whereClauseBits[0].should.eql(`crediting_rate.contract_id = :contractId`)
    response.bindParams.contractId.should.eql(3)

    // Status Param Builder
    whereClauseBits = []
    response = {bindParams: {}}
    const whereStatusEquals = whereClauseBuilders[3]
    whereStatusEquals(schema, newRequest({status: 'Approved'}), whereClauseBits, response)

    whereClauseBits[0].should.eql(`crediting_rate.status = :status`)
    response.bindParams.status.should.eql('Approved')

    // Effective Date Param Builder
    verifyDateParamBuilder(whereClauseBuilders[4], 'fromEffectiveDate', '10-31-2012', 'effective_date', 'fromEffectiveDate', 'startOf', '>=')
    verifyDateParamBuilder(whereClauseBuilders[5], 'toEffectiveDate', '10-31-2012', 'effective_date', 'toEffectiveDate', 'endOf', '<=')
    verifyDateParamBuilder(whereClauseBuilders[6], 'fromReferenceDate', '10-31-2012', 'reference_date', 'fromReferenceDate', 'startOf', '>=')
    verifyDateParamBuilder(whereClauseBuilders[7], 'toReferenceDate', '10-31-2012', 'reference_date', 'toReferenceDate', 'endOf', '<=')

    // addOrderBy Param Builder
    whereClauseBits = []
    response = {bindParams: {}}
    const addOrderBy = whereClauseBuilders[9]
    addOrderBy(schema, newRequest({sortBy: 'marketValue', sortOrder: 'desc'}), whereClauseBits, response)

    whereClauseBits.should.eql([])
    response.should.eql({bindParams: {}, orderBy: 'crediting_rate.market_value DESC'})

    // Effective Date in near future Param Builder
    whereClauseBits = []
    response = {bindParams: {}}
    const whereEffectiveDateInNearFuture = whereClauseBuilders[8]
    whereEffectiveDateInNearFuture(schema, newRequest({
      sortBy: 'marketValue',
      sortOrder: 'desc'
    }), whereClauseBits, response)

    whereClauseBits[0].should.eql('crediting_rate.EFFECTIVE_DATE between sysdate and (sysdate + 31)')
  })

  const verifyDateParamBuilder = (paramBuilderFn, paramName, date, dbColumn, bindParamName, momentOf, comparator) => {
    const whereClauseBits = []
    const response = {bindParams: {}}
    paramBuilderFn(schema, newRequest({[bindParamName]: date}), whereClauseBits, response)

    whereClauseBits[0].should.eql(`crediting_rate.${dbColumn} ${comparator} :${bindParamName}`)
    response.bindParams[bindParamName].should.eql(moment(date)[momentOf]('day').toDate())
  }
})
