SELECT 
    b.effective_date as "effectiveDate",
    b.BEGINNING_FEE as "beginningFee",
    b.ACCRUED_FEE as "accruedFee",
    b.ACCRUED_FEE_MTD as "accruedFeeMtd",
    b.FEE_RECEIVED as "feeReceived",
    b.FEE_ADJUSTMENTS as "feeAdjustments",
    b.ENDING_FEE as "endingFee",
    b.BEGINNING_BOOK_VALUE as "beginningBookValue",
    b.ENDING_BOOK_VALUE as "endingBookValue",
    b.DEPOSITS as "deposits",
    b.WITHDRAWALS as "withdrawals",
    b.ADJUSTMENTS as "adjustments",
    b.INTEREST as "interest",
    b.RATE as "rate",
    b.RATE_EFFECTIVE_DATE as "rateEffectiveDate",
    i.invoice_number as "invoiceNumber",
    c.crediting_rate_round_decimals as "creditingRateRoundDecimals"
FROM SCH_STBV.BALANCE b
LEFT OUTER JOIN SCH_STBV.TRANSACTION t
    ON t.contract_id=b.contract_id 
    AND t.effective_date=b.effective_date
    AND t.transaction_type='FEE_RECEIPT'
LEFT OUTER JOIN SCH_STBV.INVOICE i
    ON i.contract_id=b.contract_id 
    AND i.invoice_date=b.effective_date
    AND t.effective_date=i.invoice_date
LEFT OUTER JOIN SCH_STBV.CONTRACT c
    ON c.contract_id=b.contract_id
WHERE b.effective_date BETWEEN 
    TO_DATE('<%= start %>', 'MM-DD-YYYY')
    AND
    TO_DATE('<%= end %>', 'MM-DD-YYYY')
    AND b.contract_id=<%= contractId %>
ORDER BY b.effective_date ASC