const createError = require('http-errors')
const log = require('../../../log')
const updateDb = require('../../db/updateDb')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const getUserId = require('../../getUserId')
const buildUpdateParams = require('../../common/buildUpdateParams')
const contractSchema = require('../../contracts/contractSchema')
const schema = require('../creditingRatesSchema')

const canClearCreditingRate = (contract, creditingRate, userId) => {
  const isActiveContract = contract && contract.status === 'ACTIVE'
  const isUnlockedCreditingRateNotForecastOrAwaiting = creditingRate && !creditingRate.locked &&
    ((creditingRate.status !== 'AWAITING' && creditingRate.status !== 'FORECAST'))
  const updatable = isActiveContract && isUnlockedCreditingRateNotForecastOrAwaiting
  log.info({domain: schema.tableShort, contract, creditingRate}, 'can we clear crediting rate')
  return updatable
}

module.exports = async ({params: {contractId, id}, headers, body}, res, next) => {
  const creditingRateId = id
  const userId = getUserId(headers)
  log.info({domain: schema.tableShort, contractId, creditingRateId, userId}, 'clear crediting rate api')

  const dbContract = await selectByIdFromDb(contractSchema, contractId)
  const dbCreditingRate = await selectByIdFromDb(schema, creditingRateId)

  if (canClearCreditingRate(dbContract, dbCreditingRate, userId)) {
    const params = {
      grossRate: null,
      rate: null,
      adjustment: null,
      premiumRate: null,
      managerFeeRate: null,
      advisorFeeRate: null,
      minimumNetCreditingRate: null,
      creditingRateCalcMethod: null,
      creditingRateRoundDecimals: null,
      creditingRateType: null,
      netOfThirdPartyFees: null,
      creditingRateManagerFee: null,
      creditingRateAdvisorFee: null,
      status: 'AWAITING'
    }
    const updateParams = buildUpdateParams(schema.columns, userId, params)
    const result = await updateDb(schema, updateParams, creditingRateId, userId)
    return res.status(200).json(result)
  } else {
    throw createError.BadRequest('Only Awaiting, Forecast or Pending last modified by you Crediting Rates on Active Contracts can be cleared.')
  }
}
