const log = require('../../log')
module.exports = async (creditingRateReferenceDate) => {
  log.warn({unimplemented: true}, 'check for market values is not implemented yet!')
  // look at ValuationService.checkForMarketValues() in the old app
  // this is called from updateCreditingRateApi.js  if the market value on save is different than in db
  // this returns a boolean for logging purposes...
  return false
}
