const {dbSchemaName} = require('../config')
const {numberConverter} = require('../db/converters')

const table = 'reason_watch_list'

const zeroDefault = () => 0
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: 'watch_list_id',
  orderBy: 'watch_list_id',
  columns: [
    {name: 'watchListId', column: 'watch_list_id', converter: numberConverter},
    {name: 'reasonId', column: 'reason_id', converter: numberConverter}
  ],
  required: {
    new: ['watchListId', 'reasonId'],
    update: ['watchListId', 'reasonId']
  }
}
