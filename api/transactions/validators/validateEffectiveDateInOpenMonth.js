const moment = require('moment')
const log = require('../../../log')
const isFutureMonth = require('../../monthlyCycles/isFutureMonth')
const isMonthOpen = require('../../monthlyCycles/isMonthOpen')
const schema = require('../transactionSchema')

const checkEffectiveDateNotInOpenMonth = async (clientTransaction) =>
  !await isMonthOpen(clientTransaction.effectiveDate)
      && (clientTransaction.transactionType === 'TERMINATION' || !await isFutureMonth(clientTransaction.effectiveDate))

const checkEffectiveDateAfterOrEqualToContractEffectiveDate = (clientTransaction, contract) => {
  const transactionEffDate = moment(clientTransaction.effectiveDate).utc()
  const contractEffDate = moment(contract.effectiveDate).utc()
  return transactionEffDate.isBefore(contractEffDate)
}

module.exports = async (transaction, contract) => {
  log.info({domain: schema.tableShort, transactionEffectiveDate: transaction.effectiveDate, contractEffectiveDate: contract.effectiveDate}, 'validate effective date in open month and after or equal to contract effective date')
  let error = null
  if (!transaction.effectiveDate) {
    error = 'The effective date is required.'
  } else if (checkEffectiveDateAfterOrEqualToContractEffectiveDate(transaction, contract)) {
    error = 'The effective date cannot be before the contract effective date.'
  } else if (await checkEffectiveDateNotInOpenMonth(transaction)) {
    error = 'The effective date must be for an open month.'
  }
  log.info({domain: schema.tableShort, transactionEffectiveDate: transaction.effectiveDate, contractEffectiveDate: contract.effectiveDate, error: error}, 'validated effective date in open month and after or equal to contract effective date')

  return error
}
