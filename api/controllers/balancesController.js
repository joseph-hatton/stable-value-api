const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const getByIdApi = require('../common/getByIdApi')
const schema = require('../balances/balancesSchema')
const whereClauseBuilder = require('../balances/buildContractIdAndEffectiveDateWhereClause')
const getDailyFees = require('../balances/daily/getDailyFees')
const getMonthlyFees = require('../balances/monthly/getMonthlyFees')

module.exports = createController({
  getMany: getManyApi(schema, whereClauseBuilder),
  getOne: getByIdApi(schema),
  getDaily: getDailyFees,
  getMonthly: getMonthlyFees
})
