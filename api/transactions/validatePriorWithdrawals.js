const Decimal = require('decimal.js')
const moment = require('moment')
const _ = require('lodash')

const log = require('../../log')

const selectByIdFromDb = require('../db/selectByIdFromDb')
const selectFromDb = require('../db/selectFromDb')

const contractSchemaOriginal = require('../contracts/contractSchema')
const contractSchema = {
  ...contractSchemaOriginal,
  overrideSelectClause: null
}
const transactionSchemaOriginal = require('../transactions/transactionSchema')
const transactionSchema = {
  ...transactionSchemaOriginal,
  overrideSelectClause: null
}

const getBookValueByContractAndEffectiveDate = require('../balances/getBookValueByContractAndEffectiveDate')

const OFF_LIMIT_MESSAGE = 'Total Withdrawal % will be equal to or more than the Corridor Option Limits-Annual. Are you sure you want to create this transaction?'
const IN_LIMIT_MESSAGE = 'Total Withdrawal % will be within 5% of the Corridor Option Limits-Annual. Are you sure you want to create this transaction?'

const {table: transactionTable} = transactionSchema
const withdrawalsWhereClauseBits = [
  `${transactionTable}.contract_id = :contractId`,
  `${transactionTable}.effective_date >= :beginningDate`,
  `${transactionTable}.effective_date <= :endDate`,
  `${transactionTable}.transaction_type = :transactionType`
]

const buildWithdrawalsClause = ({contractId, effectiveDate, corrOptionRollingMonths, transactionId, corrOptionCalcMethod}) => {
  const endDate = moment(effectiveDate).utc().endOf('day').toDate()

  let beginningDate
  if (corrOptionCalcMethod === 'ROLLING') {
    beginningDate = moment(endDate).subtract(corrOptionRollingMonths, 'months').add(1, 'day').startOf('day').toDate()
  } else {
    beginningDate = moment(endDate).startOf('year').toDate()
  }

  let bindParams = {
    contractId,
    beginningDate,
    endDate,
    transactionType: 'WITHDRAWAL'
  }

  if (transactionId != null) {
    bindParams = _.assign(bindParams, {transactionId})
    withdrawalsWhereClauseBits.push(`${transactionTable}.transaction_id != :transactionId`)
  }

  const whereClause = `WHERE ${withdrawalsWhereClauseBits.join(' AND ')}`
  return {whereClause, bindParams}
}

const getTotalWithdrawals = async ({contractId, effectiveDate, corrOptionRollingMonths, transactionId, corrOptionCalcMethod}) => {
  const whereClause = buildWithdrawalsClause({contractId, effectiveDate, corrOptionRollingMonths, transactionId, corrOptionCalcMethod})
  const withdrawals = await selectFromDb(transactionSchema, whereClause)

  log.info({
    domain: transactionSchema.tableShort,
    id: contractId,
    effectiveDate,
    withdrawals
  }, 'withdrawals contract id and an effective date')

  return (withdrawals || []).reduce((sum, {amount}) => sum + amount, 0)
}

const valid = (res) => res.status(200).json({
  confirmationRequired: false
})

const validatePriorWithdrawals = async (req, res) => {
  const {query: {contractId, effectiveDate, amount, transactionId}} = req

  const {corrOptionLimitsAnnual, corrOption, corrOptionRollingMonths, corrOptionCalcMethod} = await selectByIdFromDb(contractSchema, contractId)

  if (corrOption && corrOptionLimitsAnnual != null) {
    const bookValue = await getBookValueByContractAndEffectiveDate(contractId, effectiveDate)

    if (!bookValue) {
      return valid(res)
    }

    const pastWithdrawals = await getTotalWithdrawals({contractId, effectiveDate, corrOptionRollingMonths, transactionId, corrOptionCalcMethod})
    const totalWithdrawals = Math.abs(pastWithdrawals) + Math.abs(amount)

    const withdrawalPercent = Decimal(totalWithdrawals).div(bookValue).mul(100).toNumber()
    const limitDiff = corrOptionLimitsAnnual - withdrawalPercent

    const info = {
      domain: transactionSchema.tableShort,
      id: contractId,
      effectiveDate,
      bookValue,
      pastWithdrawals,
      totalWithdrawals,
      corrOptionLimitsAnnual,
      withdrawalPercent,
      limitDiff,
      corrOptionRollingMonths
    }

    log.info(info, 'prior withdrawals validation')

  let message
    if (limitDiff > 0.0 && limitDiff <= 5.0) {
      message = IN_LIMIT_MESSAGE
    } else if (limitDiff <= 0.0) {
      message = OFF_LIMIT_MESSAGE
    }
    if (message) {
      return res.status(200).json({
        confirmationRequired: true,
        message
      })
    }
  }

  return valid(res)
}

module.exports = validatePriorWithdrawals
