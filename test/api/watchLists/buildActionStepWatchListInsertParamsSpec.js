const proxyquire = require('../../proxyquire')
const target = require('../../../api/watchLists/buildActionStepWatchListInsertParams')

const user = 'a user'
const watchListId = 55
const actionStepId = 88

describe('build action step watch list insert params', () => {
  it('defaults all columns', () => {
    const params = target(user, {}, watchListId, actionStepId)
    params.should.eql({
      actionStepId,
      watchListId
    })
  })
})
