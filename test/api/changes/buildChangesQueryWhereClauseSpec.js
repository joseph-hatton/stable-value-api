const target = require('../../../api/changes/buildChangesQueryWhereClause')
const {schema} = require('../../data/testSchema')

const nullPagination = {pageNumber: null, pageSize: null}
const DEFAULT_STATE = { bindParams: {}, whereClause: '', pagination: nullPagination }

describe('build changes query where clause', () => {
  it('handles empty schema and req', () => target(schema, {}).should.eql(DEFAULT_STATE))

  it('handles objectClass query parameter', () => {
    return target(schema, {query: {objectClass: 'hi'}}).should.eql({ bindParams: {objectClass: 'hi'}, whereClause: 'WHERE lower(object_class) = lower(:objectClass)', pagination: nullPagination })
  })

  it('handles objectId query parameter', () => {
    return target(schema, {query: {objectId: 8}}).should.eql({ bindParams: {objectId: 8}, whereClause: 'WHERE object_id = :objectId', pagination: nullPagination })
  })

  it('handles objectClass and objectId query parameters', () => {
    return target(schema, {query: {objectClass: 'hi', objectId: 8}}).should.eql({ bindParams: {objectClass: 'hi', objectId: 8}, whereClause: 'WHERE lower(object_class) = lower(:objectClass) AND object_id = :objectId', pagination: nullPagination })
  })

  it('ignores other query parameters', () => {
    return target(schema, {path: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
