const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const whereContractIdEquals = (schema, req, whereClauseBits, response) => {
  if (req.params && req.params.contractId) {
    whereClauseBits.push(`${schema.table}.contract_id = :contractId`)
    response.bindParams.contractId = req.params.contractId * 1
  }
}

module.exports = (schema, req) =>
  buildWhereClauseAndParamsWithPaging(schema, req, [whereContractIdEquals])
// { whereClause, bindParams, executeOptions, pagination }
