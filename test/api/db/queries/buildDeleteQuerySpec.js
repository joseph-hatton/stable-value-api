/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const testSchema = require('../../../data/testSchema')
const params = {
  bColumn: 'bValue',
  cColumn: 'cValue'
}
const THE_ID = 8675309
const aResult = { id: [THE_ID] }

describe('build delete query', () => {
  let target, convertBindParamsToDbValues, schema
  beforeEach(() => {
    schema = Object.assign({}, testSchema.schema)
    convertBindParamsToDbValues = sinon.stub().returns({someBindParam: true})
    target = proxyquire('api/db/queries/buildDeleteQuery', {
      '../convertBindParamsToDbValues': convertBindParamsToDbValues
    })
  })

  it('returns object with update sql', () =>
    target(schema, THE_ID).sql.should.eql('delete from aTable where aTableId = :id'))

  it('returns object with default bindParams', () =>
    target(schema, THE_ID).bindParams.should.eql({id: THE_ID}))

  it('returns object with default executeOptions', () =>
    target(schema, THE_ID).executeOptions.should.eql({ autoCommit: true }))

  it('returns object with mapResult', () =>
    target(schema, THE_ID).mapResult(aResult).should.eql({id: THE_ID}))

  it('throws not found error', () => {
    try {
      target(schema, THE_ID).mapResult({rowsAffected: 0})
    } catch (error) {
      error.should.eql(new Error('not found'))
    }
  })

  it('throws other exceptions', () => {
    try {
      target(schema, THE_ID).mapResult({error: {message: 'some other message'}})
    } catch (error) {
      error.should.eql(new Error('some other message'))
    }
  })
})
