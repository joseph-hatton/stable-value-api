/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const contractSchemaOriginal = require('../../../api/contracts/contractSchema')
const contractSchema = {
  ...contractSchemaOriginal,
  overrideSelectClause: null
}
const transactionSchemaOriginal = require('../../../api/transactions/transactionSchema')
const transactionSchema = {
  ...transactionSchemaOriginal,
  overrideSelectClause: null
}

const contract = {
  corrOptionLimitsAnnual: 25,
  corrOption: true
}

describe('Validate Prior Withdrawals', () => {
  let target, res, selectByIdFromDb, selectFromDb, getBookValueByContractAndEffectiveDate, jsonStub

  beforeEach(() => {
    jsonStub = sinon.stub()
    res = {status: sinon.stub().returns({json: jsonStub})}
    selectByIdFromDb = sinon.stub()
    selectFromDb = sinon.stub()
    getBookValueByContractAndEffectiveDate = sinon.stub()

    target = proxyquire('api/transactions/validatePriorWithdrawals', {
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../db/selectFromDb': selectFromDb,
      '../balances/getBookValueByContractAndEffectiveDate': getBookValueByContractAndEffectiveDate
    })
  })

  it('No book value throws error', async () => {
    selectByIdFromDb.resolves(contract)
    selectFromDb.resolves([{amount: -3}, {amount: -5}])

    const req = {
      query: {
        contractId: 500,
        amount: 5,
        effectiveDate: '2018-01-01T00:00:00Z'
      }
    }

    try {
      await target(req, res)
    } catch (err) {
      err.message.should.eql('[DecimalError] Invalid argument: undefined')
    }
  })

  it('Contract Without corrOptionLimitsAnnual doesnt need prompt for withdrawal transaction', async () => {
    selectByIdFromDb.resolves({})

    const req = {
      query: {
        contractId: 500,
        amount: 5,
        effectiveDate: '2018-01-01T00:00:00Z'
      }
    }

    await target(req, res)

    jsonStub.should.have.been.calledWithExactly({confirmationRequired: false})
  })

  const bookValue = 100
  const withdrawals = [{amount: -3}, {amount: -5}]

  it('confirmation not required if total withdrawal percent including requested amount is more than 5% inside the limits', async () => {
    const req = {
      query: {
        contractId: 500,
        amount: 5,
        effectiveDate: '2018-01-01T00:00:00Z'
      }
    }

    const {query: {contractId, effectiveDate}} = req

    selectByIdFromDb.resolves(contract)
    getBookValueByContractAndEffectiveDate.withArgs(contractId, effectiveDate).returns(bookValue)
    selectFromDb.resolves(withdrawals)

    await target(req, res)

    jsonStub.should.have.been.calledWithExactly({confirmationRequired: false})
  })

  it('confirmation is required if total withdrawal percent including requested amount is within 5% inside the limits', async () => {
    const req = {
      query: {
        contractId: 500,
        amount: -15,
        effectiveDate: '2018-01-01T00:00:00Z'
      }
    }

    const {query: {contractId, effectiveDate}} = req

    selectByIdFromDb.resolves(contract)
    getBookValueByContractAndEffectiveDate.withArgs(contractId, effectiveDate).returns(bookValue)
    selectFromDb.resolves(withdrawals)

    await target(req, res)

    jsonStub.should.have.been.calledWithExactly({
      confirmationRequired: true,
      message: 'Total Withdrawal % will be within 5% of the Corridor Option Limits-Annual. Are you sure you want to create this transaction?'
    })
  })

  it('confirmation is required if total withdrawal percent including requested amount is over 5% of the limits', async () => {
    const req = {
      query: {
        contractId: 500,
        amount: -20,
        effectiveDate: '2018-01-01T00:00:00Z'
      }
    }

    const {query: {contractId, effectiveDate}} = req

    selectByIdFromDb.resolves(contract)
    getBookValueByContractAndEffectiveDate.withArgs(contractId, effectiveDate).returns(bookValue)
    selectFromDb.resolves(withdrawals)

    await target(req, res)

    jsonStub.should.have.been.calledWithExactly({
      confirmationRequired: true,
      message: 'Total Withdrawal % will be equal to or more than the Corridor Option Limits-Annual. Are you sure you want to create this transaction?'
    })
  })

  it('confirmation is required if total withdrawal percent including requested amount is over 5% of the limits 2', async () => {
    const req = {
      query: {
        contractId: 500,
        amount: -1000000,
        effectiveDate: '2018-01-01T00:00:00Z'
      }
    }

    const withdrawals = [{amount: -2653063.17}, {amount: -2000000.00}]

    const {query: {contractId, effectiveDate}} = req

    selectByIdFromDb.resolves({corrOptionLimitsAnnual: 5, corrOption: true})
    getBookValueByContractAndEffectiveDate.withArgs(contractId, effectiveDate).returns(53704391.086770490967319)
    selectFromDb.resolves(withdrawals)

    await target(req, res)

    jsonStub.should.have.been.calledWithExactly({
      confirmationRequired: true,
      message: 'Total Withdrawal % will be equal to or more than the Corridor Option Limits-Annual. Are you sure you want to create this transaction?'
    })
  })
})
