const moment = require('moment')
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

const CONTRACT_ID = 55
const USER_ID = 'somebody'

describe('create forecasts', () => {
  let target, effectiveDatesAsMoments, selectFromDb, insertIntoDb, buildDefaultParams
  beforeEach(() => {
    effectiveDatesAsMoments = [moment()]
    effectiveDatesAsMoments.push(effectiveDatesAsMoments[0].clone().add(1, 'day'))
    const selectFromDbArgs = { whereClause: `WHERE ${schema.table}.contract_id = :contractId`, bindParams: {contractId: CONTRACT_ID} }
    selectFromDb = sinon.stub().withArgs(schema, selectFromDbArgs).resolves([])
    buildDefaultParams = sinon.stub().returns({params: true})
    insertIntoDb = sinon.spy()
    target = proxyquire('api/creditingRates/forecasts/createForecasts', {
      '../../db/selectFromDb': selectFromDb,
      '../../db/insertIntoDb': insertIntoDb,
      '../../common/buildDefaultParams': buildDefaultParams
    })
  })

  it('insert into db called twice', async () => {
    const result = await target(CONTRACT_ID, effectiveDatesAsMoments, USER_ID)
    insertIntoDb.callCount.should.eql(2)
  })

  it('insert into db called with defauled params effectiveDate', async () => {
    const result = await target(CONTRACT_ID, effectiveDatesAsMoments, USER_ID)
    insertIntoDb.getCall(0).args[1].should.eql({params: true})
  })
})
