const getUserId = require('../getUserId')
const updateDb = require('../db/updateDb')
const buildUpdateParams = require('./buildUpdateParams')
const validateThenCallApi = require('./validateThenCallApi')
const log = require('../../log')

const attemptUpdateApi = async (schema, body, res, next, {userId, params}) => {
  const updateParams = buildUpdateParams(schema.columns, userId, body)
  const result = await updateDb(schema, updateParams, params.id, userId)
  return res.status(200).json(result)
}

module.exports = (schema) =>
  async ({ query, params, headers, body }, res, next) => {
    log.info({domain: schema.tableShort, id: params.id}, 'update by id api')
    const userId = getUserId(headers)
    const requiredColumns = (schema && schema.required && schema.required.update) ? schema.required.update : []
    return await validateThenCallApi(schema, body, requiredColumns, res, next, attemptUpdateApi, {userId, params})
  }
