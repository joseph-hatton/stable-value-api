const validateSchema = require('../validateSchema')
const target = require('../../../api/changes/propertyChangeSchema')
const {createdDate, createdId} = require('../../../api/common/auditColumns')
const {numberConverter} = require('../../../api/db/converters')

describe('property change schema', () => {
  validateSchema(target, {
    tableShort: 'property_change',
    table: 'sch_stbv.property_change',
    tableId: 'property_change_id',
    sequence: 'sch_stbv.property_change_seq.NEXTVAL',
    orderBy: 'property_change.property_change_id',
    columns: [
      {name: 'propertyChangeId', column: 'property_change_id', sequence: 'sch_stbv.property_change_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'changeId', column: 'change_id', immutable: true, converter: numberConverter},
      {name: 'name', immutable: true},
      {name: 'oldValue', column: 'old_value', immutable: true},
      {name: 'newValue', column: 'new_value', immutable: true},
      {name: 'oldId', column: 'old_id', immutable: true, converter: numberConverter},
      {name: 'newId', column: 'new_id', immutable: true, converter: numberConverter}
    ]
  })
})
