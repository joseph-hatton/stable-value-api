module.exports = (amount, transaction) => {
  let message
  if (transaction.transactionType === 'DEPOSIT' || transaction.transactionType === 'FEE_RECEIPT') {
    message = (amount >= 0.0) ? undefined : `Invalid Negative Amount for type ${transaction.transactionType}`
  } else if (transaction.transactionType === 'WITHDRAWAL' || transaction.transactionType === 'TERMINATION') {
    message = (amount <= 0.0) ? undefined : `Invalid Positive Amount for type ${transaction.transactionType}`
  }
  return message
}
