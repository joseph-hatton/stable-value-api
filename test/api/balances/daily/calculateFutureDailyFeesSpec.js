const moment = require('moment')
const proxyquire = require('../../../proxyquire')

const checkNumber = (variable, expectedValue) => {
  variable = variable.toFixed(3) * 1
  return variable.should.eql(expectedValue)
}

describe('calculateFutureDailyFees', () => {
  let contractId, balancesTable, target
  const simpleSelectFromDb = sinon.stub()
  beforeEach(() => {
    target = proxyquire('api/balances/daily/calculateFutureDailyFees', {
      '../../db/simpleSelectFromDb': simpleSelectFromDb
    })
    contractId = 123
    balancesTable = []
  })
  describe('should calculate appropiately', () => {
    it('with positive premium rate', async () => {
      const table = {
        results: [{
          contractId,
          balanceRate: '0.0229',
          endingBookValue: '1000000.00',
          endingFee: '20000.00',
          latestPremiumRate: '0.0018'
        }]
      }
      const date = moment('2018-01-01')
      simpleSelectFromDb.resolves(table)

      await target(date, contractId, balancesTable)

      checkNumber(balancesTable.results[0].beginningBookValue, 1000000)
      checkNumber(balancesTable.results[0].beginningFee, 20000)
      checkNumber(balancesTable.results[0].interest, 62.034)
      checkNumber(balancesTable.results[0].endingBookValue, 1000062.034)
      checkNumber(balancesTable.results[0].accruedFee, 4.927)
      checkNumber(balancesTable.results[0].accruedFeeMtd, 4.927)
      checkNumber(balancesTable.results[0].endingFee, 20004.927)

      checkNumber(balancesTable.results[15].beginningBookValue, 1000930.915)
      checkNumber(balancesTable.results[15].beginningFee, 20073.943)
      checkNumber(balancesTable.results[15].interest, 62.092)
      checkNumber(balancesTable.results[15].endingBookValue, 1000993.007)
      checkNumber(balancesTable.results[15].accruedFee, 4.932)
      checkNumber(balancesTable.results[15].accruedFeeMtd, 78.875)
      checkNumber(balancesTable.results[15].endingFee, 20078.875)

      checkNumber(balancesTable.results[30].beginningBookValue, 1001862.697)
      checkNumber(balancesTable.results[30].beginningFee, 20147.955)
      checkNumber(balancesTable.results[30].interest, 62.15)
      checkNumber(balancesTable.results[30].endingBookValue, 1001924.846)
      checkNumber(balancesTable.results[30].accruedFee, 4.937)
      checkNumber(balancesTable.results[30].accruedFeeMtd, 152.891)
      checkNumber(balancesTable.results[30].endingFee, 20152.891)
    })
    it('with negative premium rate', async () => {
      const table = {
        results: [{
          contractId,
          balanceRate: '0.0229',
          endingBookValue: '1000000.00',
          endingFee: '20000.00',
          latestPremiumRate: '-0.0018'
        }]
      }
      const date = moment('2018-01-01')
      simpleSelectFromDb.resolves(table)

      await target(date, contractId, balancesTable)

      checkNumber(balancesTable.results[0].beginningBookValue, 1000000)
      checkNumber(balancesTable.results[0].beginningFee, 20000)
      checkNumber(balancesTable.results[0].interest, 62.034)
      checkNumber(balancesTable.results[0].endingBookValue, 1000062.034)
      checkNumber(balancesTable.results[0].accruedFee, 0)
      checkNumber(balancesTable.results[0].accruedFeeMtd, 0)
      checkNumber(balancesTable.results[0].endingFee, 20000)

      checkNumber(balancesTable.results[15].beginningBookValue, 1000930.915)
      checkNumber(balancesTable.results[15].beginningFee, 20000)
      checkNumber(balancesTable.results[15].interest, 62.092)
      checkNumber(balancesTable.results[15].endingBookValue, 1000993.007)
      checkNumber(balancesTable.results[15].accruedFee, 0)
      checkNumber(balancesTable.results[15].accruedFeeMtd, 0)
      checkNumber(balancesTable.results[15].endingFee, 20000)

      checkNumber(balancesTable.results[30].beginningBookValue, 1001862.697)
      checkNumber(balancesTable.results[30].beginningFee, 20000)
      checkNumber(balancesTable.results[30].interest, 62.15)
      checkNumber(balancesTable.results[30].endingBookValue, 1001924.846)
      checkNumber(balancesTable.results[30].accruedFee, 0)
      checkNumber(balancesTable.results[30].accruedFeeMtd, 0)
      checkNumber(balancesTable.results[30].endingFee, 20000)
    })
  })
  it('should use 365 for days in year when not leap year', async () => {
    const table = {
      results: [{
        contractId,
        balanceRate: '.01',
        endingBookValue: '1',
        endingFee: '20000.00',
        latestPremiumRate: '0'
      }]
    }
    const date = moment('2018-01-01')
    simpleSelectFromDb.resolves(table)

    await target(date, contractId, balancesTable)

    balancesTable.results[0].interest.should.eql(Math.pow(1.01, 1 / 365) - 1)
  })
  it('should use 366 for days in year when leap year', async () => {
    const table = {
      results: [{
        contractId,
        balanceRate: '.01',
        endingBookValue: '1',
        endingFee: '20000.00',
        latestPremiumRate: '0'
      }]
    }
    const date = moment('2016-01-01')
    simpleSelectFromDb.resolves(table)

    await target(date, contractId, balancesTable)

    balancesTable.results[0].interest.should.eql(Math.pow(1.01, 1 / 366) - 1)
  })
})
