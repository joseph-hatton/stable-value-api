/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/contracts/derivativeTypes/contractDerivativeTypeSchema')

const userId = 'a user'
const contractId = 5

const derivativeType1 = {derivativeTypeId: 'type1'}
const derivativeType2 = {derivativeTypeId: 'type2'}
describe('update contract derivatives', () => {
  let target, body, deleteFromDb, selectFromDb, simpleInsertToDb

  beforeEach(() => {
    body = { status: 'ACTIVE' }

    deleteFromDb = sinon.spy()
    selectFromDb = sinon.stub().resolves([derivativeType1, derivativeType2])
    simpleInsertToDb = sinon.spy()

    target = proxyquire('api/contracts/derivativeTypes/updateContractDerivatives', {
      '../../db/deleteFromDb': deleteFromDb,
      '../../db/selectFromDb': selectFromDb,
      '../../db/simpleInsertToDb': simpleInsertToDb
    })
  })

  it('selects current derivative types from db', async () => {
    await target(body, contractId, userId)
    selectFromDb.should.have.been.calledWithExactly(schema, { whereClause: 'WHERE sch_stbv.contract_derivative_type.contract_id = :id', bindParams: {id: contractId} })
  })

  describe('body has same derivatives as db', () => {
    beforeEach(() => {
      body = {derivativeTypes: [derivativeType1, derivativeType2]}
    })
    it('delete current contract derivatives not called', async () => {
      await target(body, contractId, userId)
      deleteFromDb.should.have.not.been.called
    })
    it('insert contract derivatives not called', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.not.been.called
    })
  })

  describe('body has no derivatives', () => {
    beforeEach(() => {
      body = {derivativeTypes: []}
    })
    it('delete current contract derivatives called', async () => {
      await target(body, contractId, userId)
      deleteFromDb.should.have.been.calledWithExactly('delete from sch_stbv.contract_derivative_type where contract_id = :contractId', {contractId})
    })
    it('insert contract derivatives not called', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.not.been.called
    })
  })

  describe('body has different derivatives', () => {
    beforeEach(() => {
      body = {derivativeTypes: [{derivativeTypeId: 'type3'}]}
    })
    it('delete current contract derivatives called', async () => {
      await target(body, contractId, userId)
      deleteFromDb.should.have.been.calledWithExactly('delete from sch_stbv.contract_derivative_type where contract_id = :contractId', {contractId})
    })
    it('insert contract derivatives called once', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.been.calledOnce
    })
    it('insert contract derivatives called with type3', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.been.calledWithExactly(schema, {contractId, derivativeTypeId: 'type3'})
    })
  })

  describe('body some same but also different derivatives', () => {
    beforeEach(() => {
      body = {derivativeTypes: [derivativeType1, {derivativeTypeId: 'type3'}]}
    })
    it('delete current contract derivatives called', async () => {
      await target(body, contractId, userId)
      deleteFromDb.should.have.been.calledWithExactly('delete from sch_stbv.contract_derivative_type where contract_id = :contractId', {contractId})
    })
    it('insert contract derivatives called twice', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.been.calledTwice
    })
    it('insert contract derivatives called with type1', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.been.calledWithExactly(schema, {contractId, derivativeTypeId: 'type1'})
    })
    it('insert contract derivatives called with type3', async () => {
      await target(body, contractId, userId)
      simpleInsertToDb.should.have.been.calledWithExactly(schema, {contractId, derivativeTypeId: 'type3'})
    })
  })
})
