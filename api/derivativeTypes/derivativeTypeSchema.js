const {dbSchemaName} = require('../config')
const table = 'derivative_type'
const tableId = 'derivative_type_id'

const zeroDefault = () => 0

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  tableIdType: 'STRING',
  sequence: null,
  orderBy: tableId,
  columns: [
    {name: 'derivativeTypeId', column: tableId, pk: true, immutable: true},
    {name: 'description', column: 'description'}
  ],
  required: {
    new: ['derivativeTypeId', 'description'],
    update: ['description']
  }
}
