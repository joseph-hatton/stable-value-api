const {dbSchemaName} = require('../../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const {booleanConverter, numberConverter} = require('../../../api/db/converters')
const overrideGetManyContactContractsSelect = require('./overrideGetManyContactContractsSelect')

const table = 'contact_contract'
const tableId = 'contact_contract_id'
const sequence = `${dbSchemaName}.contact_contract_seq.NEXTVAL`

const zeroDefault = () => 0
const falseDefault = () => false
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: tableId,
  columns: [
    {name: 'contactContractId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'contactId', column: 'contact_id', converter: numberConverter},
    {name: 'receiveInvoice', column: 'receive_invoice', defaultValue: falseDefault, converter: booleanConverter},
    {name: 'receiveStatement', column: 'receive_statement', defaultValue: falseDefault, converter: booleanConverter},
    {name: 'receiveScheduleA', column: 'receive_schedule_a', defaultValue: falseDefault, converter: booleanConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId
  ],
  required: {
    new: ['contractId', 'contactId', 'receiveInvoice', 'receiveStatement', 'receiveScheduleA'],
    update: []
  },
  overrideSelectClause: {
    selectOne: overrideGetManyContactContractsSelect,
    selectMany: overrideGetManyContactContractsSelect
  }
}
