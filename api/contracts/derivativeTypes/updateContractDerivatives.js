const union = require('lodash/union')
const log = require('../../../log')
const deleteFromDb = require('../../db/deleteFromDb')
const selectFromDb = require('../../db/selectFromDb')
const simpleInsertToDb = require('../../db/simpleInsertToDb')
const schema = require('./contractDerivativeTypeSchema')

const toId = (type) => type.derivativeTypeId

const deleteSql = `delete from ${schema.table} where contract_id = :contractId`

const updateContractDerivativeTypes = async (contractId, userId, body, dbContractDerivativeTypes) => {
  const dbContractDerivativeTypesIds = (dbContractDerivativeTypes && dbContractDerivativeTypes.length > 0) ? dbContractDerivativeTypes.map(toId) : []
  const bodyDerivativeTypesIds = (body && body.derivativeTypes && body.derivativeTypes) ? body.derivativeTypes.map(toId) : []

  const uniqueTypes = union(dbContractDerivativeTypesIds, bodyDerivativeTypesIds)
  log.info({ dbContractDerivativeTypesIds, bodyDerivativeTypesIds, uniqueTypes }, 'update contract derivative types')
  if (dbContractDerivativeTypesIds.length !== bodyDerivativeTypesIds.length || uniqueTypes.length !== dbContractDerivativeTypesIds.length) {
    await deleteFromDb(deleteSql, {contractId})

    if (bodyDerivativeTypesIds.length > 0) {
      for (let derivativeTypeId of bodyDerivativeTypesIds) {
        await simpleInsertToDb(schema, {contractId, derivativeTypeId})
      }
    }
    return 'updated contract derivate types'
  } else {
    return 'no changes required for contract derivate types'
  }
}

module.exports = async (body, contractId, userId) => {
  log.info({domain: schema.tableShort, id: contractId}, 'update contract derivatives')
  const dbContractDerivativeTypes = await selectFromDb(schema, { whereClause: `WHERE ${schema.table}.contract_id = :id`, bindParams: {id: contractId} })
  return await updateContractDerivativeTypes(contractId, userId, body, dbContractDerivativeTypes)
}
