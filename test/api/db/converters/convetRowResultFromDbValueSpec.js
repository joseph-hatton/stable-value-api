const convert = require('../../../../api/db/converters/convertRowResultFromDbValue')

describe('convert row result from db values', () => {
  let rowResult, columns

  beforeEach(() => {
    rowResult = null
    columns = [
      {name: 'aColumn'},
      {name: 'bColumn'},
      {name: 'cColumn', converter: { fromDb: (value) => `${value}_converted` }},
      {name: 'dColumn', column: 'd_column'}
    ]
  })

  it('returns empty when passed empty', () => {
    rowResult = {}
    convert(columns, rowResult).should.eql(rowResult)
  })

  it('returns camelCased key when column not specified', () => {
    rowResult = {a_column: 'value one'}
    convert(columns, rowResult).should.eql({aColumn: 'value one'})
  })

  it('returns camelcased key when column not specified', () => {
    rowResult = {a_column: 'value one'}
    convert(columns, rowResult).should.eql({aColumn: 'value one'})
  })

  it('returns rowResult when no converter found', () => {
    rowResult = {a_column: 'value one', bColumn: 'value two'}
    convert(columns, rowResult).should.eql({aColumn: 'value one', bColumn: 'value two'})
  })

  it('returns modified rowResult when converter found', () => {
    rowResult = {aColumn: 'value one', cColumn: 'value two'}
    convert(columns, rowResult).should.eql({aColumn: 'value one', cColumn: 'value two_converted'})
  })

  it('converter is not hard coded', () => {
    columns[2].converter.fromDb = (value) => 'a different value'
    rowResult = {aColumn: 'value one', cColumn: 'value two'}
    convert(columns, rowResult).should.eql({aColumn: 'value one', cColumn: 'a different value'})
  })
})
