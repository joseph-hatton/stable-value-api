const mapInvoicesAndFees = require('../../../../api/balances/daily/mapInvoicesAndFees')

let table1 = {
  rows: [
    {
      effectiveDate: '2017-07-01T00:00:00.000Z',
      invoiceNumber: '123'
    },
    {
      effectiveDate: '2017-07-01T00:00:00.000Z',
      invoiceNumber: '123'
    }
  ]
}
let table2 = {
  rows: [
    {
      effectiveDate: '2017-07-02T00:00:00.000Z',
      invoiceNumber: '123'
    }
  ]
}
let table3 = {
  rows: [
    {
      effectiveDate: '2017-07-03T00:00:00.000Z',
      invoiceNumber: null
    }
  ]
}
let table4 = {
  rows: [
    {
      effectiveDate: '2017-07-01T00:00:00.000Z',
      invoiceNumber: '123'
    },
    {
      effectiveDate: '2017-07-01T00:00:00.000Z',
      invoiceNumber: '321'
    },
    {
      effectiveDate: '2017-07-02T00:00:00.000Z',
      invoiceNumber: '456'
    }
  ]
}

describe('mapInvoicesAndFees', () => {
  it('should setup invoiceNumbers array approiately for multiple', () => {
    const result = mapInvoicesAndFees(table1)
    result.total.should.eql(1)
    result.results[0].invoiceNumbers.length.should.eql(2)
  })
  it('should setup invoiceNumbers array approiately for one', () => {
    const result = mapInvoicesAndFees(table2)
    result.total.should.eql(1)
    result.results[0].invoiceNumbers.length.should.eql(1)
  })
  it('should setup invoiceNumbers array approiately for none', () => {
    const result = mapInvoicesAndFees(table3)
    result.total.should.eql(1)
    result.results[0].invoiceNumbers.length.should.eql(0)
  })
  it('should setup invoiceNumbers array approiately for mixed', () => {
    const result = mapInvoicesAndFees(table4)
    result.total.should.eql(2)
    result.results[0].invoiceNumbers.length.should.eql(2)
    result.results[1].invoiceNumbers.length.should.eql(1)
  })
})
