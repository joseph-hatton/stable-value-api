/* eslint-disable no-useless-escape */
const SUPPORT = 'app_stable_value_it_support'
const ADMIN = 'app_stable_value_ops_admin'
const COMPLIANCE = 'app_stable_value_compliance'
const RISK = 'app_stable_value_risk'
const ALL = [SUPPORT, ADMIN, COMPLIANCE, RISK]
const SUPPORT_COMPLIANCE = [SUPPORT, COMPLIANCE]
const ADMIN_SUPPORT = [ADMIN, SUPPORT]
const ADMIN_COMPLIANCE = [ADMIN, COMPLIANCE]
const ADMIN_COMPLIANCE_RISK = [ADMIN, COMPLIANCE, RISK]
const RISK_SUPPORT = [RISK, SUPPORT]
const ADMIN_ONLY = [ADMIN]
const RISK_ONLY = [RISK]

module.exports = {
  '/api/v1/companies(/\*)?': {
    get: ALL,
    post: ADMIN_COMPLIANCE,
    put: ADMIN_COMPLIANCE,
    delete: ADMIN_COMPLIANCE
  },
  '/api/v1/contacts(/\*)?': {
    get: ALL,
    post: ADMIN_COMPLIANCE,
    put: ADMIN_COMPLIANCE,
    delete: ADMIN_COMPLIANCE
  },
  '/api/v1/contracts/\*/contacts(/\*)?': {
    get: ALL,
    post: ADMIN_COMPLIANCE_RISK,
    put: ADMIN_COMPLIANCE,
    delete: ADMIN_COMPLIANCE
  },
  '^\/api\/v1\/contracts(\/\\w*)?$': {
    get: ALL,
    post: ADMIN_COMPLIANCE_RISK,
    put: ADMIN_COMPLIANCE,
    delete: ADMIN_COMPLIANCE
  },
  '/api/v1/crediting-rates(/\*)?': {
    get: ALL,
    post: ADMIN_ONLY,
    put: ADMIN_ONLY,
    delete: ADMIN_ONLY
  },
  '/api/v1/contracts/\*/crediting-rates(/\*)?': {
    get: ALL,
    post: ADMIN_ONLY,
    put: ADMIN_ONLY,
    delete: ADMIN_ONLY
  },
  '/api/v1/transactions(/\*)?': {
    get: ALL,
    post: ADMIN_ONLY,
    put: ADMIN_ONLY,
    delete: ADMIN_ONLY
  },
  '/api/v1/contracts/\*/stable-value-funds(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/contracts/\*/underwriting(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/contracts/\*/risk-scorecards/calculate-adjusted': {
    put: ALL
  },
  '^\/api\/v1\/contracts\/(\\d*)\/risk-scorecards((\/\\d*)|(!calculate-adjusted))?$': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/reasons(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/portfolios(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/portfolio-allocations(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/action-steps(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/watch-lists(/\*)?': {
    get: ALL,
    post: RISK_ONLY,
    put: RISK_ONLY,
    delete: RISK_ONLY
  },
  '/api/v1/contracts/\*/open-invoices': {
    get: ALL
  },
  '/api/v1/cycles/daily': {
    get: ADMIN_SUPPORT,
    post: ADMIN_ONLY,
    put: ADMIN_ONLY,
    delete: ADMIN_ONLY
  },
  '/api/v1/cycles/monthly': {
    get: ADMIN_SUPPORT,
    post: ADMIN_ONLY,
    put: ADMIN_ONLY,
    delete: ADMIN_ONLY
  },
  '/api/v1/cycles/monthly/finalize': {
    get: ADMIN_SUPPORT,
    post: ADMIN_ONLY,
    put: ADMIN_ONLY,
    delete: ADMIN_ONLY
  },
  '/api/v1/monthly-cycles': {
    get: ALL
  },
  '/api/v1/contracts/\*/balances(/\*)?': {
    get: ALL
  },
  '/api/v1/balances/monthly': {
    get: ALL
  },
  '/api/v1/balances/daily': {
    get: ALL
  }
}
