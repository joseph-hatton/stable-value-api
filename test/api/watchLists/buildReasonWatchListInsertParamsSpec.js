const proxyquire = require('../../proxyquire')
const target = require('../../../api/watchLists/buildReasonWatchListInsertParams')

const user = 'a user'
const watchListId = 88
const reasonId = 234

describe('build watch list insert params', () => {
  it('defaults all columns', () => {
    const params = target(user, {}, watchListId, reasonId)
    params.should.eql({
      reasonId: 234,
      watchListId: 88
    })
  })
})
