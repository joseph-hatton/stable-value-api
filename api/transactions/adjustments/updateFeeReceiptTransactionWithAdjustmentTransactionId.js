const log = require('../../../log')
const buildUpdateParams = require('../../common/buildUpdateParams')
const updateDb = require('../../db/updateDb')

const schema = require('../transactionSchema')

module.exports = async (feeReceiptTransactionId, adjustmentTransactionId, userId) => {
  log.info({domain: schema.tableShort, transactionId: feeReceiptTransactionId, adjustmentTransactionId, userId}, 'update fee receipt transaction with adjustment transaction id')
  const updateParams = buildUpdateParams(schema.columns, userId, {adjustmentTransactionId})
  await updateDb(schema, updateParams, feeReceiptTransactionId)
}
