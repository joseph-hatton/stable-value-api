const buildActionStepWatchListInsertParams = require('./buildActionStepWatchListInsertParams')
const actionStepWatchListSchema = require('./actionStepWatchListSchema')
const selectFromDb = require('../db/selectFromDb')
const deleteFromDb = require('../db/deleteFromDb')
const insertIntoDb = require('../db/insertIntoDb')
const log = require('../../log')
const union = require('lodash/union')

module.exports = async (body, watchListId, userId, SKIP_AUDIT) => {
    const deleteActionStepWatchListSql = `delete from ${actionStepWatchListSchema.table} where watch_list_id = :watchListId`
    const actionStepIds = (i) => i.actionStepId

    let actionStepsCurrentIds = await selectFromDb(actionStepWatchListSchema, { whereClause: `WHERE ${actionStepWatchListSchema.table}.watch_list_id = :id`, bindParams: {id: watchListId} })
    actionStepsCurrentIds = actionStepsCurrentIds.map(actionStepIds)
    let actionStepsNewIds = body.actionSteps.map(actionStepIds)
    let unionOfNewAndOldActionSteps = union(actionStepsCurrentIds, actionStepsNewIds)

    if (actionStepsCurrentIds.length !== unionOfNewAndOldActionSteps.length || actionStepsNewIds.length !== unionOfNewAndOldActionSteps.length) {
      await deleteFromDb(deleteActionStepWatchListSql, {watchListId})
      return await Promise.all(body.actionSteps.map(async (actionStep) => {
        const actionStepWatchListInsertParams = buildActionStepWatchListInsertParams(userId, body, watchListId, actionStep.actionStepId)
        return await insertIntoDb(actionStepWatchListSchema, actionStepWatchListInsertParams, userId, SKIP_AUDIT)
      }))
    }
}
