const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/changes/propertyChangeSchema')

const auditedSchema = {columns: [{audit: true, name: 'hat'}]}
const notAuditedSchema = {columns: [{audit: false}]}
const changeId = 55
const oldObject = {hat: 'Bowler'}
const bindParams = {bindParams: true}

describe('build delete property change queries', () => {
  let target, buildInsertQuery, buildPropertyChangeBindParams

  beforeEach(() => {
    const selectBind = {
      whereClause: `WHERE ${schema.table}.contract_id = :id`,
      bindParams: { id: 542 }
    }
    buildInsertQuery = sinon.stub().withArgs(schema, bindParams).returns('property change insert query')
    buildPropertyChangeBindParams = sinon.stub().returns(bindParams)
    target = proxyquire('api/db/queries/buildDeletePropertyChangeQueries', {
      './buildInsertQuery': buildInsertQuery,
      './buildPropertyChangeBindParams': buildPropertyChangeBindParams
    })
  })

  it('schema with no audit columns returns empty array', () => {
    target(notAuditedSchema, changeId, oldObject).should.eql([])
  })

  it('schema with audit columns returns insert queries', () => {
    const result = target(auditedSchema, changeId, oldObject).should.eql(['property change insert query'])
  })
})
