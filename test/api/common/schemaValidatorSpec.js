const target = require('../../../api/common/schemaValidator')
const testSchema = require('../../data/testSchema')
const cloneDeep = require('lodash/cloneDeep')

describe('schema validator', () => {
  let schema, body, validator1
  beforeEach(() => {
    schema = cloneDeep(testSchema.schema)
    body = {a: true}
    validator1 = sinon.stub().returns(undefined)
    schema.columns.push({name: 'validatedColumn', validators: [validator1]})
  })

  it('should not have any invalid results',
    () => target(schema, [], body).should.eql([]))

  it('should have one invalid results', () => {
    validator1.returns('an error')
    body = {validatedColumn: 'something'}
    target(schema, ['validatedColumn'], body).should.eql(['an error'])
  })

  it('validator called with', () => {
    validator1.returns('an error')
    body = {validatedColumn: 'something'}
    target(schema, ['validatedColumn'], body)
    validator1.should.have.been.calledWithExactly('something', body)
  })

  it('should return multiple errors', () => {
    schema.columns.push({name: 'validatedColumn2', validators: [sinon.stub().returns('another error')]})
    validator1.returns('an error')
    body = {validatedColumn: 'something', validatedColumn2: 'somethingElse'}
    target(schema, ['validatedColumn', 'validatedColumn2'], body).should.eql(['an error', 'another error'])
  })

  it('strips duplicates', () => {
    schema.columns.push({name: 'validatedColumn2', validators: [sinon.stub().returns('an error'), sinon.stub().returns('an error')]})
    validator1.returns('an error')
    body = {validatedColumn: 'something', validatedColumn2: 'somethingElse'}
    target(schema, ['validatedColumn', 'validatedColumn2'], body).should.eql(['an error'])
  })
})
