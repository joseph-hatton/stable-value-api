const log = require('../../../log')
const determineEditHistoryDataForEffectiveDate = require('../../contractEditHistories/determineEditHistoryDataForEffectiveDate')
const schema = require('../creditingRatesSchema')
const calculateGrossRate = require('./calculateGrossRate')
const calculateNetRate = require('./calculateNetRate')

module.exports = (dbCreditingRate, dbContract, body, balance, userId) => {
  const errors = []
  const params = Object.assign({}, dbCreditingRate)
  const effectDateInformation = determineEditHistoryDataForEffectiveDate(dbContract, body.effectiveDate)
  params.effectiveDate = body.effectiveDate
  params.locked = false
  params.minimumNetCreditingRate = effectDateInformation.minimumNetCreditingRate
  params.creditingRateCalcMethod = effectDateInformation.creditingRateCalcMethod
  params.creditingRateRoundDecimals = effectDateInformation.creditingRateRoundDecimals
  params.creditingRateType = effectDateInformation.creditingRateType
  params.netOfThirdPartyFees = effectDateInformation.netOfThirdPartyFees
  if (dbContract.creditingRateType === 'NET') {
    params.premiumRate = effectDateInformation.premiumRate
    if (params.premiumRate == null) {
      errors.push('Premium rate for contract was not found for effective date!')
    }
    params.creditingRateManagerFee = effectDateInformation.creditingRateManagerFee
    params.creditingRateAdvisorFee = effectDateInformation.creditingRateAdvisorFee
    if (params.creditingRateManagerFee) {
      params.managerFeeRate = effectDateInformation.managerFeeRate
      if (params.managerFeeRate == null) {
        errors.push('Manager fee rate for contract was not found for effective date!')
      }
    } else {
      params.managerFeeRate = null
    }
    if (params.creditingRateAdvisorFee) {
      params.advisorFeeRate = effectDateInformation.advisorFeeRate
      if (params.advisorFeeRate == null) {
        errors.push('Advisor fee rate for contract was not found for effective date!')
      }
    } else {
      params.advisorFeeRate = null
    }
  } else {
    params.creditingRateManagerFee = false
    params.creditingRateAdvisorFee = false
  }
  if (!params.bookValue) {
    log.warn({domain: schema.tableShort, contractId: dbContract.contractId, id: dbCreditingRate.creditingRateId}, 'we are probably not handling balances correctly here atm')
    params.bookValue = (dbCreditingRate.referenceDate === dbContract.effectiveDate) ?
      balance.beginningBookValue : balance.endingBookValue
  }
  if (errors.length === 0) {
    params.grossRate = calculateGrossRate(params) * 100
    params.rate = calculateNetRate(params)
    params.calculatedBy = userId
    params.status = 'PENDING'
  }
  return {errors, params}
}
