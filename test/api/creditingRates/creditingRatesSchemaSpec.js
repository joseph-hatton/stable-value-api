const validateSchema = require('../validateSchema')
const target = require('../../../api/creditingRates/creditingRatesSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')
const creditingRateStatusesValidator = require('../../../api/creditingRates/validators/validateCreditingRateStatus')
const creditingRateCalculationMethodsValidator = require('../../../api/creditingRates/validators/validateCreditingRateCalculateMethod')
const creditingRateTypesValidator = require('../../../api/creditingRates/validators/validateCreditingRateType')

const falseDefault = () => 'N'

describe('crediting rate schema', () => {
  validateSchema(target, {
    tableShort: 'crediting_rate',
    table: 'sch_stbv.crediting_rate',
    tableId: 'crediting_rate_id',
    sequence: 'sch_stbv.crediting_rate_seq.NEXTVAL',
    orderBy: 'crediting_rate.reference_date desc',
    columns: [
      {name: 'creditingRateId', column: 'crediting_rate_id', sequence: 'sch_stbv.crediting_rate_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'annualEffectiveYield', column: 'annual_effective_yield', audit: true, converter: percentConverter},
      {name: 'duration', audit: true, converter: numberConverter},
      {name: 'grossRate', column: 'gross_rate', audit: true, converter: percentConverter},
      {name: 'rate', audit: true, converter: percentConverter},
      {name: 'adjustment', audit: true, converter: percentConverter},
      {name: 'referenceDate', column: 'reference_date', audit: true, converter: beginningOfDayDateConverter},
      {name: 'effectiveDate', column: 'effective_date', audit: true, converter: beginningOfDayDateConverter},
      {name: 'status', validators: [creditingRateStatusesValidator]},
      createdDate,
      version,
      {name: 'premiumRate', column: 'premium_rate', audit: true, converter: bpsConverter},
      {name: 'managerFeeRate', column: 'manager_fee_rate', audit: true, converter: bpsConverter},
      {name: 'advisorFeeRate', column: 'advisor_fee_rate', audit: true, converter: bpsConverter},
      {name: 'creditingRateRoundDecimals', column: 'crediting_rate_round_decimals', audit: true, converter: numberConverter},
      {name: 'creditingRateCalcMethod', column: 'crediting_rate_calc_method', audit: true, validators: [creditingRateCalculationMethodsValidator]},
      {name: 'minimumNetCreditingRate', column: 'minimum_net_crediting_rate', audit: true, converter: numberConverter},
      {name: 'creditingRateType', column: 'crediting_rate_type', audit: true, validators: [creditingRateTypesValidator]},
      {name: 'netOfThirdPartyFees', column: 'net_of_third_party_fees', audit: true, converter: booleanConverter},
      {name: 'creditingRateManagerFee', column: 'crediting_rate_manager_fee', audit: true, converter: booleanConverter},
      {name: 'creditingRateAdvisorFee', column: 'crediting_rate_advisor_fee', audit: true, converter: booleanConverter},
      createdId,
      modifiedDate,
      modifiedId,
      {name: 'calculatedBy', column: 'calculated_by', audit: true},
      {name: 'bookValue', column: 'book_value', audit: true, converter: numberConverter},
      {name: 'locked', audit: true, defaultValue: falseDefault, converter: booleanConverter},
      {name: 'marketValue', column: 'market_value', audit: true, converter: numberConverter},
      {name: 'creditingRateRateComments', column: 'crediting_rate_rate_comments', audit: true}
    ],
    required: {
      new: ['contractId', 'status', 'locked'],
      update: ['contractId', 'status', 'locked']
    },
    audit: {
      objectClass: 'Crediting Rate',
      buildDescription: (creditingRate) => (creditingRate.referenceDate) ? `Ref Date: ${creditingRate.referenceDate}` : null
    }
  })
})
