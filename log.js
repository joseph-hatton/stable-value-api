const bunyan = require('bunyan')

const log = bunyan.createLogger({name: 'stable-value-api'})

module.exports = log
