/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const moment = require('moment')

const req = {query: {}}
const res = {status: () => ({json: (tbl) => tbl})}
const sql = 'sql'

describe('getDailyFees', () => {
  let target, simpleSelectFromDb, mapInvoicesAndFees, calculateFutureDailyFees, sqlCompiled, template
  beforeEach(() => {
    calculateFutureDailyFees = sinon.spy()
    simpleSelectFromDb = sinon.stub().resolves({results: []})
    sqlCompiled = sinon.stub()
    template = sinon.stub().returns(sqlCompiled)
    mapInvoicesAndFees = 'mapInvoicesAndFees'
    target = proxyquire('api/balances/daily/getDailyFees', {
      './getDailyFees.sql': sql,
      '../../db/simpleSelectFromDb': simpleSelectFromDb,
      './mapInvoicesAndFees': mapInvoicesAndFees,
      './calculateFutureDailyFees': calculateFutureDailyFees,
      'lodash/template': template
    })
  })
  it('should call simpleSelectFromDb', async () => {
    await target(req, res)

    simpleSelectFromDb.should.have.been.called
  })
  it('should pass mapInvoicesAndFees into simpleSelectFromDb', async () => {
    await target(req, res)

    simpleSelectFromDb.args[0][1].should.eql(mapInvoicesAndFees)
  })
  it('should pass in the correct sql to template', async () => {
    await target(req, res)

    template.args[0][0].should.eql(sql)
  })
  it('should pass in contract id and formatted date to sqlCompiled', async () => {
    req.query.dateFilter = moment('01-01-2018')
    req.query.contractId = 123

    await target(req, res)

    sqlCompiled.args[0][0].contractId.should.eql(123)
    sqlCompiled.args[0][0].start.should.eql('01-01-2018')
    sqlCompiled.args[0][0].end.should.eql('01-31-2018')
  })
  it('should call calculateFutureDailyFees if the chosen month is the next month', async () => {
    req.query.dateFilter = moment().add(1, 'month')

    await target(req, res)

    calculateFutureDailyFees.should.have.been.called
  })
})
