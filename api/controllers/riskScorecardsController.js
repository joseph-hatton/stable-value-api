const createController = require('./createController')
const updateByIdApi = require('../common/updateByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../riskScorecards/riskScorecardsSchema')

module.exports = createController({
  getMany: require('../riskScorecards/queryRiskScorecardsApi'),
  getOne: require('../riskScorecards/getRiskScorecardByIdApi'),
  create: require('../riskScorecards/createScorecardApi'),
  update: require('../riskScorecards/updateRiskScorecardByIdApi'),
  delete: deleteByIdApi(schema),
  calculateAdjusted: require('../riskScorecards/calculateAdjustedRiskScorecardApi')
})
