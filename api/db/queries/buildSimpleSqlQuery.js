module.exports = (sql, mapResult) => ({
  sql: sql,
  bindParams: {},
  executeOptions: {maxRows: 1000},
  mapResult: mapResult
})
