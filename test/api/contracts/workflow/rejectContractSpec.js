/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

const userId = 'a user'
const contractId = 5

describe('reject contract', () => {
  let target, transitionContractInWorkflow
  beforeEach(() => {
    transitionContractInWorkflow = sinon.stub().returns('base returned')
    target = proxyquire('api/contracts/workflow/rejectContract', {
      './transitionContractInWorkflow': transitionContractInWorkflow
    })
  })

  it('returns the base result', () => {
    target.should.eql('base returned')
  })

  it('base should have been called once', () => {
    transitionContractInWorkflow.should.have.been.called
  })

  it('sets action verb', () => {
    transitionContractInWorkflow.getCall(0).args[0].actionVerb.should.eql('reject')
  })

  it('sets next contract status', () => {
    transitionContractInWorkflow.getCall(0).args[0].nextContractStatus.should.eql('PENDING')
  })

  it('does not set a callBeforeChangingState', () => {
    should.not.exist(transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState)
  })

  describe('validate can transition', () => {
    let asyncValidate
    beforeEach(() => {
      asyncValidate = transitionContractInWorkflow.getCall(0).args[0].validateCanTransition
    })

    it('fails with PENDING contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'PENDING'}})

      result.should.eql('Only SUBMITTED Contracts may be rejected.')
    })

    it('passes with submitted contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'SUBMITTED'}})

      should.not.exist(result)
    })
  })
})
