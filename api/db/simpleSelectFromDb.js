const dbExecutePromise = require('./executors/dbExecutePromise')
const dbExecuteOne = require('./executors/dbExecuteOne')
const buildSimpleSql = require('./queries/buildSimpleSqlQuery')

module.exports = (sql, mapResult) =>
  dbExecutePromise((connection) =>
    dbExecuteOne(connection, buildSimpleSql(sql, mapResult)))
