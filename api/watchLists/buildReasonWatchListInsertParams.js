const buildDefaultParams = require('../common/buildDefaultParams')
const schema = require('./reasonWatchListSchema')

module.exports = (userId, body, watchListId, reasonId) =>
  buildDefaultParams(schema,
  body,
  {
    watchListId: watchListId,
    reasonId: reasonId
  })
