const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const whereCompanyNameLike = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.name) {
    whereClauseBits.push(`lower(name) LIKE lower(:name)`)
    response.bindParams.name = `${req.query.name}%`
  }
}

module.exports = (schema, req) =>
  buildWhereClauseAndParamsWithPaging(schema, req, [whereCompanyNameLike])
// { whereClause, bindParams, executeOptions, pagination }
