#!/usr/bin/env bash

export API_URL=https://stable-value-api-dev.rgare.net
export OAUTH_REALM_URL=https://stsdev.rgare.com/secureauth31
export VAULT_PATH=StableValue_dev
export VAULT_USER_ID=StableValue_dev_USER


if [ -z "$VAULT_APP_ID" ]
then
  echo 'Required vault password `VAULT_APP_ID` was not found in the environment!  Exiting.'
  $SHELL
fi