/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const contractManagers = {
  550: {
    managerId: 1550,
    name: 'personA'
  },
  600: {
    managerId: 1600,
    name: 'personB'
  },
  700: {
    managerId: 1550,
    name: 'personA'
  }
}

const creditingRates = [
  {contractId: 550, rate: 1.4},
  {contractId: 600, rate: 1.6}
]

describe('query transaction api', () => {
  let target,
    req,
    res,
    selectFromDbWithPagination,
    buildCreditingRatesQueryWhereClause,
    overrideGetManyCreditingRateWithPaginationSelect,
    getContractManagers,
    whereClause

  beforeEach(async () => {
    res = {json: sinon.stub()}
    whereClause = sinon.stub()
    buildCreditingRatesQueryWhereClause = sinon.stub().returns(whereClause)
    selectFromDbWithPagination = sinon.stub().resolves({results: creditingRates})
    overrideGetManyCreditingRateWithPaginationSelect = 'overrideGetManyCreditingRateWithPaginationSelect'
    getContractManagers = sinon.stub().returns(contractManagers)

    target = proxyquire('api/creditingRates/queryCreditingRatesApi', {
      '../db/selectFromDbWithPagination': selectFromDbWithPagination,
      './buildCreditingRatesQueryWhereClause': buildCreditingRatesQueryWhereClause,
      './overrideGetManyCreditingRateWithPaginationSelect': overrideGetManyCreditingRateWithPaginationSelect,
      '../contracts/getContractManagers': getContractManagers
    })
  })

  const isManager = (actual, expectedId, expectedName) => {
    actual.managerId.should.eql(expectedId)
    actual.managerName.should.eql(expectedName)
  }

  const assertIsPersonA = (actual) => isManager(actual, 1550, 'personA')
  const assertIsPersonB = (actual) => isManager(actual, 1600, 'personB')

  it('returns crediting rates mapped with manager id for given request', async () => {
    req = {
      query: {}
    }
    await target(req, res)

    const schema = buildCreditingRatesQueryWhereClause.getCall(0).args[0]
    schema.overrideSelectClause.selectMany.should.eql(overrideGetManyCreditingRateWithPaginationSelect)
    const response = res.json.getCall(0).args[0]

    assertIsPersonA(response.results[0])
    assertIsPersonB(response.results[1])
  })

  it('uses manager id to find contracts and their crediting rates', async () => {
    req = {query: {managerId: 1550}}
    await target(req, res)

    const schema = buildCreditingRatesQueryWhereClause.getCall(0).args[0]
    schema.overrideSelectClause.selectMany.should.eql(overrideGetManyCreditingRateWithPaginationSelect)
    const response = res.json.getCall(0).args[0]

    req.query.contractIds.should.eql(['550', '700'])
    assertIsPersonA(response.results[0])
    assertIsPersonA(response.results[1])
  })

  it('sorts by managerName', async () => {
    req = {query: {sortBy: 'managerName'}}
    await target(req, res)

    const schema = buildCreditingRatesQueryWhereClause.getCall(0).args[0]
    schema.overrideSelectClause.selectMany.should.eql(overrideGetManyCreditingRateWithPaginationSelect)
    const response = res.json.getCall(0).args[0]

    assertIsPersonA(response.results[0])
    assertIsPersonB(response.results[1])
  })

  it('sorts by managerName asc', async () => {
    req = {query: {sortBy: 'managerName', sortOrder: 'asc'}}
    await target(req, res)

    const schema = buildCreditingRatesQueryWhereClause.getCall(0).args[0]
    schema.overrideSelectClause.selectMany.should.eql(overrideGetManyCreditingRateWithPaginationSelect)
    const response = res.json.getCall(0).args[0]

    assertIsPersonA(response.results[0])
    assertIsPersonB(response.results[1])
  })

  it('sorts by managerName desc', async () => {
    req = {query: {sortBy: 'managerName', sortOrder: 'desc'}}
    await target(req, res)

    const schema = buildCreditingRatesQueryWhereClause.getCall(0).args[0]
    schema.overrideSelectClause.selectMany.should.eql(overrideGetManyCreditingRateWithPaginationSelect)
    const response = res.json.getCall(0).args[0]

    assertIsPersonB(response.results[0])
    assertIsPersonA(response.results[1])
  })
})
