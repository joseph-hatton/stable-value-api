const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/transactions/transactionSchema')

const dbTransaction = {
  adjustmentTransactionId: 5,
  contractId: 88,
  effectiveDate: 'yesterday'
}

const userId = 'somebody'

describe('update fee receipt transaction with adjustment transaction id', () => {
  let target, updateDb, buildUpdateParams
  beforeEach(() => {
    updateDb = sinon.stub().resolves('done')
    buildUpdateParams = sinon.stub().returns('transaction params')
    target = proxyquire('api/transactions/adjustments/updateFeeReceiptTransactionWithAdjustmentTransactionId', {
      '../../db/updateDb': updateDb,
      '../../common/buildUpdateParams': buildUpdateParams
    })
  })

  it('update db called with', async () => {
    await target(5, 8, userId)
    updateDb.should.have.been.calledWithExactly(schema, 'transaction params', 5)
  })
})
