const target = require('../../../../api/creditingRates/forecasts/calculateCreditingRateForecastDates')

const contractId = 5
const baseContract = {contractId, effectiveDate: '2017-01-01T00:00:00.000Z'}

describe('calculate crediting rate forecast', () => {
  let startDate, contract

  describe('for daily', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'DAILY', creditingRateFirstReset: startDate}, baseContract)
    })
    it('validate the first result', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql(startDate)
    })
    it('validate the last result', () => {
      const result = target(contract, startDate)
      result[result.length - 1].toISOString().should.eql('2017-12-31T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(365)
    })
  })

  describe('for daily with reset date in future', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'DAILY', creditingRateFirstReset: '2017-02-01T00:00:00.000Z'}, baseContract)
    })
    it('validate the first result', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql('2017-02-01T00:00:00.000Z')
    })
    it('validate the last result', () => {
      const result = target(contract, startDate)
      result[result.length - 1].toISOString().should.eql('2017-12-31T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(334)
    })
  })

  describe('for daily with first reset date a little later', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'DAILY', creditingRateFirstReset: '2017-01-03T00:00:00.000Z'}, baseContract)
    })
    it('validate the first result', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql('2017-01-03T00:00:00.000Z')
    })
    it('validate the last result', () => {
      const result = target(contract, startDate)
      result[result.length - 1].toISOString().should.eql('2017-12-31T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(363)
    })
  })

  describe('for monthly', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'MONTHLY'}, baseContract)
    })
    it('validate the first result', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql(startDate)
    })
    it('validate the last result', () => {
      const result = target(contract, startDate)
      result[result.length - 1].toISOString().should.eql('2017-12-01T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(12)
    })
  })

  describe('for monthly with rate month', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'MONTHLY', creditingRateMonth: 'FEBRUARY'}, baseContract)
    })
    it('validate the first result', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql('2017-02-01T00:00:00.000Z')
    })
    it('validate the last result', () => {
      const result = target(contract, startDate)
      result[result.length - 1].toISOString().should.eql('2017-12-01T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(11)
    })
  })

  describe('for quarterly', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'QUARTERLY', creditingRateMonth: 'JANUARY'}, baseContract)
    })
    it('validate the results', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql(startDate)
      result[1].toISOString().should.eql('2017-04-01T00:00:00.000Z')
      result[2].toISOString().should.eql('2017-07-01T00:00:00.000Z')
      result[3].toISOString().should.eql('2017-10-01T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(4)
    })
  })

  describe('for quarterly with later rate month', () => {
    beforeEach(() => {
      startDate = '2017-01-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'QUARTERLY', creditingRateMonth: 'FEBRUARY'}, baseContract)
    })
    it('validate the results', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql('2017-02-01T00:00:00.000Z')
      result[1].toISOString().should.eql('2017-05-01T00:00:00.000Z')
      result[2].toISOString().should.eql('2017-08-01T00:00:00.000Z')
      result[3].toISOString().should.eql('2017-11-01T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(4)
    })
  })

  describe('for quarterly with later rate month', () => {
    beforeEach(() => {
      startDate = '2017-03-01T00:00:00.000Z'
      contract = Object.assign({creditingRateFrequency: 'QUARTERLY', creditingRateMonth: 'FEBRUARY'}, baseContract)
    })
    it('validate the results', () => {
      const result = target(contract, startDate)
      result[0].toISOString().should.eql('2017-05-01T00:00:00.000Z')
      result[1].toISOString().should.eql('2017-08-01T00:00:00.000Z')
      result[2].toISOString().should.eql('2017-11-01T00:00:00.000Z')
      result[3].toISOString().should.eql('2018-02-01T00:00:00.000Z')
    })
    it('validate the length', () => {
      const result = target(contract, startDate)
      result.length.should.eql(4)
    })
  })
})
