/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')
const contractSchema = require('../../../../api/contracts/contractSchema')

const userId = 'a user'
const headers = {headers: true}
const params = {contractId: 55, id: 42}
const updateParams = {updateParams: true}
const body = {adjustment: 1.1, rate: 11.1, creditingRateRateComments: 'some comments'}
const cantMessage = 'Only Awaiting, Forecast or Pending last modified by you Crediting Rates on Active Contracts can be updated.'

describe('update crediting rate', () => {
  let target, res, resJson, updateDb, selectByIdFromDb, getUserId, buildUpdateParams
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    updateDb = sinon.stub().resolves('update db response')
    selectByIdFromDb = sinon.stub()
    selectByIdFromDb.withArgs(contractSchema, 55).resolves({status: 'ACTIVE'})
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'PENDING', modifiedId: userId, rate: 10, adjustment: 1, grossRate: 12})
    getUserId = sinon.stub().withArgs(headers).returns(userId)
    buildUpdateParams = sinon.stub().withArgs(schema.columns, userId, body).returns(updateParams)
    target = proxyquire('api/creditingRates/creditingRate/updateCreditingRateApi', {
      '../../db/updateDb': updateDb,
      '../../db/selectByIdFromDb': selectByIdFromDb,
      '../../getUserId': getUserId,
      '../../common/buildUpdateParams': buildUpdateParams
    })
  })

  it('tests update db called when pending crediting rate and same user updates', async () => {
    await target({params, headers, body}, res)
    updateDb.should.have.been.calledWithExactly(schema, updateParams, 42, userId)
    resJson.should.have.been.calledWithExactly('update db response')
  })

  it('tests update db called when forecast', async () => {
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'FORECAST', modifiedId: 'anybody', rate: 10, adjustment: 1, grossRate: 12})
    await target({params, headers, body}, res)
    updateDb.should.have.been.calledWithExactly(schema, updateParams, 42, userId)
    resJson.should.have.been.calledWithExactly('update db response')
  })

  it('throws error if contract not active', async () => {
    selectByIdFromDb.withArgs(contractSchema, 55).resolves({status: 'PENDING'})
    try {
      await target({params, headers, body}, res)
    } catch (err) {
      err.message.should.eql(cantMessage)
    }
  })

  it('throws error if crediting rate is not pending', async () => {
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'APPROVED', modifiedId: 'somebody else'})
    try {
      await target({params, headers, body}, res)
    } catch (err) {
      err.message.should.eql(cantMessage)
    }
  })

  it('throws error if crediting rate was last modified by same user', async () => {
    selectByIdFromDb.withArgs(schema, 42).resolves({status: 'PENDING', modifiedId: userId, rate: 10, adjustment: 1, grossRate: 12})
    try {
      await target({params, headers, body}, res)
    } catch (err) {
      err.message.should.eql(cantMessage)
    }
  })
})
