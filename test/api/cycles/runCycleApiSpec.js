/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const {oldAppBaseUrl} = require('../../../api/config')
const contractNumber = 'RGA00888'

describe('run cycle api', () => {
  let target, res, jsonStub, httpPost, httpPostQuery

  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    // httpPost = sinon.stub().resolves({body: {running: true}})
    httpPostQuery = sinon.stub().resolves({body: {running: true}})
    httpPost = sinon.stub().returns({query: httpPostQuery})
    target = proxyquire('api/cycles/runCycleApi', {
      'superagent': {post: httpPost}
    })
  })

  describe('daily cycle', () => {
    it('returns result', async () => {
      await target()({query: {}}, res)

      jsonStub.should.have.been.calledWithExactly({running: true})
    })

    it('executes run', async () => {
      await target()({query: {}}, res)

      httpPost.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/dailyCycle/external`)
    })

    it('executes run with contract', async () => {
      await target()({query: {contractNumber}}, res)

      httpPostQuery.should.have.been.calledWithExactly({contractNumber})
    })
  })

  describe('preliminary monthly cycle', () => {
    it('sends cycle data to json response', async () => {
      await target('monthlyPreliminary')({query: {}}, res)

      jsonStub.should.have.been.calledWithExactly({running: true})
    })

    it('executes run', async () => {
      await target('monthlyPreliminary')({query: {}}, res)

      httpPost.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/monthlyCycle/external`)
    })

    it('executes run with statements and invoices', async () => {
      await target('monthlyPreliminary')({query: {}, body: {statements: true, invoices: true}}, res)

      httpPostQuery.should.have.been.calledWithExactly({statements: true, invoices: true})
    })
  })

  describe('finalize monthly cycle', () => {
    it('sends cycle data to json response', async () => {
      await target('monthlyFinalize')({query: {}}, res)

      jsonStub.should.have.been.calledWithExactly({running: true})
    })

    it('executes run', async () => {
      await target('monthlyFinalize')({query: {}}, res)

      httpPost.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/monthlyCycle/finalize`)
    })
  })
})
