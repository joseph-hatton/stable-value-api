/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

const userId = 'a user'
const contractId = 5

describe('schedule pending termination contract', () => {
  let target, transitionContractInWorkflow, deleteForecasts, buildUpdateParams, updateDb
  beforeEach(() => {
    deleteForecasts = sinon.stub()
    transitionContractInWorkflow = sinon.stub().returns('base returned')
    buildUpdateParams = sinon.stub().returns('update params')
    updateDb = sinon.stub()
    target = proxyquire('api/contracts/workflow/schedulePendingTerminationContract', {
      './transitionContractInWorkflow': transitionContractInWorkflow,
      '../../creditingRates/forecasts/deleteForecasts': deleteForecasts,
      '../../common/buildUpdateParams': buildUpdateParams,
      '../../db/updateDb': updateDb
    })
  })

  it('returns the base result', () => {
    target.should.eql('base returned')
  })

  it('base should have been called once', () => {
    transitionContractInWorkflow.should.have.been.called
  })

  it('sets action verb', () => {
    transitionContractInWorkflow.getCall(0).args[0].actionVerb.should.eql('schedulePendingTermination')
  })

  it('sets next contract status', () => {
    transitionContractInWorkflow.getCall(0).args[0].nextContractStatus.should.eql('PENDING_TERMINATION')
  })

  it('sets a callBeforeChangingState', () => {
    should.exist(transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState)
  })

  describe('validate can transition', () => {
    let asyncValidate
    beforeEach(() => {
      asyncValidate = transitionContractInWorkflow.getCall(0).args[0].validateCanTransition
    })

    it('fails with PENDING contract', async () => {
      const result = await asyncValidate({contract: {status: 'PENDING'}})

      result.should.eql('Only ACTIVE Contracts may be scheduled for pending termination.')
    })

    it('passes with ACTIVE contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'ACTIVE'}})

      should.not.exist(result)
    })
  })

  describe('callBeforeChangingState', () => {
    let callBeforeChangingState
    beforeEach(() => {
      callBeforeChangingState = transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState
    })

    it('calls calculate and save forecasts', async () => {
      const terminationDate = (new Date().toISOString)
      await callBeforeChangingState({contract: {contractId, status: 'PENDING'}, userId, terminationDate})

      deleteForecasts.should.have.been.calledWithExactly(contractId, terminationDate, userId)
    })

    it('updates contract with stated maturity date', async () => {
      const terminationDate = (new Date().toISOString)
      const statedMaturityDate = (new Date().toISOString)
      await callBeforeChangingState({contract: {contractId, status: 'PENDING'}, userId, terminationDate, statedMaturityDate})

      buildUpdateParams.should.have.been.called
      buildUpdateParams.getCall(0).args[2].should.eql({statedMaturityDate})
    })
  })
})
