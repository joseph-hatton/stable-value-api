const toColumns = require('../converters/toColumns')
const convertBindParamsToDbValues = require('../converters/convertBindParamsToDbValues')

const buildSetSql = (columns, params) =>
  toColumns(columns, params)
    .map((paramWithColumn) => `${paramWithColumn.column} = :${paramWithColumn.key}`)
    .join(', ')

module.exports = (schema, bindParams, id) => ({
  sql: `UPDATE ${schema.table} SET ${buildSetSql(schema.columns, bindParams)} WHERE ${schema.table}.${schema.tableId} = :id`,
  bindParams: Object.assign({ id }, convertBindParamsToDbValues(schema.columns, bindParams)),
  executeOptions: { autoCommit: true },
  mapResult: (result) => result
})
