const mem = require('mem')
const agent = require('superagent')
const {oldAppBaseUrl} = require('../config')
const log = require('../../log')

const cycleUrl = `${oldAppBaseUrl}/api/cycle`

const getStatus = async () => {
  // log.info({domain: 'cycle', agent}, 'get cycle status')

  const result = await agent.get(cycleUrl)
  return result && result.body ? result.body : null
}

module.exports = mem(getStatus, {maxAge: 3000})
