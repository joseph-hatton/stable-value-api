const target = require('../../../../api/transactions/validators/validateTransactionDetailedType')

describe('validate transaction detailed type', () => {
  const validCases = [
    'INITIAL_DEPOSIT',
    'ADDITIONAL_DEPOSIT',
    'PARTICIPANT_ACTIVITY',
    'EMPLOYER_ACTIVITY',
    'MANAGER_ACTIVITY',
    'MARKET_VALUE_ADJUSTMENT',
    'PUT_OPTION',
    'BOOK_VALUE_ADJUSTMENT',
    'INTEREST_ADJUSTMENT',
    'TERMINATE_MV_ZERO',
    'TERMINATE_BV_ZERO',
    'TERMINATE_BY_OWNER',
    'TERMINATE_EXTENDED_TERMINATION',
    'TERMINATE_BY_COMPANY',
    'TERMINATE_OTHER'
  ]

  describe('test valid cases', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Transaction Detail Type')
   })
})
