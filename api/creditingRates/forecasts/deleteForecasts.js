const log = require('../../../log')
const deleteFromDb = require('../../db/deleteFromDb')
const schema = require('../creditingRatesSchema')
const applySchemaConverters = require('../../db/converters/applySchemaConverters')

const deleteSql = `delete from ${schema.table} where contract_id = :contractId and effective_date >= :effectiveDate and status = 'FORECAST'`

const buildDeleteParams = (contractId, terminationDate) =>
  applySchemaConverters({columns: schema.columns},
  {
    contractId,
    effectiveDate: terminationDate
  })

module.exports = async (contractId, terminationDate, userId) => {
  log.info({domain: schema.tableShort, id: contractId, terminationDate, userId}, 'delete crediting rate forecasts')

  const bindParams = buildDeleteParams(contractId, terminationDate)

  return await deleteFromDb(deleteSql, bindParams)
}
