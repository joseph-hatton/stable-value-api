const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {beginningOfDayDateConverter, booleanConverter, bpsConverter, dateConverter, numberConverter, percentConverter} = require('../db/converters')

const table = 'contract_edit_history'
const tableId = 'contract_edit_history_id'
const sequence = `${dbSchemaName}.edit_history_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: tableId,
  columns: [
    {name: 'contractEditHistoryId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'effectiveDate', column: 'effective_date', defaultValue: () => new Date(), converter: beginningOfDayDateConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId,
    {name: 'jurisdiction'},
    {name: 'premiumRate', column: 'premium_rate', converter: bpsConverter},
    {name: 'managerFeeRate', column: 'manager_fee_rate', converter: bpsConverter},
    {name: 'advisorFeeRate', column: 'advisor_fee_rate', converter: bpsConverter},
    {name: 'creditingRateCalcMethod', column: 'crediting_rate_calc_method'},
    {name: 'assignmentState', column: 'assignment_state'},
    {name: 'legalFormName', column: 'legal_form_name'},
    {name: 'managerId', column: 'manager_id', converter: numberConverter},
    {name: 'fundLevelUtilization', column: 'fund_level_utilization', converter: percentConverter},
    {name: 'planLevelUtilization', column: 'plan_level_utilization', converter: percentConverter},
    {name: 'fundActiveRate', column: 'fund_active_rate', converter: percentConverter},
    {name: 'fundSeniorRate', column: 'fund_senior_rate', converter: percentConverter},
    {name: 'spRating', column: 'sp_rating'},
    {name: 'moodyRating', column: 'moody_rating'},
    {name: 'fitchRating', column: 'fitch_rating'},
    {name: 'overallRating', column: 'overall_rating'},
    {name: 'accountDurationLimit', column: 'account_duration_limit', converter: numberConverter}
  ],
  required: {
    new: ['contractId', 'createdDate'],
    update: ['contractId', 'createdDate']
  }
}
