module.exports = ({columns}, objToConvert) => {
  const newResult = {}
  Object.entries(objToConvert)
  .forEach((entry) => {
    const matchingColumn = columns.find((column) => (column.name === entry[0]))
    const hasFromDbConverter = (matchingColumn && matchingColumn.converter && matchingColumn.converter.fromDb)
    if (hasFromDbConverter) {
    }
    newResult[entry[0]] = (hasFromDbConverter) ? matchingColumn.converter.fromDb(entry[1]) : entry[1]
  })
  return newResult
}
