const target = require('../../../api/contracts/validateContractStatus')

describe('contract status validator', () => {
  const validCases = [
    'PENDING', 'PENDING_JASPER', 'SUBMITTED', 'ACTIVE', 'PENDING_TERMINATION', 'TERMINATED', 'INACTIVE'
  ]

  describe('test valid contract statues', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Contract Status')
   })
})
