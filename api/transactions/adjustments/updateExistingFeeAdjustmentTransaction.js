const log = require('../../../log')
const buildUpdateParams = require('../../common/buildUpdateParams')
const updateDb = require('../../db/updateDb')
const schema = require('../transactionSchema')

module.exports = async (adjustmentTransactionId, clientBody, totalAmount, userId) => {
  log.info({domain: schema.tableShort, adjustmentTransactionId, clientBody, totalAmount, userId}, 'update existing fee adjustment transaction')
  const updateParams = buildUpdateParams(schema.columns, userId, {
    amount: totalAmount,
    contractId: clientBody.contractId,
    effectiveDate: clientBody.effectiveDate
  })
  await updateDb(schema, updateParams, adjustmentTransactionId)
}
