const target = require('../../../api/common/buildCustomSelectMapper')

const row1 = {id: 1, letter: 'A'}
const row2 = {id: 2, letter: 'B'}
const row2a = {id: 2, letter: 'C'}

describe('build custom select mapper', () => {
  describe('with simple rules', () => {
    let mapper, uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel, topLevel
    beforeEach(() => {
      uniqueRowProperty = 'id'
      buildNewTopLevelObjectFromRow = (row) => ({id: row.id, children: []})
      appendChildToTopLevel = (topLevel, row) => topLevel.children.push(row.letter)
      mapper = target(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
    })

    it('should map single child per row', () => {
      mapper([row1, row2]).should.eql([{id: 1, children: ['A']}, {id: 2, children: ['B']}])
    })

    it('should map multiple children per row', () => {
      mapper([row1, row2, row2a]).should.eql([{id: 1, children: ['A']}, {id: 2, children: ['B', 'C']}])
    })
  })
})
