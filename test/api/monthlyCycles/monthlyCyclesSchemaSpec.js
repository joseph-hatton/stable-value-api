const validateSchema = require('../validateSchema')
const target = require('../../../api/monthlyCycles/monthlyCyclesSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

describe('monthly cycle schema', () => {
  validateSchema(target, {
    tableShort: 'monthly_cycle',
    table: 'sch_stbv.monthly_cycle',
    tableId: 'monthly_cycle_id',
    sequence: 'sch_stbv.monthly_cycle_seq.NEXTVAL',
    orderBy: 'monthly_cycle.start_date desc',
    columns: [
      {name: 'monthlyCycleId', column: 'monthly_cycle_id', sequence: 'sch_stbv.monthly_cycle_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'startDate', column: 'start_date', converter: dateConverter},
      {name: 'endDate', column: 'end_date', converter: dateConverter},
      {name: 'closed', converter: booleanConverter}
    ],
    required: {
      new: ['startDate', 'endDate'],
      update: ['startDate', 'endDate']
    }
  })
})
