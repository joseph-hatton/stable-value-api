const _ = require('lodash')
const log = require('../../log')
const selectFromDbWithPagination = require('../db/selectFromDbWithPagination')
const buildCreditingRatesQueryWhereClause = require('./buildCreditingRatesQueryWhereClause')
const overrideGetManyCreditingRateWithPaginationSelect = require('./overrideGetManyCreditingRateWithPaginationSelect')
const getContractManagers = require('../contracts/getContractManagers')
const schemaOriginal = require('./creditingRatesSchema')

const schema = {
  ...schemaOriginal,
  overrideSelectClause: {
    selectMany: overrideGetManyCreditingRateWithPaginationSelect
  }
}

const mapCreditingRates = (req, contractManagers, creditingRates) => {
  const {query: {managerId}} = req

  creditingRates.forEach(creditingRate => {
    if (managerId) {
      creditingRate.managerId = managerId
      const matchingManager = Object.values(contractManagers).find(manager => manager.managerId === managerId)
      creditingRate.managerName = matchingManager ? matchingManager.name : null
    } else {
      const matchingManager = contractManagers[creditingRate.contractId]
      if (matchingManager) {
        creditingRate.managerId = matchingManager.managerId
        creditingRate.managerName = matchingManager.name
      }
    }
    if (!creditingRate.bookValue) {
      creditingRate.bookValue = creditingRate.endingBookValue
    }
  })
}

const sortByManagerName = (req, creditingResults) => {
  if (req.query && req.query.sortBy === 'managerName') {
    const desc = req.query && req.query.sortOrder && req.query.sortOrder.toUpperCase() === 'DESC'
    creditingResults.results = _.orderBy(creditingResults.results, ['managerName'], [(desc) ? 'desc' : 'asc'])
  }
}

module.exports = async (req, res) => {
  log.info({domain: schema.tableShort}, 'get many crediting rates api')

  const contractManagers = await getContractManagers()

  if (req.query && req.query.managerId) {
    req.query.contractIds = _.invertBy(contractManagers, (contractManager) => contractManager.managerId)[req.query.managerId]
  }

  const whereClause = buildCreditingRatesQueryWhereClause(schema, req)

  log.info({domain: schema.tableShort, whereClause}, 'get many crediting rates whereClause')
  const creditingResults = await selectFromDbWithPagination(schema, whereClause)

  mapCreditingRates(req, contractManagers, creditingResults.results)

  sortByManagerName(req, creditingResults)

  log.info({domain: schema.tableShort}, 'get many crediting rates api results')
  return res.json(creditingResults)
}
