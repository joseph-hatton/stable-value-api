require('require-sql')
const sql = require('./overrideGetOneTransactionSelect.sql')
const omit = require('lodash/omit')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')

const uniqueRowProperty = 'transactionId'

const buildNewTopLevelObjectFromRow = (row) => {
  const ommitedRow = omit(row, [
    'fdfeeDistributionId',
    'fdFeeDistributionId',
    'fdInvoiceId',
    'iInvoiceNumber',
    'fdFeeAmount',
    'fdCreatedDate',
    'fdCreatedId',
    'fdModifiedDate',
    'fdModifiedId',
    'fdVersion',
    'fdBeginningBalance',
    'fdAdjustment',
    'fdEndingBalance'
  ])
  return {
    feeDistributions: [],
    ...ommitedRow,
    balanceId: (row.balanceId) ? row.balanceId * 1 : row.balanceId,
    beginningBookValue: (row.beginningBookValue) ? row.beginningBookValue * 1 : row.beginningBookValue
  }
}

const appendChildToTopLevel = (topLevel, row) => {
  if (row.fdFeeDistributionId) {
    topLevel.feeDistributions.push({
      feeDistributionId: row.fdFeeDistributionId * 1,
      invoiceId: (row.fdInvoiceId) ? row.fdInvoiceId * 1 : row.fdInvoiceId,
      invoiceNumber: (row.iInvoiceNumber) ? row.iInvoiceNumber * 1 : row.iInvoiceNumber,
      amount: (row.fdFeeAmount) ? row.fdFeeAmount * 1 : row.fdFeeAmount,
      createdDate: row.fdCreatedDate,
      createdId: row.fdCreatedId,
      modifiedDate: row.fdModifiedDate,
      modifiedId: row.fdModifiedId,
      version: row.fdVersion * 1,
      beginningBalance: (row.fdBeginningBalance) ? row.fdBeginningBalance * 1 : row.fdBeginningBalance,
      adjustment: (row.fdAdjustment) ? row.fdAdjustment * 1 : row.fdAdjustment,
      endingBalance: (row.fdEndingBalance) ? row.fdEndingBalance * 1 : row.fdEndingBalance
    })
  }
}

module.exports = {
  sql,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
