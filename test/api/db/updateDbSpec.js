/* eslint-disable no-unused-expressions */
const cloneDeep = require('lodash/cloneDeep')
const proxyquire = require('../../proxyquire')
const testSchema = require('../../data/testSchema')
const THE_ID = 8675309
const USER_ID = 'someUser'
const CHANGE_ID = 4443
const params = {
  bColumn: 'bValue',
  cColumn: 'cValue'
}
const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'
const EXECUTE_SELECT_BY_ID_RESULT = 'EXECUTE_SELECT_BY_ID_RESULT'

describe('update db', () => {
  let target, schema, connection, dbExecutePromise, executeUpdateQuery, executeSelectByIdQuery, executeInsertChangeQuery, executePropertyChanges

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    connection = sinon.spy()
    executeUpdateQuery = sinon.stub()
    executeSelectByIdQuery = sinon.stub().resolves(EXECUTE_SELECT_BY_ID_RESULT)
    executeInsertChangeQuery = sinon.stub().resolves({id: CHANGE_ID})
    executePropertyChanges = sinon.stub()
    target = proxyquire('api/db/updateDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/executeUpdateQuery': executeUpdateQuery,
      './executors/executeSelectByIdQuery': executeSelectByIdQuery,
      './executors/executeInsertChangeQuery': executeInsertChangeQuery,
      './executors/executeUpdatePropertyChangeQueries': executePropertyChanges
    })
  })

  const executeTarget = (schemaOverides = {}) => {
    schema = Object.assign(cloneDeep(testSchema.schema), schemaOverides)
    return target(schema, params, THE_ID, USER_ID)
    .then((result) =>
      dbExecutePromise.getCall(0).args[0](connection)
      .then(() => result))
  }

  describe('execute promise', () => {
    it('called once', () =>
      executeTarget()
      .then(() => dbExecutePromise.callCount.should.eql(1)))

    it('returns result', () =>
      executeTarget()
      .then((result) => result.should.eql(DB_EXECUTE_PROMISE_RESULT)))
  })

  describe('with no audit columns', () => {
    it('execute update query called once', () =>
      executeTarget()
      .then(() => executeUpdateQuery.callCount.should.eql(1)))

    it('execute update query called with', () =>
      executeTarget()
      .then(() => executeUpdateQuery.should.have.been.calledWithExactly(connection, schema, params, THE_ID)))

    it('execute select by id query called once', () =>
      executeTarget()
      .then(() => executeSelectByIdQuery.callCount.should.eql(1)))

    it('execute select id query called with', () =>
      executeTarget()
      .then(() => executeSelectByIdQuery.should.have.been.calledWithExactly(connection, schema, THE_ID)))

    it('execute select id query called after execute update query', () =>
      executeTarget()
      .then(() => executeSelectByIdQuery.should.have.been.calledAfter(executeUpdateQuery)))
  })

  describe('with audit columns', () => {
    it('execute select by id query called once', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeSelectByIdQuery.callCount.should.eql(2)))

    it('execute select id query called with', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeSelectByIdQuery.getCall(0).should.have.been.calledWithExactly(connection, schema, THE_ID)))

    it('execute update query called once', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeUpdateQuery.callCount.should.eql(1)))

    it('execute update query called with', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeUpdateQuery.should.have.been.calledWithExactly(connection, schema, params, THE_ID)))

    it('execute insert change query called once', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeInsertChangeQuery.callCount.should.eql(1)))

    it('execute insert change called with', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeInsertChangeQuery.should.have.been.calledWithExactly(connection, schema, THE_ID, USER_ID, params, 'UPDATE')))

    it('execute property changes called once', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executePropertyChanges.callCount.should.eql(1)))

    it('execute property changes called with', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executePropertyChanges.should.have.been.calledWithExactly(connection, schema, CHANGE_ID, EXECUTE_SELECT_BY_ID_RESULT, params)))

    it('execute last select id query called with', () =>
      executeTarget({columns: [{name: 'auditedColumn', audit: true}]})
      .then(() => executeSelectByIdQuery.getCall(1).should.have.been.calledWithExactly(connection, schema, THE_ID)))
  })
})
