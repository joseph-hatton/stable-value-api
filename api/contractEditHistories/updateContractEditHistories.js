const log = require('../../log')
const schema = require('./contractEditHistorySchema')
const getContractEditHistories = require('./getContractEditHistories')
const updateContractEditHistory = require('./updateContractEditHistory')
const insertNewContractEditHistory = require('./insertNewContractEditHistory')

const updateEditHistoryParam = async ({modifiedId, modifiedDate}, editHistory, dbEditHistoryRecord) => {
  const userId = modifiedId
  const updateParamsAndBody = Object.assign({}, editHistory, {
    effectiveDate: dbEditHistoryRecord.effectiveDate.toISOString(),
    modifiedId,
    modifiedDate
  })
  const combiningDbHistoryWithUpdateParams = Object.assign({}, dbEditHistoryRecord, updateParamsAndBody)
  log.info({updateParamsAndBody, userId}, 'update edit history params')
  return await updateContractEditHistory(combiningDbHistoryWithUpdateParams, dbEditHistoryRecord.contractEditHistoryId, userId)
}

const updateOrInsertEditHistoriesByEffectiveDate = async (editHistoriesToUpdateOrCreate, dbEditHistoryRecords, contractUpdateParams, contractId) => {
  log.info({domain: schema.tableShort, id: contractId}, 'contract is not pending, determining update or insert by effective date of edit history')
  for (let editHistoryToUpdateOrCreate of editHistoriesToUpdateOrCreate) {
    const matchingDbEditHistoryRecord = dbEditHistoryRecords.find((dbEditHistoryRecord) => dbEditHistoryRecord.effectiveDate.toISOString() === editHistoryToUpdateOrCreate.effectiveDate)
    if (matchingDbEditHistoryRecord) {
      log.info({effectiveDate: editHistoryToUpdateOrCreate.effectiveDate, matchingDbEditHistoryRecord}, 'found matching db edit history')
      await updateEditHistoryParam(contractUpdateParams, editHistoryToUpdateOrCreate, matchingDbEditHistoryRecord)
    } else {
      log.info({effectiveDate: editHistoryToUpdateOrCreate.effectiveDate, dbEditHistoryRecords}, 'did not find matching db edit history')
      await insertNewContractEditHistory(contractUpdateParams, editHistoryToUpdateOrCreate, contractId, contractUpdateParams.modifiedId)
    }
  }
}

module.exports = async (contractUpdateParams, body, contractId) => {
  log.info({domain: schema.tableShort, id: contractId, contractUpdateParams}, 'update contract edit histories')

  const editHistoriesToUpdateOrCreate = (body && body.editHistories && body.editHistories.length > 0) ? body.editHistories.filter((editHistory) => editHistory) : null
  log.info({domain: schema.tableShort, id: contractId, editHistoriesToUpdateOrCreate}, 'filtered edit histories to create')

  if (editHistoriesToUpdateOrCreate && editHistoriesToUpdateOrCreate.length > 0) {
    const dbEditHistoryRecords = await getContractEditHistories(contractId)
    if (body.status === 'PENDING' && dbEditHistoryRecords.length === 1) {
      log.info({domain: schema.tableShort, id: contractId}, 'contract is pending, updating only edit history record')
      return await updateEditHistoryParam(contractUpdateParams, editHistoriesToUpdateOrCreate[0], dbEditHistoryRecords[0])
    } else {
      await updateOrInsertEditHistoriesByEffectiveDate(editHistoriesToUpdateOrCreate, dbEditHistoryRecords, contractUpdateParams, contractId)
    }
  }
}
