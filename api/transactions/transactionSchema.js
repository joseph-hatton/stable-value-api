const omit = require('lodash/omit')
const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {booleanConverter, dateConverter, numberConverter, percentConverter} = require('../db/converters')
const stateValidator = require('../states/stateValidator')
const transactionTypesValidator = require('./validators/validateTransactionType')
const transactionDetailedTypesValidator = require('./validators/validateTransactionDetailedType')
const amountValidator = require('./validators/validateTransactionAmount')
const overrideGetOneTransactionSelect = require('./overrideGetOneTransactionSelect')
const overrideGetManyTransactionsSelect = require('./overrideGetManyTransactionsSelect')

const table = 'transaction'
const tableId = 'transaction_id'
const sequence = `${dbSchemaName}.transaction_seq.NEXTVAL`
const falseDefault = () => false

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'transaction.effective_date desc',
  columns: [
    {name: 'transactionId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'transactionType', column: 'transaction_type', audit: true, validators: [transactionTypesValidator]},
    {name: 'effectiveDate', column: 'effective_date', audit: true, converter: dateConverter},
    {name: 'detail', audit: true, validators: [transactionDetailedTypesValidator]},
    {name: 'amount', audit: true, converter: numberConverter, validators: [amountValidator]},
    {name: 'comments', audit: true},
    {name: 'locked', defaultValue: falseDefault, converter: booleanConverter, audit: true},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId,
    version,
    {name: 'adjustmentDate', column: 'adjustment_date', audit: true, converter: dateConverter},
    {name: 'adjustmentTransactionId', column: 'adjustment_transaction_id', audit: true, converter: numberConverter}
  ],
  required: {
    new: ['contractId', 'transactionType', 'effectiveDate', 'amount', 'locked'],
    update: ['contractId', 'transactionType', 'effectiveDate', 'amount', 'locked']
  },
  overrideSelectClause: {
    selectOne: overrideGetOneTransactionSelect,
    selectMany: overrideGetManyTransactionsSelect
  },
  audit: {
    objectClass: 'Transaction',
    buildDescription: (transaction) => `${transaction.transactionType}: ${Math.abs(transaction.amount)}${(transaction.detail) ? ', Detail: ' + transaction.detail : ''}`
  }
}
