/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/transactions/transactionSchema')
const contractSchema = require('../../../api/contracts/contractSchema')
const cloneDeep = require('lodash/cloneDeep')

const dbTransaction = {transactionId: 542, effectiveDate: 'yesterday', contractId: 11}
const CONTRACT_INSERT_PARAMS = {stuff: true}
const userId = 'me'
const HEADERS = 'HEADERS'
const contract = {contractId: 11, status: 'ACTIVE'}

describe('create transaction api', () => {
  let target, res, jsonStub, next, body, buildDefaultParams, getUserId, insertIntoDb, selectByIdFromDb, schedulePendingTerminationContract, validateThenCallApi, validateEffectiveDateInOpenMonth, updateAdjustments, updateDistributions
  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    next = sinon.spy()
    buildDefaultParams = sinon.stub().returns('defaultParams')
    getUserId = sinon.stub().returns(userId)
    insertIntoDb = sinon.stub().resolves(dbTransaction)
    validateEffectiveDateInOpenMonth = sinon.stub()
    selectByIdFromDb = sinon.stub().resolves(contract)
    schedulePendingTerminationContract = sinon.stub()
    updateAdjustments = sinon.stub()
    updateDistributions = sinon.stub()
    validateThenCallApi = sinon.stub().resolves('VALIDATION RESULT')
    target = proxyquire('api/transactions/createTransactionApi', {
      '../common/buildDefaultParams': buildDefaultParams,
      '../getUserId': getUserId,
      '../contracts/workflow/schedulePendingTerminationContract': schedulePendingTerminationContract,
      '../db/insertIntoDb': insertIntoDb,
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../common/validateThenCallApi': validateThenCallApi,
      './validators/validateEffectiveDateInOpenMonth': validateEffectiveDateInOpenMonth,
      './adjustments/updateAdjustments': updateAdjustments,
      './distributions/updateDistributions': updateDistributions
    })
  })

  describe('validate then call api', () => {
    let response
    beforeEach(async () => {
      body = {}
      response = await target({headers: HEADERS, body: body}, res, next)
    })

    it('returns validation result', () => {
      response.should.eql('VALIDATION RESULT')
    })

    it('then calls api', () => {
      validateThenCallApi.should.have.been.called
    })

    it('then calls api with schema', () => {
      validateThenCallApi.getCall(0).args[0].should.eql(schema)
    })

    it('then calls api with body', () => {
      validateThenCallApi.getCall(0).args[1].should.eql(body)
    })

    it('then calls with required fields', () => {
      validateThenCallApi.getCall(0).args[2].should.eql(schema.required.new)
    })

    it('then calls with response', () => {
      validateThenCallApi.getCall(0).args[3].should.eql(res)
    })

    it('then calls with next', () => {
      validateThenCallApi.getCall(0).args[4].should.eql(next)
    })

    it('then calls with user id', () => {
      validateThenCallApi.getCall(0).args[6].should.eql({userId, contract})
    })
  })

  describe('attempts to insert', () => {
    let response

    beforeEach(async () => {
      response = await target({headers: HEADERS, body: body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId})
    })

    it('calls insert into db with', () => {
      insertIntoDb.should.have.been.calledWithExactly(schema, 'defaultParams', userId)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.transactionId)
    })

    it('sends select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly(contract)
    })

    it('update distributions not called', () => {
      updateDistributions.should.have.not.been.called
    })

    it('update adjustments not called', () => {
      updateAdjustments.should.have.not.been.called
    })

    it('schedule pending termination not called', () => {
      schedulePendingTerminationContract.should.have.not.been.called
    })
  })

  describe('attempts to insert fee receipt', () => {
    let response

    beforeEach(async () => {
      body = {transactionType: 'FEE_RECEIPT', feeDistributions: ['a', 'b'], amount: 555}
      response = await target({headers: HEADERS, body: body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId})
    })

    it('calls insert into db with', () => {
      insertIntoDb.should.have.been.calledWithExactly(schema, 'defaultParams', userId)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.transactionId)
    })

    it('sends select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly(contract)
    })

    it('update distributions called', () => {
      updateDistributions.should.have.been.calledWithExactly(dbTransaction, body.feeDistributions, body.amount, userId)
    })

    it('update adjustments called', () => {
      updateAdjustments.should.have.been.calledWithExactly(dbTransaction, body, userId)
    })

    it('schedule pending termination not called', () => {
      schedulePendingTerminationContract.should.have.not.been.called
    })
  })

  describe('attempts to insert termination on active contract', () => {
    let response

    beforeEach(async () => {
      body = {transactionType: 'TERMINATION', feeDistributions: ['a', 'b'], amount: 555, statedMaturityDate: (new Date()).toISOString()}
      selectByIdFromDb.withArgs(contractSchema, dbTransaction.contractId).resolves(contract)
      response = await target({headers: HEADERS, body: body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId, contract})
    })

    it('calls insert into db with', () => {
      insertIntoDb.should.have.been.calledWithExactly(schema, 'defaultParams', userId)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.transactionId)
    })

    it('sends select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly(contract)
    })

    it('schedule pending termination called', () => {
      schedulePendingTerminationContract.should.have.been.calledWithExactly({contract, userId, terminationDate: dbTransaction.effectiveDate, statedMaturityDate: body.statedMaturityDate})
    })

    it('update distributions not called', () => {
      updateDistributions.should.have.not.been.called
    })

    it('update adjustments not called', () => {
      updateAdjustments.should.have.not.been.called
    })
  })

  describe('does not insert termination on non-active contract', () => {
    it('schedule pending termination not called', async () => {
      body = {contractId: 11, transactionType: 'TERMINATION', feeDistributions: ['a', 'b'], amount: 555, statedMaturityDate: (new Date()).toISOString()}
      selectByIdFromDb.withArgs(contractSchema, dbTransaction.contractId).resolves({...contract, status: 'TERMINATED'})
      try {
        await target({headers: HEADERS, body: body}, res, next)
      } catch (err) {
        err.message.should.eql('Contract must be Active to add transactions.')
      }
    })
  })
})
