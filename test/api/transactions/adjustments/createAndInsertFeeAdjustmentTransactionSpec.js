const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/transactions/transactionSchema')

const dbTransaction = {
  adjustmentTransactionId: 5,
  contractId: 88,
  effectiveDate: 'yesterday'
}

describe('create and insert fee adjustment transaction', () => {
  let target, insertIntoDb, buildDefaultParams
  beforeEach(() => {
    insertIntoDb = sinon.stub().resolves('done')
    buildDefaultParams = sinon.stub().returns('transaction params')
    target = proxyquire('api/transactions/adjustments/createAndInsertFeeAdjustmentTransaction', {
      '../../db/insertIntoDb': insertIntoDb,
      '../../common/buildDefaultParams': buildDefaultParams
    })
  })

  it('returns insert into db result', async () => {
    const result = await target(dbTransaction, 999, 'somebody')
    result.should.eql('done')
  })

  it('insert into db called with', async () => {
    await target(dbTransaction, 999, 'somebody')
    insertIntoDb.should.have.been.calledWithExactly(schema, 'transaction params')
  })

  it('build default params called with', async () => {
    await target(dbTransaction, 999, 'somebody')
    buildDefaultParams.should.have.been.calledWithExactly(schema, {
      contractId: dbTransaction.contractId,
      locked: false,
      transactionType: 'FEE_ADJUSTMENT',
      effectiveDate: dbTransaction.effectiveDate,
      amount: 999,
      createdId: 'somebody',
      modifiedId: 'somebody'
    })
  })
})
