const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contractEditHistories/contractEditHistorySchema')

const userId = 'me'
const aDateString = (new Date()).toISOString()
const contractInsertParams = {someEffectiveDate: 'yep', createdId: userId, modifiedId: userId, createdDate: aDateString, modifiedDate: aDateString}
const contractId = 542
const body = {name: 'hi'}

describe('insert new contract edit history', () => {
  let target, response, insertIntoDb, buildDefaultParams
  beforeEach(async () => {
    insertIntoDb = sinon.stub().resolves('done')
    buildDefaultParams = sinon.stub().returns('params ready to go')
    target = proxyquire('api/contractEditHistories/insertNewContractEditHistory', {
      '../db/insertIntoDb': insertIntoDb,
      '../common/buildDefaultParams': buildDefaultParams
    })

    response = await target(contractInsertParams, body, contractId, userId)
  })

  it('returns insert into db response', () => response.should.eql('done'))

  it('buildDefaultParams called once', () => buildDefaultParams.should.have.been.called)

  it('buildDefaultParams called with schema', () => buildDefaultParams.getCall(0).args[0].should.eql(schema))

  it('buildDefaultParams called with editHistory', () => buildDefaultParams.getCall(0).args[1].should.eql(body))

  it('buildDefaultParams called with contractId dates', () => {
    const bodyOverrides = buildDefaultParams.getCall(0).args[2]
    bodyOverrides.should.have.property('contractId')
    bodyOverrides.contractId.should.eql(contractId)

    bodyOverrides.should.have.property('effectiveDate')

    bodyOverrides.should.have.property('createdId')
    bodyOverrides.createdId.should.eql(userId)

    bodyOverrides.should.have.property('modifiedId')
    bodyOverrides.modifiedId.should.eql(userId)

    bodyOverrides.should.have.property('createdDate')
    bodyOverrides.createdDate.should.eql(aDateString)

    bodyOverrides.should.have.property('modifiedDate')
    bodyOverrides.modifiedDate.should.eql(aDateString)
  })

  it('insert into db called with', () => insertIntoDb.should.have.been.calledWithExactly(schema, 'params ready to go', userId))
})
