module.exports = ({table, tableId}, id) => ({
    sql: `delete from ${table} where ${tableId} = :id`,
    bindParams: { id },
    executeOptions: { autoCommit: true },
    mapResult: (result) => {
      if (result && result.error) {
        throw new Error(result.error)
      } else if (result && result.rowsAffected === 0) {
        throw new Error('not found')
      } else {
        return { id }
      }
    }
  })
