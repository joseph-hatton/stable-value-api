const {getContractContactsByContractId, validateContractContacts} = require('./validateAssignedContacts')
const deleteByIdFromDb = require('../../db/deleteByIdFromDb')
const getUserId = require('../../getUserId')
const createError = require('http-errors')

const unAssingneContactDbDelete = async (schema, headers, contactContractId, res) => {
  const userId = getUserId(headers)
  await deleteByIdFromDb(schema, contactContractId, userId)
 return res.status(200).json('Succesfully Delete')
}

const unAssingneContactMap = async (contractId, contractContactId) => {
  const contractContacts = await getContractContactsByContractId(contractId)
  const contactId = (contractContacts.find(i => i.contactContractId === contractContactId)).contactId
  const contactContractNewValues = contractContacts
  .filter(i => i.contactId !== contactId)
  .filter(i => i.contractStatus !== 'PENDING')
  return {
    errors: await validateContractContacts(contactContractNewValues)
  }
}
module.exports = (schema) => async ({ query, params, headers, body }, res, next) => {
  const unAssingneContact = await unAssingneContactMap(params.contractId, params.id)
  const err = await unAssingneContact.errors
  if (err.length > 0) {
    throw createError.BadRequest('Updating this Contact will result in violations to mandatory Contacts.')
  } else {
    unAssingneContactDbDelete(schema, headers, params.id, res)
  }
}
