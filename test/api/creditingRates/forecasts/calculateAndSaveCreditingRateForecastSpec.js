const proxyquire = require('../../../proxyquire')

const contract = {contractId: 55}
const startingDate = new Date()
const forecastDates = [startingDate]

describe('calculate and save crediting rate forecasts', () => {
  let target, calculateCreditingRateForecastDates, createForecasts
  beforeEach(() => {
    calculateCreditingRateForecastDates = sinon.stub().withArgs(contract, startingDate).returns(forecastDates)
    createForecasts = sinon.stub().withArgs(55, forecastDates).resolves('create forecasts result')
    target = proxyquire('api/creditingRates/forecasts/calculateAndSaveCreditingRateForecast', {
      './calculateCreditingRateForecastDates': calculateCreditingRateForecastDates,
      './createForecasts': createForecasts
    })
  })

  it('returns create forecasts result', async () => {
    const result = await target(contract, startingDate)
    result.should.eql('create forecasts result')
  })
})
