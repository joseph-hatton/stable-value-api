module.exports = {
  toDb: (apiBoolean) => {
    if (apiBoolean === true || apiBoolean === false) {
      return (apiBoolean === true) ? 'Y' : 'N'
    } else {
      return null
    }
  },
  fromDb: (dbString) => (dbString) ? dbString === 'Y' : null
}
