/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/feeDistributions/feeDistributionsSchema')

const userId = 'somebody'

describe('delete fee distribution', () => {
  let target, deleteByIdFromDb
  beforeEach(() => {
    deleteByIdFromDb = sinon.stub()
    target = proxyquire('api/transactions/distributions/deleteFeeDistribution', {
      '../../db/deleteByIdFromDb': deleteByIdFromDb
    })
  })

  it('delete fee distribution if present', async () => {
    const clientFeeDistribution = {feeDistributionId: 878}
    await target(99, clientFeeDistribution, userId)
    deleteByIdFromDb.should.have.been.calledWithExactly(schema, 878, userId)
  })

  it('does not delete fee distribution if not present', async () => {
    const clientFeeDistribution = null
    await target(99, clientFeeDistribution, userId)
    deleteByIdFromDb.should.have.not.been.calledWithExactly
  })
})
