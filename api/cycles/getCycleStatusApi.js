const createError = require('http-errors')
const {oldAppBaseUrl} = require('../config')
const log = require('../../log')
const getCycleStatus = require('./getCycleStatus')
const cycleUrl = `${oldAppBaseUrl}/api/cycle`

module.exports = async (req, res) => {
  // log.info({domain: 'cycle'}, 'get cycle status api')

  const cycleStatus = await getCycleStatus()

  if (cycleStatus) {
    res.status(200).json(cycleStatus)
  } else {
    throw createError.InternalServerError(`Unable to query cycle at ${cycleUrl}`)
  }
}
