const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const whereChangeIdEquals = (schema, req, whereClauseBits, response) => {
  if (req.params && req.params.id) {
    whereClauseBits.push(`${schema.table}.change_id = :id`)
    response.bindParams.id = req.params.id
  }
}

module.exports = (schema, req) =>
  buildWhereClauseAndParamsWithPaging(schema, req, [whereChangeIdEquals])
// { whereClause, bindParams, executeOptions, pagination }
