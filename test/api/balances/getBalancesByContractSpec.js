const proxyquire = require('../../proxyquire')
const schema = require('../../../api/balances/balancesSchema')

describe('get balances by contract', () => {
  let target, selectFromDb, effectiveDate, whereContractIdAndEffectiveDate
  beforeEach(() => {
    effectiveDate = '2017-11-13T00:00:00.000Z'
    selectFromDb = sinon.stub().resolves(['one', 'two'])
    whereContractIdAndEffectiveDate = sinon.stub().returns('bind params')
    target = proxyquire('api/balances/getBalancesByContract', {
      '../db/selectFromDb': selectFromDb,
      './whereContractIdAndEffectiveDate': whereContractIdAndEffectiveDate
    })
  })

  it('returns result', async () => {
    const result = await target(52, {})
    result.should.have.been.eql(['one', 'two'])
  })

  it('calls select from db with ', async () => {
    await target(52, {})
    selectFromDb.should.have.been.calledWithExactly(schema, 'bind params')
  })

  it('calls where contract id and effective date with null effective date ', async () => {
    await target(52, {})
    whereContractIdAndEffectiveDate.should.have.been.calledWithExactly(52, null)
  })

  it('calls where contract id and effective date with an effective date ', async () => {
    await target(52, {effectiveDate: 'yesterday'})
    whereContractIdAndEffectiveDate.should.have.been.calledWithExactly(52, 'yesterday')
  })
})
