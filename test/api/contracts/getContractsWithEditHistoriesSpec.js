/* eslint-disable no-unused-expressions */
require('require-sql')
const sql = require('../../../api/contracts/getContractsWithEditHistories.sql')
const target = require('../../../api/contracts/getContractsWithEditHistories')

const contractId = 5

const buildCehRecord = (overrides) => Object.assign({
    contractEditHistoryId: undefined,
    version: undefined,
    effectiveDate: null,
    createdDate: null,
    createdId: undefined,
    modifiedDate: null,
    modifiedId: undefined,
    jurisdiction: undefined,
    premiumRate: undefined,
    managerFeeRate: undefined,
    advisorFeeRate: undefined,
    creditingRateCalcMethod: undefined,
    assignmentState: undefined,
    legalFormName: undefined,
    managerId: undefined,
    fundLevelUtilization: undefined,
    planLevelUtilization: undefined,
    fundActiveRate: undefined,
    fundSeniorRate: undefined,
    spRating: undefined,
    moodyRating: undefined,
    fitchRating: undefined,
    overallRating: undefined,
    accountDurationLimit: undefined
  },
  overrides)

describe('get contracts with edit histories', () => {
  it('has sql', async () => {
    target.sql.should.eql(sql)
  })

  describe('builds a mapper', () => {
    it('has a mapper', async () => {
      target.mapper.should.exist
    })

    it('copies all non ceh keys directly', async () => {
      target.mapper([{key: 'ok', contractId}]).should.eql([{editHistories: [], derivativeTypes: [], key: 'ok', contractId}])
    })

    it('strips ceh keys if cehId exists', async () => {
      target.mapper([{contractId, cehId: 42}]).should.eql([{editHistories: [buildCehRecord({contractEditHistoryId: 42})], derivativeTypes: [], contractId}])
    })

    it('maps all ceh keys', async () => {
      const row = {
        contractId,
        cehId: 42,
        cehVersion: 5,
        cehEffectiveDate: (new Date()).toISOString(),
        cehCreatedDate: (new Date()).toISOString(),
        cehCreatedId: 55,
        cehModifiedDate: (new Date()).toISOString(),
        cehModifiedId: 54,
        cehJurisdiction: 'IL',
        cehPremiumRate: 81,
        cehManagerFeeRate: 82,
        cehAdvisorFeeRate: 83,
        cehCreditingRateCalcMethod: 'yep',
        cehAssignmentState: 'MO',
        cehLegalFormName: 'paperwork',
        cehManagerId: 56,
        cehFundLevelUtilization: 9.0,
        cehPlanLevelUtilization: 9.1,
        cehFundActiveRate: 8.4,
        cehFundSeniorRate: 8.5,
        cehSpRating: 'A',
        cehMoodyRating: 'B',
        cehFitchRating: 'C',
        cehOverallRating: 'D',
        cehAccountDurationLimit: 92
      }
      target.mapper([row]).should.eql([
        {
          editHistories: [
            buildCehRecord({
              contractEditHistoryId: row.cehId,
              version: row.cehVersion,
              effectiveDate: new Date(row.cehEffectiveDate),
              createdDate: new Date(row.cehCreatedDate),
              createdId: row.cehCreatedId,
              modifiedDate: new Date(row.cehModifiedDate),
              modifiedId: row.cehModifiedId,
              jurisdiction: row.cehJurisdiction,
              premiumRate: row.cehPremiumRate * 10000,
              managerFeeRate: row.cehManagerFeeRate * 10000,
              advisorFeeRate: row.cehAdvisorFeeRate * 10000,
              creditingRateCalcMethod: row.cehCreditingRateCalcMethod,
              assignmentState: row.cehAssignmentState,
              legalFormName: row.cehLegalFormName,
              managerId: row.cehManagerId,
              fundLevelUtilization: row.cehFundLevelUtilization * 100,
              planLevelUtilization: row.cehPlanLevelUtilization * 100,
              fundActiveRate: row.cehFundActiveRate * 100,
              fundSeniorRate: row.cehFundSeniorRate * 100,
              spRating: row.cehSpRating,
              moodyRating: row.cehMoodyRating,
              fitchRating: row.cehFitchRating,
              overallRating: row.cehOverallRating,
              accountDurationLimit: row.cehAccountDurationLimit
            })],
            derivativeTypes: [],
            contractId
      }])
    })
  })
})
