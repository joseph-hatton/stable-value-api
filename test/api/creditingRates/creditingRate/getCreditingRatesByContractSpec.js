const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

describe('get crediting rates by contract', () => {
  let target, selectFromDb, effectiveDate, referenceDate
  beforeEach(() => {
    effectiveDate = '2017-11-13T00:00:00.000Z'
    referenceDate = '2017-12-01T00:00:00.000Z'
    selectFromDb = sinon.stub().resolves(['one', 'two'])
    target = proxyquire('api/creditingRates/creditingRate/getCreditingRatesByContract', {
      '../../db/selectFromDb': selectFromDb
    })
  })

  it('returns result', async () => {
    const result = await target(52, {})
    result.should.have.been.eql(['one', 'two'])
  })

  it('calls select from db with schema', async () => {
    await target(52, {})
    selectFromDb.getCall(0).args[0].should.eql(schema)
  })

  it('calls select from db with whereClause', async () => {
    await target(52, {})
    selectFromDb.getCall(0).args[1].whereClause.should.eql('WHERE sch_stbv.crediting_rate.contract_id = :id')
  })

  it('calls select from db with bindParams', async () => {
    await target(52, {})
    selectFromDb.getCall(0).args[1].bindParams.should.eql({id: 52})
  })

  describe('with effectiveDate', () => {
    it('calls select from db with whereClause', async () => {
      await target(52, {effectiveDate})
      selectFromDb.getCall(0).args[1].whereClause.should.eql('WHERE sch_stbv.crediting_rate.contract_id = :id AND sch_stbv.crediting_rate.effective_date = :effectiveDate')
    })

    it('calls select from db with bindParams', async () => {
      await target(52, {effectiveDate})
      selectFromDb.getCall(0).args[1].bindParams.should.eql({id: 52, effectiveDate: new Date(effectiveDate)})
    })
  })

  describe('with referenceDate', () => {
    it('calls select from db with whereClause', async () => {
      await target(52, {referenceDate})
      selectFromDb.getCall(0).args[1].whereClause.should.eql('WHERE sch_stbv.crediting_rate.contract_id = :id AND sch_stbv.crediting_rate.reference_date = :referenceDate')
    })

    it('calls select from db with bindParams', async () => {
      await target(52, {referenceDate})
      selectFromDb.getCall(0).args[1].bindParams.should.eql({id: 52, referenceDate: new Date(referenceDate)})
    })
  })

  describe('with referenceDate and effectiveDate', () => {
    it('calls select from db with whereClause', async () => {
      await target(52, {referenceDate, effectiveDate})
      selectFromDb.getCall(0).args[1].whereClause.should.eql('WHERE sch_stbv.crediting_rate.contract_id = :id AND sch_stbv.crediting_rate.reference_date = :referenceDate AND sch_stbv.crediting_rate.effective_date = :effectiveDate')
    })

    it('calls select from db with bindParams', async () => {
      await target(52, {referenceDate, effectiveDate})
      selectFromDb.getCall(0).args[1].bindParams.should.eql({id: 52, referenceDate: new Date(referenceDate), effectiveDate: new Date(effectiveDate)})
    })
  })
})
