const target = require('../../../../api/transactions/validators/validateTransactionAmount')

const testAmount = (transactionType, errorAmount, passingAmount) => {
  it(`${transactionType} with ${(errorAmount) < 0 ? 'negative' : 'positive'} amount returns error`, () => {
    target(errorAmount, {transactionType}).should.eql(`Invalid ${(errorAmount) < 0 ? 'Negative' : 'Positive'} Amount for type ${transactionType}`)
  })

  it(`${transactionType} with ${(errorAmount) < 0 ? 'positive' : 'negative'} amount returns no error`, () => {
    should.not.exist(target(passingAmount, {transactionType}))
  })
}

describe('validate transaction amount', () => {
  testAmount('DEPOSIT', -1, 555)
  testAmount('FEE_RECEIPT', -1, 555)

  testAmount('WITHDRAWAL', 555, -1)
  testAmount('TERMINATION', 555, -1)
})
