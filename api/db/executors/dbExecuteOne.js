const createError = require('http-errors')
const log = require('../../../log')

module.exports = (connection, {sql, bindParams, executeOptions, mapResult}) => {
  log.info({domain: 'db', sql, bindParams}, 'execute one')
  return connection.execute(sql, bindParams, executeOptions)
    .then((mapResult))
    .catch((error) => {
      log.error({domain: 'db', sql, bindParams, error: error.message.trim()}, 'error executing one')
      throw createError.BadRequest(error.message.trim())
    })
}
