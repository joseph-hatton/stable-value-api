const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contractEditHistories/contractEditHistorySchema')

describe('insert new contract edit history', () => {
  let target, selectFromDb
  beforeEach(() => {
    const selectBind = {
      whereClause: `WHERE ${schema.table}.contract_id = :id`,
      bindParams: { id: 542 }
    }
    selectFromDb = sinon.stub().withArgs(schema, selectBind).resolves('done')
    target = proxyquire('api/contractEditHistories/getContractEditHistories', {
      '../db/selectFromDb': selectFromDb
    })
  })

  it('returns select response', async () => {
    const response = await target(542)
    response.should.eql('done')
  })
})
