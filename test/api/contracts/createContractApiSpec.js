/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contracts/contractSchema')
const cloneDeep = require('lodash/cloneDeep')

const CONTRACT_INSERT_RESPONSE = {contractId: 542}
const CONTRACT_INSERT_PARAMS = {stuff: true}
const USER_ID = 'me'
const HEADERS = 'HEADERS'

describe('create contract api', () => {
  let target, res, jsonStub, next, body, getUserId, insertIntoDb, selectByIdFromDb, buildContractInsertParams, validateThenCallApi, insertNewContractEditHistory, buildContractNumber
  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    next = sinon.spy()
    body = {}
    getUserId = sinon.stub().returns(USER_ID)
    insertIntoDb = sinon.stub().resolves(CONTRACT_INSERT_RESPONSE)
    selectByIdFromDb = sinon.stub().resolves('SELECT_BY_ID_RESPONSE')
    buildContractInsertParams = sinon.stub().returns(CONTRACT_INSERT_PARAMS)
    validateThenCallApi = sinon.stub().resolves('VALIDATION RESULT')
    insertNewContractEditHistory = sinon.stub().resolves('okay')
    buildContractNumber = sinon.stub().resolves('CONTRACT_NUMBER')
    target = proxyquire('api/contracts/createContractApi', {
      '../getUserId': getUserId,
      '../db/insertIntoDb': insertIntoDb,
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../common/validateThenCallApi': validateThenCallApi,
      '../contractEditHistories/insertNewContractEditHistory': insertNewContractEditHistory,
      './contractSchema': schema,
      './buildContractNumber': buildContractNumber,
      './buildContractInsertParams': buildContractInsertParams
    })
  })

  describe('validate then call api', () => {
    let response
    beforeEach(async () => {
      response = await target({headers: HEADERS, body: body}, res, next)
    })

    it('returns validation result', () => {
      response.should.eql('VALIDATION RESULT')
    })

    it('then calls api', () => {
      validateThenCallApi.should.have.been.called
    })

    it('then calls api with schema', () => {
      validateThenCallApi.getCall(0).args[0].should.eql(schema)
    })

    it('then calls api with body', () => {
      validateThenCallApi.getCall(0).args[1].should.eql(body)
    })

    it('then calls with required fields', () => {
      validateThenCallApi.getCall(0).args[2].should.eql(schema.required.new)
    })

    it('then calls with response', () => {
      validateThenCallApi.getCall(0).args[3].should.eql(res)
    })

    it('then calls with next', () => {
      validateThenCallApi.getCall(0).args[4].should.eql(next)
    })

    it('then calls with user id', () => {
      validateThenCallApi.getCall(0).args[6].should.eql({userId: USER_ID})
    })
  })

  describe('attempts to insert', () => {
    let response, editHistory

    beforeEach(async () => {
      editHistory = {effectiveDate: (new Date()).toISOString()}
      body.editHistories = [editHistory]
      response = await target({headers: HEADERS, body: body}, res, next)
      await validateThenCallApi.getCall(0).args[5](schema, body, res, next, {userId: USER_ID})
    })

    it('calls build contract number', () => {
      buildContractNumber.should.have.been.called
    })

    it('calls build contract insert params', () => {
      buildContractInsertParams.should.have.been.calledWithExactly(USER_ID, body, 'CONTRACT_NUMBER')
    })

    it('calls insert into db with', () => {
      insertIntoDb.should.have.been.calledWithExactly(schema, CONTRACT_INSERT_PARAMS, USER_ID, true)
    })

    it('calls insert new contract edit history', () => {
      insertNewContractEditHistory.should.have.been.calledWithExactly(CONTRACT_INSERT_PARAMS, editHistory, CONTRACT_INSERT_RESPONSE.contractId, USER_ID)
    })

    it('calls select by id', () => {
      selectByIdFromDb.should.have.been.calledWithExactly(schema, CONTRACT_INSERT_RESPONSE.contractId)
    })

    it('sendsd select by id to response json', () => {
      jsonStub.should.have.been.calledWithExactly('SELECT_BY_ID_RESPONSE')
    })
  })
})
