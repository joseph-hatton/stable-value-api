const buildContractIdWhereClause = require('../../../api/contracts/buildContractIdWhereClause')

const nullPagination = {pageNumber: null, pageSize: null}
const DEFAULT_STATE = { bindParams: {}, whereClause: '', pagination: nullPagination }

describe('build contracts contacts query where clause', () => {
  it('handles empty schema and req', () => buildContractIdWhereClause({}, {}).should.eql(DEFAULT_STATE))

  it('handles contractId path parameter', () => {
    return buildContractIdWhereClause({table: 'table'}, {params: {contractId: 12345}}).should.eql({ bindParams: {contractId: 12345}, whereClause: 'WHERE table.contract_id = :contractId', pagination: nullPagination })
  })

  it('ignores other path parameters', () => {
    return buildContractIdWhereClause({table: 'table'}, {params: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
