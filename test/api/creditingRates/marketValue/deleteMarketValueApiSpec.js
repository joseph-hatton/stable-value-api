/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')
const contractSchema = require('../../../../api/contracts/contractSchema')

const params = {params: true}
const headers = ['header']
const body = {body: true}
const creditingRateId = 552
const contractId = 54
const userId = 'a user'

describe('delete market value api', () => {
  let target, res, resJson, next, updateDb, getUserId, selectByIdFromDb, deleteByIdFromDb
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    deleteByIdFromDb = sinon.stub().resolves('delete by id result')
    selectByIdFromDb = sinon.stub()
    selectByIdFromDb.withArgs(contractSchema, contractId).resolves({status: 'ACTIVE', contractId})
    selectByIdFromDb.withArgs(schema, creditingRateId).resolves({status: 'AWAITING', creditingRateId, effectiveDate: (new Date()).toISOString()})
    getUserId = sinon.stub().returns(userId)
    target = proxyquire('api/creditingRates/marketValue/deleteMarketValueApi', {
      '../../getUserId': getUserId,
      '../../db/deleteByIdFromDb': deleteByIdFromDb,
      '../../db/selectByIdFromDb': selectByIdFromDb
    })
  })

  describe('attempt to delete', () => {
    let apiParams
    beforeEach(async () => {
      apiParams = {contractId, userId, id: creditingRateId}
    })

    it('null contract throws error', async () => {
      selectByIdFromDb.withArgs(contractSchema, 54).resolves(null)
      try {
        await target({params: apiParams, headers: 'headers'}, res)
      } catch (err) {
        err.message.should.eql('Forecasted, Awaiting Calc, Pending or Crediting Rates approved in current open month can be deleted.')
      }
    })

    it('null crediting rate throws error', async () => {
      selectByIdFromDb.withArgs(schema, creditingRateId).resolves(null)
      try {
        await target({params: apiParams, headers: 'headers'}, res)
      } catch (err) {
        err.message.should.eql('Forecasted, Awaiting Calc, Pending or Crediting Rates approved in current open month can be deleted.')
      }
    })

    it('non active contract throws error', async () => {
      selectByIdFromDb.withArgs(contractSchema, contractId).resolves({status: 'PENDING'})
      try {
        await target({params: apiParams, headers: 'headers'}, res)
      } catch (err) {
        err.message.should.eql('Forecasted, Awaiting Calc, Pending or Crediting Rates approved in current open month can be deleted.')
      }
    })

    it('non approved crediting rate throws error', async () => {
      selectByIdFromDb.withArgs(schema, creditingRateId).resolves({status: 'PENDING'})
      try {
        await target({params: apiParams, headers: 'headers'}, res)
      } catch (err) {
        err.message.should.eql('Forecasted, Awaiting Calc, Pending or Crediting Rates approved in current open month can be deleted.')
      }
    })

    it('utc in different month crediting rate throws error', async () => {
      selectByIdFromDb.withArgs(schema, creditingRateId).resolves({status: 'APPROVED', creditingRateId, effectiveDate: (new Date(0)).toISOString()})
      try {
        await target({params: apiParams, headers: 'headers'}, res)
      } catch (err) {
        err.message.should.eql('Forecasted, Awaiting Calc, Pending or Crediting Rates approved in current open month can be deleted.')
      }
    })

    it('calls delete by id', async () => {
      await target({params: apiParams, headers: 'headers'}, res)
      deleteByIdFromDb.should.have.been.calledWithExactly(schema, creditingRateId, userId)
    })

    it('sends delete by id response', async () => {
      await target({params: apiParams, headers: 'headers'}, res)
      resJson.should.have.been.calledWithExactly('delete by id result')
    })
  })
})
