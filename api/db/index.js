const createError = require('http-errors')
const oracledb = require('oracledb')
const log = require('../../log')
const {db} = require('../config')

oracledb.outFormat = oracledb.OBJECT

oracledb.fetchAsString = [ oracledb.NUMBER ]

oracledb.maxRows = 10000

const connectToDb = async () => {
  try {
    return await oracledb.getConnection(db)
  } catch (err) {
    const message = 'Cannot connect to Oracle database.'
    log.error({user: db.user, connectString: db.connectString}, message)
    throw createError.InternalServerError(message)
  }
}

connectToDb.BIND_OUT = oracledb.BIND_OUT

connectToDb.TYPES = {
  DATE: oracledb.DATE,
  NUMBER: oracledb.NUMBER,
  STRING: oracledb.STRING
}

module.exports = connectToDb
