const log = require('../../../log')
const dbExecuteOne = require('./dbExecuteOne')

const executeAll = (connection, multipleExecutions, executionResults = []) => {
  if (multipleExecutions.length > 0) {
    return dbExecuteOne(connection, multipleExecutions[0])
    .then((result) => {
      executionResults.push(result)
      return executeAll(connection, multipleExecutions.slice(1, multipleExecutions.length), executionResults)
    })
    .catch(error => {
      log.error(error)
      throw error
    })
  } else {
    return Promise.resolve(executionResults)
  }
}

module.exports = executeAll
