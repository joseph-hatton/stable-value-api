SELECT  contract.contract_id,
        contract.contract_number,
        contract.contract_type,
        contract.short_plan_name,
        contract.status,
        contract.effective_date,
        contract.additional_deposits,
        contract_edit_history.assignment_state as ceh_assignment_state,
        contract_edit_history.manager_id as ceh_manager_id,
        contract_edit_history.contract_edit_history_id as ceh_id,
        contract_edit_history.effective_date as ceh_effective_date
FROM sch_stbv.contract
LEFT OUTER JOIN sch_stbv.contract_edit_history
ON contract.contract_id = contract_edit_history.contract_id
ORDER BY contract.contract_number