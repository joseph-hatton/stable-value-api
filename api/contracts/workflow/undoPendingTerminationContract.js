const createError = require('http-errors')
const log = require('../../../log')
const schema = require('../contractSchema')
const transitionContractInWorkflow = require('./transitionContractInWorkflow')
const calculateAndSaveCreditingRateForecast = require('../../creditingRates/forecasts/calculateAndSaveCreditingRateForecast')
const buildUpdateParams = require('../../common/buildUpdateParams')
const updateDb = require('../../db/updateDb')

const validateCanTransition = async ({contract, userId}) => {
  if (contract.status !== 'PENDING_TERMINATION') {
    return 'Only PENDING_TERMINATION Contracts may be undone from a scheduled pending termination.'
  }
}

const callBeforeChangingState = async ({contract, userId}) => {
  const contractId = contract.contractId
  log.info({domain: schema.tableShort, id: contractId, userId}, 'undo scheduled pending termination contract')

  await calculateAndSaveCreditingRateForecast(contract)

  const updateParams = buildUpdateParams(schema.columns, userId, {statedMaturityDate: new Date('2999-01-01T00:00:00.000Z')})
  await updateDb(schema, updateParams, contractId, userId)
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'undoScheduledPendingTermination',
  nextContractStatus: 'ACTIVE',
  validateCanTransition,
  callBeforeChangingState
})
