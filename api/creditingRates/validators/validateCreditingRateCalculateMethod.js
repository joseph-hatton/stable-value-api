const validateIdAndText = require('../../common/validateIdTextField')
const {creditingRateCalculationMethods} = require('../../referenceData.json')
module.exports = validateIdAndText(creditingRateCalculationMethods, 'Invalid Crediting Rate Calculation Method')
