/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/transactions/transactionSchema')

describe('query transaction summary', () => {
  let target, req, connection, dbExecutePromise, dbExecuteOne
  beforeEach(() => {
    connection = 'the connection'
    req = {someRequest: true}
    dbExecuteOne = sinon.stub().resolves({selectFromDb: true})
    dbExecutePromise = sinon.stub().returns((connection) => connection)
    target = proxyquire('api/transactions/queryTransactionSummary', {
      './summarySqlSelect.sql': 'summarySqlSelect',
      './summarySqlWhereClause.sql': 'summarySqlWhereClause',
      '../db/executors/dbExecutePromise': dbExecutePromise,
      '../db/executors/dbExecuteOne': dbExecuteOne
    })
  })

  describe('whereClause null', () => {
    beforeEach(async () => {
      await target(req, {whereClause: null})
      await dbExecutePromise.getCall(0).args[0](connection)
    })

    it('select transactions from db with pagination called', () => {
      dbExecuteOne.should.have.been.called
    })

    it('executeOne called with connection', () => {
      dbExecuteOne.getCall(0).args[0].should.eql(connection)
    })

    it('executeOne called with connection', () => {
      dbExecuteOne.getCall(0).args[1].sql.should.eql('summarySqlSelect WHERE summarySqlWhereClause')
    })

    it('mapResult', () => {
      const {mapResult} = dbExecuteOne.getCall(0).args[1]
      const row = {
        DEPOSITS: '1.1',
        WITHDRAWALS: '2.2',
        ADJUSTMENTS: '3.3',
        FEE_RECEIPTS: '4.4',
        FEE_ADJUSTMENTS: '5.5',
        TERMINATIONS: '6.6',
        AMOUNT_BV_PERCENT: '7.7',
        WITHDRAWL_BV_PERCENT: '8.8',
        DEPOSIT_BV_PERCENT: '9.9'
      }
      mapResult({rows: [row]}).should.eql({
        deposits: 1.1,
        withdrawals: 2.2,
        adjustments: 3.3,
        feeReceipts: 4.4,
        feeAdjustments: 5.5,
        terminations: 6.6,
        amountBvPercent: 7.7,
        withdrawalBvPercent: 8.8,
        depositBvPercent: 9.9
      })
    })
  })

  describe('whereClause not null', () => {
    beforeEach(async () => {
      await target(req, {whereClause: 'some where clause'})
      await dbExecutePromise.getCall(0).args[0](connection)
    })

    it('executeOne called with connection', () => {
      dbExecuteOne.getCall(0).args[1].sql.should.eql('summarySqlSelect WHERE summarySqlWhereClause AND some  clause')
    })
  })
})
