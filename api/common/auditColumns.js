const {dateConverter, numberConverter} = require('../db/converters')

module.exports = {
  createdDate: {name: 'createdDate', column: 'created_date', defaultValue: () => new Date(), immutable: true, converter: dateConverter},
  createdId: {name: 'createdId', column: 'created_id', defaultValue: () => 'System', immutable: true},
  modifiedDate: {name: 'modifiedDate', column: 'modified_date', defaultValue: () => new Date(), converter: dateConverter},
  modifiedId: {name: 'modifiedId', column: 'modified_id', defaultValue: () => 'System'},
  version: {name: 'version', defaultValue: () => 0, converter: numberConverter}
}
