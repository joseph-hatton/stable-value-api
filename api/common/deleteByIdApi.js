const getUserId = require('../getUserId')
const deleteByIdFromDb = require('../db/deleteByIdFromDb')
const log = require('../../log')

module.exports = (schema) =>
  async ({ params, headers }, res, next) => {
    log.info({domain: schema.tableShort, id: params.id}, 'delete by id api')

    const userId = getUserId(headers)

    const result = await deleteByIdFromDb(schema, params.id, userId)

    res.status(200).json(result)
  }
