const validateSchema = require('../validateSchema')
const target = require('../../../api/companies/companySchema')
const {createdDate, createdId, modifiedDate, modifiedId} = require('../../../api/common/auditColumns')
const {booleanConverter, numberConverter} = require('../../../api/db/converters')
const stateValidator = require('../../../api/states/stateValidator')

const falseDefault = () => false

describe('company schema', () => {
  validateSchema(target, {
    tableShort: 'company',
    table: 'sch_stbv.company',
    tableId: 'company_id',
    sequence: 'sch_stbv.company_seq.NEXTVAL',
    orderBy: 'name',
    columns: [
      {name: 'companyId', column: 'company_id', sequence: 'sch_stbv.company_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'name', audit: true},
      {name: 'address1', audit: true},
      {name: 'address2', audit: true},
      {name: 'city', audit: true},
      {name: 'state', audit: true, validators: [stateValidator]},
      {name: 'zipCode', column: 'zip_code', audit: true},
      {name: 'version', defaultValue: () => 0, converter: numberConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId,
      {name: 'receivesEdiSw', column: 'receives_edi_sw', defaultValue: falseDefault, converter: booleanConverter}
    ],
    audit: {
      objectClass: 'Company',
      buildDescription: (company) => `Name: ${company.name}`
    }
  })
})
