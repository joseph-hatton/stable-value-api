/* eslint-disable no-return-assign */
module.exports = () => {
  const methods = [
    'type',
    'json',
    'status',
    'send',
    'redirect',
    'set',
    'end',
    'contentType',
    'sendStatus'
  ]

  const mockResponse = {
    listeners: {
    }
  }

  methods.forEach(m => mockResponse.listeners[m] = [])

  mockResponse.on = (event, listener) => mockResponse.listeners[event].push(listener)
  mockResponse.emit = (event, args) => mockResponse.listeners[event].forEach(listener => listener(args))

  methods.forEach(method => mockResponse[method] = sinon.spy((...args) => {
    mockResponse.emit(method, args)
    return mockResponse
  }))

  return mockResponse
}
