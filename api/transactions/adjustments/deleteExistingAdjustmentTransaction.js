const log = require('../../../log')
const deleteByIdFromDb = require('../../db/deleteByIdFromDb')
const schema = require('../transactionSchema')

module.exports = async (dbTransaction, userId) => {
  if (dbTransaction.adjustmentTransactionId) {
    log.info({domain: schema.tableShort, transactionId: dbTransaction.adjustmentTransactionId, userId}, 'deleting adjustment transaction')
    await deleteByIdFromDb(schema, dbTransaction.adjustmentTransactionId, userId)
  }
}
