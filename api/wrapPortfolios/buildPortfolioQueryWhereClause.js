const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const wherePortfolioManagerIdEquals = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.portfolioManagerId) {
    whereClauseBits.push(`PORTFOLIO_MANAGER_ID = :portfolioManagerId`)
    response.bindParams.portfolioManagerId = req.query.portfolioManagerId * 1
  }
}

module.exports = (schema, req) =>
  buildWhereClauseAndParamsWithPaging(schema, req, [wherePortfolioManagerIdEquals])
// { whereClause, bindParams, executeOptions, pagination }
