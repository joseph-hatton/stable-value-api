/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const BIND_PARAMS = {binding: true}
const INSERT_QUERY = 'INSERT_QUERY'

describe('execute insert query', () => {
  let target, dbExecuteOne, connection, buildInsertQuery
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves('ONE_EXECUTED')
    buildInsertQuery = sinon.stub().returns(INSERT_QUERY)
    target = proxyquire('api/db/executors/executeInsertQuery', {
      './dbExecuteOne': dbExecuteOne,
      '../queries/buildInsertQuery': buildInsertQuery
    })
  })

  it('returns execute result', () =>
    target(connection, schema, BIND_PARAMS)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute one should only be called once', () =>
    target(connection, schema, BIND_PARAMS)
    .then(() => dbExecuteOne.callCount.should.eql(1)))

  it('execute one  should only be called with', () =>
    target(connection, schema, BIND_PARAMS)
    .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, INSERT_QUERY)))

  it('build insert query should have been called with', () =>
    target(connection, schema, BIND_PARAMS)
    .then(() => buildInsertQuery.should.have.been.calledWithExactly(schema, BIND_PARAMS)))
})
