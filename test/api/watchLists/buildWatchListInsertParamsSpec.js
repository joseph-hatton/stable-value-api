const proxyquire = require('../../proxyquire')
const target = require('../../../api/watchLists/buildWatchListInsertParams')

const user = 'a user'

describe('build watch list insert params', () => {
  it('defaults all columns', () => {
    const params = target(user, {})
    params.should.eql({
      comments: null,
      contractId: null,
      createdDate: params.createdDate,
      createdId: user,
      effectiveDate: null,
      modifiedDate: params.modifiedDate,
      modifiedId: user,
      version: 0
    })
  })

  it('allows overrides from body', () => {
    const body = {
      comments: 'some comments',
      contractId: 55,
      effectiveDate: new Date()
    }
    const params = target(user, body)
    params.should.eql({
      comments: body.comments,
      contractId: body.contractId,
      createdDate: params.createdDate,
      createdId: user,
      effectiveDate: body.effectiveDate,
      modifiedDate: params.modifiedDate,
      modifiedId: user,
      version: 0
    })
  })
})
