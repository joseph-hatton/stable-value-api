/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'

const sql = 'sql'
const bindParams = {bindParams: true}

describe('delete from db', () => {
  let target, connection, dbExecutePromise, dbExecuteOne

  beforeEach(() => {
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves('db execute one')
    target = proxyquire('api/db/deleteFromDb', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/dbExecuteOne': dbExecuteOne
    })
  })

  const executeTarget = async (mapResult) => {
    const result = await target('sql', bindParams)
    return await dbExecutePromise.getCall(0).args[0](connection)
  }

  describe('execute promise', () => {
    it('called once', async () => {
      await executeTarget()
      dbExecutePromise.callCount.should.eql(1)
    })

    it('returns result', async () => {
      const result = await executeTarget()
      result.should.eql('db execute one')
    })

    it('db execute one called with', async () => {
      const result = await executeTarget()
      dbExecuteOne.getCall(0).args[0].should.eql(connection)
      dbExecuteOne.getCall(0).args[1].sql.should.eql(sql)
      dbExecuteOne.getCall(0).args[1].bindParams.should.eql(bindParams)
      dbExecuteOne.getCall(0).args[1].executeOptions.should.eql({autoCommit: true})
      dbExecuteOne.getCall(0).args[1].mapResult('test').should.eql('test')
    })
  })
})
