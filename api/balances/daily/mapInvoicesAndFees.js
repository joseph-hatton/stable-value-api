const _ = require('lodash')

module.exports = ({rows}) => {
  const fees = Object.values(rows
    .reduce((list, item, index) => {
      list[item.effectiveDate] = list[item.effectiveDate] ? list[item.effectiveDate] : { ..._.omit(item, ['invoiceNumber']),
        invoiceNumbers: []
      }
      if (item.invoiceNumber) {
        list[item.effectiveDate].invoiceNumbers.push(item.invoiceNumber)
      }
      return list
    }, []))

  return {
    results: fees,
    total: fees.length
  }
}
