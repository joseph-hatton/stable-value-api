const target = require('../../../api/balances/buildContractIdAndEffectiveDateWhereClause')

const DEFAULT_STATE = { bindParams: {id: null}, whereClause: 'WHERE sch_stbv.balance.contract_id = :id' }

describe('build contract id and effective date query where clause', () => {
  it('handles empty schema and req', () => target({}, {}).should.eql(DEFAULT_STATE))

  it('handles contractId params parameter', () => {
    return target({}, {params: {contractId: 12345}}).should.eql({ bindParams: {id: 12345}, whereClause: 'WHERE sch_stbv.balance.contract_id = :id' })
  })

  it('handles contractId and effective date params parameter', () => {
    const effectiveDate = '2017-12-19T18:32:45.800Z'
    return target({}, {params: {contractId: 12345}, query: {effectiveDate}}).should.eql({ bindParams: { id: 12345, effectiveDate: new Date('2017-12-19T00:00:00.000Z') }, whereClause: 'WHERE sch_stbv.balance.contract_id = :id AND sch_stbv.balance.effective_date = :effectiveDate' })
  })

  it('ignores other query parameters', () => {
    return target({}, {query: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
