SELECT
SUM (CASE TRANSACTION_TYPE WHEN 'DEPOSIT' THEN AMOUNT ELSE 0 END)
   AS DEPOSITS,
SUM (CASE TRANSACTION_TYPE WHEN 'WITHDRAWAL' THEN AMOUNT ELSE 0 END)
   AS WITHDRAWALS,
SUM (CASE TRANSACTION_TYPE WHEN 'ADJUSTMENT' THEN AMOUNT ELSE 0 END)
   AS ADJUSTMENTS,
SUM (CASE TRANSACTION_TYPE WHEN 'FEE_RECEIPT' THEN AMOUNT ELSE 0 END)
   AS FEE_RECEIPTS,
SUM (CASE TRANSACTION_TYPE WHEN 'FEE_ADJUSTMENT' THEN AMOUNT ELSE 0 END)
   AS FEE_ADJUSTMENTS,
SUM (CASE TRANSACTION_TYPE WHEN 'TERMINATION' THEN AMOUNT ELSE 0 END)
   AS TERMINATIONS,
SUM (CASE WHEN BEGINNING_BOOK_VALUE = 0 
   THEN 0 
   ELSE ROUND(((ABS(AMOUNT) * 100.00) / BEGINNING_BOOK_VALUE), 2) 
   END) AS AMOUNT_BV_PERCENT,
SUM(
   (SELECT
      SUM(CASE WHEN BALANCE2.BEGINNING_BOOK_VALUE = 0 
         THEN 0 
         ELSE ROUND(((ABS(TRANSACTION2.AMOUNT) * 100.00) / BALANCE2.BEGINNING_BOOK_VALUE), 2) 
         END)
    FROM SCH_STBV.TRANSACTION TRANSACTION2, SCH_STBV.BALANCE BALANCE2
   WHERE BALANCE2.CONTRACT_ID = TRANSACTION2.CONTRACT_ID 
      AND BALANCE2.EFFECTIVE_DATE = TRANSACTION2.EFFECTIVE_DATE 
      AND (TRANSACTION2.TRANSACTION_TYPE = 'WITHDRAWAL') 
      AND TRANSACTION2.EFFECTIVE_DATE = TRANSACTION.EFFECTIVE_DATE 
      AND TRANSACTION2.CONTRACT_ID = TRANSACTION.CONTRACT_ID)) AS WITHDRAWL_BV_PERCENT,
SUM(
   (SELECT
      SUM(CASE WHEN BALANCE3.BEGINNING_BOOK_VALUE = 0 
         THEN 0 
         ELSE ROUND(((ABS(TRANSACTION3.AMOUNT) * 100.00) / BALANCE3.BEGINNING_BOOK_VALUE), 2) 
         END)
    FROM SCH_STBV.TRANSACTION TRANSACTION3, SCH_STBV.BALANCE BALANCE3
   WHERE BALANCE3.CONTRACT_ID = TRANSACTION3.CONTRACT_ID 
      AND BALANCE3.EFFECTIVE_DATE = TRANSACTION3.EFFECTIVE_DATE 
      AND (TRANSACTION3.TRANSACTION_TYPE = 'DEPOSIT') 
      AND TRANSACTION3.EFFECTIVE_DATE = TRANSACTION.EFFECTIVE_DATE 
      AND TRANSACTION3.CONTRACT_ID = TRANSACTION.CONTRACT_ID)) AS DEPOSIT_BV_PERCENT
FROM SCH_STBV.TRANSACTION TRANSACTION, SCH_STBV.BALANCE, SCH_STBV.CONTRACT