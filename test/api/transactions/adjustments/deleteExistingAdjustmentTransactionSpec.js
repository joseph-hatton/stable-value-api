const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/transactions/transactionSchema')

const dbTransaction = {
  adjustmentTransactionId: 5,
  contractId: 88,
  effectiveDate: 'yesterday'
}

const userId = 'somebody'

describe('delete existing adjustment transaction', () => {
  let target, deleteByIdFromDb
  beforeEach(() => {
    deleteByIdFromDb = sinon.stub()
    target = proxyquire('api/transactions/adjustments/deleteExistingAdjustmentTransaction', {
      '../../db/deleteByIdFromDb': deleteByIdFromDb
    })
  })

  it('insert into db called with', async () => {
    await target(dbTransaction, 'somebody')
    deleteByIdFromDb.should.have.been.calledWithExactly(schema, dbTransaction.adjustmentTransactionId, userId)
  })
})
