const log = require('../../log')
const selectFromDb = require('../db/selectFromDb')
const schema = require('./contractEditHistorySchema')

module.exports = async (contractId) => {
  log.info({domain: schema.tableShort, id: contractId}, 'get contract edit histories')
  return await selectFromDb(schema, { whereClause: `WHERE ${schema.table}.contract_id = :id`, bindParams: {id: contractId} })
}
