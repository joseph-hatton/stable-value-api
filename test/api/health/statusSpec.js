const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('healthcheck status', () => {
  let getStatus, status, req, res, next

  beforeEach(() => {
    req = {
      headers: {}
    }
    res = mockResponse()
    next = sinon.stub()
    getStatus = sinon.stub().resolves({})

    status = proxyquire('api/health/status', {
      './getStatus': getStatus
    })
  })

  const apply = () => status(req, res, next)

  it('responds with ok: false if an error occurs', async () => {
    const message = 'crap'
    getStatus.resolves({error: new Error(message)})

    await apply()

    res.json.should.have.been.calledWithExactly({ok: false, error: message})
  })

  it('responds with ok: true if the service is healthy', async () => {
    await apply()

    res.json.should.have.been.calledWithExactly({ok: true})
  })

  it('includes clientId if present', async () => {
    const clientId = 'clientfoo'
    req.headers['client-id'] = clientId

    await apply()

    res.json.should.have.been.calledWithExactly({ok: true, clientId})
  })

  it('includes userId if present', async () => {
    const userId = 'user23'
    req.headers['user-id'] = userId

    await apply()

    res.json.should.have.been.calledWithExactly({ok: true, userId})
  })

  it('invokes next if something goes wrong', async () => {
    const error = new Error('!')
    getStatus.rejects(error)

    await apply()

    next.should.have.been.calledWithExactly(error)
  })
})
