const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/underwritings/underwritingSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

const zeroDefault = () => 0

describe('underwriting schema', () => {
  validateSchema(target, {
    tableShort: 'underwriting',
    table: 'sch_stbv.underwriting',
    tableId: 'underwriting_id',
    sequence: 'sch_stbv.underwriting_seq.NEXTVAL',
    orderBy: 'contract_id',
    columns: [
      {name: 'underwritingId', column: 'underwriting_id', sequence: 'sch_stbv.underwriting_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      version,
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      createdDate,
      {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
      {name: 'contributionRate', column: 'contribution_rate', converter: percentConverter},
      {name: 'withdrawalRate', column: 'withdrawal_rate', converter: percentConverter},
      {name: 'withdrawalVolatilityRate', column: 'withdrawal_volatility_rate', converter: percentConverter},
      {name: 'maximumWithdrawalRate', column: 'maximum_withdrawal_rate', converter: percentConverter},
      {name: 'bufferLevelRate', column: 'buffer_level_rate', converter: percentConverter},
      {name: 'extTerminationRate', column: 'ext_termination_rate', converter: percentConverter},
      {name: 'putQueueWithdrawalRate', column: 'put_queue_withdrawal_rate', converter: percentConverter, defaultValue: zeroDefault},
      {name: 'putLengthMonths', column: 'put_length_months', converter: numberConverter, defaultValue: zeroDefault},
      {name: 'maximumPutQueueRate', column: 'maximum_put_queue_rate', converter: percentConverter, defaultValue: zeroDefault},
      {name: 'underwritingComments', column: 'underwriting_comments'},
      createdId,
      modifiedDate,
      modifiedId
    ],
    required: {
      new: ['contractId', 'effectiveDate', 'contributionRate', 'contributionRate', 'withdrawalRate', 'withdrawalVolatilityRate', 'maximumWithdrawalRate', 'bufferLevelRate', 'extTerminationRate'],
      update: []
    }
  })
})
