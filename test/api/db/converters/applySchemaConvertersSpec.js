const applySchemaConverters = require('../../../../api/db/converters/applySchemaConverters')

describe('apply schema converters', () => {
  let schema, objToConvert, overridingData

  beforeEach(() => {
    objToConvert = { test: 'ok' }
    overridingData = {}
    schema = {
      columns: [
        {
          name: 'alwaysExpected'
        }
      ]
    }
  })

  it('ignores columns with no converter', () => {
    schema.columns.push({name: 'test'})
    applySchemaConverters(schema, objToConvert).should.eql(objToConvert)
  })

  it('converts columns with converter', () => {
    schema.columns.push({name: 'test', converter: {fromDb: (from) => `${from}_converted`}})
    applySchemaConverters(schema, objToConvert).should.eql({ test: 'ok_converted' })
  })
})
