const log = require('../../log')
const buildUpdateParams = require('../common/buildUpdateParams')
const updateDb = require('../db/updateDb')
const schema = require('./contractEditHistorySchema')

module.exports = async (body, contractEditHistoryId, userId) => {
  log.info({domain: schema.tableShort, id: contractEditHistoryId, userId}, 'update contract edit history')
  const updateParamsAndBody = Object.assign({}, body, {
    modifiedId: userId,
    modifiedDate: new Date()
  })
  log.info({updateParamsAndBody, userId}, 'update edit history params')
  const updateParams = await buildUpdateParams(schema.columns, userId, updateParamsAndBody)
  return await updateDb(schema, updateParams, contractEditHistoryId, userId)
}
