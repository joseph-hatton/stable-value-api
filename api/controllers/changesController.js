const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const getByIdApi = require('../common/getByIdApi')
const changesSchema = require('../changes/changeSchema')
const propertyChangeSchema = require('../changes/propertyChangeSchema')
const buildChangesQueryWhereClause = require('../changes/buildChangesQueryWhereClause')
const buildPropertyChangesQueryWhereClause = require('../changes/buildPropertyChangesQueryWhereClause')

module.exports = createController({
  getMany: getManyApi(changesSchema, buildChangesQueryWhereClause),
  getOne: getByIdApi(changesSchema),
  getManyPropertyChanges: getManyApi(propertyChangeSchema, buildPropertyChangesQueryWhereClause),
  getOnePropertyChange: getByIdApi(propertyChangeSchema)
})
