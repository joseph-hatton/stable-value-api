const log = require('../../log')
const {responses: {unauthorized}} = require('../config')
const getUserId = require('../getUserId')
const getCycleStatus = require('./getCycleStatus')

const runningCycleMessage = {message: 'Please wait, cycle in progress...'}

module.exports = async (req, res, next) => {
  const userId = getUserId(req.headers)
  const cycleStatus = await getCycleStatus()
  if (cycleStatus && cycleStatus.isRunning) {
    log.info({userId, method: req.method, cycleStatus}, runningCycleMessage)
    res.status(503).json(runningCycleMessage)
  } else {
    next()
  }
}
