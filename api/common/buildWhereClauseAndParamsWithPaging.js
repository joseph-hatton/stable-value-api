const buildWhereClauseAndParams = require('./buildWhereClauseAndParams')

const wherePaging = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.pageNumber) {
    response.pagination.pageNumber = req.query.pageNumber * 1
    response.pagination.pageSize = (req.query.pageSize) ? req.query.pageSize * 1 : 50
  }
}

module.exports = (schema, req, customRequestParsers = []) => {
  customRequestParsers.push(wherePaging)

  return buildWhereClauseAndParams(schema, req, customRequestParsers)
}
// { whereClause, bindParams, executeOptions, pagination }
