const log = require('../../../log')
const propertyChangeSchema = require('../../changes/propertyChangeSchema')
const buildInsertQuery = require('./buildInsertQuery')
const buildPropertyChangeBindParams = require('./buildPropertyChangeBindParams')

module.exports = (schema, changeId, newObject) => {
  const auditableColumns = schema.columns.filter((column) => column.audit === true)
  if (auditableColumns.length > 0) {
    const propertyChangeQueries = []
    log.info({domain: schema.tableShort, auditableColumnsLength: auditableColumns.length}, 'should enable insert properties audit')
    auditableColumns.forEach((column) => {
      let newValue = newObject[column.name]
      log.info({domain: schema.tableShort, auditableColumn: column.name, newValue}, 'should enable insert properties audit for column')
      if (newValue) {
        newValue = newValue.toString()
        propertyChangeQueries.push(buildInsertQuery(propertyChangeSchema, buildPropertyChangeBindParams(schema, changeId, column.name, null, newValue)))
      }
    })
    return propertyChangeQueries
  } else {
    log.info({domain: schema.tableShort}, 'should NOT enable insert properties audit')
    return []
  }
}
