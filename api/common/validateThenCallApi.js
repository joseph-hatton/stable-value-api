const schemaValidator = require('./schemaValidator')

module.exports = (schema, body, requiredFields, res, next, apiCall, apiCallOpts) => {
  const validationResult = schemaValidator(schema, requiredFields, body)
  return (validationResult && validationResult.length > 0)
    ? res.status(400).json({ message: validationResult.join(' and ') })
    : apiCall(schema, body, res, next, apiCallOpts)
}
