const proxyquire = require('../../proxyquire')

const SQL = 'SOME SQL'

const row1 = {
  transactionId: 88,
  fdFeeDistributionId: '5',
  fdInvoiceId: '343',
  fdFeeAmount: '888',
  fdCreatedDate: null,
  fdCreatedId: null,
  fdModifiedDate: null,
  fdModifiedId: null,
  fdVersion: '0',
  fdBeginningBalance: '777',
  fdAdjustment: '222',
  fdEndingBalance: '879',
  balanceId: '888',
  beginningBookValue: '1234',
  iInvoiceNumber: '3433'
}

describe('override get one transaction select', () => {
  let target
  beforeEach(() => {
    target = proxyquire('api/transactions/overrideGetOneTransactionSelect', {
      './overrideGetOneTransactionSelect.sql': SQL
    })
  })

  it('has an expected sql', () =>
    target.sql.should.eql(SQL))

  describe('maps db results', () => {
    it('returns empty results when passed empty', () =>
      target.mapper([]).should.eql([]))

    it('returns fee distribution moved to array', () =>
      target.mapper([row1]).should.eql([
        {
          transactionId: 88,
          balanceId: 888,
          beginningBookValue: 1234,
          feeDistributions: [{
          feeDistributionId: 5,
          invoiceId: 343,
          invoiceNumber: 3433,
          amount: 888,
          createdDate: null,
          createdId: null,
          modifiedDate: null,
          modifiedId: null,
          version: 0,
          beginningBalance: 777,
          adjustment: 222,
          endingBalance: 879
        }]}
      ]))
  })
})
