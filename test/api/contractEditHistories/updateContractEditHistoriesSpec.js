/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contractEditHistories/contractEditHistorySchema')

const contractId = 888
const userId = 'me'
const dbEditHistory1 = {contractEditHistoryId: 12, effectiveDate: new Date('2017-01-06T17:22:34.695Z')}

describe('update contract edit histories', () => {
  let target, body, contractUpdateParams, expectedUpdateParams, getContractEditHistories, updateContractEditHistory, insertNewContractEditHistory

  beforeEach(() => {
    body = {}
    contractUpdateParams = {modifiedId: userId, modifiedDate: (new Date()).toISOString()}
    getContractEditHistories = sinon.stub().resolves([dbEditHistory1])
    updateContractEditHistory = sinon.stub().returns('params ready to go')
    insertNewContractEditHistory = sinon.stub()
    target = proxyquire('api/contractEditHistories/updateContractEditHistories', {
      './getContractEditHistories': getContractEditHistories,
      './updateContractEditHistory': updateContractEditHistory,
      './insertNewContractEditHistory': insertNewContractEditHistory
    })
    expectedUpdateParams = {
      contractEditHistoryId: dbEditHistory1.contractEditHistoryId,
      effectiveDate: dbEditHistory1.effectiveDate.toISOString(),
      modifiedDate: contractUpdateParams.modifiedDate,
      modifiedId: contractUpdateParams.modifiedId,
      change: 'update existing change'
    }
  })

  describe('on a pending contract', () => {
    beforeEach(async () => {
      body = {status: 'PENDING', editHistories: [{change: 'update existing change'}]}

      await target(contractUpdateParams, body, contractId)
    })

    it('updates the only contract edit history', () => {
      updateContractEditHistory.should.have.been.calledWithExactly(expectedUpdateParams, 12, userId)
      insertNewContractEditHistory.should.have.not.been.called
    })
  })

  describe('on an active contract', () => {
    describe('with an update to an existing history', () => {
      beforeEach(async () => {
        body = {status: 'ACTIVE', editHistories: [{change: 'update existing change', effectiveDate: dbEditHistory1.effectiveDate.toISOString()}]}
        await target(contractUpdateParams, body, contractId)
      })

      it('updates the only contract edit history', async () => {
        updateContractEditHistory.should.have.been.calledWithExactly(expectedUpdateParams, 12, userId)
        insertNewContractEditHistory.should.have.not.been.called
      })
    })

    describe('with an update to an existing history and a new one ', () => {
      beforeEach(async () => {
        body = {
          status: 'ACTIVE',
          editHistories: [
            {change: 'update existing change', effectiveDate: dbEditHistory1.effectiveDate.toISOString()},
            {change: 'a new edit history', effectiveDate: '2017-02-06T-02:22:34.695Z'}
        ]}
        await target(contractUpdateParams, body, contractId)
      })

      it('updates the existing contract edit history', () => {
        updateContractEditHistory.should.have.been.calledWithExactly(expectedUpdateParams, 12, userId)
      })

      it('creates a new contract edit history', () => {
        insertNewContractEditHistory.should.have.been.calledWithExactly(contractUpdateParams, body.editHistories[1], contractId, userId)
      })
    })
  })
})
