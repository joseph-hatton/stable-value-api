SELECT  contract.*,
        contract_edit_history.contract_edit_history_id as ceh_id,
        contract_edit_history.version as ceh_version,
        contract_edit_history.effective_date as ceh_effective_date,
        contract_edit_history.created_date as ceh_created_date,
        contract_edit_history.created_id as ceh_created_id,
        contract_edit_history.modified_date as ceh_modified_date,
        contract_edit_history.modified_id as ceh_modified_id,
        contract_edit_history.jurisdiction as ceh_jurisdiction,
        contract_edit_history.premium_rate as ceh_premium_rate, 
        contract_edit_history.manager_fee_rate as ceh_manager_fee_rate,
        contract_edit_history.advisor_fee_rate as ceh_advisor_fee_rate,
        contract_edit_history.crediting_rate_calc_method as ceh_crediting_rate_calc_method,
        contract_edit_history.assignment_state as ceh_assignment_state,
        contract_edit_history.legal_form_name as ceh_legal_form_name,
        contract_edit_history.manager_id as ceh_manager_id,
        contract_edit_history.fund_level_utilization as ceh_fund_level_utilization,
        contract_edit_history.plan_level_utilization as ceh_plan_level_utilization,
        contract_edit_history.fund_active_rate as ceh_fund_active_rate,
        contract_edit_history.fund_senior_rate as ceh_fund_senior_rate,
        contract_edit_history.sp_rating as ceh_sp_rating,
        contract_edit_history.moody_rating as ceh_moody_rating,
        contract_edit_history.fitch_rating as ceh_fitch_rating,
        contract_edit_history.overall_rating as ceh_overall_rating,
        contract_edit_history.account_duration_limit as ceh_account_duration_limit,
        contract_derivative_type.derivative_type_id,
        derivative_type.description
FROM sch_stbv.contract
LEFT OUTER JOIN sch_stbv.contract_edit_history
ON contract.contract_id = contract_edit_history.contract_id
LEFT OUTER JOIN sch_stbv.contract_derivative_type
ON contract.contract_id = contract_derivative_type.contract_id
LEFT OUTER JOIN sch_stbv.derivative_type
ON contract_derivative_type.derivative_type_id = derivative_type.derivative_type_id