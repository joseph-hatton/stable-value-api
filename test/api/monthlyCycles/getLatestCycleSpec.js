const proxyquire = require('../../proxyquire')
const schema = require('../../../api/monthlyCycles/monthlyCyclesSchema')

describe('get latest cycle', () => {
  let target, selectFromDb
  beforeEach(() => {
    selectFromDb = sinon.stub().withArgs(schema, { executeOptions: { maxRows: 1 } }).resolves(['one', 'two'])
    target = proxyquire('api/monthlyCycles/getLatestCycle', {
      '../db/selectFromDb': selectFromDb
    })
  })

  it('returns first result', async () => {
    const result = await target()
    result.should.have.been.eql('one')
  })
})
