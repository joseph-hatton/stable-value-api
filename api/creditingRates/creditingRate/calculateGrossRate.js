const mathjs = require('mathjs')
const log = require('../../../log')

const calculator = {
  'COMPOUND_AMORTIZATION_METHOD': (marketValue, bookValue, duration, aey) => {
    // ([(1+AEY) * (MV/BV)^(1/D)] -1)
    return (((1 + aey) * (Math.pow((marketValue / bookValue), (1 / duration)))) - 1)
  },
  'BOOK_VALUE_DOLLAR_DURATION': (marketValue, bookValue, duration, annualEffectiveYield) => {
    // (AEY + [(MV – BV) / (BV * D)])
    return (annualEffectiveYield + ((marketValue - bookValue) / (bookValue * duration)))
  },
  'MARKET_VALUE_DOLLAR_DURATION': (marketValue, bookValue, duration, annualEffectiveYield) => {
  // (AEY + [(MV - BV) / (MV * D)])
    return (annualEffectiveYield + ((marketValue - bookValue) / (marketValue * duration)))
  }
}
module.exports = ({creditingRateCalcMethod, marketValue, bookValue, duration, annualEffectiveYield, creditingRateRoundDecimals = 2}) => {
  log.info({domain: 'crediting_rate', creditingRateCalcMethod, marketValue, bookValue, duration, annualEffectiveYield, creditingRateRoundDecimals}, 'calculating gross rate')

  const calc = calculator[creditingRateCalcMethod]
  const aeyForCalc = annualEffectiveYield / 100
  const grossRate = (calc) ? calc(marketValue, bookValue, duration, aeyForCalc) : 0.0
  const calculatedGrossRate = mathjs.round(grossRate, creditingRateRoundDecimals + 2)

  log.info({domain: 'crediting_rate', creditingRateCalcMethod, marketValue, bookValue, duration, annualEffectiveYield, creditingRateRoundDecimals, calculatedGrossRate, beforeRounding: grossRate}, 'calculated gross rate')
  return calculatedGrossRate
}
