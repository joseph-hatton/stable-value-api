const validateSchema = require('../validateSchema')
const target = require('../../../api/changes/changeSchema')
const {createdDate, createdId} = require('../../../api/common/auditColumns')
const {numberConverter} = require('../../../api/db/converters')

describe('change schema', () => {
  validateSchema(target, {
    tableShort: 'change',
    table: 'sch_stbv.change',
    tableId: 'change_id',
    sequence: 'sch_stbv.change_seq.NEXTVAL',
    orderBy: 'change.created_date desc',
    columns: [
      {name: 'changeId', column: 'change_id', sequence: 'sch_stbv.change_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'objectId', column: 'object_id', immutable: true, converter: numberConverter},
      {name: 'operation', immutable: true},
      {name: 'objectClass', column: 'object_class', immutable: true},
      createdDate,
      createdId,
      {name: 'effectiveDate', column: 'effective_date', immutable: true},
      {name: 'contractId', column: 'contract_id', immutable: true, converter: numberConverter},
      {name: 'description'}
    ]
  })
})
