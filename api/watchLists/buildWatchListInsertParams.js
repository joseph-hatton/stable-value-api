const buildDefaultParams = require('../common/buildDefaultParams')
const schema = require('./watchListSchema')

module.exports = (userId, body) =>
  buildDefaultParams(schema,
  body,
  {
    createdId: userId,
    modifiedId: userId
  })
