/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/feeDistributions/feeDistributionsSchema')

const userId = 'somebody'
const dbTransaction = {
  transactionId: 99,
  adjustmentTransactionId: 5,
  contractId: 88,
  effectiveDate: 'yesterday'
}
const clientAmount = 11

describe('update distribution', () => {
  let target, getInvoiceById, createFeeDistributionAndApplyPayment, deleteFeeDistribution
  beforeEach(() => {
    getInvoiceById = sinon.stub()
    createFeeDistributionAndApplyPayment = sinon.stub()
    deleteFeeDistribution = sinon.stub()
    target = proxyquire('api/transactions/distributions/updateDistribution', {
      '../../invoices/getInvoiceById': getInvoiceById,
      './createFeeDistributionAndApplyPayment': createFeeDistributionAndApplyPayment,
      './deleteFeeDistribution': deleteFeeDistribution
    })
  })

  it('create fee distribution and apply payment with adjustment present', async () => {
    const clientFeeDistribution = {adjustment: 11}
    await target(dbTransaction, clientFeeDistribution, clientAmount, userId)
    createFeeDistributionAndApplyPayment.should.have.been.calledWithExactly(clientAmount, clientFeeDistribution, null, null, dbTransaction, userId)
  })

  it('create fee distribution and apply payment with amount present', async () => {
    const clientFeeDistribution = {}
    await target(dbTransaction, clientFeeDistribution, clientAmount, userId)
    createFeeDistributionAndApplyPayment.should.have.been.calledWithExactly(clientAmount, clientFeeDistribution, null, null, dbTransaction, userId)
  })

  it('delete fee distribution when no amount or adjustment', async () => {
    const clientFeeDistribution = {}
    await target(dbTransaction, clientFeeDistribution, null, userId)
    deleteFeeDistribution.should.have.been.calledWithExactly(dbTransaction.transactionId, null, userId)
  })

  it('gets fee distribution when no amount or adjustment', async () => {
    const clientFeeDistribution = {}
    await target(dbTransaction, clientFeeDistribution, null, userId)
    deleteFeeDistribution.should.have.been.calledWithExactly(dbTransaction.transactionId, null, userId)
  })
})
