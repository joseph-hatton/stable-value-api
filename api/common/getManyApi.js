const log = require('../../log')
const selectFromDbWithPagination = require('../db/selectFromDbWithPagination')
const buildWhereClauseAndParamsWithPaging = require('./buildWhereClauseAndParamsWithPaging')

const selectRows = async (schema, whereClauseBindParamsAndExecuteOptions, res) => {
  const result = await selectFromDbWithPagination(schema, whereClauseBindParamsAndExecuteOptions)
  log.info({domain: schema.tableShort}, 'get many api results')
  return res.json(result)
}

module.exports = (schema, buildWhereClauseAndParams = buildWhereClauseAndParamsWithPaging) =>
  async (req, res, next) => {
    log.info({domain: schema.tableShort}, 'get many api')

    const whereClause = buildWhereClauseAndParams(schema, req)
    return await selectRows(schema, whereClause, res)
  }
