const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81

describe('execute delete by id query', () => {
  let target, dbExecuteOne, connection, buildDeleteQuery
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteOne = sinon.stub().resolves('ONE_EXECUTED')
    buildDeleteQuery = sinon.stub().returns('DELETE_QUERY')
    target = proxyquire('api/db/executors/executeDeleteByIdQuery', {
      './dbExecuteOne': dbExecuteOne,
      '../queries/buildDeleteQuery': buildDeleteQuery
    })
  })

  it('returns execute result', () =>
    target(connection, schema, ROW_ID)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute one should only be called once', () =>
    target(connection, schema, ROW_ID)
    .then(() => dbExecuteOne.callCount.should.eql(1)))

  it('execute one should only be called with', () =>
    target(connection, schema, ROW_ID)
    .then(() => dbExecuteOne.should.have.been.calledWithExactly(connection, 'DELETE_QUERY')))

  it('build delete query should have been called with', () =>
    target(connection, schema, ROW_ID)
    .then(() => buildDeleteQuery.should.have.been.calledWithExactly(schema, ROW_ID)))
})
