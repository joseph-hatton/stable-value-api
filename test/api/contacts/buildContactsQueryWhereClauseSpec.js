const buildContactsQueryWhereClause = require('../../../api/contacts/buildContactsQueryWhereClause')

const nullPagination = {pageNumber: null, pageSize: null}
const DEFAULT_STATE = { bindParams: {}, whereClause: '', pagination: nullPagination }

describe('build contacts query where clause', () => {
  it('handles empty schema and req', () => buildContactsQueryWhereClause({}, {}).should.eql(DEFAULT_STATE))

  it('handles companyId query parameter', () => {
    return buildContactsQueryWhereClause({}, {query: {companyId: 12345}}).should.eql({ bindParams: {companyId: 12345}, whereClause: 'WHERE contact.company_id = :companyId', pagination: nullPagination })
  })

  it('ignores other query parameters', () => {
    return buildContactsQueryWhereClause({}, {query: {address: 'something else'}}).should.eql(DEFAULT_STATE)
  })
})
