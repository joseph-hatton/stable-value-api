const queryOpenInvoicesForContract = require('../../../api/invoices/queryOpenInvoicesForContract')

describe('Query Open Invoices', () => {
  it('builds clause for open invoice query', () => {
    const schema = {table: 'invoice'}
    const req = {
      params: {
        contractId: 404
      }
    }

    const expectedClause = {
      bindParams: {contractId: 404},
      whereClause: 'WHERE invoice.contract_id = :contractId AND invoice.outstanding_balance <> 0',
      pagination: {
        pageNumber: null,
        pageSize: null
      }
    }

    const clause = queryOpenInvoicesForContract(schema, req)

    return clause.should.eql(expectedClause)
  })
})
