const validateIdAndText = require('../../common/validateIdTextField')
const {transactionDetailedTypes} = require('../../referenceData.json')
module.exports = validateIdAndText(transactionDetailedTypes, 'Invalid Transaction Detail Type')
