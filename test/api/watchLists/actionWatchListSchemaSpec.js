const validateSchema = require('../validateSchema')
const target = require('../../../api/watchLists/actionStepWatchListSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

const zeroDefault = () => 0

describe('action watch list schema', () => {
  validateSchema(target, {
    tableShort: 'action_step_watch_list',
    table: 'sch_stbv.action_step_watch_list',
    tableId: 'watch_list_id',
    orderBy: 'watch_list_id',
    columns: [
      {name: 'watchListId', column: 'watch_list_id', converter: numberConverter},
      {name: 'actionStepId', column: 'action_step_id', converter: numberConverter}
    ],
    required: {
      new: ['watchListId', 'actionStepId'],
      update: ['watchListId', 'actionStepId']
    }
  })
})
