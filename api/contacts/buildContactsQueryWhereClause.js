const buildWhereClauseAndParamsWithPaging = require('../common/buildWhereClauseAndParamsWithPaging')

const whereCompanyIdEquals = (schema, req, whereClauseBits, response) => {
  if (req.query && req.query.companyId) {
    whereClauseBits.push(`contact.company_id = :companyId`)
    response.bindParams.companyId = req.query.companyId * 1
  }
}

module.exports = (schema, req) =>
  buildWhereClauseAndParamsWithPaging(schema, req, [whereCompanyIdEquals])
// { whereClause, bindParams, executeOptions, pagination }
