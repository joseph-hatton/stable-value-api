/* eslint-disable no-unused-expressions */
const cloneDeep = require('lodash/cloneDeep')
const proxyquire = require('../../../proxyquire')
const testSchema = require('../../../data/testSchema')
const propertyChangeSchema = require('../../../../api/changes/propertyChangeSchema')

const CHANGE_ID = 42
const OLD_OBJECT = {auditableColumn: 'old'}
const NEW_OBJECT = {auditableColumn: 'new'}
const BUILD_PROPERTY_CHANGE_BIND_PARAMS_RESULT = 'BUILD_PROPERTY_CHANGE_BIND_PARAMS_RESULT'
const BUILD_INSERT_RESULT = 'BUILD_INSERT_RESULT'

describe('build update property change queries', () => {
  let target, buildInsertQuery, buildPropertyChangeBindParams

  beforeEach(() => {
    buildInsertQuery = sinon.stub().returns(BUILD_INSERT_RESULT)
    buildPropertyChangeBindParams = sinon.stub().returns(BUILD_PROPERTY_CHANGE_BIND_PARAMS_RESULT)
    target = proxyquire('api/db/queries/buildUpdatePropertyChangeQueries', {
      './buildInsertQuery': buildInsertQuery,
      './buildPropertyChangeBindParams': buildPropertyChangeBindParams
    })
  })

  it('returns empty array with no auditable columns', () =>
    target(testSchema.schema, CHANGE_ID, OLD_OBJECT, NEW_OBJECT).should.eql([]))

  describe('with auditable columns', () => {
    let schema

    beforeEach(() => {
      schema = Object.assign(cloneDeep(testSchema.schema), {columns: [{name: 'auditableColumn', audit: true}]})
    })

    it('returns build insert query', () =>
      target(schema, CHANGE_ID, OLD_OBJECT, NEW_OBJECT).should.eql([BUILD_INSERT_RESULT]))

    it('build property change bind params called with', () => {
      target(schema, CHANGE_ID, OLD_OBJECT, NEW_OBJECT)
      buildPropertyChangeBindParams.should.have.been.calledWithExactly(schema, CHANGE_ID, 'auditableColumn', OLD_OBJECT.auditableColumn, NEW_OBJECT.auditableColumn)
    })

    it('build insert called with', () => {
      target(schema, CHANGE_ID, OLD_OBJECT, NEW_OBJECT)
      buildInsertQuery.should.have.been.calledWithExactly(propertyChangeSchema, BUILD_PROPERTY_CHANGE_BIND_PARAMS_RESULT)
    })
  })
})
