/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const queryUrl = 'someQueryUrl'

describe('getUserGroups', () => {
  let target, agent
  beforeEach(() => {
    agent = {
      post: sinon.stub(),
      set: sinon.stub(),
      send: sinon.stub()
    }
    agent.post.returns(agent)
    agent.set.returns(agent)
  })
  const setupTarget = config => {
    target = proxyquire('api/auth/getUserGroups', {
      'superagent': agent,
      '../config': config
    })
  }

  describe('deployed', () => {
    it('should call the correct functions', async () => {
      setupTarget({ ldap: {queryUrl, overrideAdEntitlements: true}, isPointingToProductionDatabase: true, isDeployed: true })
      agent.send.resolves({text: '[{"memberOf": ["CN=TESTING,"]}]'})
      // process.env.NODE_ENV = 'production'

      const result = await target('user')

      result.should.eql({groups: ['testing']})
      agent.post.should.have.been.calledWith(`${queryUrl}/query`)
      agent.set.should.have.been.calledWith('Content-Type', 'application/json')
      agent.send.should.have.been.calledWith({
        filter: `(sAMAccountName=user)`,
        attributes: ['memberOf']
      })
    })
  })

  it('should throw an error', async () => {
    setupTarget({ ldap: {queryUrl, overrideAdEntitlements: true}, isPointingToProductionDatabase: true, isDeployed: false })
    agent.send.resolves({text: '[{"memberOf": ["CN=TESTING,"]}]'})
    agent.post.throws('TestError')

    const result = await target('user')

    result.should.eql({groups: []})
  })
})
