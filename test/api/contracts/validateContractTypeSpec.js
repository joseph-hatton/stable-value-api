const target = require('../../../api/contracts/validateContractType')

describe('contract type validator', () => {
  const validCases = ['SINGLE_PLAN', 'POOLED_PLAN', 'SAVINGS_529_PLAN']

  describe('test valid contract types', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Contract Type')
   })
})
