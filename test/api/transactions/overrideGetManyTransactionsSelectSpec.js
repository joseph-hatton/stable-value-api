const proxyquire = require('../../proxyquire')

const SQL = 'SOME SQL'

const row1 = {
  feeDistributionId: '5',
  invoiceId: null,
  feeAmount: '888',
  createdDate: null,
  createdId: null,
  modifiedDate: null,
  modifiedId: null,
  version: '0',
  beginningBalance: '777',
  adjustment: '222',
  endingBalance: '879',
  balanceId: '1234',
  beginningBookValue: '8898.1234'
}

const expectedRow1 = {
  feeDistributionId: '5',
  invoiceId: null,
  feeAmount: '888',
  createdDate: null,
  createdId: null,
  modifiedDate: null,
  modifiedId: null,
  version: '0',
  beginningBalance: '777',
  adjustment: '222',
  endingBalance: '879',
  balanceId: 1234,
  beginningBookValue: 8898.1234
}

describe('override get many transactions select', () => {
  let target
  beforeEach(() => {
    target = proxyquire('api/transactions/overrideGetManyTransactionsSelect', {
      './overrideGetManyTransactionsSelect.sql': SQL
    })
  })

  it('has an expected sql', () =>
    target.sql.should.eql(SQL))

  describe('maps db results', () => {
    it('returns empty results when passed empty', () =>
      target.mapper([]).should.eql([]))

    it('returns mapped row', () =>
      target.mapper([row1]).should.eql([expectedRow1]))
  })
})
