/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')

const ROW_ID = 81
const OBJECT_BEFORE = {before: true}
const DELETE_PROP_CHANGES = ['DELETE_PROP_CHANGES']

describe('execute insert delete property change queries', () => {
  let target, dbExecuteInOrder, connection, buildDeletePropertyChangeQueries
  beforeEach(() => {
    connection = sinon.spy()
    dbExecuteInOrder = sinon.stub().resolves('ONE_EXECUTED')
    buildDeletePropertyChangeQueries = sinon.stub().returns(DELETE_PROP_CHANGES)
    target = proxyquire('api/db/executors/executeDeletePropertyChangeQueries', {
      './dbExecuteInOrder': dbExecuteInOrder,
      '../queries/buildDeletePropertyChangeQueries': buildDeletePropertyChangeQueries
    })
  })

  it('returns execute result', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then((result) => result.should.eql('ONE_EXECUTED')))

  it('execute in order should only be called once', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then(() => dbExecuteInOrder.callCount.should.eql(1)))

  it('execute in order should only be called with', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then(() => dbExecuteInOrder.should.have.been.calledWithExactly(connection, DELETE_PROP_CHANGES)))

  it('build delete propery change queries should have been called with', () =>
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    .then(() => buildDeletePropertyChangeQueries.should.have.been.calledWithExactly(schema, ROW_ID, OBJECT_BEFORE)))

  it('execute in order not called if no change queries', () => {
    buildDeletePropertyChangeQueries.returns([])
    target(connection, schema, ROW_ID, OBJECT_BEFORE)
    dbExecuteInOrder.should.have.been.not.been.called
  })

  it('execute in order returns other message if no change queries', () => {
    buildDeletePropertyChangeQueries.returns([])
    target(connection, schema, ROW_ID, OBJECT_BEFORE).should.eql('no property change queries')
  })
})
