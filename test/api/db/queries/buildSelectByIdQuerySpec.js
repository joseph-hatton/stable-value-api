/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const {schema} = require('../../../data/testSchema')
const ID = 42
const SELECT_QUERY = 'SELECT_QUERY'

describe('build select by id query', () => {
  let target, buildSelectQuery
  buildSelectQuery = sinon.stub().returns(SELECT_QUERY)

  beforeEach(() => {
    target = proxyquire('api/db/queries/buildSelectByIdQuery', {
      './buildSelectQuery': buildSelectQuery
    })
  })

  it('calls build select query with schemaaaa', () => {
    target(schema, ID)
    buildSelectQuery.should.have.been.calledWithExactly(schema, false, {whereClause: 'WHERE aTable.aTableId = :id', bindParams: {id: ID}, executeOptions: {}})
  })

  it('returns build select result', () =>
    target(schema, ID).should.eql(SELECT_QUERY))
})
