const dbExecuteInOrder = require('./dbExecuteInOrder')
const buildInsertPropertyChangeQueries = require('../queries/buildInsertPropertyChangeQueries')

module.exports = (connection, schema, id, insertBindParams) => {
  const changeQueries = buildInsertPropertyChangeQueries(schema, id, insertBindParams)
  return (changeQueries.length > 0) ? dbExecuteInOrder(connection, changeQueries) : 'no property change queries'
}
