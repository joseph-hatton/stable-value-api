require('require-sql')
const sql = require('./overrideGetRiskScorecardSelect.sql')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')

const uniqueRowProperty = 'riskScorecardId'

const buildNewTopLevelObjectFromRow = (row) => row

const appendChildToTopLevel = (topLevel, row) => {}

module.exports = {
  sql,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
