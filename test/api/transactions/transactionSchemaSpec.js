const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/transactions/transactionSchema')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')
const overrideGetOneTransactionSelect = require('../../../api/transactions/overrideGetOneTransactionSelect')
const overrideGetManyTransactionsSelect = require('../../../api/transactions/overrideGetManyTransactionsSelect')
const transactionTypesValidator = require('../../../api/transactions/validators/validateTransactionType')
const transactionDetailedTypesValidator = require('../../../api/transactions/validators/validateTransactionDetailedType')
const amountValidator = require('../../../api/transactions/validators/validateTransactionAmount')

const falseDefault = () => false
const zeroDefault = () => 0

describe('transaction schema', () => {
  validateSchema(target, {
    tableShort: 'transaction',
    table: 'sch_stbv.transaction',
    tableId: 'transaction_id',
    sequence: 'sch_stbv.transaction_seq.NEXTVAL',
    orderBy: 'transaction.effective_date desc',
    columns: [
      {name: 'transactionId', column: 'transaction_id', sequence: 'sch_stbv.transaction_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'contractId', column: 'contract_id', converter: numberConverter},
      {name: 'transactionType', column: 'transaction_type', audit: true, validators: [transactionTypesValidator]},
      {name: 'effectiveDate', column: 'effective_date', audit: true, converter: dateConverter},
      {name: 'detail', audit: true, validators: [transactionDetailedTypesValidator]},
      {name: 'amount', audit: true, converter: numberConverter, validators: [amountValidator]},
      {name: 'comments', audit: true},
      {name: 'locked', defaultValue: falseDefault, converter: booleanConverter, audit: true},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId,
      version,
      {name: 'adjustmentDate', column: 'adjustment_date', audit: true, converter: dateConverter},
      {name: 'adjustmentTransactionId', column: 'adjustment_transaction_id', audit: true, converter: numberConverter}
    ],
    required: {
      new: ['contractId', 'transactionType', 'effectiveDate', 'amount', 'locked'],
      update: ['contractId', 'transactionType', 'effectiveDate', 'amount', 'locked']
    },
    overrideSelectClause: {
      selectOne: overrideGetOneTransactionSelect,
      selectMany: overrideGetManyTransactionsSelect
    },
    audit: {
      objectClass: 'Transaction',
      buildDescription: (transaction) => `${transaction.transactionType}: ${Math.abs(transaction.amount)}${(transaction.detail) ? ', Detail: ' + transaction.detail : ''}`
    }
  })
})
