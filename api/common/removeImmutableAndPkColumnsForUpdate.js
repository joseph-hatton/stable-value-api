module.exports = (columns, jsonBody) => {
  const newObj = {}
  console.log('columns...', {columns})
  const columnsThatCanBeEdited = columns.filter((column) => !column.immutable && !column.pk)
  Object.keys(jsonBody).forEach((key) => {
    const matchingColumns = columnsThatCanBeEdited.filter((column) => column.name === key)
    if (matchingColumns.length > 0) {
      newObj[key] = jsonBody[key]
    }
  })
  return newObj
}
