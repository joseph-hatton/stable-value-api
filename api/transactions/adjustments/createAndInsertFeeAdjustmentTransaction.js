const log = require('../../../log')
const insertIntoDb = require('../../db/insertIntoDb')
const buildDefaultParams = require('../../common/buildDefaultParams')
const schema = require('../transactionSchema')

module.exports = async (dbTransaction, totalAmount, userId) => {
  log.info({domain: schema.tableShort, transactionId: dbTransaction.adjustmentTransactionId, userId, totalAmount}, 'create and insert fee adjustment transaction')
  const adjustmentTransaction = {
    contractId: dbTransaction.contractId,
    locked: false,
    transactionType: 'FEE_ADJUSTMENT',
    effectiveDate: dbTransaction.effectiveDate,
    amount: totalAmount,
    createdId: userId,
    modifiedId: userId
  }
  const transactionParams = buildDefaultParams(schema, adjustmentTransaction)
  return await insertIntoDb(schema, transactionParams)
}
