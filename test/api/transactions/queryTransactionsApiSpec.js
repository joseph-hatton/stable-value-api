/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/transactions/transactionSchema')

describe('query transaction api', () => {
  let target, req, res, resJson, selectFromDbWithPagination, buildTransactionsQueryWhereClause, queryTransactionSummary
  beforeEach(async () => {
    req = {someRequest: true}
    res = {json: sinon.stub()}
    buildTransactionsQueryWhereClause = sinon.stub().returns('where clause')
    selectFromDbWithPagination = sinon.stub().resolves({selectFromDb: true})
    queryTransactionSummary = sinon.stub().resolves('transaction summary')
    target = proxyquire('api/transactions/queryTransactionsApi', {
      '../db/selectFromDbWithPagination': selectFromDbWithPagination,
      './buildTransactionsQueryWhereClause': buildTransactionsQueryWhereClause,
      './queryTransactionSummary': queryTransactionSummary
    })

    await target(req, res)
  })

  it('select transactions from db with pagination called', () => {
    selectFromDbWithPagination.should.have.been.calledWithExactly(schema, 'where clause')
  })

  it('query transaction summary called', () => {
    queryTransactionSummary.should.have.been.calledWithExactly(req, 'where clause')
  })

  it('resuls sent as response json', () => {
    res.json.should.have.been.calledWithExactly({ selectFromDb: true, transactionSummary: 'transaction summary' })
  })
})
