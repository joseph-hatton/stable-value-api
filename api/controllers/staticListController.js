const createController = require('./createController')
const getStaticList = require('../common/getStaticList')

const referenceData = require('../referenceData.json')

module.exports = createController({
  get: getStaticList(referenceData)
})
