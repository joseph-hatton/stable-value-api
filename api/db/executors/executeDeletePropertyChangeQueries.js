const dbExecuteInOrder = require('./dbExecuteInOrder')
const buildDeletePropertyChangeQueries = require('../queries/buildDeletePropertyChangeQueries')

module.exports = (connection, schema, id, objectBefore) => {
  const changeQueries = buildDeletePropertyChangeQueries(schema, id, objectBefore)
  return (changeQueries.length > 0) ? dbExecuteInOrder(connection, changeQueries) : 'no property change queries'
}
