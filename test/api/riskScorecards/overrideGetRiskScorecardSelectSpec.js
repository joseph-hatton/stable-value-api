const proxyquire = require('../../proxyquire')

const columns = ['something', 'else']
const SQL = 'SOME SQL'
const bodyAfterRemoval = {something: 'something value', else: 'else value'}
const bodyBeforeRemoval = Object.assign({toBeIgnored: true}, bodyAfterRemoval)

const riskScorecard1 = {riskScorecardId: 1, contractId: '1'}
const riskScorecard2 = {riskScorecardId: 2, contractId: '2'}

describe('get risk scorecards with contract', () => {
  let target
  beforeEach(() => {
    target = proxyquire('api/riskScorecards/overrideGetRiskScorecardSelect', {
      './overrideGetRiskScorecardSelect.sql': SQL
    })
  })

  it('has an expected sql', () =>
    target.sql.should.eql(SQL))

  describe('maps db results', () => {
    it('returns empty results when passed empty', () =>
      target.mapper([]).should.eql([]))

    it('returns contacts with contracts moved to arrays', () =>
      target.mapper([riskScorecard1, riskScorecard2]).should.eql([riskScorecard1, riskScorecard2]))
    })
})
