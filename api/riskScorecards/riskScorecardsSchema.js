const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {dateConverter, numberConverter, percentConverter} = require('../db/converters')
const validateIdAndText = require('../common/validateIdTextField')
const {contractTypes, contractStatuses} = require('../referenceData.json')
const contractTypeValidator = validateIdAndText(contractTypes, 'Invalid Contract Type')
const overrideGetRiskScorecardSelect = require('./overrideGetRiskScorecardSelect')

const table = 'risk_scorecard'
const tableId = 'risk_scorecard_id'
const sequence = `${dbSchemaName}.risk_scorecard_seq.NEXTVAL`

const zeroDefault = () => 0

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: `${table}.contract_id`,
  columns: [
    {name: 'riskScorecardId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
    {name: 'cashFlow', column: 'cash_flow', converter: numberConverter},
    {name: 'cashFlowComments', column: 'cash_flow_comments'},
    {name: 'demographics', column: 'demographics', converter: numberConverter},
    {name: 'demographicsComments', column: 'demographics_comments'},
    {name: 'competingFunds', column: 'competing_funds', converter: numberConverter},
    {name: 'competingFundsComments', column: 'competing_funds_comments'},
    {name: 'planSponsor', column: 'plan_sponsor', converter: numberConverter},
    {name: 'planSponsorComments', column: 'plan_sponsor_comments'},
    {name: 'planSvFundBalances', column: 'plan_sv_fund_balances', converter: numberConverter},
    {name: 'planSvFundBalancesComments', column: 'plan_sv_fund_balances_comments'},
    {name: 'planDesign', column: 'plan_design', converter: numberConverter},
    {name: 'planDesignComments', column: 'plan_design_comments'},
    {name: 'fundManagement', column: 'fund_management', converter: numberConverter},
    {name: 'fundManagementComments', column: 'fund_management_comments'},
    {name: 'wrappedPortfolio', column: 'wrapped_portfolio', converter: numberConverter},
    {name: 'wrappedPortfolioComments', column: 'wrapped_portfolio_comments'},
    {name: 'marketBookRatio', column: 'market_book_ratio', converter: numberConverter},
    {name: 'marketBookRatioComments', column: 'market_book_ratio_comments'},
    {name: 'overallUnadjusted', column: 'overall_unadjusted', converter: numberConverter},
    {name: 'overallUnadjustedComments', column: 'overall_unadjusted_comments'},
    {name: 'overallAdjusted', column: 'overall_adjusted', converter: numberConverter},
    {name: 'overallAdjustedComments', column: 'overall_adjusted_comments'},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId,
    {name: 'pooledFundBalances', column: 'pooled_fund_balances', converter: numberConverter},
    {name: 'pooledFundBalancesComments', column: 'pooled_fund_balances_comments'},
    {name: 'pooledFundDesign', column: 'pooled_fund_design', converter: numberConverter},
    {name: 'pooledFundDesignComments', column: 'pooled_fund_design_comments'}
  ],
  overrideSelectClause: {
    selectOne: overrideGetRiskScorecardSelect,
    selectMany: overrideGetRiskScorecardSelect
  },
  required: {
    new: ['contractId', 'effectiveDate', 'cashFlow', 'demographics', 'competingFunds', 'planSponsor', 'planSvFundBalances', 'planDesign', 'fundManagement', 'wrappedPortfolio', 'marketBookRatio', 'overallUnadjusted', 'pooledFundBalances', 'pooledFundDesign'],
    update: ['contractId', 'effectiveDate', 'cashFlow', 'demographics', 'competingFunds', 'planSponsor', 'planSvFundBalances', 'planDesign', 'fundManagement', 'wrappedPortfolio', 'marketBookRatio', 'overallUnadjusted', 'pooledFundBalances', 'pooledFundDesign']
  }
}
