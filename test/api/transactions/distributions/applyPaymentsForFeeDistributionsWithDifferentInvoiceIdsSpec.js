/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/feeDistributions/feeDistributionsSchema')

const userId = 'somebody'

const dbFeeDistributionWithInvoiceId = {base: true}
const invoice = { invoiceId: 11 }
const dbTransaction = { transactionId: 55 }

describe('apply payments for fee distributions with different invoice ids', () => {
  let target, selectFromDb, getInvoiceById, applyPayment, deleteFeeDistribution
  beforeEach(() => {
    selectFromDb = sinon.stub()
    getInvoiceById = sinon.stub()
    applyPayment = sinon.stub()
    deleteFeeDistribution = sinon.stub()
    target = proxyquire('api/transactions/distributions/applyPaymentsForFeeDistributionsWithDifferentInvoiceIds', {
      '../../db/selectFromDb': selectFromDb,
      '../../invoices/getInvoiceById': getInvoiceById,
      '../../invoices/applyPayment': applyPayment,
      './deleteFeeDistribution': deleteFeeDistribution
    })
  })

  it('finds no payments to make with no db fee distributions', async () => {
    selectFromDb.resolves([])

    await target(dbTransaction, [], userId)
    getInvoiceById.should.have.not.been.called
    deleteFeeDistribution.should.have.not.been.called
  })

  it('applies payment if different invoice id', async () => {
    selectFromDb.resolves([{invoiceId: 11, amount: 82, adjustment: 13}])
    getInvoiceById.withArgs(11).resolves(invoice)

    const clientFeeDistribution = {feeDistributionId: 878, invoiceId: 66}
    await target(dbTransaction, [clientFeeDistribution], userId)

    applyPayment.should.have.been.calledWithExactly(invoice, -82, -13, userId)
  })

  it('deletes fee distribution if different invoice id', async () => {
    const dbFeeDistribution = {invoiceId: 11, amount: 82, adjustment: 13}
    selectFromDb.resolves([dbFeeDistribution])
    getInvoiceById.withArgs(11).resolves(invoice)

    const clientFeeDistribution = {feeDistributionId: 878, invoiceId: 66}
    await target(dbTransaction, [clientFeeDistribution], userId)

    deleteFeeDistribution.should.have.been.calledWithExactly(55, dbFeeDistribution, userId)
  })
})
