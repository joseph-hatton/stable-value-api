const log = require('../../../log')
const buildDefaultParams = require('../../common/buildDefaultParams')
const insertIntoDb = require('../../db/insertIntoDb')
const applyPayment = require('../../invoices/applyPayment')
const schema = require('../../feeDistributions/feeDistributionsSchema')

module.exports = async (clientAmount, clientFeeDistribution, dbFeeDistributionWithInvoiceId, invoice, dbTransaction, userId) => {
  let feeDistribution = dbFeeDistributionWithInvoiceId
  const amount = clientFeeDistribution.amount || 0
  const adjustment = clientFeeDistribution.adjustment || 0
  let originalAmount = 0
  let originalAdjustment = 0
  if (!feeDistribution) {
    feeDistribution = {
      transactionId: dbTransaction.transactionId
    }
    if (invoice) {
      feeDistribution.invoiceId = invoice.invoiceId
    }
  } else {
    originalAmount = feeDistribution.amount || 0
    originalAdjustment = feeDistribution.adjustment || 0
  }
  feeDistribution.beginningBalance = clientFeeDistribution.beginningBalance
  feeDistribution.amount = amount
  feeDistribution.adjustment = adjustment
  feeDistribution.endingBalance = clientFeeDistribution.endingBalance
  const feeDistributionParams = buildDefaultParams(schema, feeDistribution, {
    createdId: userId,
    modifiedId: userId
  })
  log.info({domain: schema.tableShort, transactionId: dbTransaction.transactionId, feeDistribution, clientAmount, userId}, 'creating fee distribution')
  await insertIntoDb(schema, feeDistributionParams)
  if (invoice) {
    log.info({domain: schema.tableShort, transactionId: dbTransaction.transactionId, feeDistribution, clientAmount, userId, originalAmount, originalAdjustment}, 'creating fee distribution')
    await applyPayment(invoice, feeDistribution.amount - originalAmount, feeDistribution.adjustment - originalAdjustment, userId)
  }
}
