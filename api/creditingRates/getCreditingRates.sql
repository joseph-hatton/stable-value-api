SELECT  crediting_rate.market_value / NULLIF(crediting_rate.book_value, 0) as mvbv_ratio,
        crediting_rate.*,
        NVL(crediting_rate.book_value, balance.ending_book_value) as ending_book_value,
        contract.contract_number,
        contract.short_plan_name,
        contract.status as contract_status,
        contract.crediting_rate_round_decimals as current_contract_round_decimal
FROM sch_stbv.crediting_rate
INNER JOIN sch_stbv.contract
  ON  crediting_rate.contract_id = contract.contract_id
LEFT JOIN sch_stbv.balance balance
  ON crediting_rate.contract_id = balance.contract_id and crediting_rate.reference_date = balance.effective_date
