const log = require('../../../log')
const getInvoiceById = require('../../invoices/getInvoiceById')
const schema = require('../../feeDistributions/feeDistributionsSchema')
const createFeeDistributionAndApplyPayment = require('./createFeeDistributionAndApplyPayment')
const deleteFeeDistribution = require('./deleteFeeDistribution')

module.exports = async (dbTransaction, clientFeeDistribution, clientAmount, userId) => {
  const amount = clientAmount
  const adjustment = clientFeeDistribution.adjustment
  const invoice = (clientFeeDistribution.invoiceId) ? await getInvoiceById(clientFeeDistribution.invoiceId) : null
  const dbFeeDistributionWithInvoiceId = (invoice) ? dbTransaction.feeDistributions.find((fd) => fd.invoiceId === invoice.invoiceId) : null

  if (amount || adjustment) {
    await createFeeDistributionAndApplyPayment(clientAmount, clientFeeDistribution, dbFeeDistributionWithInvoiceId, invoice, dbTransaction, userId)
  } else {
    await deleteFeeDistribution(dbTransaction.transactionId, dbFeeDistributionWithInvoiceId, userId)
  }
}
