const createError = require('http-errors')
const log = require('../../../log')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const getUserId = require('../../getUserId')
const schema = require('../contractSchema')
const transitionContractInWorkflow = require('./transitionContractInWorkflow')

module.exports = ({actionVerb, transitionContractInWorkflow}) => async ({ params, headers }, res) => {
  const contractId = params.id
  log.info({domain: schema.tableShort, id: contractId}, `${actionVerb} contract api`)
  const userId = getUserId(headers)
  log.info({domain: schema.tableShort, id: contractId, userId}, 'user found')
  const contract = await selectByIdFromDb(schema, contractId)
  let contactContracts
  if (!contract) {
    log.warn({domain: schema.tableShort, id: contractId, userId}, 'contract not found')
    throw createError.NotFound('Contract not found.')
  }
  log.info({domain: schema.tableShort, id: contractId, userId}, 'contract found')

  const errorMessages = await transitionContractInWorkflow({contract, userId})

  if (!errorMessages || (Array.isArray(errorMessages) && errorMessages.length === 0)) {
    const transitionedContract = await selectByIdFromDb(schema, contractId)
    return res.status(200).json(transitionedContract)
  } else {
    log.warn({domain: schema.tableShort, id: contractId, userId, errorMessages}, `${actionVerb} contract api rejection`)
    throw createError.BadRequest(errorMessages)
  }
}
