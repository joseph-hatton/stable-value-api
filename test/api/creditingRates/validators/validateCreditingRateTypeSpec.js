const target = require('../../../../api/creditingRates/validators/validateCreditingRateType')

describe('crediting rate type validator', () => {
  const validCases = [
    'GROSS',
    'NET'
  ]

  describe('test valid crediting rate types', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Crediting Rate Type')
   })
})
