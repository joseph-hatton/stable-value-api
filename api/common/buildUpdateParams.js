const removeImmutableAndPkColumnsForUpdate = require('./removeImmutableAndPkColumnsForUpdate')

module.exports = (columns, userId, body) => {
  console.log('before...', {body})
  const strippedImmutableAndPk = removeImmutableAndPkColumnsForUpdate(columns, body)
  const hasModifiedId = columns.filter((column) => column.name === 'modifiedId').length > 0
  console.log('after...', {strippedImmutableAndPk, hasModifiedId})
  return (hasModifiedId)
    ? Object.assign(strippedImmutableAndPk,
      {
        modifiedId: userId,
        modifiedDate: new Date()
      })
    : strippedImmutableAndPk
}
