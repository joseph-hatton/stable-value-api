/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/feeDistributions/feeDistributionsSchema')

const userId = 'somebody'
const dbTransaction = {
  adjustmentTransactionId: 5,
  contractId: 88,
  effectiveDate: 'yesterday'
}
const feeDistributions = [{adjustment: 11}, {adjustment: 6}]
const clientAmount = 11

describe('update distributions', () => {
  let target, updateDistribution, applyPaymentsForFeeDistributionsWithDifferentInvoiceIds
  beforeEach(() => {
    updateDistribution = sinon.stub()
    applyPaymentsForFeeDistributionsWithDifferentInvoiceIds = sinon.stub()
    target = proxyquire('api/transactions/distributions/updateDistributions', {
      './updateDistribution': updateDistribution,
      './applyPaymentsForFeeDistributionsWithDifferentInvoiceIds': applyPaymentsForFeeDistributionsWithDifferentInvoiceIds
    })
  })

  it('update distribution called twice', async () => {
    await target(dbTransaction, feeDistributions, clientAmount, userId)
    updateDistribution.should.have.been.called.twice
  })

  it('update distribution called with first fee distribution', async () => {
    await target(dbTransaction, feeDistributions, clientAmount, userId)
    updateDistribution.should.have.been.calledWithExactly(dbTransaction, feeDistributions[0], clientAmount, userId)
  })

  it('update distribution called with second fee distribution', async () => {
    await target(dbTransaction, feeDistributions, clientAmount, userId)
    updateDistribution.should.have.been.calledWithExactly(dbTransaction, feeDistributions[1], clientAmount, userId)
  })
  it('applies payment for fee distributions with different invoice ids', async () => {
    await target(dbTransaction, feeDistributions, clientAmount, userId)
    applyPaymentsForFeeDistributionsWithDifferentInvoiceIds.should.have.been.calledWithExactly(dbTransaction, feeDistributions, userId)
  })
})
