/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

const userId = 'a user'
const contractId = 5

describe('submit contract', () => {
  let target, transitionContractInWorkflow, validateCurrentlyAssigned
  beforeEach(() => {
    validateCurrentlyAssigned = sinon.stub()
    transitionContractInWorkflow = sinon.stub().returns('base returned')
    target = proxyquire('api/contracts/workflow/submitContract', {
      '../contacts/validateAssignedContacts': { validateCurrentlyAssigned },
      './transitionContractInWorkflow': transitionContractInWorkflow
    })
  })

  it('returns the base result', () => {
    target.should.eql('base returned')
  })

  it('base should have been called once', () => {
    transitionContractInWorkflow.should.have.been.called
  })

  it('sets action verb', () => {
    transitionContractInWorkflow.getCall(0).args[0].actionVerb.should.eql('submit')
  })

  it('sets next contract status', () => {
    transitionContractInWorkflow.getCall(0).args[0].nextContractStatus.should.eql('SUBMITTED')
  })

  it('does not set a callBeforeChangingState', () => {
    should.not.exist(transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState)
  })

  describe('validate can transition', () => {
    let asyncValidate
    beforeEach(() => {
      asyncValidate = transitionContractInWorkflow.getCall(0).args[0].validateCanTransition
    })

    it('fails with PENDING contract', async () => {
      const result = await asyncValidate({contract: {status: 'ACTIVE'}})

      result.should.eql('Only PENDING or PENDING_JASPER Contracts may be submitted.')
    })

    it('passes with submitted contract', async () => {
      validateCurrentlyAssigned.withArgs(contractId).resolves([])

      const result = await asyncValidate({contract: {contractId, status: 'PENDING'}})

      should.not.exist(result)
    })

    it('fails with an assigned contact error on a pending contract', async () => {
      validateCurrentlyAssigned.withArgs(contractId).resolves(['an error'])

      const result = await asyncValidate({contract: {contractId, status: 'PENDING'}})

      result.should.eql(['an error'])
    })

    it('ignores assigned contact errors on a pending jasper contract', async () => {
      validateCurrentlyAssigned.withArgs(contractId).resolves(['an error'])

      const result = await asyncValidate({contract: {contractId, status: 'PENDING_JASPER'}})

      should.not.exist(result)
    })
  })
})
