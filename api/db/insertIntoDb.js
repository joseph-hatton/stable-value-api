const log = require('../../log')
const changeOperations = require('../changes/changeOperations')
const dbExecutePromise = require('./executors/dbExecutePromise')
const executeInsertQuery = require('./executors/executeInsertQuery')
const executeSelectByIdQuery = require('./executors/executeSelectByIdQuery')
const executeInsertChange = require('./executors/executeInsertChangeQuery')
const executePropertyChanges = require('./executors/executeInsertPropertyChangeQueries')

const attemptInsert = async (connection, schema, insertBindParams, userId, skipAudit, skipSelect) => {
  log.info({table: schema.table, userId, skipAudit}, 'insert into db')
  const auditingRequired = !skipAudit && schema.columns.filter((column) => column.audit === true).length > 0
  const insertResult = await executeInsertQuery(connection, schema, insertBindParams)
  if (auditingRequired) {
    const insertChangeResult = await executeInsertChange(connection, schema, insertResult.id, userId, insertBindParams, changeOperations.create)
    await executePropertyChanges(connection, schema, insertChangeResult.id, insertBindParams)
  }
  return (skipSelect) ? insertResult : await executeSelectByIdQuery(connection, schema, insertResult.id)
}

module.exports = (schema, insertBindParams, userId, skipAudit = false, skipSelect = false) => {
  const dbPromiseChainCreator = (connection) => attemptInsert(connection, schema, insertBindParams, userId, skipAudit, skipSelect)
  return dbExecutePromise(dbPromiseChainCreator)
}
