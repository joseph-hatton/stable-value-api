const createError = require('http-errors')
const log = require('../../log')
const getUserId = require('../getUserId')
const deleteFromDb = require('../db/deleteFromDb')
const deleteByIdFromDb = require('../db/deleteByIdFromDb')
const watchListSchema = require('./watchListSchema')
const actionStepWatchListSchema = require('./actionStepWatchListSchema')
const reasonWatchListSchema = require('./reasonWatchListSchema')

module.exports = async ({headers, params: {id}}, res, next) => {
  log.info({ domain: watchListSchema.tableShort, watchListId: id }, 'delete watchlist api')
  const userId = getUserId(headers)
  const watchListId = id

  const deleteActionStepWatchListSql = `delete from ${actionStepWatchListSchema.table} where watch_list_id = :watchListId`
  await deleteFromDb(deleteActionStepWatchListSql, {watchListId})

  const deleteReasonWatchListSql = `delete from ${reasonWatchListSchema.table} where watch_list_id = :watchListId`
  await deleteFromDb(deleteReasonWatchListSql, {watchListId})

  const deleteResponse = await deleteByIdFromDb(watchListSchema, watchListId, userId)
  return res.status(200).json(deleteResponse)
}
