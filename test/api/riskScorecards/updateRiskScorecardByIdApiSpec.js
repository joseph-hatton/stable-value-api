/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/riskScorecards/riskScorecardsSchema')
const defaultParams = {a: true}
const user = 'THE USER'
const body = {b: 'from request body'}
const THE_ID = 8675309
const VALIDATE_THEN_CALL_API_RESULT = 'VALIDATE_THEN_CALL_API_RESULT'

describe('update riskscorecard by id api', () => {
  const companies = [{ id: 'aId', name: 'a' }, { id: 'bId', name: 'b' }]
  const request = { headers: 'abc', params: {id: THE_ID} }

  let response, target, next, getUserId, responseStatusSend, responseJson, updateDb, validateThenCallApi, flattenRiskScorecardForInsertOrUpdateParam, getAdjustedRiskScorecardById

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    responseJson = sinon.spy()
    response = {
      json: responseJson,
      status: sinon.stub().returns({ send: responseStatusSend, json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    // const query = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)
    updateDb = sinon.stub().resolves({rowsAffected: 1})
    const buildDefaultParams = sinon.stub().withArgs(schema.columns).returns(defaultParams)
    validateThenCallApi = sinon.stub().returns(VALIDATE_THEN_CALL_API_RESULT)
    flattenRiskScorecardForInsertOrUpdateParam = sinon.stub()
    getAdjustedRiskScorecardById = sinon.stub().resolves('adjusted scorecard')
    target = proxyquire('api/riskScorecards/updateRiskScorecardByIdApi', {
      '../getUserId': getUserId,
      '../db/updateDb': updateDb,
      '../common/buildUpdateParams': sinon.stub().returns(defaultParams),
      '../common/validateThenCallApi': validateThenCallApi,
      './flattenRiskScorecardForInsertOrUpdateParam': flattenRiskScorecardForInsertOrUpdateParam,
      './getAdjustedRiskScorecardById': getAdjustedRiskScorecardById
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
      request.body = body
    })

    it('returns validateThenCallApiResult', async () => {
      const result = await target(request, response, next)
      result.should.eql(VALIDATE_THEN_CALL_API_RESULT)
    })

    it('validateThenCallApiResult should be called with schema', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[0].should.eql(schema)
    })

    it('validateThenCallApiResult should be called with body', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[1].should.eql(body)
    })

    it('validateThenCallApiResult should be called with required fields', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[2].should.eql(schema.required.new)
    })

    it('validateThenCallApiResult should be called with res', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[3].should.eql(response)
    })

    it('validateThenCallApiResult should be called with next', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[4].should.eql(next)
    })

    it('validateThenCallApiResult should be called with userId in options', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[6].should.eql({userId: user, params: {id: THE_ID}})
    })

    it('sends expected data into update', async () => {
      await target(request, response, next)
      await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user, params: {id: THE_ID}})
      updateDb.should.have.been.calledWithExactly(schema, defaultParams, THE_ID, user)
    })

    it('calls for adjusted riskscorecard after update', async () => {
      await target(request, response, next)
      await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user, params: {id: THE_ID}})
      getAdjustedRiskScorecardById.should.have.been.calledWithExactly(THE_ID)
    })

    it('sends adjusted riskscorecard result to response', async () => {
      await target(request, response, next)
      const result = await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user, params: {id: THE_ID}})
      response.json.should.have.been.calledWithExactly('adjusted scorecard')
    })
  })
})
