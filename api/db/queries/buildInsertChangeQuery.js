const changeSchema = require('../../changes/changeSchema')
const buildInsertQuery = require('./buildInsertQuery')
const log = require('../../../log')

const buildChangeBindParams = (schema, rowId, userId, objectBeingChanged, operation) => {
  log.info({domain: schema.tableShort, id: rowId, operation}, 'building change bind params')
  return {
    objectId: rowId,
    operation: operation,
    objectClass: schema.audit.objectClass,
    createdDate: new Date(),
    createdId: userId,
    effectiveDate: null,
    contractId: null,
    description: schema.audit.buildDescription(objectBeingChanged)
  }
}

module.exports = (schema, rowId, userId, objectBeingChanged, operation) =>
  buildInsertQuery(changeSchema, buildChangeBindParams(schema, rowId, userId, objectBeingChanged, operation))
