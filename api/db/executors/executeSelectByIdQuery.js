const dbExecuteOne = require('./dbExecuteOne')
const buildSelectByIdQuery = require('../queries/buildSelectByIdQuery')

module.exports = (connection, schema, rowId) =>
  dbExecuteOne(connection, buildSelectByIdQuery(schema, rowId))
  .then(results => (results && results.error) ? results.error : results[0])
