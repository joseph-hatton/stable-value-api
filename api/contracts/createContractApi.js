const createError = require('http-errors')
const log = require('../../log')
const getUserId = require('../getUserId')
const insertIntoDb = require('../db/insertIntoDb')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const validateThenCallApi = require('../common/validateThenCallApi')
const insertNewContractEditHistory = require('../contractEditHistories/insertNewContractEditHistory')
const schema = require('./contractSchema')
const buildContractNumber = require('./buildContractNumber')
const buildContractInsertParams = require('./buildContractInsertParams')
const SKIP_AUDIT = true

const attemptInsertApi = async (schema, body, res, next, {userId}) => {
  if (body && body.editHistories && body.editHistories.length > 1) {
    throw createError.BadRequest('New contracts or contracts in pending state can only have one edit history.')
  }
  const contractNumber = await buildContractNumber()
  const contractInsertParams = buildContractInsertParams(userId, body, contractNumber)
  const contractInsertResponse = await insertIntoDb(schema, contractInsertParams, userId, SKIP_AUDIT)

  const singleEditHistory = (body && body.editHistories && body.editHistories.length === 1) ? body.editHistories[0] : {}
  await insertNewContractEditHistory(contractInsertParams, singleEditHistory, contractInsertResponse.contractId, userId)

  const selectResponse = await selectByIdFromDb(schema, contractInsertResponse.contractId)
  return res.status(201).json(selectResponse)
}

module.exports = async ({ headers, body }, res, next) => {
  log.info({domain: schema.tableShort}, 'create contract api')
  const userId = getUserId(headers)
  return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptInsertApi, {userId})
}
