const log = require('../../log')
const selectFromDbWithPagination = require('../db/selectFromDbWithPagination')
const buildTransactionsQueryWhereClause = require('./buildTransactionsQueryWhereClause')
const schema = require('./transactionSchema')
const queryTransactionSummary = require('./queryTransactionSummary')

const queryTransactionsWithPaging = async (schema, whereClauseBindParamsAndExecuteOptions) => {
  return await selectFromDbWithPagination(schema, whereClauseBindParamsAndExecuteOptions)
}

module.exports = async (req, res) => {
  log.info({domain: schema.tableShort}, 'get many transactions api')

  const whereClause = buildTransactionsQueryWhereClause(schema, req)

  log.info({domain: schema.tableShort, whereClause}, 'get many transactions whereClause')
  const transactionResults = await queryTransactionsWithPaging(schema, whereClause)

  const transactionSummary = await queryTransactionSummary(req, whereClause)
  transactionResults.transactionSummary = transactionSummary
  log.info({domain: schema.tableShort}, 'get many transactions api results')
  return res.json(transactionResults)
}
