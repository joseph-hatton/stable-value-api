const target = require('../../../api/contracts/validateContractPlanTypeRates')

const expectedError = 'Plan type rates must total 100%.'

describe('contract plan type rates validator', () => {
  let status

  describe('active contract', () => {
    beforeEach(() => {
      status = 'ACTIVE'
    })

    it('missing rates doesnt total 100', () => target({status, plan401ARate: 10}).should.eql(expectedError))

    it('missing rates does total 100', () => should.not.exist(target({status, plan401ARate: 100})))

    it('no missing rates doesnt total 100', () => target({status, plan401ARate: 18, plan401KRate: 19, plan403BRate: 18, plan457Rate: 6, plan529Rate: 20, plan501CRate: 12, planTaftRate: 5}).should.eql(expectedError))

    it('no missing rates does total 100', () => should.not.exist(target({
      status,
      plan401ARate: 18,
      plan401KRate: 19,
      plan403BRate: 18,
      plan457Rate: 6,
      plan529Rate: 20,
      plan501CRate: 12,
      planTaftRate: 7})))
  })
})
