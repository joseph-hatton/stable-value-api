/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const {oldAppBaseUrl} = require('../../../api/config')

describe('get cycle messages api', () => {
  let target, res, jsonStub, httpGet

  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    httpGet = sinon.stub().resolves({body: {
      totalRecords: 2,
      CycleMessage: [
        {something: 'yep', link: 'ugly links'},
        {something: 'else', link: 'ugly links'}
      ]
    }})
    target = proxyquire('api/cycles/getCycleMessagesApi', {
      'superagent': {get: httpGet}
    })
  })

  describe('daily cycle', () => {
    it('sends cycle data to json response', async () => {
      await target()({query: {}}, res)

      jsonStub.should.have.been.calledWithExactly({total: 2, pageNumber: 0, pageSize: 200, results: [{ something: 'yep' }, { something: 'else' }]})
    })

    it('calls get with correct default url', async () => {
      await target()({query: {}}, res)

      httpGet.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/dailyCycle?maxRows=200&offset=0&order=asc&sort=messageDate`)
    })
  })

  describe('monthly cycle', () => {
    it('sends cycle data to json response', async () => {
      await target(false)({query: {}}, res)

      jsonStub.should.have.been.calledWithExactly({total: 2, pageNumber: 0, pageSize: 200, results: [{ something: 'yep' }, { something: 'else' }]})
    })

    it('calls get with correct default url', async () => {
      await target(false)({query: {}}, res)

      httpGet.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/monthlyCycle?maxRows=200&offset=0&order=asc&sort=messageDate`)
    })
  })

  describe('with page number and size', () => {
    it('sends cycle data to json response', async () => {
      await target(false)({query: {pageNumber: 8, pageSize: 3}}, res)

      jsonStub.should.have.been.calledWithExactly({total: 2, pageNumber: 8, pageSize: 3, results: [{ something: 'yep' }, { something: 'else' }]})
    })

    it('calls get with correct default url', async () => {
      await target(false)({query: {pageNumber: 8, pageSize: 3}}, res)

      httpGet.should.have.been.calledWithExactly(`${oldAppBaseUrl}/api/monthlyCycle?maxRows=3&offset=8&order=asc&sort=messageDate`)
    })
  })
})
