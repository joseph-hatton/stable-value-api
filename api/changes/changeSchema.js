const {dbSchemaName} = require('../config')
const {createdDate, createdId} = require('../common/auditColumns')
const {numberConverter} = require('../db/converters')
const table = 'change'
const tableId = 'change_id'
const sequence = `${dbSchemaName}.change_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'change.created_date desc',
  columns: [
    {name: 'changeId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'objectId', column: 'object_id', immutable: true, converter: numberConverter},
    {name: 'operation', immutable: true},
    {name: 'objectClass', column: 'object_class', immutable: true},
    createdDate,
    createdId,
    {name: 'effectiveDate', column: 'effective_date', immutable: true},
    {name: 'contractId', column: 'contract_id', immutable: true, converter: numberConverter},
    {name: 'description'}
  ]
}
