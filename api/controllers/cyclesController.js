const createController = require('./createController')

const getCycleMessages = require('../cycles/getCycleMessagesApi')
const runCycleApi = require('../cycles/runCycleApi')

const IS_DAILY_CYCLE = true
const IS_MONTHLY_CYCLE = false

module.exports = createController({
  getStatus: require('../cycles/getCycleStatusApi'),
  getDailyCycleMessages: getCycleMessages(IS_DAILY_CYCLE),
  getMonthlyCycleMessages: getCycleMessages(IS_MONTHLY_CYCLE),
  runDailyCycle: runCycleApi('daily'),
  runPreliminaryMonthlyCycle: runCycleApi('monthlyPreliminary'),
  finalizeMonth: runCycleApi('monthlyFinalize')
})
