const validateIdAndText = require('../../common/validateIdTextField')
const {creditingRateTypes} = require('../../referenceData.json')
module.exports = validateIdAndText(creditingRateTypes, 'Invalid Crediting Rate Type')
