const config = require('../../../api/config')
const proxyquire = require('../../proxyquire')

describe('db', () => {
  let db, oracledb, getConnection

  beforeEach(() => {
    getConnection = sinon.stub().withArgs(config.db).resolves('connected')
    oracledb = {
      OBJECT: 'expected out format',
      getConnection
    }
    db = proxyquire('api/db/index', {
      'oracledb': oracledb
    })
  })

  it('sets default outFormat',
    () => oracledb.outFormat.should.equal(oracledb.OBJECT))

  it('connects with a promise', async () => {
    const connection = await db()
    connection.should.equal('connected')
  })

  it('returns error if connection fails', async () => {
    getConnection.throws('some error')
    try {
      await db()
    } catch (err) {
      err.message.should.equal('Cannot connect to Oracle database.')
    }
  })
})
