const proxyquire = require('../../proxyquire')
const schema = require('../../../api/invoices/invoicesSchema')

const amount = 888.23
const adjustment = 113.87
const userId = 'a user'

describe('apply payment', () => {
  let target, invoice, buildUpdateParams, updateDb
  beforeEach(() => {
    invoice = {
      invoiceId: 555,
      feesReceived: 123.45,
      adjustments: 678.91,
      outstandingBalance: 1000.01
    }
    buildUpdateParams = sinon.stub().returns('update params')
    updateDb = sinon.stub().resolves('update response')
    target = proxyquire('api/invoices/applyPayment', {
      '../common/buildUpdateParams': buildUpdateParams,
      '../db/updateDb': updateDb
    })
  })

  it('calls build update params with numbers', async () => {
    const expectedParamsToUpdate = {feesReceived: 1011.68, adjustments: 792.78, outstandingBalance: 225.65}

    await target(invoice, 888.23, 113.87, userId)

    buildUpdateParams.should.have.been.calledWithExactly(schema.columns, userId, expectedParamsToUpdate)
  })

  it('calls build update params with different numbers', async () => {
    const expectedParamsToUpdate = {feesReceived: 823698.56, adjustments: 9899668.25, outstandingBalance: 9076414.24}

    await target(invoice, 823575.11, 9898989.34, userId)

    buildUpdateParams.should.have.been.calledWithExactly(schema.columns, userId, expectedParamsToUpdate)
  })

  it('update db called with', async () => {
    await target(invoice, 823575.11, 9898989.34, userId)

    updateDb.should.have.been.calledWithExactly(schema, 'update params', 555, userId)
  })

  it('returns update db result', async () => {
    const result = await target(invoice, 823575.11, 9898989.34, userId)

    result.should.eql('update response')
  })
})
