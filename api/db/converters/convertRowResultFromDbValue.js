const camelCase = require('lodash/camelCase')

const fromDbValue = (columns, columnName, value) => {
  const response = {
    key: camelCase(columnName),
    value: value
  }
  const matchingColumn = columns.find((column) => (column.column && column.column === columnName) || (response.key === column.name))
  if (matchingColumn && matchingColumn.converter && matchingColumn.converter.fromDb) {
    response.value = matchingColumn.converter.fromDb(value)
  }
  return response
}

module.exports = (columns, rowResult) => {
  const newResult = {}
  Object.keys(rowResult)
  .forEach((key) => {
    const converted = fromDbValue(columns, key, rowResult[key])
    newResult[converted.key] = converted.value
  })
  return newResult
}
