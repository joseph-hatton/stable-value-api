/* eslint-disable no-unused-expressions,no-return-assign */
const values = require('lodash/values')
const proxyquire = require('../../proxyquire')
const cloneDeep = require('lodash/cloneDeep')

describe('controllers', () => {
  const shouldConfigureController = (path, endpoints) => {
    const createController = sinon.stub().returns('controller!')
    const mocks = {
      './createController': createController
    }
    values(endpoints).forEach(endpointPath => mocks[endpointPath] = sinon.stub())
    proxyquire(path, mocks).should.equal('controller!')

    Object.keys(createController.lastCall.args[0]).should.eql(Object.keys(endpoints))
  }

  const baseEndpoints = {
    query: '../common/getManyApi',
    create: '../common/createApi',
    get: '../common/getByIdApi',
    update: '../common/updateByIdApi',
    delete: '../common/deleteByIdApi'
  }

  const commonBaseEndpoints = {
    getMany: '../common/getManyApi',
    getOne: '../common/getByIdApi',
    create: '../common/createApi',
    update: '../common/updateByIdApi',
    delete: '../common/deleteByIdApi'
  }

  describe('action steps', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/actionStepsController', endpoints)
    })
  })

  describe('balances', () => {
    const endpoints = {
      getMany: commonBaseEndpoints.getMany,
      getOne: commonBaseEndpoints.getOne,
      getDaily: '../balances/daily/getDailyFees',
      getMonthly: '../balances/monthly/getMonthlyFees'
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/balancesController', endpoints)
    })
  })

  describe('changes', () => {
    const endpoints = {
      getMany: commonBaseEndpoints.getMany,
      getOne: commonBaseEndpoints.getOne,
      getManyPropertyChanges: commonBaseEndpoints.getMany,
      getOnePropertyChange: commonBaseEndpoints.getOne
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/changesController', endpoints)
    })
  })

  describe('companies', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/companiesController', endpoints)
    })
  })

  describe('contacts', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/contactsController', endpoints)
    })
  })

 describe('contact contracts', () => {
    const endpoints = Object.assign(cloneDeep(commonBaseEndpoints), {
      delete: '../contracts/contacts/validateunassingBeforeDelete',
      getValidationContactContract: '../contracts/contacts/updateContactContractApi'
    })
    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/contactContractsController', endpoints)
    })
  })

  describe('contracts', () => {
    const endpoints = Object.assign(cloneDeep(commonBaseEndpoints), {
      getMany: '../contracts/queryContractsApi',
      create: '../contracts/createContractApi',
      update: '../contracts/updateContractApi',
      delete: '../contracts/deleteContractApi',
      overrideModifiedId: '../contracts/workflow/overrideContractModifiedIdApi',
      submit: '../contracts/workflow/transitionContractInWorkflowApi',
      approve: '../contracts/workflow/transitionContractInWorkflowApi',
      reject: '../contracts/workflow/transitionContractInWorkflowApi',
      inactivate: '../contracts/workflow/transitionContractInWorkflowApi',
      reactivate: '../contracts/workflow/transitionContractInWorkflowApi'
    })

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/contractsController', endpoints)
    })
  })

  describe('creditingRates', () => {
    const endpoints = {
      getManyCreditingRates: baseEndpoints.query,
      getCreditingRatesForContracts: '../creditingRates/queryCreditingRatesApi',
      getOneCreditingRate: baseEndpoints.get,
      createMarketValue: '../creditingRates/marketValue/createMarketValueApi',
      updateMarketValue: '../creditingRates/marketValue/updateMarketValueApi',
      deleteMarketValue: '../creditingRates/marketValue/deleteMarketValueApi',
      calculateCreditingRate: '../creditingRates/creditingRate/calculateCreditingRateApi',
      updateCreditingRate: '../creditingRates/creditingRate/updateCreditingRateApi',
      clearCreditingRate: '../creditingRates/creditingRate/clearCreditingRateApi',
      overrideModifiedId: '../creditingRates/creditingRate/overrideCreditingRateModifiedIdApi',
      approveCreditingRate: '../creditingRates/creditingRate/approveCreditingRateApi'
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/creditingRatesController', endpoints)
    })
  })

  describe('cycles', () => {
    const endpoints = {
      getStatus: '../cycle/getCycleStatusApi',
      getDailyCycleMessages: '../cycles/getCycleMessagesApi',
      getMonthlyCycleMessages: '../cycles/getCycleMessagesApi',
      runDailyCycle: '../cycles/runCycleApi',
      runPreliminaryMonthlyCycle: '../cycles/runCycleApi',
      finalizeMonth: '../cycles/runCycleApi'
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/cyclesController', endpoints)
    })
  })

  describe('derivative types', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/derivativeTypesController', endpoints)
    })
  })

  describe('health', () => {
    const endpoints = {
      ping: '../health/ping',
      status: '../health/status'
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/healthController', endpoints)
    })
  })

  describe('monthly cycles', () => {
    const endpoints = {
      getMany: commonBaseEndpoints.getMany
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/monthlyCyclesController', endpoints)
    })
  })

  describe('portfolios', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/portfoliosController', endpoints)
    })
  })
  describe('portfolioAllocations', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/portfolioAllocationsController', endpoints)
    })
  })

  describe('profiles', () => {
    const endpoints = {
      getPic: '../profiles/getProfilePicApi'
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/profilesController', endpoints)
    })
  })

  describe('reasons', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/reasonsController', endpoints)
    })
  })

  describe('risk scorecards', () => {
    const endpoints = Object.assign(cloneDeep(commonBaseEndpoints), {
      getMany: '../riskScorecards/queryRiskScorecardsApi',
      getOne: '../riskScorecards/getRiskScorecardByIdApi',
      create: '../riskScorecards/createScorecardApi',
      update: '../riskScorecards/updateRiskScorecardByIdApi',
      calculateAdjusted: '../riskScorecards/calculateAdjustedRiskScorecardApi'
    })

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/riskScorecardsController', endpoints)
    })
  })

  describe('stable value funds', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/stableValueFundsController', endpoints)
    })
  })

  describe('static list', () => {
    const endpoints = {
      get: '../common/getStaticList'
    }

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/staticListController', endpoints)
    })
  })

  describe('transactions', () => {
    const endpoints = Object.assign(cloneDeep(commonBaseEndpoints), {
      getMany: '../transactions/queryTransactionsApi',
      create: '../transactions/createTransactionApi',
      update: '../transactions/updateTransactionApi',
      delete: '../transactions/deleteTransactionApi',
      validatePriorWithdrawals: '../transactions/validatePriorWithdrawals'
    })

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/transactionsController', endpoints)
    })
  })

  describe('underwriting', () => {
    const endpoints = Object.assign({}, commonBaseEndpoints)

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/underwritingsController', endpoints)
    })
  })

  describe('watch lists', () => {
    const endpoints = Object.assign(cloneDeep(commonBaseEndpoints), {
      create: '../watchLists/createReasonsAndActionStepsinWatchlist',
      update: '../watchLists/updateReasonsAndActionStepsinWatchlist',
      delete: '../watchLists/deleteReasonsAndActionStepsinWatchlist'
    })

    it('configures endpoints', () => {
      shouldConfigureController('api/controllers/watchListsController', endpoints)
    })
  })
})
