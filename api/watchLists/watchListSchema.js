const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {dateConverter, numberConverter} = require('../db/converters')
const overrideGetWatchListSelect = require('./overrideGetWatchListSelect')

const table = 'watch_list'
const tableId = 'watch_list_id'
const sequence = `${dbSchemaName}.watch_list_seq.NEXTVAL`

const zeroDefault = () => 0
module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'watch_list.contract_id',
  columns: [
    {name: 'watchListId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    version,
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'effectiveDate', column: 'effective_date', converter: dateConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId,
    {name: 'comments'}
  ],
  required: {
    new: ['watchListId', 'version', 'contractId', 'effectiveDate', 'createdDate'],
    update: ['watchListId', 'version', 'contractId', 'effectiveDate', 'createdDate']
  },
  overrideSelectClause: {
    selectOne: overrideGetWatchListSelect,
    selectMany: overrideGetWatchListSelect
  }
}
