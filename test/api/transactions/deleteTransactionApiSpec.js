/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/transactions/transactionSchema')
const contractSchema = require('../../../api/contracts/contractSchema')
const cloneDeep = require('lodash/cloneDeep')

const CONTRACT_INSERT_PARAMS = {stuff: true}
const userId = 'me'
const HEADERS = 'HEADERS'
const contract = {contractId: 11}
const req = {params: {id: 542}, headers: HEADERS}

describe('delete transaction api', () => {
  let target, res, jsonStub, dbTransaction, buildUpdateParams, getUserId, undoPendingTerminationContract, deleteByIdFromDb, updateDb, selectFromDb, selectByIdFromDb, deleteFeeDistributionsByTransactionId
  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    dbTransaction = {transactionId: 542, effectiveDate: 'yesterday', contractId: 11}
    buildUpdateParams = sinon.stub().returns('updateParams')
    getUserId = sinon.stub().withArgs(HEADERS).returns(userId)
    undoPendingTerminationContract = sinon.stub()
    deleteByIdFromDb = sinon.stub().resolves('delete by id result')
    updateDb = sinon.stub().resolves(dbTransaction)
    selectFromDb = sinon.stub()
    selectByIdFromDb = sinon.stub()
    deleteFeeDistributionsByTransactionId = sinon.stub()
    target = proxyquire('api/transactions/deleteTransactionApi', {
      '../common/buildUpdateParams': buildUpdateParams,
      '../contracts/workflow/undoPendingTerminationContract': undoPendingTerminationContract,
      '../db/deleteByIdFromDb': deleteByIdFromDb,
      '../db/updateDb': updateDb,
      '../db/selectFromDb': selectFromDb,
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../feeDistributions/deleteFeeDistributionsByTransactionId': deleteFeeDistributionsByTransactionId,
      '../getUserId': getUserId
    })
  })

  describe('locked transaction', () => {
    it('throws locked error message', async () => {
      dbTransaction = {locked: true, transactionId: 542, transactionType: 'DEPOSIT', effectiveDate: 'yesterday', contractId: 11}
      selectByIdFromDb.withArgs(schema, 542).resolves(dbTransaction)
      try {
        await target(req, res)
      } catch (err) {
        err.message.should.eql('This transaction is locked.')
      }
    })
  })

  describe('deletes deposit transaction', () => {
    beforeEach(async () => {
      dbTransaction = {transactionId: 542, transactionType: 'DEPOSIT', effectiveDate: 'yesterday', contractId: 11}
      selectByIdFromDb.withArgs(schema, 542).resolves(dbTransaction)
      await target(req, res)
    })

    it('deletes the transaction record', () => {
      deleteByIdFromDb.should.have.been.calledWithExactly(schema, 542, userId)
    })

    it('sends delete transaction result', () => {
      jsonStub.should.have.been.calledWithExactly('delete by id result')
    })

    it('buildUpdateParams not called', () => buildUpdateParams.should.have.not.been.called)
    it('updateDb not called', () => updateDb.should.have.not.been.called)
    it('deleteFeeDistributionsByTransactionId not called', () => deleteFeeDistributionsByTransactionId.should.have.not.been.called)
    it('undoPendingTerminationContract not called', () => undoPendingTerminationContract.should.have.not.been.called)
  })

  describe('deletes termination transaction', () => {
    beforeEach(async () => {
      dbTransaction = {transactionId: 542, transactionType: 'TERMINATION', effectiveDate: 'yesterday', contractId: 11}
      selectByIdFromDb.withArgs(schema, 542).resolves(dbTransaction)
      selectByIdFromDb.withArgs(contractSchema, 11).resolves(contract)
      await target(req, res)
    })

    it('deletes the transaction record', () => {
      deleteByIdFromDb.should.have.been.calledWithExactly(schema, 542, userId)
    })

    it('sends delete transaction result', () => {
      jsonStub.should.have.been.calledWithExactly('delete by id result')
    })

    it('undoes pending termination', () => {
      undoPendingTerminationContract.should.have.been.calledWithExactly({contract, userId})
    })

    it('buildUpdateParams not called', () => buildUpdateParams.should.have.not.been.called)
    it('updateDb not called', () => updateDb.should.have.not.been.called)
    it('deleteFeeDistributionsByTransactionId not called', () => deleteFeeDistributionsByTransactionId.should.have.not.been.called)
  })

  describe('deletes fee receipt transaction', () => {
    describe('without adjustmentTransactionId', () => {
      beforeEach(async () => {
        dbTransaction = {transactionId: 542, transactionType: 'FEE_RECEIPT', effectiveDate: 'yesterday', contractId: 11}
        selectByIdFromDb.withArgs(schema, 542).resolves(dbTransaction)
        await target(req, res)
      })

      it('deletes the transaction record', () => {
        deleteByIdFromDb.should.have.been.calledWithExactly(schema, 542, userId)
      })

      it('sends delete transaction result', () => {
        jsonStub.should.have.been.calledWithExactly('delete by id result')
      })

      it('delete fee distributions', () => {
        deleteFeeDistributionsByTransactionId.should.have.been.calledWithExactly(542, userId)
      })

      it('buildUpdateParams not called', () => buildUpdateParams.should.have.not.been.called)
      it('updateDb not called', () => updateDb.should.have.not.been.called)
      it('undoPendingTerminationContract not called', () => undoPendingTerminationContract.should.have.not.been.called)
    })

    describe('with adjustmentTransactionId', () => {
      beforeEach(async () => {
        dbTransaction = {transactionId: 542, transactionType: 'FEE_RECEIPT', effectiveDate: 'yesterday', contractId: 11, adjustmentTransactionId: 653}
        selectByIdFromDb.withArgs(schema, 542).resolves(dbTransaction)
        await target(req, res)
      })

      it('deletes the transaction record', () => {
        deleteByIdFromDb.should.have.been.calledWithExactly(schema, 542, userId)
      })

      it('sends delete transaction result', () => {
        jsonStub.should.have.been.calledWithExactly('delete by id result')
      })

      it('update db called', () => {
        updateDb.should.have.been.calledWithExactly(schema, 'updateParams', 542, userId)
      })

      it('deletes the adjustment transaction record', () => {
        deleteByIdFromDb.should.have.been.calledWithExactly(schema, 653)
      })

      it('delete fee distributions', () => {
        deleteFeeDistributionsByTransactionId.should.have.been.calledWithExactly(542, userId)
      })

      it('undoPendingTerminationContract not called', () => undoPendingTerminationContract.should.have.not.been.called)
    })
  })

  describe('deletes fee adjustment transaction', () => {
    beforeEach(async () => {
      dbTransaction = {transactionId: 542, transactionType: 'FEE_ADJUSTMENT', effectiveDate: 'yesterday', contractId: 11}
      selectByIdFromDb.withArgs(schema, 542).resolves(dbTransaction)
      selectByIdFromDb.withArgs(contractSchema, 11).resolves(contract)
      selectFromDb.resolves([{transactionId: 867}])
      await target(req, res)
    })

    it('deletes the transaction record', () => {
      deleteByIdFromDb.should.have.been.calledWithExactly(schema, 542, userId)
    })

    it('sends delete transaction result', () => {
      jsonStub.should.have.been.calledWithExactly('delete by id result')
    })

    it('deletes the adjustment transaction record', () => {
      deleteByIdFromDb.should.have.been.calledWithExactly(schema, 867, userId)
    })

    it('delete fee distributions', () => {
      deleteFeeDistributionsByTransactionId.should.have.been.calledWithExactly(867)
    })

    it('buildUpdateParams not called', () => buildUpdateParams.should.have.not.been.called)
    it('updateDb not called', () => updateDb.should.have.not.been.called)
    it('undoPendingTerminationContract not called', () => undoPendingTerminationContract.should.have.not.been.called)
  })
})
