require('require-sql')
const log = require('../../../log')
const simpleSelectFromDb = require('../../db/simpleSelectFromDb')
const sql = require('./getDailyFees.sql')
const mapInvoicesAndFees = require('./mapInvoicesAndFees')
const moment = require('moment')
const isLeapYear = require('../isLeapYear')
const calculateFutureDailyFees = require('./calculateFutureDailyFees')
const template = require('lodash/template')

const DATE_FORMAT = 'MM-DD-YYYY'

module.exports = async (req, res, next) => {
  let {query: {dateFilter, contractId}} = req
  log.info('get fees by contract', {dateFilter, contractId})

  dateFilter = moment(dateFilter)

  const sqlCompiled = template(sql)
  const sqlQuery = sqlCompiled({
    contractId,
    start: dateFilter.startOf('month').format(DATE_FORMAT),
    end: dateFilter.endOf('month').format(DATE_FORMAT)
  })

  const balancesTable = await simpleSelectFromDb(sqlQuery, mapInvoicesAndFees)

  const thisMonth = moment().endOf('month')
  const nextNextMonth = moment().add(2, 'months').startOf('month')

  if (dateFilter.isBetween(thisMonth, nextNextMonth)) {
    await calculateFutureDailyFees(dateFilter, contractId, balancesTable)
  }

  balancesTable.results.forEach((balance) => {
    balance.accruedFee = balance.accruedFee * 1
    balance.accruedFeeMtd = balance.accruedFeeMtd * 1
    balance.beginningFee = balance.beginningFee * 1
    balance.endingFee = balance.endingFee * 1
    balance.feeAdjustments = balance.feeAdjustments * 1
    balance.feeReceived = balance.feeReceived * 1
    balance.adjustments = balance.adjustments * 1
    balance.beginningBookValue = balance.beginningBookValue * 1
    balance.deposits = balance.deposits * 1
    balance.endingBookValue = balance.endingBookValue * 1
    balance.interest = balance.interest * 1
    balance.rate = balance.rate * 1
    balance.withdrawals = balance.withdrawals * 1
    balance.creditingRateRoundDecimals = balance.creditingRateRoundDecimals * 1
  })

  return res.status(200).json(balancesTable)
}
