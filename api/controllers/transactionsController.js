const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const getByIdApi = require('../common/getByIdApi')
const schema = require('../transactions/transactionSchema')
const queryWhereClauseBuilder = require('../transactions/buildTransactionsQueryWhereClause')

module.exports = createController({
  // getMany: getManyApi(schema, queryWhereClauseBuilder),
  getMany: require('../transactions/queryTransactionsApi'),
  getOne: getByIdApi(schema),
  create: require('../transactions/createTransactionApi'),
  update: require('../transactions/updateTransactionApi'),
  delete: require('../transactions/deleteTransactionApi'),
  validatePriorWithdrawals: require('../transactions/validatePriorWithdrawals')
})
