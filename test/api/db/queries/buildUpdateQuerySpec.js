/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const testSchema = require('../../../data/testSchema')
const params = {
  bColumn: 'bValue',
  cColumn: 'cValue'
}
const THE_ID = 8675309
const aResult = { rows: [ { aColumn: 8 } ] }

describe('build update query', () => {
  let target, convertBindParamsToDbValues, schema, sql
  beforeEach(() => {
    schema = Object.assign({}, testSchema.schema)
    sql = `INSERT INTO ${schema.table} VALUES (${schema.columns[0].sequence}, :${schema.columns[1].name}, :${schema.columns[2].name}) RETURNING ${schema.tableId} INTO :id`

    convertBindParamsToDbValues = sinon.stub().returns({someBindParam: true})
    target = proxyquire('api/db/queries/buildUpdateQuery', {
      '../converters/convertBindParamsToDbValues': convertBindParamsToDbValues
    })
  })

  it('returns object with update sql', () =>
    target(schema, params, THE_ID).sql.should.eql('UPDATE aTable SET bColumn = :bColumn, cColumn = :cColumn WHERE aTable.aTableId = :id'))

  it('returns object with default bindParams', () =>
    target(schema, params, THE_ID).bindParams.should.eql({id: THE_ID, someBindParam: true}))

  it('returns object with default executeOptions', () =>
    target(schema, params, THE_ID).executeOptions.should.eql({ autoCommit: true }))

  it('returns object with mapResult', () =>
    target(schema, params, THE_ID).mapResult(aResult).should.eql(aResult))
})
