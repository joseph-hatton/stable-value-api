const proxyquire = require('../../proxyquire')

const columns = ['something', 'else']
const SQL = 'SOME SQL'
const bodyAfterRemoval = {something: 'something value', else: 'else value'}
const bodyBeforeRemoval = Object.assign({toBeIgnored: true}, bodyAfterRemoval)

const contact1 = {contactId: 1, contractId: 'a', contractNumber: 'a1'}
const contact2b = {contactId: 2, contractId: 'b', contractNumber: 'b1'}
const contact2c = {contactId: 2, contractId: 'c', contractNumber: 'c1'}

describe('build contracts contract number', () => {
  let target, executeSimpleSqlQuery, connection
  beforeEach(() => {
    connection = sinon.spy()
    executeSimpleSqlQuery = sinon.stub().resolves(42)
    target = proxyquire('api/contracts/buildContractNumber', {
      './buildContractNumber.sql': SQL,
      '../db/simpleSelectFromDb': executeSimpleSqlQuery
    })
  })

  it('execute simple query called once', () => {
    target(connection)
    .then(() => executeSimpleSqlQuery.should.have.been.called)
  })

  it('execute simple query called with sql', () => {
    target(connection)
    .then(() => executeSimpleSqlQuery.getCall(0).args[0].should.eql(SQL))
  })

  it('gets a contract number mapper', () => {
    target(connection)
    .then(() => executeSimpleSqlQuery.getCall(0).args[1]({rows: [{MIN: 42}]}).should.eql(42))
  })
})
