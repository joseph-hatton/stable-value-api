const toDbValue = (columns, key, value) => {
  const matchingColumn = columns.find((column) => column.name === key)
  return (matchingColumn && matchingColumn.converter) ? matchingColumn.converter.toDb(value) : value
}

module.exports = (columns, bindParams) => {
  const newBindParams = {}
  Object.keys(bindParams)
  .forEach((key) => {
    newBindParams[key] = toDbValue(columns, key, bindParams[key])
  })
  return newBindParams
}
