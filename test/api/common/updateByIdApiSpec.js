const proxyquire = require('../../proxyquire')
const {schema} = require('../../data/testSchema')
const defaultParams = {a: true}
const user = 'THE USER'
const body = {b: 'from request body'}
const THE_ID = 8675309
const VALIDATE_THEN_CALL_API_RESULT = 'VALIDATE_THEN_CALL_API_RESULT'

describe('update by id api', () => {
  const companies = [{ id: 'aId', name: 'a' }, { id: 'bId', name: 'b' }]
  const request = { headers: 'abc', params: {id: THE_ID} }

  let response, target, next, getUserId, responseStatusSend, responseJson, updateDb, validateThenCallApi

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    responseJson = sinon.spy()
    response = {
      json: responseJson,
      status: sinon.stub().returns({ send: responseStatusSend, json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    const query = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)
    updateDb = sinon.stub().resolves({rowsAffected: 1})
    const buildDefaultParams = sinon.stub().withArgs(schema.columns).returns(defaultParams)
    validateThenCallApi = sinon.stub().returns(VALIDATE_THEN_CALL_API_RESULT)
    target = proxyquire('api/common/updateByIdApi', {
      '../getUserId': getUserId,
      '../db/updateDb': updateDb,
      './buildDefaultParams': buildDefaultParams,
      './buildUpdateParams': sinon.stub().returns(defaultParams),
      './validateThenCallApi': validateThenCallApi
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
      request.body = body
    })

    it('returns validateThenCallApiResult', () =>
    target(schema)(request, response, next)
      .then((result) => result.should.eql(VALIDATE_THEN_CALL_API_RESULT))
    )

    it('validateThenCallApiResult should be called with schema', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[0].should.eql(schema))
    )

    it('validateThenCallApiResult should be called with body', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[1].should.eql(body))
    )

    it('validateThenCallApiResult should be called with required fields', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[2].should.eql([]))
    )

    it('validateThenCallApiResult should be called with res', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[3].should.eql(response))
    )

    it('validateThenCallApiResult should be called with next', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[4].should.eql(next))
    )

    it('validateThenCallApiResult should be called with userId in options', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[6].should.eql({userId: user, params: {id: THE_ID}}))
    )

    it('sends expected data into update', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user, params: {id: THE_ID}}))
        .then(() => {
          updateDb.should.have.been.calledWithExactly(schema, defaultParams, THE_ID, user)
        })
    )

    it('sends update results to response', () =>
      target(schema)(request, response, next)
        .then(() => validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user, params: {id: THE_ID}}))
        .then((result) => {
          response.json.should.have.been.calledWithExactly({rowsAffected: 1})
        })
    )
  })
})
