const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const getByIdApi = require('../common/getByIdApi')
const createApi = require('../common/createApi')
const updateByIdApi = require('../common/updateByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../reasons/reasonSchema')

module.exports = createController({
  getMany: getManyApi(schema),
  getOne: getByIdApi(schema),
  create: createApi(schema),
  update: updateByIdApi(schema),
  delete: deleteByIdApi(schema)
})
