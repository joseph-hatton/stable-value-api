const createError = require('http-errors')
const log = require('../../../log')
const getBalancesByContract = require('../../balances/getBalancesByContract')
const buildUpdateParams = require('../../common/buildUpdateParams')
const validateThenCallApi = require('../../common/validateThenCallApi')
const contractSchema = require('../../contracts/contractSchema')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const updateDb = require('../../db/updateDb')
const isMonthOpen = require('../../monthlyCycles/isMonthOpen')
const isFutureMonth = require('../../monthlyCycles/isFutureMonth')
const getUserId = require('../../getUserId')
const schema = require('../creditingRatesSchema')
const calculateCreditingRateBuildParamsOrErrors = require('./calculateCreditingRateBuildParamsOrErrors')
const getCreditingRatesByContract = require('./getCreditingRatesByContract')

const canCalculate = (contract, creditingRate) => {
  const contractStatusIsActive = contract && contract.status === 'ACTIVE'
  const creditingRateStatusIsAwaitingOrForecast = creditingRate && (creditingRate.status === 'AWAITING' || creditingRate.status === 'FORECAST')
  const creditingRateCalcFieldsNotNull = creditingRate && creditingRate.referenceDate != null && creditingRate.marketValue != null && creditingRate.annualEffectiveYield != null && creditingRate.duration != null
  return contractStatusIsActive
    && creditingRateStatusIsAwaitingOrForecast
    && creditingRateCalcFieldsNotNull
}

const isEffectiveDateAfterContractEffectiveDateAndInOpenMonth = async (effectiveDate, dbContract) => {
  const isBeforeContractEffectiveDate = effectiveDate < dbContract.effectiveDate
  const monthIsOpen = await isMonthOpen(effectiveDate)
  const monthIsFuture = await isFutureMonth(effectiveDate)
  return !(isBeforeContractEffectiveDate || (!monthIsOpen && !monthIsFuture))
}

const updateCreditingRate = async (dbCreditingRate, dbContract, creditingRateId, userId, params) => {
  const updateParams = buildUpdateParams(schema.columns, userId, params)
  log.info({domain: schema.tableShort, contractId: dbContract.contractId, id: creditingRateId, updateParams}, 'update crediting rate params')
  return await updateDb(schema, updateParams, creditingRateId, userId)
}

const isEffectiveDateNotAlreadyTakenForContractId = async (contractId, creditingRateId, effectiveDate) => {
  const creditingRatesWithSameEffectiveDate = await getCreditingRatesByContract(contractId, {effectiveDate})
  return creditingRatesWithSameEffectiveDate.filter((creditingRate) => creditingRate.creditingRateId !== creditingRateId).length === 0
}

const updateAndBuildResponse = async (res, creditingRateId, userId, dbCreditingRate, dbContract, body, balance) => {
  const {errors, params} = calculateCreditingRateBuildParamsOrErrors(dbCreditingRate, dbContract, body, balance, userId)
  let result
  if (!errors || errors.length === 0) {
    result = await updateCreditingRate(dbCreditingRate, dbContract, creditingRateId, userId, params)
  }
  return {errors, result}
}

const attemptCalculateApi = async (schema, body, res, next, {contractId, userId, creditingRateId}) => {
  const dbContract = await selectByIdFromDb(contractSchema, contractId)
  const dbCreditingRate = await selectByIdFromDb(schema, creditingRateId)
  if (canCalculate(dbContract, dbCreditingRate)) {
    const effectiveDateValid = await isEffectiveDateAfterContractEffectiveDateAndInOpenMonth(body.effectiveDate, dbContract)
    if (effectiveDateValid) {
      const isEffectiveDateUnique = await isEffectiveDateNotAlreadyTakenForContractId(contractId, creditingRateId, body.effectiveDate)
      if (!isEffectiveDateUnique) {
        throw createError.BadRequest('Effective date is not unique for contract.')
      } else {
        const balance = (dbCreditingRate.bookValue) ? dbCreditingRate.bookValue : await getBalancesByContract(contractId, {effectiveDate: dbCreditingRate.referenceDate})
        if (!balance) {
          throw createError.BadRequest('No balance information found for contract.')
        } else {
          const {errors, result} = await updateAndBuildResponse(res, creditingRateId, userId, dbCreditingRate, dbContract, body, balance)
          if (errors && errors.length > 0) {
            throw createError.BadRequest({messages: errors})
          } else {
            return res.status(200).json(result)
          }
        }
      }
    } else {
      throw createError.BadRequest('Effective date must be after the contract effective date and in an open month.')
    }
  } else {
    throw createError.BadRequest('Only Awaiting OR Forecast Crediting Rates with referenceDate, marketValue, annualEffectiveYield and duration on Active Contracts can be calculated.')
  }
}

module.exports = async ({ params, headers, body }, res, next) => {
  log.info({domain: schema.tableShort, contractId: params.contractId, id: params.id}, 'calculate crediting rate api')
  const userId = getUserId(headers)
  return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptCalculateApi, {contractId: params.contractId, userId, creditingRateId: params.id})
}
