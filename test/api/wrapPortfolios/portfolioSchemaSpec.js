require('require-sql')
const validateSchema = require('../validateSchema')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../../../api/common/auditColumns')
const target = require('../../../api/wrapPortfolios/portfolioSchema')
const sql = require('../../../api/wrapPortfolios/getPorfolioFull.sql')
const {beginningOfDayDateConverter, bpsConverter, booleanConverter, dateConverter, numberConverter, percentConverter} = require('../../../api/db/converters')

const zeroDefault = () => 0

describe('portfolio schema', () => {
  validateSchema(target, {
    tableShort: 'portfolio',
    table: 'sch_stbv.portfolio',
    tableId: 'portfolio_id',
    sequence: 'sch_stbv.portfolio_seq.NEXTVAL',
    orderBy: 'portfolio_id', //
    columns: [  //  order by the column ids in the table
      {name: 'portfolioId', column: 'portfolio_id', sequence: 'sch_stbv.portfolio_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      {name: 'version', converter: numberConverter},
      {name: 'portfolioName', column: 'portfolio_name', audit: true},
      {name: 'portfolioManagerId', column: 'portfolio_manager_id', audit: true, converter: numberConverter},
      createdDate,
      createdId,
      modifiedDate,
      modifiedId
    ],
    overrideSelectClause: {
      selectOne: {sql: sql},
      selectMany: {sql: sql}
    },
    required: {
      new: ['portfolioManagerId', 'portfolioName']
    }
  })
})
