const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const schema = require('../invoices/invoicesSchema')
const queryOpenInvoicesForContract = require('../invoices/queryOpenInvoicesForContract')

module.exports = createController({
  getMany: getManyApi(schema, queryOpenInvoicesForContract)
})
