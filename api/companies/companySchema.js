const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId} = require('../common/auditColumns')
const {booleanConverter, numberConverter} = require('../db/converters')
const stateValidator = require('../states/stateValidator')
const getCompaniesWithContacts = require('./getCompaniesWithContacts')

const table = 'company'
const tableId = 'company_id'
const sequence = `${dbSchemaName}.company_seq.NEXTVAL`

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'name',
  columns: [
    {name: 'companyId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'name', audit: true},
    {name: 'address1', audit: true},
    {name: 'address2', audit: true},
    {name: 'city', audit: true},
    {name: 'state', audit: true, validators: [stateValidator]},
    {name: 'zipCode', column: 'zip_code', audit: true},
    {name: 'version', defaultValue: () => 0, converter: numberConverter},
    createdDate,
    createdId,
    modifiedDate,
    modifiedId,
    {name: 'receivesEdiSw', column: 'receives_edi_sw', defaultValue: () => false, converter: booleanConverter}
  ],
  audit: {
    objectClass: 'Company',
    buildDescription: (company) => `Name: ${company.name}`
  },
  overrideSelectClause: {
    selectMany: getCompaniesWithContacts
  }
}
