module.exports = {
  toDb: (apiValue) => (apiValue !== null)
    ? (typeof apiValue === 'string')
      ? new Date(apiValue)
      : apiValue
    : null,
  fromDb: (dbString) => (dbString) ? new Date(dbString) : null
}
