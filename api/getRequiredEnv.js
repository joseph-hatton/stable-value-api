const requiredEnv = envVariableName => {
  throw new Error(`missing environment variable ${envVariableName}`)
}

module.exports = envVariableName => process.env[envVariableName] || requiredEnv(envVariableName)
