SELECT  transaction.*,
				contract.contract_number,
				contract.short_plan_name,
				balance.balance_id,
				balance.beginning_book_value,
				fee_distribution.fee_distribution_id as fd_fee_distribution_id,
				fee_distribution.invoice_id as fd_invoice_id,
				invoice.invoice_number as i_invoice_number,
				fee_distribution.amount as fd_fee_amount,
				fee_distribution.created_date as fd_created_date,
				fee_distribution.created_id as fd_created_id,
				fee_distribution.modified_date as fd_modified_date,
				fee_distribution.modified_id as fd_modified_id,
				fee_distribution.version as fd_version,
				fee_distribution.beginning_balance as fd_beginning_balance,
				fee_distribution.adjustment as fd_adjustment,
				fee_distribution.ending_balance as fd_ending_balance
FROM sch_stbv.transaction
INNER JOIN sch_stbv.contract
  ON  transaction.contract_id = contract.contract_id
LEFT OUTER JOIN sch_stbv.balance
  ON  transaction.contract_id = balance.contract_id
  AND transaction.effective_date = balance.effective_date
LEFT OUTER JOIN sch_stbv.fee_distribution
	ON transaction.transaction_id = fee_distribution.transaction_id
LEFT OUTER JOIN sch_stbv.invoice
	ON fee_distribution.invoice_id = invoice.invoice_id