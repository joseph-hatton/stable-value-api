require('require-sql')
const sql = require('./overrideGetManyTransactionsSelect.sql')

const mapResults = (dbResults) => {
  return dbResults.map((row) => {
    return {
      ...row,
      balanceId: (row.balanceId) ? row.balanceId * 1 : row.balanceId,
      beginningBookValue: (row.beginningBookValue) ? row.beginningBookValue * 1 : row.beginningBookValue
    }
  })
}

module.exports = {
  sql,
  mapper: mapResults
}
