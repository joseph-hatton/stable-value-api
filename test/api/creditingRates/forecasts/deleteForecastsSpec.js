const moment = require('moment')
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

const contractId = 55
const userId = 'somebody'
const terminationDate = (new Date()).toISOString()
const convertedBindParams = {convertedFromApplySchema: true}

describe('delete forecasts', () => {
  let target, deleteFromDb, applySchemaConverters
  beforeEach(() => {
    applySchemaConverters = sinon.stub().withArgs({columns: schema.columns}, {contractId, effectiveDate: terminationDate}).returns(convertedBindParams)
    deleteFromDb = sinon.spy()
    target = proxyquire('api/creditingRates/forecasts/deleteForecasts', {
      '../../db/deleteFromDb': deleteFromDb,
      '../../db/converters/applySchemaConverters': applySchemaConverters
    })
  })

  it('calls delete with contract id and effective date', async () => {
    const result = await target(contractId, terminationDate, userId)
    deleteFromDb.should.have.been.calledWithExactly('delete from sch_stbv.crediting_rate where contract_id = :contractId and effective_date >= :effectiveDate and status = \'FORECAST\'', convertedBindParams)
  })
})
