const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contracts/contractSchema')
const cloneDeep = require('lodash/cloneDeep')

const USER_ID = 'me'
const HEADERS = 'HEADERS'

describe('override contract modified id api', () => {
  let target, res, jsonStub, next, body, getUserId, updateDb
  beforeEach(() => {
    jsonStub = sinon.stub()
    res = { status: sinon.stub().returns({json: jsonStub}) }
    next = sinon.spy()
    getUserId = sinon.stub().withArgs(HEADERS).returns(USER_ID)
    updateDb = sinon.stub().withArgs(schema, {modifiedId: 'testing-override'}, 542, USER_ID, true).resolves('update db response')
    target = proxyquire('api/contracts/workflow/overrideContractModifiedIdApi', {
      '../../getUserId': getUserId,
      '../../db/updateDb': updateDb
    })
  })

  it('returns validation result', async () => {
    await target({params: {id: 542}, headers: HEADERS, body: body}, res, next)
    jsonStub.should.have.been.calledWithExactly('update db response')
  })
})
