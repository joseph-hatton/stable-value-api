const createError = require('http-errors')
const log = require('../../log')
const schema = require('./riskScorecardsSchema')
const getAdjustedRiskScorecardById = require('./getAdjustedRiskScorecardById')

module.exports = async (req, res) => {
    log.info({domain: schema.tableShort, id: req.params.id}, 'get by id api')
    const adjustedRiskScorecard = await getAdjustedRiskScorecardById(req.params.id)
    if (adjustedRiskScorecard) {
      res.status(200).json(adjustedRiskScorecard)
    } else {
      throw createError.NotFound('Requested resource does not exist.')
    }
  }
