const log = require('../../../log')
const convertRowResultFromDbValue = require('../converters/convertRowResultFromDbValue')

const determineSelectClause = (schema) => (schema && schema.overrideSelectClause && schema.overrideSelectClause.selectMany) ? schema.overrideSelectClause.selectMany : schema.overrideSelectClause.selectOne

const getOverrideSelectAndMapper = (schema) => {
  const hasOverride = (schema.overrideSelectClause && (schema.overrideSelectClause.selectOne || schema.overrideSelectClause.selectMany))
  return (hasOverride)
    ? schema.overrideSelectClause.selectMany || schema.overrideSelectClause.selectOne
    : null
}

const buildSql = (schema, whereClause, overrideSelectAndMapper) => {
  const querySql = (overrideSelectAndMapper)
    ? `${overrideSelectAndMapper.sql} ${whereClause} order by ${schema.orderBy}`
    : `select * from ${schema.table} ${whereClause} order by ${schema.orderBy}`

  const upperSql = querySql.toUpperCase()
  const indexOfFrom = upperSql.indexOf('FROM')
  const sqlWithSelectRemoved = querySql.substr(indexOfFrom, querySql.length)
  const countSql = `select count(*) ${sqlWithSelectRemoved}`

  log.info({querySql, countSql}, 'build select count sql')
  return countSql
}

const mapRowsFromDbValue = (schema, overrideSelectAndMapper) =>
  ({ rows }) => {
    log.info({overrideSelectAndMapper, rows}, 'select count result from db')

    const mappedRows = rows.map((row) => convertRowResultFromDbValue(schema.columns, row))
    return (overrideSelectAndMapper && overrideSelectAndMapper.mapper) ? overrideSelectAndMapper.mapper(mappedRows) : mappedRows
  }

module.exports = (schema, {whereClause = '', bindParams = {}, executeOptions = {}, pagination = {pageSize: null, pageNumber: null}}) => {
  const overrideSelectAndMapper = getOverrideSelectAndMapper(schema)
  return {
    sql: buildSql(schema, whereClause, overrideSelectAndMapper),
    bindParams,
    executeOptions,
    mapResult: mapRowsFromDbValue(schema, overrideSelectAndMapper)
  }
}
