const {getContractContactsByContractId, validateContractContacts, validateCurrentlyAssigned} = require('./validateAssignedContacts')
const createError = require('http-errors')
const selectFromDb = require('../../db/selectFromDb')
const schema = require('./contactContractSchema')
const updateDb = require('../../db/updateDb')
const getUserId = require('../../getUserId')
const buildUpdateParams = require('../../common/buildUpdateParams')
const getContractsWithContact = async (contactId) =>
  await selectFromDb(schema, {bindParams: {contactId}, whereClause: `WHERE ${schema.table}.contact_id = :contactId`})

const validateCurrentlyAssignedWithNewValues = async (contactId, contractId, newValues) => {
  const contactContracts = await getContractContactsByContractId(contractId)
  let contractContactToSave = null
  const contactContractNewValues = contactContracts.map((item) => {
    let returnValue = item
    if (item.contactId === contactId) {
      returnValue = {
        ...item,
        receiveStatement: newValues.receiveStatement,
        receiveInvoice: newValues.receiveInvoice,
        receiveScheduleA: newValues.receiveScheduleA,
        contactType: newValues.contactType
      }
      contractContactToSave = returnValue
    }
    console.log('SSSSS', returnValue, contactId)
    return returnValue
  })
  .filter(i => i.contractStatus !== 'PENDING')
  return {
    errors: await validateContractContacts(contactContractNewValues),
    contractContactToSave
  }
}

const updateContactContractWithNewvaluesDB = async (headers, contractContactToSave, res) => {
  const userId = getUserId(headers)
  for (let contractContact of contractContactToSave) {
    const updateParams = await buildUpdateParams(schema.columns, userId, contractContact)
     await updateDb(schema, updateParams, contractContact.contactContractId, userId)
  }
 return res.status(200).json('Succesfully Save')
}

module.exports = (schema) => async ({ query, params, headers, body }, res, next) => {
  const contractsWithContact = await getContractsWithContact(params.id)
  const err = []
  const contractContactsToSave = []
  for (let contractWithContact of contractsWithContact) {
    const {errors, contractContactToSave} = await validateCurrentlyAssignedWithNewValues(params.id, contractWithContact.contractId, body)
    if (errors.length > 0) {
       err.push(errors)
    }
    if (contractContactToSave) {
      contractContactsToSave.push(contractContactToSave)
    }
  }
  if (err.length > 0) {
    throw createError.BadRequest('Updating this Contact will result in violations to mandatory Contacts.')
  } else {
    updateContactContractWithNewvaluesDB(headers, contractContactsToSave, res)
  }
}
