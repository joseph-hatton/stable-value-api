const log = require('../../log')
const dbExecutePromise = require('./executors/dbExecutePromise')
const dbExecuteOne = require('./executors/dbExecuteOne')

const executeOptions = {autoCommit: true}

module.exports = (sql, bindParams, mapResult = (result) => result) =>
  dbExecutePromise((connection) =>
    dbExecuteOne(connection, {sql, bindParams, executeOptions, mapResult}))
