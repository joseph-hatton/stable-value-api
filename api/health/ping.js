const getStatus = require('./getStatus')
const log = require('../../log')

module.exports = (req, res, next) =>
  getStatus()
  .then(({error}) => {
    if (error) {
      log.info(error, 'healthcheck ping failed')
      res.status(500).send()
    } else {
      res.send()
    }
  })
  .catch(next)
