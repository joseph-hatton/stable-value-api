const createError = require('http-errors')
const httpntlm = require('node-http-ntlm')
const {oldAppBaseUrl, ldap: {user, password}} = require('../config')
const log = require('../../log')

module.exports = async (req, res) => {
  const userId = req.params.id
  const preventCache = new Date().getTime()
  const url = `https://mysites.rgare.com/User%20Photos/Profile%20Pictures/${userId}_LThumb.jpg?t=${preventCache}`
  log.info({domain: 'profile', userId, url}, 'get profile pic api')
  return await new Promise((resolve, reject) => {
    return httpntlm.get({
      url,
      username: user,
      password,
      workstation: 'stable-value-api',
      domain: 'rgare',
      binary: true
    }, (err, response) => {
      log.info('in callback')
      if (err) {
        log.error({domain: 'profile', userId, url}, 'unable to get profile image')
        reject(err)
      } else {
        log.info({domain: 'profile', userId, url}, 'received profile image')
        res.type('jpg').send(response.body)
        resolve(response.body)
      }
    })
  })
}
