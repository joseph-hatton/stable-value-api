const proxyquire = require('../../proxyquire')

const req = { params: { id: 'someUserId' } }

describe('get profile pic', () => {
  let target, res, resSend, httpntlm
  resSend = sinon.stub()
  res = {type: sinon.stub().returns({send: resSend})}
  httpntlm = {get: sinon.stub()}
  beforeEach(() => {
    target = proxyquire('api/profiles/getProfilePicApi', {
      'node-http-ntlm': httpntlm
    })
  })

  describe('successfully gets pic', () => {
    beforeEach(() => {
      httpntlm.get.yields(null, {body: 'the pic body'})
    })

    it('resolves message', async () => {
      const result = await target(req, res)

      result.should.eql('the pic body')
    })

    it('sends pic to response', async () => {
      const result = await target(req, res)

      resSend.should.have.been.calledWithExactly('the pic body')
    })
  })

  describe('fails to get pic', () => {
    beforeEach(() => {
      httpntlm.get.yields(new Error('some error'), {body: null})
    })

    it('resolves message', async () => {
      try {
        await target(req, res)
      } catch (err) {
        err.message.should.eql('some error')
      }
    })
  })
})
