const selectFromDb = require('../db/selectFromDb')
const riskLookupsSchema = require('./riskLookupsSchema')
const adjustRiskScorecards = require('./adjustRiskScorecards')

module.exports = async (riskScorecardsByContract) => {
  const riskLookups = await selectFromDb(riskLookupsSchema, {})
  const adjustedScorecards = adjustRiskScorecards(riskLookups, riskScorecardsByContract)
  return adjustedScorecards
}
