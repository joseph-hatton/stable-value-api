/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/riskScorecards/riskScorecardsSchema')
const user = 'THE USER'
const body = {contractType: 'SINGLE_PLAN'}
const THE_ID = 8675309
const VALIDATE_THEN_CALL_API_RESULT = 'VALIDATE_THEN_CALL_API_RESULT'

describe('calculate adjusted risk scorecard api', () => {
  const scorecard = {riskScorecardId: 5}
  const adjustedScorecard = {riskScorecardId: 5, adjusted: true}
  const request = { headers: 'abc', params: {id: THE_ID} }

  let response, target, next, getUserId, responseJson, updateDb, validateThenCallApi, flattenRiskScorecardForInsertOrUpdateParam, getLookupsAndAdjustRiskScorecards

  beforeEach(() => {
    responseJson = sinon.spy()
    response = {
      status: sinon.stub().withArgs(200).returns({ json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    // const query = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)
    validateThenCallApi = sinon.stub().returns(VALIDATE_THEN_CALL_API_RESULT)
    flattenRiskScorecardForInsertOrUpdateParam = sinon.stub()
    getLookupsAndAdjustRiskScorecards = sinon.stub().withArgs([scorecard]).resolves([adjustedScorecard])
    target = proxyquire('api/riskScorecards/calculateAdjustedRiskScorecardApi', {
      '../getUserId': getUserId,
      '../common/validateThenCallApi': validateThenCallApi,
      './flattenRiskScorecardForInsertOrUpdateParam': flattenRiskScorecardForInsertOrUpdateParam,
      './getLookupsAndAdjustRiskScorecards': getLookupsAndAdjustRiskScorecards
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
      request.body = body
    })

    it('returns validateThenCallApiResult', async () => {
      const result = await target(request, response, next)
      result.should.eql(VALIDATE_THEN_CALL_API_RESULT)
    })

    it('validateThenCallApiResult should be called with schema', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[0].should.eql(schema)
    })

    it('validateThenCallApiResult should be called with body', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[1].should.eql(body)
    })

    it('validateThenCallApiResult should be called with required fields', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[2].should.eql(schema.required.new)
    })

    it('validateThenCallApiResult should be called with res', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[3].should.eql(response)
    })

    it('validateThenCallApiResult should be called with next', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[4].should.eql(next)
    })

    it('validateThenCallApiResult should be called with userId in options', async () => {
      await target(request, response, next)
      validateThenCallApi.lastCall.args[6].should.eql({userId: user, params: {id: THE_ID}})
    })

    it('sends adjusted riskScorecard result to response', async () => {
      await target(request, response, next)
      const result = await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user, params: {id: THE_ID}})
      responseJson.should.have.been.calledWithExactly({ adjusted: true, contractType: 'SINGLE_PLAN', riskScorecardId: 5 })
    })
  })
})
