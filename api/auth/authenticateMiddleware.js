const log = require('../../log')
const {responses: {unauthorized}} = require('../config')
const getUserId = require('../getUserId')

module.exports = (req, res, next) => {
  const userId = getUserId(req.headers)
  log.info({userId}, 'authenticate middleware found required user')
  return (userId)
    ? next()
    : res.status(unauthorized.status).send(unauthorized.message)
}
