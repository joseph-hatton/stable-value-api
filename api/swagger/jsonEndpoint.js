const fs = require('fs')
const yaml = require('js-yaml')

const fileName = `${__dirname}/swagger.yaml`

const swaggerJson = yaml.safeLoad(fs.readFileSync(fileName))

swaggerJson.host = `stable-value-api-dev.rgare.net`

module.exports = (req, res) => res.json(swaggerJson)

module.exports.swaggerJson = swaggerJson
