/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const SKIP_AUDIT = true
const params = {id: 123}
const headers = ['header']
const body = {actionSteps: [{actionStepId: 1}, {actionStepId: 2}], reasons: [{reasonId: 1}, {reasonId: 2}, {reasonId: 3}]}
const schema = {}
const userId = 'user'
const selectResponse = 'selectResponse'

describe('delete reasons and action steps in watchlist', () => {
  let target, validateThenCallApi, res, next, resJson, getUserId, deleteFromDb, deleteByIdFromDb
  beforeEach(() => {
    resJson = sinon.spy()
    res = {status: () => ({json: resJson})}
    next = sinon.spy()
    getUserId = sinon.stub().returns(userId)
    deleteFromDb = sinon.stub().resolves('')
    deleteByIdFromDb = sinon.stub().resolves('')

    target = proxyquire('api/watchLists/deleteReasonsAndActionStepsinWatchlist', {
      '../getUserId': getUserId,
      '../db/deleteFromDb': deleteFromDb,
      '../db/deleteByIdFromDb': deleteByIdFromDb
    })
  })
  describe('calling attemptUpdateApi', () => {
    describe('should call', () => {
      beforeEach(async () => {
        await target({headers, params}, res, next)
      })
      it('getUserId', () => getUserId.should.have.been.called)
      it('deleteFromDb', () => deleteFromDb.should.have.been.calledTwice)
      it('deleteByIdFromDb', () => deleteByIdFromDb.should.have.been.calledOnce)
    })
  })
})
