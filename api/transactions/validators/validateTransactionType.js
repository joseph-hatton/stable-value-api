const validateIdAndText = require('../../common/validateIdTextField')
const {transactionTypes} = require('../../referenceData.json')
module.exports = validateIdAndText(transactionTypes, 'Invalid Transaction Type')
