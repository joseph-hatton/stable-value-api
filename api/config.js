const {env} = process
const log = require('../log')
const getRequiredEnv = require('./getRequiredEnv')
const password = getRequiredEnv('ORACLE_PASSWORD')
const ldapPassword = getRequiredEnv('LDAP_PASSWORD')

const {ORACLE_HOST, ORACLE_PORT, ORACLE_SERVICE_NAME, ORACLE_USER, OLD_APP_BASE_URL, LDAP_USER} = env
const user = ORACLE_USER || 'app_stbv'
const host = ORACLE_HOST || 'stldora20.rgare.net'
const port = ORACLE_PORT || '1534'
const serviceName = ORACLE_SERVICE_NAME || 'stldrga1'
const oldAppBaseUrl = OLD_APP_BASE_URL || 'http://stldstbvalapp01.rgare.net:8080/stableValue'
const ldapUser = LDAP_USER || 'svc4finreldap'

log.info({user, host, port, serviceName, oldAppBaseUrl}, 'envs')

const prodPrefix = 'stlp'

module.exports = {
  port: parseInt(env.PORT || 3000),
  oldAppBaseUrl,
  maxSigned32: 2147483647,
  db: {
    user,
    password,
    connectString: `${host}:${port}/${serviceName}`
  },
  isPointingToProductionDatabase: host.toLowerCase().includes(prodPrefix) || serviceName.toLocaleUpperCase().includes(prodPrefix),
  isDeployed: env.NODE_ENV === 'production',
  dbSchemaName: 'sch_stbv',
  defaultPagingLimit: 50,
  responses: {
    noUpdate: {
      status: 422,
      message: 'nothing to update'
    },
    unauthorized: {
      status: 401,
      message: 'Unauthorized'
    }
  },
  ldap: {
    overrideAdEntitlements: Boolean(env.OVERRIDE_AD_ENTITLEMENTS),
    user: ldapUser,
    password: ldapPassword,
    queryUrl: 'scaffold_dapper:3000/v1/ldap'
    // queryUrl: 'http://stldstbvldock01.rgare.net:30001/v1/ldap'
  }
}
