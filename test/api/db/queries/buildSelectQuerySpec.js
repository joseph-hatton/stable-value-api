/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const testSchema = require('../../../data/testSchema')

const overrideSelectMany = {
  sql: 'select many',
  mapper: (rows) => 'many mapped results'
}

const overrideSelectOne = {
  sql: 'select one',
  mapper: (rows) => 'one mapped result'
}

const paginationArgs = {pageSize: 2, pageNumber: 2}

describe('build select query', () => {
  let target, convertRowResultFromDbValue, schema, sql, params
  beforeEach(() => {
    schema = Object.assign({}, testSchema.schema)
    params = {
      bColumn: 'bValue',
      cColumn: 'cValue'
    }
    sql = `INSERT INTO ${schema.table} VALUES (${schema.columns[0].sequence}, :${schema.columns[1].name}, :${schema.columns[2].name}) RETURNING ${schema.tableId} INTO :id`

    convertRowResultFromDbValue = sinon.stub().returns('fromDb1')
    target = proxyquire('api/db/queries/buildSelectQuery', {
      '../converters/convertRowResultFromDbValue': convertRowResultFromDbValue
    })
  })

  it('returns object with insert sql', () =>
    target(schema, true, params).sql.should.eql('select * from aTable  order by bColumn'))

  it('returns object with default bindParams', () =>
    target(schema, true, params).bindParams.should.eql({}))

  it('returns object with default executeOptions', () =>
    target(schema, true, params).executeOptions.should.eql({ maxRows: 1000 }))

  it('returns object with mapResult', () =>
    target(schema, true, params).mapResult({rows: [{aColumn: 8}]}).should.eql(['fromDb1']))

  const assertOverrideSelectUsed = (shouldSelectMany, overrideSelectClause, sql, mappedRowsResult, pagination) => {
    it('returns object with custom schema sql if exists', () => {
      schema.overrideSelectClause = overrideSelectClause
      if (pagination) {
        params.pagination = pagination
      }
      target(schema, shouldSelectMany, params, pagination).sql.should.eql(sql)
    })

    it('returns object with custom mapResult mapped', () => {
      schema.overrideSelectClause = overrideSelectClause
      if (pagination) {
        params.pagination = pagination
      }
      target(schema, shouldSelectMany, params).mapResult({rows: [{aColumn: 8}]}).should.eql(mappedRowsResult)
    })
  }

  const assertOverrideSelectManyUsed = (shouldSelectMany, overrideSelectClause) => {
    assertOverrideSelectUsed(shouldSelectMany, overrideSelectClause, 'select many  order by bColumn', 'many mapped results')
  }

  const assertOverrideSelectOneUsed = (shouldSelectMany, overrideSelectClause) => {
    assertOverrideSelectUsed(shouldSelectMany, overrideSelectClause, 'select one  order by bColumn', 'one mapped result')
  }

  describe('selecting many', () => {
    describe('with custom schema select many', () => {
      assertOverrideSelectManyUsed(true, { selectMany: overrideSelectMany })
    })

    describe('with custom order by', () => {
      beforeEach(() => {
        params.orderBy = 'donkey desc'
      })
      assertOverrideSelectUsed(true, { selectMany: overrideSelectMany }, 'select many  order by donkey desc', 'many mapped results')
    })

    describe('falls back on select one if select many doesnt exist', () => {
      assertOverrideSelectOneUsed(true, { selectOne: overrideSelectOne })
    })

    describe('with pagination ignores pagination', () => {
      const expectSql = 'SELECT * FROM ( SELECT a.*, rownum r__ FROM (select many  order by bColumn ) a WHERE rownum < ((2 * 2) + 1 ) ) WHERE r__ >= (((2-1) * 2) + 1)'
      assertOverrideSelectUsed(true, { selectMany: overrideSelectMany }, expectSql, 'many mapped results', paginationArgs)
    })
  })

  describe('selecting one', () => {
    describe('uses select one override if exists', () => {
      assertOverrideSelectOneUsed(false, { selectOne: overrideSelectOne })
    })

    describe('with custom schema select many, defaults to select *', () => {
      assertOverrideSelectUsed(false, { selectMany: overrideSelectMany }, 'select * from aTable  order by bColumn', [ 'fromDb1' ])
    })

    describe('with pagination ignores pagination', () => {
      assertOverrideSelectUsed(false, { selectMany: overrideSelectMany }, 'select * from aTable  order by bColumn', [ 'fromDb1' ], paginationArgs)
    })
  })
})
