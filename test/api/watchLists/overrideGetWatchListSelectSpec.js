const proxyquire = require('../../proxyquire')

const SQL = 'Some SQL'

const watchList1 = { watchListId: 1, actionStepId: 1, actionStepDescription: 'Desc 1' }
const watchList2 = { watchListId: 1, actionStepId: 2, actionStepDescription: 'Desc 2' }

const watchList3 = { watchListId: 1, reasonId: 1, reasonDescription: 'Desc 1' }
const watchList4 = { watchListId: 1, reasonId: 2, reasonDescription: 'Desc 2' }

describe('get watch lists with action steps and reasons', () => {
  let target
  beforeEach(() => {
    target = proxyquire('api/watchLists/overrideGetWatchListSelect', {
      './overrideGetWatchListSelect.sql': SQL
    })
  })

  it('has an expected sql', () =>
    target.sql.should.eql(SQL))

  describe('maps db results', () => {
    it('should return an empty array if an empty array is passed in', () =>
      target.mapper([]).should.eql([]))

    it('maps actionSteps if there are more than one', () =>
    target.mapper([watchList1, watchList2]).should.eql([{
      watchListId: 1,
      actionSteps: [
        { actionStepId: 1, description: 'Desc 1' },
        { actionStepId: 2, description: 'Desc 2' }
      ],
      reasons: []
    }]))
    it('maps reasons if there are more than one', () =>
    target.mapper([watchList3, watchList4]).should.eql([{
      watchListId: 1,
      actionSteps: [],
      reasons: [
        { reasonId: 1, description: 'Desc 1' },
        { reasonId: 2, description: 'Desc 2' }
      ]
    }]))
    it('maps reasons if there are more than one and doesn\'t map actionSteps if there aren\t more than one', () =>
    target.mapper([watchList1, watchList3, watchList4]).should.eql([{
      watchListId: 1,
      actionSteps: [
        { actionStepId: 1, description: 'Desc 1' }
      ],
      reasons: [
        { reasonId: 1, description: 'Desc 1' },
        { reasonId: 2, description: 'Desc 2' }
      ]
    }]))
    it('maps actionSteps if there are more than one and doesn\'t map reasons if there aren\t more than one', () =>
    target.mapper([watchList1, watchList2, watchList3]).should.eql([{
      watchListId: 1,
      actionSteps: [
        { actionStepId: 1, description: 'Desc 1' },
        { actionStepId: 2, description: 'Desc 2' }
      ],
      reasons: [
        { reasonId: 1, description: 'Desc 1' }
      ]
    }]))
  })
})
