const target = require('../../api/getRequiredEnv')

const envName = 'STABLE_VALUE_API_TEMP_TESTING_ENV'

describe('get required env', () => {
  beforeEach(() => {
    process.env[envName] = 'some value'
  })

  it('get env when null', () => {
    try {
      target(envName + '2')
    } catch (err) {
      err.message.should.eql('missing environment variable STABLE_VALUE_API_TEMP_TESTING_ENV2')
    }
  })

  it('get env when set', () => {
    target(envName).should.eql('some value')
  })
})
