const dbExecuteOne = require('./dbExecuteOne')
const buildUpdateQuery = require('../queries/buildUpdateQuery')

module.exports = (connection, schema, bindParams, rowId) =>
  dbExecuteOne(connection, buildUpdateQuery(schema, bindParams, rowId))
