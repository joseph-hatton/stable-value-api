#!/bin/bash
servicePort=${PORT:-'3000'}
status=$(curl --write-out "%{http_code}\n" --silent --output /dev/null --head localhost:${servicePort}/api/v1/health/ping)

if [ $status -eq 500 ]
then
  echo "Healthcheck failed"
  exit 1
else
  echo "Healthcheck passed"
  exit 0
fi