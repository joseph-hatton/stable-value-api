const log = require('../../../log')
const db = require('../../db')

const wrapPromiseChain = async (connection, dbPromiseChainCreator) => {
  let result
  try {
    result = await dbPromiseChainCreator(connection)
  } catch (error) {
    log.error(error)
    throw error
  } finally {
    connection.close()
  }
  return result
}

module.exports = async (dbPromiseChainCreator) => {
  const connection = await db()
  return await wrapPromiseChain(connection, dbPromiseChainCreator)
}
