const proxyquire = require('../../proxyquire')
const {schema} = require('../../data/testSchema')
const defaultParams = {a: true}
const insertResult = {rowsAffected: 1}
const user = 'THE USER'
const body = {b: 'from request body'}
const VALIDATE_THEN_CALL_API_RESULT = 'VALIDATE_THEN_CALL_API_RESULT'

describe('create api', () => {
  const companies = [{ id: 'aId', name: 'a' }, { id: 'bId', name: 'b' }]
  const request = { headers: 'abc' }

  let response, target, next, getUserId, responseStatusSend, responseJson, insertInto, validateThenCallApi

  beforeEach(() => {
    responseStatusSend = sinon.spy()
    responseJson = sinon.spy()
    response = {
      json: responseJson,
      status: sinon.stub().returns({ send: responseStatusSend, json: responseJson })
    }
    next = sinon.spy()
    getUserId = sinon.stub()

    const query = sinon.stub().withArgs({sql: `select * from ${schema.table} order by ${schema.orderBy}`}).resolves(companies)
    insertInto = sinon.stub().resolves(insertResult)
    const buildDefaultParams = sinon.stub().returns(defaultParams)
    validateThenCallApi = sinon.stub().returns(VALIDATE_THEN_CALL_API_RESULT)
    target = proxyquire('api/common/createApi', {
      '../getUserId': getUserId,
      '../db/insertIntoDb': insertInto,
      './buildDefaultParams': buildDefaultParams,
      './validateThenCallApi': validateThenCallApi
    })
  })

  describe('with valid user', () => {
    beforeEach(() => {
      getUserId.withArgs(request.headers).returns(user)
      request.body = body
    })

    it('returns validateThenCallApiResult', async () => {
      const result = await target(schema)(request, response, next)

      result.should.eql(VALIDATE_THEN_CALL_API_RESULT)
    })

    it('validateThenCallApiResult should be called with schema', async () => {
      await target(schema)(request, response, next)

      validateThenCallApi.lastCall.args[0].should.eql(schema)
    })

    it('validateThenCallApiResult should be called with body', async () => {
      await target(schema)(request, response, next)

      validateThenCallApi.lastCall.args[1].should.eql(body)
    })

    it('validateThenCallApiResult should be called with empty required fields', async () => {
      await target(schema)(request, response, next)

      validateThenCallApi.lastCall.args[2].should.eql([])
    })

    it('validateThenCallApiResult should be called with res', async () => {
      await target(schema)(request, response, next)

      validateThenCallApi.lastCall.args[3].should.eql(response)
    })

    it('validateThenCallApiResult should be called with next', async () => {
      await target(schema)(request, response, next)

      validateThenCallApi.lastCall.args[4].should.eql(next)
    })

    it('validateThenCallApiResult should be called with userId in options', async () => {
      await target(schema)(request, response, next)

      validateThenCallApi.lastCall.args[6].should.eql({userId: user})
    })

    it('sends expected data into insert into', async () => {
      await target(schema)(request, response, next)
      await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user})

      insertInto.should.have.been.calledWithExactly(schema, defaultParams, user)
    })

    it('sends insert results to response', async () => {
      await target(schema)(request, response, next)
      await validateThenCallApi.lastCall.args[5](schema, body, response, next, {userId: user})

      response.json.should.have.been.calledWithExactly(insertResult)
    })
  })
})
