const moment = require('moment')
const log = require('../../log')
const schema = require('./monthlyCyclesSchema')
const getLatestCycle = require('./getLatestCycle')

module.exports = async (dateString) => {
  log.info({domain: schema.tableShort, date: dateString}, 'is future month')
  const dateAsMoment = moment(dateString).utc()
  const latestCycle = await getLatestCycle()
  const latestCycleStart = moment(latestCycle.startDate).utc()
  const isFutureMonth = dateAsMoment.isAfter(latestCycleStart) &&
    (dateAsMoment.get('year') !== latestCycleStart.get('year') ||
      dateAsMoment.get('month') !== latestCycleStart.get('month'))

  log.info({domain: schema.tableShort, date: dateString, isFutureMonth}, 'is future month result')
  return isFutureMonth
}
