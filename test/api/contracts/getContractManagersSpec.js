/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contractEditHistories/contractEditHistorySchema')

describe('Get Contract Managers', () => {
  let target, dbExecuteOne, dbExecutePromise, connection

  beforeEach(() => {
    dbExecutePromise = sinon.stub()
    dbExecuteOne = sinon.stub()
    connection = sinon.stub()

    target = proxyquire('api/contracts/getContractManagers', {
      '../db/executors/dbExecutePromise': dbExecutePromise,
      '../db/executors/dbExecuteOne': dbExecuteOne
    })
  })

  it('loads all contract managers', async () => {
    await target()

    const callbackToExecuteQuery = dbExecutePromise.getCall(0).args[0]
    callbackToExecuteQuery(connection)

    const actualConnectionForQuery = dbExecuteOne.getCall(0).args[0]
    actualConnectionForQuery.should.eql(connection)

    const dbRequest = dbExecuteOne.getCall(0).args[1]
    dbRequest.sql.should.eql(`SELECT contract_id, manager_id, effective_date, company.name FROM ${schema.table} INNER JOIN sch_stbv.COMPANY ON manager_id = company.COMPANY_ID WHERE manager_id is not null`)
    dbRequest.executeOptions.should.eql({maxRows: 1000})
    dbRequest.bindParams.should.eql({})

    const {mapResult: getContractManagers} = dbRequest

    const dbResult = {
      rows: [
        {
          CONTRACT_ID: 500,
          MANAGER_ID: 501,
          EFFECTIVE_DATE: new Date('11-25-2016'),
          NAME: 'name A'
        },
        {
          CONTRACT_ID: 500,
          MANAGER_ID: 502,
          EFFECTIVE_DATE: new Date('03-25-2017'),
          NAME: 'name B'
        },
        {
          CONTRACT_ID: 600,
          MANAGER_ID: 601,
          EFFECTIVE_DATE: new Date('04-25-2017'),
          NAME: 'name C'
        }
      ]
    }

    const contractManagers = getContractManagers(dbResult)

    contractManagers.should.eql({
      500: {
        managerId: 502,
        name: 'name B'
      },
      600: {
        managerId: 601,
        name: 'name C'
      }
    })
  })
})
