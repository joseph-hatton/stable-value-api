const log = require('../../log')
const getUserId = require('../getUserId')
const validateThenCallApi = require('../common/validateThenCallApi')
const schema = require('./riskScorecardsSchema')
const flattenRiskScorecardForInsertOrUpdateParam = require('./flattenRiskScorecardForInsertOrUpdateParam')
const getLookupsAndAdjustRiskScorecards = require('./getLookupsAndAdjustRiskScorecards')

const calculateATemporaryAdjustedRiskScorecard = async (schema, body, res, next, {userId, params}) => {
  const flattened = flattenRiskScorecardForInsertOrUpdateParam(body, true)
  const adjustedScorecards = await getLookupsAndAdjustRiskScorecards([flattened])
  log.info({domain: schema.tableShort, adjustedScorecards}, 'temporarily adjusted scorecards')
  return res.status(200).json(Object.assign({contractType: body.contractType}, adjustedScorecards[0]))
}

module.exports = async ({ query, params, headers, body }, res, next) => {
  log.info({domain: schema.tableShort, id: params.id}, 'calculate a temporary risk scorecard api')
  const userId = getUserId(headers)
  return await validateThenCallApi(schema, body, schema.required.update, res, next, calculateATemporaryAdjustedRiskScorecard, {userId, params})
}
