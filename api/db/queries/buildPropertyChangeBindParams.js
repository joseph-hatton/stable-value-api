module.exports = (schema, changeId, name, oldValue = null, newValue = null) => {
  return {
    changeId,
    name,
    oldValue,
    newValue,
    oldId: null,
    newId: null
  }
}
