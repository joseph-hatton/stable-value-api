const proxyquire = require('../../proxyquire')
const schema = require('../../../api/monthlyCycles/monthlyCyclesSchema')

const passedInDateString = '2017-11-13T17:09:29.705Z'

describe('is current open or future month cycle', () => {
  let target, getLatestCycle
  beforeEach(() => {
    getLatestCycle = sinon.stub()
    target = proxyquire('api/monthlyCycles/isCurrentOpenOrFutureMonth', {
      './getLatestCycle': getLatestCycle
    })
  })

  it('is open', async () => {
    getLatestCycle.resolves({startDate: new Date('2017-10-01T00:00:00.000Z')})
    const result = await target(passedInDateString)
    result.should.have.been.eql(true)
  })

  it('is not open', async () => {
    getLatestCycle.resolves({startDate: new Date('2017-12-01T00:00:00.000Z')})
    const result = await target(passedInDateString)
    result.should.have.been.eql(false)
  })
})
