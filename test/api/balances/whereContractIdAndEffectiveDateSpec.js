const target = require('../../../api/balances/whereContractIdAndEffectiveDate')

const DEFAULT_STATE = { bindParams: {id: {}}, whereClause: 'WHERE sch_stbv.balance.contract_id = :id' }

describe('where contract id and effective date query where clause', () => {
  it('handles contractId', () => {
    return target(12345).should.eql({ bindParams: {id: 12345}, whereClause: 'WHERE sch_stbv.balance.contract_id = :id' })
  })

  it('handles contractId and effective date params parameter', () => {
    const effectiveDate = '2017-12-19T18:32:45.800Z'
    return target(12345, {effectiveDate}).should.eql({ bindParams: { id: 12345, effectiveDate: new Date('2017-12-19T00:00:00.000Z') }, whereClause: 'WHERE sch_stbv.balance.contract_id = :id AND sch_stbv.balance.effective_date = :effectiveDate' })
  })
})
