const log = require('../../../log')
const deleteByIdFromDb = require('../../db/deleteByIdFromDb')
const schema = require('../../feeDistributions/feeDistributionsSchema')

module.exports = async (transactionId, feeDistribution, userId) => {
  if (feeDistribution) {
    log.info({domain: schema.tableShort, transactionId, feeDistribution, userId}, 'deleting fee distribution')
    await deleteByIdFromDb(schema, feeDistribution.feeDistributionId, userId)
  }
}
