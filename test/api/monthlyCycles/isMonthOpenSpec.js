const proxyquire = require('../../proxyquire')
const schema = require('../../../api/monthlyCycles/monthlyCyclesSchema')

describe('is month open', () => {
  let target, getLatestCycle
  beforeEach(() => {
    getLatestCycle = sinon.stub().resolves({startDate: new Date('2013-12-01T00:00:00.000Z'), closed: false})
    target = proxyquire('api/monthlyCycles/isMonthOpen', {
      './getLatestCycle': getLatestCycle
    })
  })

  describe('monthly cycle not closed', () => {
    it('same as start', async () => {
      const result = await target('2013-12-01T00:00:00.000Z')
      result.should.have.been.eql(true)
    })

    it('same month later day', async () => {
      const result = await target('2013-12-15T00:00:00.000Z')
      result.should.have.been.eql(true)
    })

    it('same month last day', async () => {
      const result = await target('2013-12-31T00:00:00.000Z')
      result.should.have.been.eql(true)
    })

    it('next year', async () => {
      const result = await target('2014-01-02T00:00:00.000Z')
      result.should.have.been.eql(false)
    })

    it('next year later', async () => {
      const result = await target('2014-01-20T00:00:00.000Z')
      result.should.have.been.eql(false)
    })

    it('next year latest', async () => {
      const result = await target('2014-02-20T00:00:00.000Z')
      result.should.have.been.eql(false)
    })
  })

  it('monthly cycle closed', async () => {
    getLatestCycle.resolves({startDate: new Date('2013-12-01T00:00:00.000Z'), closed: true})
    const result = await target('2014-02-20T00:00:00.000Z')
    result.should.have.been.eql(false)
  })
})
