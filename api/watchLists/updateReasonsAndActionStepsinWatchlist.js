const createError = require('http-errors')
const log = require('../../log')
const getUserId = require('../getUserId')
const updateDb = require('../db/updateDb')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const validateThenCallApi = require('../common/validateThenCallApi')
const watchListSchema = require('./watchListSchema')
const updateActionSteps = require('./updateActionSteps')
const updateReasons = require('./updateReasons')
const buildUpdateParams = require('../common/buildUpdateParams')
const SKIP_AUDIT = true

const attemptUpdateApi = async (schema, body, res, next, {userId}) => {
  if (!body) {
    throw createError.BadRequest('Body isn\'t set!')
  }

  const watchListId = body.watchListId
  const watchListUpdateParams = buildUpdateParams(watchListSchema.columns, userId, body)
  await updateDb(watchListSchema, watchListUpdateParams, watchListId, SKIP_AUDIT)

  await updateActionSteps(body, watchListId, userId, SKIP_AUDIT)
  await updateReasons(body, watchListId, userId, SKIP_AUDIT)

  const selectResponse = await selectByIdFromDb(watchListSchema, watchListId)
  return res.status(200).json(selectResponse)
}

module.exports = async ({ headers, body }, res, next) => {
  log.info({domain: watchListSchema.tableShort}, 'update watchlist api')
  const userId = getUserId(headers)
  return await validateThenCallApi(watchListSchema, body, watchListSchema.required.new, res, next, attemptUpdateApi, {userId})
}
