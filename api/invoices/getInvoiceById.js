const selectByIdFromDb = require('../db/selectByIdFromDb')
const schema = require('./invoicesSchema')

module.exports = (invoiceId) => selectByIdFromDb(schema, invoiceId)
