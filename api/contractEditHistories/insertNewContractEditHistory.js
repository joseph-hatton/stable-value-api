const insertIntoDb = require('../db/insertIntoDb')
const buildDefaultParams = require('../common/buildDefaultParams')
const schema = require('./contractEditHistorySchema')
const log = require('../../log')

const buildEditHistoryInsertParams = (contractInsertOrUpdateParams, editHistory, contractId) => {
  const defaultWho = contractInsertOrUpdateParams.modifiedId || contractInsertOrUpdateParams.createdId
  const defaultWhen = contractInsertOrUpdateParams.modifiedDate || contractInsertOrUpdateParams.createdDate
  return buildDefaultParams(schema,
    editHistory,
    {
      contractId: contractId,
      effectiveDate: editHistory.effectiveDate || contractInsertOrUpdateParams.effectiveDate || new Date(),
      createdId: contractInsertOrUpdateParams.createdId || defaultWho,
      createdDate: contractInsertOrUpdateParams.createdDate || defaultWhen,
      modifiedId: contractInsertOrUpdateParams.modifiedId || defaultWho,
      modifiedDate: contractInsertOrUpdateParams.modifiedDate || defaultWhen
    })
}

module.exports = (contractInsertOrUpdateParams, editHistory = {}, contractId, userId) => {
  log.info({domain: schema.tableShort, id: contractId, contractInsertOrUpdateParams, editHistory, userId}, 'inserting contract edit history args')

  const editHistoryInsertParams = buildEditHistoryInsertParams(contractInsertOrUpdateParams, editHistory, contractId)

  log.info({domain: schema.tableShort, id: contractId, contractEditHistory: editHistoryInsertParams, userId}, 'inserting contract edit history')
  return insertIntoDb(schema, editHistoryInsertParams, userId)
}
