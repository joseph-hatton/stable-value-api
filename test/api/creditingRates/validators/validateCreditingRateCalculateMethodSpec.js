const target = require('../../../../api/creditingRates/validators/validateCreditingRateCalculateMethod')

describe('crediting rate calculate method validator', () => {
  const validCases = [
    'COMPOUND_AMORTIZATION_METHOD',
    'BOOK_VALUE_DOLLAR_DURATION',
    'MARKET_VALUE_DOLLAR_DURATION',
    'IRR'
  ]

  describe('test valid crediting rate calculate methods', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Crediting Rate Calculation Method')
   })
})
