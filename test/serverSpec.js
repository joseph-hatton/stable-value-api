/* eslint-disable no-unused-expressions */
const util = require('util')
const proxyquire = require('./proxyquire')
const mockResponse = require('./mockResponse')
const jsonEndpoint = require('../api/swagger/jsonEndpoint')

describe('server', () => {
  let config, express, app, search, swaggerExpress, swagger, log, bodyParser, authorizationMiddleware

  beforeEach(done => {
    app = {
      get: sinon.stub(),
      use: sinon.stub(),
      put: sinon.stub(),
      post: sinon.stub(),
      delete: sinon.stub(),
      listen: sinon.stub()
    }
    search = sinon.stub()
    express = sinon.stub().returns(app)
    swaggerExpress = {
      create: sinon.stub()
    }
    bodyParser = {
      json: sinon.stub().returns('json'),
      raw: sinon.stub().returns('raw')
    }
    swagger = {register: sinon.stub()}

    log = sinon.stub()
    log.info = sinon.stub()
    log.error = sinon.stub()
    config = {
      initialize: sinon.stub().resolves(),
      port: 3000
    }

    authorizationMiddleware = sinon.stub()

    proxyquire('server', {
      express,
      'swagger-express-mw': swaggerExpress,
      'body-parser': bodyParser,
      './api/config': config,
      './log': log,
      './api/search': search,
      './api/auth/authorizationMiddleware': authorizationMiddleware
    })
    setTimeout(done, 1)
  })

  const apply = error => {
    swaggerExpress.create.lastCall.args[1](error, swagger)
  }

  describe('errorHandler', () => {
    let errorHandler, res, next

    beforeEach(() => {
      res = mockResponse()
      next = sinon.stub()
      apply()
      errorHandler = app.use.lastCall.args[0]
    })

    const triggerError = stack => {
      const error = new Error()
      error.stack = stack
      errorHandler(error, {}, res, next)
      return error
    }

    describe('on error', () => {
      it('logs stack when present', () => {
        const stack = '123'
        triggerError(stack)

        log.error.should.have.been.calledWithExactly(stack)
      })

      it('sends json', () => {
        const error = triggerError()

        res.json.should.have.been.calledWithExactly(error)
      })

      it('does not log when no stack exists', () => {
        triggerError()

        log.error.should.not.have.been.called
      })
    })

    it('invokes next if no error', () => {
      errorHandler(undefined, {}, res, next)

      next.should.have.been.calledWithExactly()
    })
  })

  it('uses raw and json body parsers', () => {
    apply()
    app.use.should.have.been.calledWithExactly('/api', 'json')
    app.use.should.have.been.calledWithExactly('/api', 'raw')
  })

  it('serves swagger json', () => {
    apply()
    app.get.should.have.been.calledWithExactly('/api/swagger.json', jsonEndpoint)
  })

  it('registers app', () => {
    apply()

    swagger.register.should.have.been.calledWithExactly(app)
  })

  it('listens and logs', () => {
    apply()
    app.listen.should.have.been.calledWith(3000)
    app.listen.lastCall.args[1]()

    log.info.should.have.been.calledWithExactly('running: http://localhost:3000')
  })

  describe('on startup error', () => {
    let error

    beforeEach(() => {
      error = new Error()
      error.validationErrors = ['error 1', 'error 2']
      apply(error)
    })

    it('logs and quits', () => {
      log.error.should.have.been.calledWithExactly('failed to initialize swagger', util.inspect(error, {depth: null, color: true}))
      app.listen.should.not.have.been.called
    })
  })
})
