const _ = require('lodash')
const getUserGroups = require('./getUserGroups')
const ENTITLEMENTS = require('./entitlements')

module.exports = async (user) => {
  const {groups} = await getUserGroups(user)
  return _.transform(ENTITLEMENTS, (result, methods, url) => {
    result[url] = _.transform(methods, (res, method, key) => {
      res[key] = _.intersection(method, groups).length > 0
    }, {})
  }, {})
}
