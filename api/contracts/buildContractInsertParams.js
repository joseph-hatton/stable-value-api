const buildDefaultParams = require('../common/buildDefaultParams')
const schema = require('./contractSchema')

module.exports = (userId, body, contractNumber) =>
  buildDefaultParams(schema,
  body,
  {
    createdId: userId,
    modifiedId: userId,
    contractNumber: contractNumber
  })
