const schema = require('../../../api/contracts/contractSchema')
const target = require('../../../api/contracts/buildContractInsertParams')

const USER_ID = 'me'
const CONTRACT_NUMBER = 559

const contractTypeAndShortPlanName = {contractType: 'POOLED_PLAN', shortPlanName: 'a short plan name'}

const nonSequenceSchemaColumnNames = () => schema.columns.filter(column => !column.sequence).map(column => column.name)

const filterFieldsIntoEntries = (targetResponse, filter) => Object.keys(targetResponse).filter(filter).map((key) => [key, targetResponse[key]])
const toNullFields = (targetResponse) => filterFieldsIntoEntries(targetResponse, (key) => targetResponse[key] === null)
const toNonNullFields = (targetResponse) => filterFieldsIntoEntries(targetResponse, (key) => targetResponse[key] !== null)
const mapKey = (entry) => entry[0]

const testAnInsertParam = (description, body, nonNullFields) => {
  const targetResponse = target(USER_ID, body, CONTRACT_NUMBER)
  const nonNullFieldEntries = Object.keys(nonNullFields).map((key) => [key, targetResponse[key]])
  describe(description, () => {
    it('has expected keys in order', () => Object.keys(targetResponse).should.eql(nonSequenceSchemaColumnNames()))

    it(`has ${schema.columns.length - nonNullFieldEntries.length - 1} null fields`, () => toNullFields(targetResponse).length.should.eql(schema.columns.length - nonNullFieldEntries.length - 1))

    it(`has ${nonNullFieldEntries.length} nonnull fields`, () => toNonNullFields(targetResponse).map(mapKey).should.eql(nonNullFieldEntries.map(mapKey)))

    nonNullFieldEntries.forEach((nonNullField) => {
      it(`has ${nonNullField[0]}`, () => {
        (typeof nonNullField[1] === 'function')
          ? nonNullField[1](targetResponse[nonNullField[0]])
          : targetResponse[nonNullField[0]].should.eql(nonNullField[1])
      })
    })
  })
}

const validateANewDate = (actualValue) => actualValue.should.be.an.instanceOf(Date) && (new Date().getTime() - actualValue.getTime()).should.be.lessThan(200)

const defaultInsertParams = {
  contractNumber: 9999,
  contractType: 'CHANGES',
  statedMaturityDate: new Date('2999-01-01T00:00:00.000Z'),
  status: 'PENDING',
  minimumNetCreditingRate: 0,
  netOfThirdPartyFees: 'N',
  creditingRateManagerFee: 'N',
  creditingRateAdvisorFee: 'N',
  version: 0,
  issueCompany: 'RGA',
  createdDate: validateANewDate,
  createdId: 'CHANGES',
  modifiedDate: validateANewDate,
  modifiedId: 'CHANGES',
  feeDailyRateFormula: 'DAILY_FEE',
  brokerageEquityWashRate: 0,
  moneyMarketEquityWashRate: 0,
  otherEquityWashRate: 0,
  replenishmentMinimum: 0,
  replenishmentMaximum: 0,
  plan401ARate: 0,
  plan401KRate: 0,
  plan403BRate: 0,
  plan457Rate: 0,
  plan529Rate: 0,
  shortPlanName: 'CHANGES',
  plan501CRate: 0,
  planTaftRate: 0,
  calendarTypeBenefitDays: 'CALENDAR',
  calendarTypePaymentDays: 'CALENDAR',
  calendarTypeTerminationDays: 'CALENDAR',
  calendarTypeMinimumDays: 'CALENDAR'
}
const extendDefaultInsertParams = (extension) => {
  const paramsPreSort = Object.assign({}, defaultInsertParams, extension, {
    contractNumber: 559,
    createdId: 'me',
    modifiedId: 'me'
  })
  const postSort = {}
  schema.columns.forEach((column) => {
    if (paramsPreSort[column.name] !== undefined) {
      postSort[column.name] = paramsPreSort[column.name]
    }
  })
  return postSort
}

describe('build contract insert params', () => {
  const expectedMinimalResponseEntries = extendDefaultInsertParams({
    contractType: 'POOLED_PLAN',
    shortPlanName: 'a short plan name'
  })

  testAnInsertParam('minimal request',
    contractTypeAndShortPlanName,
    expectedMinimalResponseEntries)

  testAnInsertParam('removes unwanted fields',
    Object.assign({unwanted1: true, unwanted2: 'i shouldnt be here'}, contractTypeAndShortPlanName),
    expectedMinimalResponseEntries)

  const firstTabUnregistered = {
    premiumRate: 2.3,
    premiumRateEffectiveDate: '',
    managerFeeRate: 3.0,
    managerFeeRateEffectiveDate: '',
    advisorFeeRate: 1.7,
    advisorFeeRateEffectiveDate: '',
    jurisdiction: 'CT',
    jurisdictionEffectiveDate: '',
    assignmentStateEffectiveDate: '',
    manager: 1381,
    managerEffectiveDate: '',
    planAnniversaryMonth: 'DECEMBER',
    planAnniversaryDay: '31',
    legalFormNameEffectiveDate: '',
    creditingRateCalculationMethodEffectiveDate: '',
    accountDurationLimitEffectiveDate: '',
    spRatingEffectiveDate: '',
    moodyRatingEffectiveDate: '',
    fitchRatingEffectiveDate: '',
    overallRatingEffectiveDate: '',
    fundActiveRateEffectiveDate: '',
    fundSeniorRateEffectiveDate: '',
    fundLevelUtilizationEffectiveDate: '',
    planLevelUtilizationEffectiveDate: '',
    plan401A: 0,
    plan401K: 0,
    plan403B: 0,
    plan457: 0,
    plan501C: 0,
    plan529: 0,
    planTaft: 0
  }

  const firstTabFilledBody = {
    contractType: 'SINGLE_PLAN',
    shortPlanName: 'another short plan name',
    effectiveDate: '2017-10-25T00:00:00.000Z',
    initialCoveredBookValue: 2999999.00,
    initialCoveredMarketValue: 2999999.00,
    status: 'PENDING',
    initialCreditingRate: 1.230,
    cashBufferTargetLevel: '2.30',
    owner: 'All Owners',
    plans: 'All Plans',
    stableFundValue: 'My Fund',
    custodyAccounts: 'Cust-AC1, Cust-AC2',
    statedMaturityDate: '2999-01-01T00:00:00.000Z',
    premiumPayor: 'ACCOUNT',
    managerFeePayor: 'ACCOUNT',
    issueCompany: 'RGA',
    assignmentState: 'CT',
    advisorFeePayor: 'PLAN',
    calendarTypeBenefitDays: 'CALENDAR',
    calendarTypeMinimumDays: 'CALENDAR',
    feeDailyRateFormula: 'DAILY_FEE',
    replenishmentMinimum: 0,
    replenishmentMaximum: 0,
    minimumNetCreditingRate: 0.000,
    calendarTypeTerminationDays: 'CALENDAR',
    calendarTypePaymentDays: 'CALENDAR',
    brokerageEquityWashRate: 0,
    moneyMarketEquityWashRate: 0,
    otherEquityWashRate: 0,
    planAnniversary: 'DECEMBER 31'
  }

  const firstTabFilledInsertParams = extendDefaultInsertParams(firstTabFilledBody)

  testAnInsertParam('first tab filled', Object.assign({}, firstTabFilledBody, firstTabUnregistered), firstTabFilledInsertParams)
})
