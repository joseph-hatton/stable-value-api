const {dbSchemaName} = require('../config')
const {createdDate, createdId, modifiedDate, modifiedId, version} = require('../common/auditColumns')
const {beginningOfDayDateConverter, booleanConverter, dateConverter, numberConverter, percentConverter, bpsConverter} = require('../db/converters')
const creditingRateStatusesValidator = require('./validators/validateCreditingRateStatus')
const creditingRateCalculationMethodsValidator = require('./validators/validateCreditingRateCalculateMethod')
const creditingRateTypesValidator = require('./validators/validateCreditingRateType')

const table = 'crediting_rate'
const tableId = 'crediting_rate_id'
const sequence = `${dbSchemaName}.crediting_rate_seq.NEXTVAL`

const falseDefault = () => 'N'

// const adjustmentPercentConverter = {
//   toDb: (something) => (something) ? parseFloat((parseFloat(something) / 100).toFixed(4)) : something,
//   fromDb: (something) => (something) ? parseFloat((parseFloat(something) * 100).toFixed(4)) : something
// }

module.exports = {
  tableShort: table,
  table: `${dbSchemaName}.${table}`,
  tableId: tableId,
  sequence: sequence,
  orderBy: 'crediting_rate.reference_date desc',
  columns: [
    {name: 'creditingRateId', column: tableId, sequence: sequence, pk: true, immutable: true, converter: numberConverter},
    {name: 'contractId', column: 'contract_id', converter: numberConverter},
    {name: 'annualEffectiveYield', column: 'annual_effective_yield', audit: true, converter: percentConverter},
    {name: 'duration', audit: true, converter: numberConverter},
    {name: 'grossRate', column: 'gross_rate', audit: true, converter: percentConverter},
    {name: 'rate', audit: true, converter: percentConverter},
    {name: 'adjustment', audit: true, converter: percentConverter},
    {name: 'referenceDate', column: 'reference_date', audit: true, converter: beginningOfDayDateConverter},
    {name: 'effectiveDate', column: 'effective_date', audit: true, converter: beginningOfDayDateConverter},
    {name: 'status', validators: [creditingRateStatusesValidator]},
    createdDate,
    version,
    {name: 'premiumRate', column: 'premium_rate', audit: true, converter: bpsConverter},
    {name: 'managerFeeRate', column: 'manager_fee_rate', audit: true, converter: bpsConverter},
    {name: 'advisorFeeRate', column: 'advisor_fee_rate', audit: true, converter: bpsConverter},
    {name: 'creditingRateRoundDecimals', column: 'crediting_rate_round_decimals', audit: true, converter: numberConverter},
    {name: 'creditingRateCalcMethod', column: 'crediting_rate_calc_method', audit: true, validators: [creditingRateCalculationMethodsValidator]},
    {name: 'minimumNetCreditingRate', column: 'minimum_net_crediting_rate', audit: true, converter: numberConverter},
    {name: 'creditingRateType', column: 'crediting_rate_type', audit: true, validators: [creditingRateTypesValidator]},
    {name: 'netOfThirdPartyFees', column: 'net_of_third_party_fees', audit: true, converter: booleanConverter},
    {name: 'creditingRateManagerFee', column: 'crediting_rate_manager_fee', audit: true, converter: booleanConverter},
    {name: 'creditingRateAdvisorFee', column: 'crediting_rate_advisor_fee', audit: true, converter: booleanConverter},
    createdId,
    modifiedDate,
    modifiedId,
    {name: 'calculatedBy', column: 'calculated_by', audit: true},
    {name: 'bookValue', column: 'book_value', audit: true, converter: numberConverter},
    {name: 'locked', audit: true, defaultValue: falseDefault, converter: booleanConverter},
    {name: 'marketValue', column: 'market_value', audit: true, converter: numberConverter},
    {name: 'creditingRateRateComments', column: 'crediting_rate_rate_comments', audit: true}
  ],
  required: {
    new: ['contractId', 'status', 'locked'],
    update: ['contractId', 'status', 'locked']
  },
  audit: {
    objectClass: 'Crediting Rate',
    buildDescription: (creditingRate) => (creditingRate.referenceDate) ? `Ref Date: ${creditingRate.referenceDate}` : null
  }
}
