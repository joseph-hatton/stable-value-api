const validateSchema = require('../validateSchema')
const {version} = require('../../../api/common/auditColumns')
const target = require('../../../api/actionSteps/actionStepSchema')
const {booleanConverter, numberConverter} = require('../../../api/db/converters')

describe('action step schema', () => {
  validateSchema(target, {
    tableShort: 'action_step',
    table: 'sch_stbv.action_step',
    tableId: 'action_step_id',
    sequence: 'sch_stbv.action_steps_seq.NEXTVAL',
    orderBy: 'action_step_id',
    columns: [
      {name: 'actionStepId', column: 'action_step_id', sequence: 'sch_stbv.action_steps_seq.NEXTVAL', pk: true, immutable: true, converter: numberConverter},
      version,
      {name: 'description'},
      {name: 'standardSw', column: 'standard_sw', converter: booleanConverter}
    ],
    required: {
      new: ['actionStepId', 'version', 'description', 'standardSw'],
      update: ['actionStepId', 'version', 'description', 'standardSw']
    }
  })
})
