/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const moment = require('moment')

const req = {query: {}}
const res = {status: () => ({json: (tbl) => tbl})}
const table1 = {rows: [{}]}
const table2 = {rows: [{}, {}]}
const sql = 'sql'

describe('getFees', () => {
  let target, template, sqlCompiled, simpleSelectFromDb, calculateFutureMonthlyFees
  beforeEach(() => {
    calculateFutureMonthlyFees = sinon.spy()
    simpleSelectFromDb = sinon.stub().resolves({results: []})
    sqlCompiled = sinon.stub()
    template = sinon.stub().returns(sqlCompiled)
    target = proxyquire('api/balances/monthly/getMonthlyFees', {
      '../../db/simpleSelectFromDb': simpleSelectFromDb,
      './calculateFutureMonthlyFees': calculateFutureMonthlyFees,
      './getMonthlyFees.sql': sql,
      'lodash/template': template
    })
  })
  it('should call getFeesQuery and simpleSelectFromDb', async () => {
    await target(req, res)

    simpleSelectFromDb.should.have.been.called
  })
  describe('the mapper for simpleSelectFromDb should map appropriately', () => {
    it('for one', async () => {
      await target(req, res)
      const mapper = simpleSelectFromDb.args[0][1]
      const result = mapper(table1)

      result.results.should.eql(table1.rows)
      result.total.should.eql(1)
    })
    it('for multiple', async () => {
      await target(req, res)
      const mapper = simpleSelectFromDb.args[0][1]
      const result = mapper(table2)

      result.results.should.eql(table2.rows)
      result.total.should.eql(2)
    })
  })
  describe('when the filter month is next month', () => {
    it('should pass in the right sql when calling template', async () => {
      await target(req, res)

      template.args[0][0].should.eql(sql)
    })
    it('should set dateStr to the last day of the prev month', async () => {
      simpleSelectFromDb.resolves({results: []})
      req.query.dateFilter = moment().add(1, 'month')

      await target(req, res)

      const expectedDateStr = moment().endOf('month').format('MM/DD/YYYY')
      sqlCompiled.args[0][0].start.should.eql(expectedDateStr)
      sqlCompiled.args[0][0].end.should.eql(expectedDateStr)
    })
    it('should call calculateFuture for each result', async () => {
      simpleSelectFromDb.resolves({results: [{}, {}]})
      req.query.dateFilter = moment().add(1, 'month')

      await target(req, res)

      calculateFutureMonthlyFees.should.have.beenCalledTwice
    })
    it('should not call calculateFuture if there aren\'t any results', async () => {
      simpleSelectFromDb.resolves({results: []})
      req.query.dateFilter = moment().add(1, 'month')

      await target(req, res)

      calculateFutureMonthlyFees.should.not.have.beenCalled
    })
  })
})
