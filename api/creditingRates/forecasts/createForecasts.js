const log = require('../../../log')
const selectFromDb = require('../../db/selectFromDb')
const insertIntoDb = require('../../db/insertIntoDb')
const buildDefaultParams = require('../../common/buildDefaultParams')
const schema = require('../creditingRatesSchema')

const buildInsertParams = (columns, userId, contractId, effectiveDate) =>
  buildDefaultParams({columns},
  {
    contractId,
    status: 'FORECAST',
    effectiveDate,
    locked: false,
    createdId: userId,
    modifiedId: userId
  })

module.exports = async (contractId, effectiveDatesAsMoments, userId) => {
  log.info({domain: schema.tableShort, id: contractId, effectiveDatesAsMoments, userId}, 'create crediting rate forecasts')

  const existingForecasts = await selectFromDb(schema, { whereClause: `WHERE ${schema.table}.contract_id = :contractId`, bindParams: {contractId} })
  for (let effectiveDate of effectiveDatesAsMoments) {
    const effectiveDateAsDate = effectiveDate.toDate()
    const matchingForecasts = existingForecasts.filter((existingForecast) => existingForecast.effectiveDate && existingForecast.effectiveDate.toISOString() === effectiveDateAsDate.toISOString())
    if (matchingForecasts.length === 0) {
      const insertParams = buildInsertParams(schema.columns, userId, contractId, effectiveDateAsDate)
      await insertIntoDb(schema, insertParams, userId, true, true)
    }
  }
}
