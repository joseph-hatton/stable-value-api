require('require-sql')
const template = require('lodash/template')
const log = require('../../../log')
const simpleSelectFromDb = require('../../db/simpleSelectFromDb')
const sql = require('./getMonthlyFees.sql')
const calculateFutureMonthlyFees = require('./calculateFutureMonthlyFees')
const moment = require('moment')

const DATE_FORMAT = 'MM/DD/YYYY'

module.exports = async (req, res, next) => {
  let {query: {dateFilter, filterOnDay}} = req
  log.info('get fees grouped by contract', dateFilter)

  const dateFilterNotSet = !dateFilter
  dateFilter = moment(dateFilter)

  const thisMonth = moment().endOf('month')
  const nextNextMonth = moment().add(2, 'months').startOf('month')
  const nextMonth = dateFilter.isBetween(thisMonth, nextNextMonth)

  let start, end
  if (nextMonth) { // future month
    start = end = dateFilter.clone().subtract(1, 'month').endOf('month').format(DATE_FORMAT)
  } else if (dateFilterNotSet || !filterOnDay) { // no filter passed in or filter on day isn't specified
    start = dateFilter.startOf('month').format(DATE_FORMAT)
    end = dateFilter.endOf('month').format(DATE_FORMAT)
  } else { // filter on day is specified
    start = end = dateFilter.format(DATE_FORMAT)
  }

  const sqlCompiled = template(sql)
  const query = sqlCompiled({start, end})

  const balancesTable = await simpleSelectFromDb(query, (tbl) => ({
    results: tbl.rows,
    total: tbl.rows.length
  }))

  if (nextMonth) {
    await Promise.all(
      balancesTable.results
      .map((balance) => calculateFutureMonthlyFees(balance, dateFilter, filterOnDay))
    )
  }

  balancesTable.results.forEach((balance) => {
    balance.accruedFee = balance.accruedFee * 1
    balance.accruedFeeMtd = balance.accruedFeeMtd * 1
    balance.adjustments = balance.adjustments * 1
    balance.balanceRate = balance.balanceRate * 1
    balance.beginningBookValue = balance.beginningBookValue * 1
    balance.beginningFee = balance.beginningFee * 1
    balance.contractId = balance.contractId * 1
    balance.deposits = balance.deposits * 1
    balance.endingBookValue = balance.endingBookValue * 1
    balance.endingFee = balance.endingFee * 1
    balance.feeAdjustments = balance.feeAdjustments * 1
    balance.feeReceived = balance.feeReceived * 1
    balance.interest = balance.interest * 1
    balance.latestPremiumRate = balance.latestPremiumRate * 1
    balance.withdrawals = balance.withdrawals * 1
  })

  return res.status(200).json(balancesTable)
}
