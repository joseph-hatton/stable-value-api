require('require-sql')
const sql = require('./getCompaniesWithContacts.sql')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')
const omit = require('lodash/omit')

const uniqueRowProperty = 'companyId'

const buildNewTopLevelObjectFromRow = (row) =>
  Object.assign({contacts: []}, omit(row, 'contactId', 'firstName', 'lastName', 'middleName'))

const appendChildToTopLevel = (topLevel, row) => {
  if (row.contactId != null) {
    topLevel.contacts.push({
      contactId: row.contactId,
      firstName: row.firstName,
      lastName: row.lastName,
      middleName: row.middleName
    })
  }
}

module.exports = {
  sql,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
