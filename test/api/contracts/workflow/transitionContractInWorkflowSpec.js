/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/contracts/contractSchema')

const userId = 'a user'
const contractId = 5

describe('transition contract in workflow', () => {
  let target, contract, updateContractStatus, validateCanTransition, targetArgs
  beforeEach(() => {
    contract = {contractId, status: 'ACTIVE'}
    updateContractStatus = sinon.spy()
    validateCanTransition = sinon.stub()
    targetArgs = {actionVerb: 'testingSomeAction', nextContractStatus: 'someContractStatus', validateCanTransition}
    target = proxyquire('api/contracts/workflow/transitionContractInWorkflow', {
      './updateContractStatus': updateContractStatus
    })
  })

  it('returns error when validate fails with string', async () => {
    validateCanTransition.resolves('Unable to approve contract.')

    const result = await target(targetArgs)({contract, userId})

    result.should.eql('Unable to approve contract.')
  })

  it('returns error when validate fails with array of errors', async () => {
    validateCanTransition.resolves(['Unable to approve contract.', 'stop'])

    const result = await target(targetArgs)({contract, userId})

    result.should.eql(['Unable to approve contract.', 'stop'])
  })

  it('updates contract status when validate transition returns empty array', async () => {
    validateCanTransition.resolves([])

    await target(targetArgs)({contract, userId})

    updateContractStatus.should.have.been.calledWithExactly(contractId, userId, targetArgs.nextContractStatus, true)
  })

  describe('when contract is found and no errors', () => {
    beforeEach(() => {
      validateCanTransition.resolves(null)
    })

    it('updates contract status when validate transition returns null', async () => {
      await target(targetArgs)({contract, userId})

      updateContractStatus.should.have.been.calledWithExactly(contractId, userId, targetArgs.nextContractStatus, true)
    })
  })
})
