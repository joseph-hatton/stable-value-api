require('require-sql')
const sql = require('./getManyContractsWithAssignmentState.sql')
const omit = require('lodash/omit')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')
const applySchemaConverters = require('../db/converters/applySchemaConverters')
const contractEditHistorySchema = require('../contractEditHistories/contractEditHistorySchema')

const uniqueRowProperty = 'contractId'

const buildNewTopLevelObjectFromRow = (row) => {
  const editHistoryFields = Object.keys(row).filter((row) => row.startsWith('ceh'))
  return Object.assign({editHistories: []}, omit(row, editHistoryFields))
}

const appendChildToTopLevel = (topLevel, row) => {
  if (row.cehId) {
    topLevel.editHistories.push(applySchemaConverters(contractEditHistorySchema, {
      contractEditHistoryId: row.cehId,
      effectiveDate: row.cehEffectiveDate,
      assignmentState: row.cehAssignmentState,
      managerId: row.cehManagerId
    }))
  }
}

module.exports = {
  sql,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
