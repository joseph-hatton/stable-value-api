const moment = require('moment')

const startTime = moment().format('YYYY-MM-DD h:mm:ss a Z')

module.exports = `
<html>
<head>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <title>Stable Value Companies API</title>
    <style>
        body {
            padding: 20px;
        }
        .startup-time {
            opacity: 0.8;
        }
    </style>
</head>
<body>
    <h3>Example REST API endpoints</h3>
    <ul>
        <li>
            <a href='/api/v1/companies?q=rule&initiative=PLAN&initiativeDate=201509'>companies</a>
        </li>
        <li>
            <a href='/api/v1/batch-labels?q=rule'>labels</a>
        </li>
    </ul>
    <hr/>
    <div class='startup-time'>started: ${startTime}</div>
</body>
</html>
`
