const log = require('../../log')
const schema = require('../feeDistributions/feeDistributionsSchema')
const selectFromDb = require('../db/selectFromDb')
const deleteByIdFromDb = require('../db/deleteByIdFromDb')
const getInvoiceById = require('../invoices/getInvoiceById')
const applyPayment = require('../invoices/applyPayment')
const deleteSql = `delete from ${schema.table} where transaction_id = :transactionId`

const applyPaymentAndDelete = async (feeDistribution, userId) => {
  if (feeDistribution.invoiceId) {
    const invoice = await getInvoiceById(feeDistribution.invoiceId)
    await applyPayment(invoice, feeDistribution.amount * -1, feeDistribution.adjustment * -1, userId)
  }

  log.warn({
    domain: schema.tableShort,
    feeDistributionId: feeDistribution.feeDistributionId,
    feeDistribution
  }, 'would have deleted fee distribution with different invoice id (non null)')

  await deleteByIdFromDb(schema, feeDistribution.feeDistributionId, userId)
}

module.exports = async (transactionId, userId) => {
  const feeDistributions = await selectFromDb(schema, {
    whereClause: 'WHERE transaction_id = :transactionId',
    bindParams: {
      transactionId: transactionId
    }
  })

  log.info({domain: schema.tableShort, transactionId: transactionId, feeDistributions}, 'Deleting Fee Distributions')

  for (const dbFeeDistribution of feeDistributions) {
    await applyPaymentAndDelete(dbFeeDistribution, userId)
  }
}
