const buildDefaultParams = require('../common/buildDefaultParams')
const schema = require('./actionStepWatchListSchema')

module.exports = (userId, body, watchListId, actionStepId) =>
  buildDefaultParams(schema,
  body,
  {
    watchListId: watchListId,
    actionStepId: actionStepId
  })
