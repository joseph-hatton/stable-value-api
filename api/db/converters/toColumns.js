const toColumn = (columns, key) => {
  const matchingColumn = columns.find((column) => column.name === key)
  return (matchingColumn) ? matchingColumn.column || matchingColumn.name : null
}

const toColumns = (columns, params) =>
  Object.keys(params)
  .map((key) => ({key, column: toColumn(columns, key), value: params[key]}))

module.exports = toColumns
