const log = require('../../log')
const selectByIdFromDb = require('../db/selectByIdFromDb')
const schema = require('./riskScorecardsSchema')
const getLookupsAndAdjustRiskScorecards = require('./getLookupsAndAdjustRiskScorecards')

module.exports = async (riskScorecardId) => {
    log.info({domain: schema.tableShort, id: riskScorecardId}, 'get risk scorecard by id')
    const riskScorecard = await selectByIdFromDb(schema, riskScorecardId)
    if (riskScorecard) {
      const adjustedScorecards = await getLookupsAndAdjustRiskScorecards([riskScorecard])
      log.info({domain: schema.tableShort, id: riskScorecardId, adjustedScorecards}, 'got adjusted scorecards')
      return adjustedScorecards[0]
    } else {
      return null
    }
  }
