
const target = require('../../../../api/db/converters/bpsConverter')

describe('bps converter', () => {
  it('to db', () => target.toDb(12.34).should.eql(0.001234))

  it('from db', () => target.fromDb(0.001234).should.eql(12.34))
})
