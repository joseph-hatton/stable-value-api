const validateIdAndText = require('../common/validateIdTextField')
const {contactTypes} = require('../referenceData.json')
module.exports = validateIdAndText(contactTypes, 'Invalid Contact Type')
