const createError = require('http-errors')
const agent = require('superagent')
const omit = require('lodash/omit')
const {oldAppBaseUrl} = require('../config')
const log = require('../../log')

const cyclesMapping = {
  daily: {
    url: `${oldAppBaseUrl}/api/dailyCycle/external`,
    requestParameters: [
      'contractNumber'
    ],
    bodyParameters: []
  },
  monthlyPreliminary: {
    url: `${oldAppBaseUrl}/api/monthlyCycle/external`,
    requestParameters: [],
    bodyParameters: [
      'statements',
      'invoices'
    ]
  },
  monthlyFinalize: {
    url: `${oldAppBaseUrl}/api/monthlyCycle/finalize`,
    requestParameters: [],
    bodyParameters: []
  }
}

module.exports = (cycleName) => {
  const {url, requestParameters, bodyParameters} = cyclesMapping[cycleName] || cyclesMapping.daily
  return async (req, res, next) => {
    const postParams = {}
    requestParameters.forEach(parameter => {
      const value = (req.query && req.query[parameter]) ? req.query[parameter] : null
      if (value) {
        postParams[parameter] = value
      }
    })
    bodyParameters.forEach(parameter => {
      const value = (req.body && req.body[parameter]) ? req.body[parameter] : null
      if (value) {
        postParams[parameter] = value
      }
    })
    log.info({domain: 'cycle', cycleName, url, postParams}, 'run cycle api')
    try {
      const result = await agent.post(url).query(postParams)
      if (result) {
        res.status(200).json(result.body)
      } else {
        throw createError.InternalServerError(`Unable to run cycle at ${url}`)
      }
    } catch (error) {
      log.error({domain: 'cycle', cycleName, url, postParams, error}, 'error running cycle api')
      throw createError.InternalServerError(`Error running cycle at ${url}`)
    }
  }
}
