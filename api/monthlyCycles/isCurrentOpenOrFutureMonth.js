const moment = require('moment')
const log = require('../../log')
const schema = require('./monthlyCyclesSchema')
const getLatestCycle = require('./getLatestCycle')

module.exports = async (dateString) => {
  log.info({domain: schema.tableShort, date: dateString}, 'is current future or open')
  const dateAsMoment = moment(dateString).utc()
  const latestCycle = await getLatestCycle()
  const latestCycleStart = moment(latestCycle.startDate).utc()

  const isCurrentOpenOrFuture = latestCycleStart.get('year') <= dateAsMoment.get('year') &&
    latestCycleStart.get('month') <= dateAsMoment.get('month')

  log.info({domain: schema.tableShort, date: dateString, isCurrentOpenOrFuture}, 'is current future or open result')
  return isCurrentOpenOrFuture
}
