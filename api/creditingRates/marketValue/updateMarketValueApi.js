const createError = require('http-errors')
const pick = require('lodash/pick')
const log = require('../../../log')
const updateDb = require('../../db/updateDb')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const getUserId = require('../../getUserId')
const buildUpdateParams = require('../../common/buildUpdateParams')
const validateThenCallApi = require('../../common/validateThenCallApi')
const contractSchema = require('../../contracts/contractSchema')
const checkForMarketValues = require('../../valuations/checkForMarketValues')
const getCreditingRatesByContract = require('../creditingRate/getCreditingRatesByContract')
const schema = require('../creditingRatesSchema')

const limitUpdateToMarketValueData = (body) => pick(body, ['contractId', 'referenceDate', 'marketValue', 'annualEffectiveYield', 'duration', 'effectiveDate', 'bookValue'])

const canUpdate = (contract, creditingRate) => {
  const isActiveContract = contract && contract.status === 'ACTIVE'
  const isForecastOrAwaitingCreditingRate = creditingRate && (creditingRate.status === 'AWAITING' || creditingRate.status === 'FORECAST')
  const updatable = isActiveContract && isForecastOrAwaitingCreditingRate
  log.info({domain: schema.tableShort, contract, creditingRate}, 'can we update market value')
  return updatable
}

const isReferenceDataAlreadyTaken = async (contractId, creditingRateId, referenceDate) => {
  const creditingRatesWithSameReferenceDate = await getCreditingRatesByContract(contractId, {referenceDate})
  log.info({domain: schema.tableShort, id: contractId, creditingRatesWithSameReferenceDate}, 'crediting rates with same reference date')
  const otherCreditingRates = (creditingRatesWithSameReferenceDate && creditingRatesWithSameReferenceDate.length > 0) ? creditingRatesWithSameReferenceDate : []
  return otherCreditingRates.filter((otherCreditingRate) => otherCreditingRate.creditingRateId !== creditingRateId).length > 0
}

const attemptUpdateApi = async (schema, body, res, next, {contractId, userId, creditingRateId}) => {
  const dbContract = await selectByIdFromDb(contractSchema, contractId)
  const dbCreditingRate = await selectByIdFromDb(schema, creditingRateId)
  if (canUpdate(dbContract, dbCreditingRate)) {
    const referenceDataAlreadyTaken = await isReferenceDataAlreadyTaken(contractId, creditingRateId, body.referenceDate)
    if (referenceDataAlreadyTaken) {
      throw createError.BadRequest('Reference date must be unique for each market value for the contract.')
    } else {
      const marketValueBody = limitUpdateToMarketValueData(body)
      const updateParams = buildUpdateParams(schema.columns, userId, marketValueBody)
      const result = await updateDb(schema, updateParams, creditingRateId, userId)
      if (result.marketValue !== dbCreditingRate.marketValue) {
        const marketValuesChecked = await checkForMarketValues(result.referenceDate)
        if (marketValuesChecked) {
          log.info({domain: schema.tableShort, id: contractId}, 'All Market Values are current. Sending email to Valuation')
        }
      }
      res.status(200).json(result)
    }
  } else {
    throw createError.BadRequest('Only Awaiting OR Forecast Crediting Rates on Active Contracts can be updated.')
  }
}

module.exports = async ({ params, headers, body }, res, next) => {
  log.info({domain: schema.tableShort, contractId: params.contractId, creditingRateId: params.id}, 'update market value api')
  const userId = getUserId(headers)
  return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptUpdateApi, {contractId: params.contractId, userId, creditingRateId: params.id})
}
