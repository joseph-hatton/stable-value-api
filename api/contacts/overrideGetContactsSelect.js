require('require-sql')
const overrideGetContactsSelect = require('./overrideGetContactsSelect.sql')
const omit = require('lodash/omit')
const buildCustomSelectMapper = require('../common/buildCustomSelectMapper')

const uniqueRowProperty = 'contactId'

const buildNewTopLevelObjectFromRow = (row) =>
  Object.assign({contracts: []}, omit(row, 'contractId', 'contractNumber', 'shortPlanName', 'status', 'contactContractId'))

const appendChildToTopLevel = (topLevel, row) => {
  if (row.contractId && row.contractNumber) {
    topLevel.contracts.push({contractId: row.contractId * 1, contractNumber: row.contractNumber, shortPlanName: row.shortPlanName, status: row.status, contactContractId: row.contactContractId * 1})
  }
}

module.exports = {
  sql: overrideGetContactsSelect,
  mapper: buildCustomSelectMapper(uniqueRowProperty, buildNewTopLevelObjectFromRow, appendChildToTopLevel)
}
