/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')
const schema = require('../../../../api/creditingRates/creditingRatesSchema')

const params = {params: true}
const headers = ['header']
const body = {effectiveDate: '2017-11-13T00:00:00.000Z'}
const userId = 'a user'

describe('calculate crediting rate build params or errors', () => {
  let target, determineEditHistoryDataForEffectiveDate, calculateGrossRate, calculateNetRate, dbCreditingRate, dbContract, body, balance, effectiveDateInformation

  beforeEach(() => {
    dbCreditingRate = {dbCreditingRate: true}
    dbContract = {contractId: 55, effectiveDate: new Date(), creditingRateType: 'GROSS'}
    body = {effectiveDate: (new Date()).toISOString()}
    balance = {beginningBookValue: 11111, endingBookValue: 99999}
    effectiveDateInformation = {
      minimumNetCreditingRate: 5,
      creditingRateCalcMethod: 'math',
      creditingRateRoundDecimals: 2,
      creditingRateType: 'more math',
      netOfThirdPartyFees: true,
      creditingRateManagerFee: true,
      mangerFeeRate: 3,
      creditingRateAdvisorFee: true,
      advisorFeeRate: 4,
      locked: false
    }
    calculateGrossRate = sinon.stub().returns(123.4567)
    calculateNetRate = sinon.stub().returns(23456.78)
    determineEditHistoryDataForEffectiveDate = sinon.stub().returns(effectiveDateInformation)
    target = proxyquire('api/creditingRates/creditingRate/calculateCreditingRateBuildParamsOrErrors', {
      '../../contractEditHistories/determineEditHistoryDataForEffectiveDate': determineEditHistoryDataForEffectiveDate,
      './calculateGrossRate': calculateGrossRate,
      './calculateNetRate': calculateNetRate
    })
  })

  it('returns empty errors', async () => {
    const {errors, params} = target(dbCreditingRate, dbContract, body, balance, userId)
    errors.should.have.been.eql([])
  })

  it('returns params', async () => {
    const {errors, params} = target(dbCreditingRate, dbContract, body, balance, userId)
    params.should.have.been.eql({
      dbCreditingRate: true,
      bookValue: 99999,
      creditingRateAdvisorFee: false,
      creditingRateManagerFee: false,
      creditingRateCalcMethod: 'math',
      creditingRateRoundDecimals: 2,
      creditingRateType: 'more math',
      netOfThirdPartyFees: true,
      minimumNetCreditingRate: 5,
      effectiveDate: body.effectiveDate,
      grossRate: 12345.67,
      rate: 23456.78,
      status: 'PENDING',
      calculatedBy: userId,
      locked: false
    })
  })

  it('returns errors with missing manager fee rate', async () => {
    dbContract.creditingRateType = 'NET'
    const {errors, params} = target(dbCreditingRate, dbContract, body, balance, userId)
    errors.should.have.been.eql(['Premium rate for contract was not found for effective date!', 'Manager fee rate for contract was not found for effective date!'])
  })
})
