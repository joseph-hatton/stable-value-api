/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')
const schema = require('../../../api/contracts/contractSchema')

const user = 'a user'
const contractId = 5
describe('update contract api', () => {
  let target, req, res, resJson, next, body, selectResponse, buildUpdateParams, validateThenCallApi, selectByIdFromDb, updateDb, getUserId, updateContractEditHistories, updateContractDerivatives, recalculateForecasts, validateContractPlanTypeRates

  beforeEach(() => {
    body = { status: 'ACTIVE' }
    selectResponse = Object.assign({selectResponse: true}, body)
    req = { params: {id: contractId}, headers: sinon.spy(), body: body }
    resJson = sinon.spy()
    res = { status: sinon.stub().returns({ json: resJson }) }
    next = sinon.spy()
    getUserId = sinon.stub().withArgs(req.headers).returns(user)
    buildUpdateParams = sinon.stub().returns('update params')
    validateThenCallApi = sinon.stub().resolves('validate api response')
    selectByIdFromDb = sinon.stub().withArgs(schema, contractId).resolves(selectResponse)
    updateDb = sinon.stub().resolves('update response')
    updateContractEditHistories = sinon.spy()
    updateContractDerivatives = sinon.spy()
    recalculateForecasts = sinon.spy()
    validateContractPlanTypeRates = sinon.stub()
    target = proxyquire('api/contracts/updateContractApi', {
      '../common/buildUpdateParams': buildUpdateParams,
      '../common/validateThenCallApi': validateThenCallApi,
      '../db/selectByIdFromDb': selectByIdFromDb,
      '../db/updateDb': updateDb,
      '../getUserId': getUserId,
      '../contractEditHistories/updateContractEditHistories': updateContractEditHistories,
      './derivativeTypes/updateContractDerivatives': updateContractDerivatives,
      '../creditingRates/forecasts/recalculateForecasts': recalculateForecasts,
      './validateContractPlanTypeRates': validateContractPlanTypeRates
    })
  })

  describe('validate then call api', () => {
    it('gets user id', async () => {
     await target(req, res, next)
     getUserId.callCount.should.equal(1)
    })

    it('calls validate then call api once', async () => {
      await target(req, res, next)
      validateThenCallApi.should.have.been.called
     })

     it('calls validate then call api with schema', async () => {
      await target(req, res, next)
      validateThenCallApi.getCall(0).args[0].should.eql(schema)
     })

     it('calls validate then call api with body', async () => {
      await target(req, res, next)
      validateThenCallApi.getCall(0).args[1].should.eql(body)
     })

     it('calls validate then call api with required update fields', async () => {
      await target(req, res, next)
      validateThenCallApi.getCall(0).args[2].should.eql(schema.required.update)
     })

     it('calls validate then call api with res', async () => {
      await target(req, res, next)
      validateThenCallApi.getCall(0).args[3].should.eql(res)
     })

     it('calls validate then call api with next', async () => {
      await target(req, res, next)
      validateThenCallApi.getCall(0).args[4].should.eql(next)
     })

     it('calls validate then call api with api params', async () => {
      await target(req, res, next)
      validateThenCallApi.getCall(0).args[6].should.eql({userId: user, contractId})
     })
  })

  describe('calls attempt update api', () => {
    let attemptUpdateApi, apiParams
    beforeEach(async () => {
      apiParams = {userId: user, contractId}
      await target(req, res)
      attemptUpdateApi = validateThenCallApi.getCall(0).args[5]
    })

    it('throws error for pending contract with more than one edit history', async () => {
      try {
        await attemptUpdateApi(schema, { status: 'PENDING', editHistories: [1, 2] }, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('New contracts or contracts in pending state can only have one edit history.')
      }
    })

    it('throws error for when validate contract plan type rates fails', async () => {
      validateContractPlanTypeRates.returns('oops')
      try {
        await attemptUpdateApi(schema, body, res, next, apiParams)
      } catch (err) {
        err.message.should.eql('oops')
      }
    })

    it('updates db with active contract', async () => {
      await attemptUpdateApi(schema, body, res, next, apiParams)
      updateDb.should.have.been.calledWithExactly(schema, 'update params', contractId, user, false)
    })

    it('updates db with pending contract skips update', async () => {
      await attemptUpdateApi(schema, {status: 'PENDING'}, res, next, apiParams)
      updateDb.should.have.been.calledWithExactly(schema, 'update params', contractId, user, true)
    })

    it('updates contract edit histories', async () => {
      await attemptUpdateApi(schema, body, res, next, apiParams)
      updateContractEditHistories.should.have.been.calledWithExactly('update params', body, contractId)
    })

    it('updates contract derivatives', async () => {
      await attemptUpdateApi(schema, body, res, next, apiParams)
      updateContractDerivatives.should.have.been.calledWithExactly(body, contractId)
    })

    it('does not recalculate forecasts with no forecast field changes', async () => {
      await attemptUpdateApi(schema, body, res, next, apiParams)
      recalculateForecasts.should.have.not.been.called
    })

    it('recalculates forecasts on crediting rate month change', async () => {
      await attemptUpdateApi(schema, Object.assign({creditingRateMonth: 'FEBRUARY'}, body), res, next, apiParams)
      recalculateForecasts.should.have.been.calledWithExactly(selectResponse, user)
    })

    it('recalculates forecasts on crediting rate frequency change', async () => {
      await attemptUpdateApi(schema, Object.assign({creditingRateFrequency: 'QUARTERLY'}, body), res, next, apiParams)
      recalculateForecasts.should.have.been.calledWithExactly(selectResponse, user)
    })

    it('recalculates forecasts on crediting rate first reset change', async () => {
      await attemptUpdateApi(schema, Object.assign({creditingRateFirstReset: 'FEBRUARY'}, body), res, next, apiParams)
      recalculateForecasts.should.have.been.calledWithExactly(selectResponse, user)
    })

    it('response sent result of select by id', async () => {
      await attemptUpdateApi(schema, body, res, next, apiParams)
      resJson.should.have.been.calledWithExactly(selectResponse)
    })
  })
})
