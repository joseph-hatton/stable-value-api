const log = require('../../log')
const insertIntoDb = require('../db/insertIntoDb')
const getUserId = require('../getUserId')
const buildDefaultParams = require('../common/buildDefaultParams')
const validateThenCallApi = require('../common/validateThenCallApi')
const schema = require('./riskScorecardsSchema')
const getAdjustedRiskScorecardById = require('./getAdjustedRiskScorecardById')
const flattenRiskScorecardForInsertOrUpdateParam = require('./flattenRiskScorecardForInsertOrUpdateParam')

const buildInsertParams = (columns, userId, body) =>
  buildDefaultParams({columns}, body,
  {
    createdId: userId,
    modifiedId: userId
  })

const attemptInsertApi = async (schema, body, res, next, {userId}) => {
  const flattenedBody = flattenRiskScorecardForInsertOrUpdateParam(body)
  const insertParams = buildInsertParams(schema.columns, userId, flattenedBody)
  const result = await insertIntoDb(schema, insertParams, userId)
  const adjustedScoreCard = await getAdjustedRiskScorecardById(result.riskScorecardId)
  res.status(201).json(adjustedScoreCard)
}

module.exports = async ({ headers, body }, res, next) => {
    log.info({domain: schema.tableShort}, 'create scorecard api')
    const userId = getUserId(headers)
    return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptInsertApi, {userId})
  }
