const log = require('../../../log')
const convertRowResultFromDbValue = require('../converters/convertRowResultFromDbValue')

const defaultMaxRows = 1000

const getOverrideSelectAndMapper = (schema, shouldSelectMany) => {
  const hasOverride = (schema.overrideSelectClause && (schema.overrideSelectClause.selectOne || schema.overrideSelectClause.selectMany))
  return (hasOverride)
    ? (shouldSelectMany && schema.overrideSelectClause.selectMany) ? schema.overrideSelectClause.selectMany : schema.overrideSelectClause.selectOne
    : null
}

const buildSql = (schema, whereClause, orderBy, shouldSelectMany, overrideSelectAndMapper, {pageNumber, pageSize}) => {
  const orderByToUse = orderBy || schema.orderBy
  const querySql = (overrideSelectAndMapper)
    ? `${overrideSelectAndMapper.sql} ${whereClause} order by ${orderByToUse}`
    : `select * from ${schema.table} ${whereClause} order by ${orderByToUse}`

  log.info({overrideSelectAndMapper, shouldSelectMany, pageNumber, pageSize, querySql}, 'build sql')

  return (shouldSelectMany && (pageNumber || pageSize))
    ? `SELECT * FROM ( SELECT a.*, rownum r__ FROM (${querySql} ) a WHERE rownum < ((${pageNumber} * ${pageSize}) + 1 ) ) WHERE r__ >= (((${pageNumber}-1) * ${pageSize}) + 1)`
    : querySql
}

const mapRowsFromDbValue = (schema, overrideSelectAndMapper) =>
  ({ rows }) => {
    const mappedRows = rows.map((row) => convertRowResultFromDbValue(schema.columns, row))
    return (overrideSelectAndMapper && overrideSelectAndMapper.mapper) ? overrideSelectAndMapper.mapper(mappedRows) : mappedRows
  }

module.exports = (schema, shouldSelectMany, {whereClause = '', orderBy = null, bindParams = {}, executeOptions = { maxRows: defaultMaxRows }, pagination = {pageSize: null, pageNumber: null}}) => {
  const overrideSelectAndMapper = getOverrideSelectAndMapper(schema, shouldSelectMany)
  return {
    sql: buildSql(schema, whereClause, orderBy, shouldSelectMany, overrideSelectAndMapper, pagination),
    bindParams: bindParams,
    executeOptions: executeOptions,
    mapResult: mapRowsFromDbValue(schema, overrideSelectAndMapper)
  }
}
