const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const createApi = require('../common/createApi')
const getByIdApi = require('../common/getByIdApi')
const updateByIdApi = require('../common/updateByIdApi')
const deleteByIdApi = require('../common/deleteByIdApi')
const schema = require('../contracts/contacts/contactContractSchema')
const buildContractIdWhereClause = require('../contracts/buildContractIdWhereClause')
const updateContactContractApi = require('../contracts/contacts/updateContactContractApi')
const validateunassingBeforeDelete = require('../contracts/contacts/validateunassingBeforeDelete')

module.exports = createController({
  getMany: getManyApi(schema, buildContractIdWhereClause),
  getOne: getByIdApi(schema),
  create: createApi(schema),
  update: updateByIdApi(schema),
  delete: validateunassingBeforeDelete(schema),
  getValidationContactContract: updateContactContractApi(schema)
})
