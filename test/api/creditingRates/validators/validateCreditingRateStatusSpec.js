const target = require('../../../../api/creditingRates/validators/validateCreditingRateStatus')

describe('crediting rate status', () => {
  const validCases = [
    'FORECAST',
    'AWAITING',
    'PENDING',
    'APPROVED'
  ]

  describe('test valid crediting rate statuses', () => {
    validCases.forEach((validCase) => {
      it(`${validCase} is valid`, () => should.not.exist(target(validCase)))
    })
  })

  it('tests an invalid case', async () => {
    target('something invalid').should.eql('Invalid Crediting Rate Status')
   })
})
