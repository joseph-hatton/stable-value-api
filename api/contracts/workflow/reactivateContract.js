const transitionContractInWorkflow = require('./transitionContractInWorkflow')

const validateCanTransition = async ({contract, userId}) => {
  if (contract.status !== 'INACTIVE') {
    return 'Only INACTIVE Contracts may be reactivated.'
  }
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'reactivate',
  nextContractStatus: 'PENDING',
  validateCanTransition
})
