const createController = require('./createController')
const getManyApi = require('../common/getManyApi')
const schema = require('../monthlyCycles/monthlyCyclesSchema')

module.exports = createController({
  getMany: getManyApi(schema)
})
