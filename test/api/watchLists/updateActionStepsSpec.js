/* eslint-disable no-unused-expressions */
const proxyquire = require('../../proxyquire')

const WATCH_LIST_ID = 123
let body = { actionSteps: [] }

describe('update action steps for watch lists', () => {
  let target, deleteFromDbSpy, selectFromDbStub, insertIntoDbSpy
  beforeEach(() => {
    deleteFromDbSpy = sinon.spy()
    selectFromDbStub = sinon.stub()
    insertIntoDbSpy = sinon.stub().resolves({})
    target = proxyquire('api/watchLists/updateActionSteps', {
      '../db/deleteFromDb': deleteFromDbSpy,
      '../db/selectFromDb': selectFromDbStub,
      '../db/insertIntoDb': insertIntoDbSpy
    })
  })

  describe('deleteFromDbSpy', () => {
    it('should not be called when the db is empty and the incoming list is as well', async () => {
      selectFromDbStub.resolves([])

      await target(body, WATCH_LIST_ID, '')

      deleteFromDbSpy.should.not.have.been.called
      insertIntoDbSpy.should.not.have.been.called
    })
    it('should be called when the db is empty and the incoming list is not', async () => {
      selectFromDbStub.resolves([])
      body.actionSteps = [{ actionStepId: 123 }]

      await target(body, WATCH_LIST_ID, '')

      deleteFromDbSpy.should.have.been.called
      insertIntoDbSpy.should.have.been.called
    })
    it('should be called when the db is not empty and the incoming list is', async () => {
      selectFromDbStub.resolves([{ actionStepId: 123 }])
      body.actionSteps = []

      await target(body, WATCH_LIST_ID, '')

      deleteFromDbSpy.should.have.been.called
      insertIntoDbSpy.should.not.have.been.called
    })
    it('should not be called when the db is not empty and the incoming list is not empty as well', async () => {
      selectFromDbStub.resolves([{ actionStepId: 123 }])
      body.actionSteps = [{ actionStepId: 123 }]

      await target(body, WATCH_LIST_ID, '')

      deleteFromDbSpy.should.not.have.been.called
      insertIntoDbSpy.should.not.have.been.called
    })
  })
})
