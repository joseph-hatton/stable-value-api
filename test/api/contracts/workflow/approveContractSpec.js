/* eslint-disable no-unused-expressions */
const proxyquire = require('../../../proxyquire')

const userId = 'a user'
const contractId = 5

describe('approve contract api', () => {
  let target, transitionContractInWorkflow, calculateAndSaveCreditingRateForecast, addInitialCreditingRate, getContractEditHistories, updateContractEditHistory
  beforeEach(() => {
    calculateAndSaveCreditingRateForecast = sinon.stub()
    addInitialCreditingRate = sinon.stub()
    getContractEditHistories = sinon.stub().resolves([{contractEditHistoryId: 111}])
    updateContractEditHistory = sinon.stub()
    transitionContractInWorkflow = sinon.stub().returns('base returned')
    target = proxyquire('api/contracts/workflow/approveContract', {
      '../../creditingRates/forecasts/calculateAndSaveCreditingRateForecast': calculateAndSaveCreditingRateForecast,
      '../../creditingRates/creditingRate/addInitialCreditingRate': addInitialCreditingRate,
      '../../contractEditHistories/getContractEditHistories': getContractEditHistories,
      '../../contractEditHistories/updateContractEditHistory': updateContractEditHistory,
      './transitionContractInWorkflow': transitionContractInWorkflow
    })
  })

  it('returns the base result', () => {
    target.should.eql('base returned')
  })

  it('base should have been called once', () => {
    transitionContractInWorkflow.should.have.been.called
  })

  it('sets action verb', () => {
    transitionContractInWorkflow.getCall(0).args[0].actionVerb.should.eql('approve')
  })

  it('sets next contract status', () => {
    transitionContractInWorkflow.getCall(0).args[0].nextContractStatus.should.eql('ACTIVE')
  })

  it('sets a callBeforeChangingState', () => {
    should.exist(transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState)
  })

  describe('validate can transition', () => {
    let asyncValidate
    beforeEach(() => {
      asyncValidate = transitionContractInWorkflow.getCall(0).args[0].validateCanTransition
    })

    it('fails with PENDING contract', async () => {
      const result = await asyncValidate({contract: {status: 'PENDING'}})

      result.should.eql('Only SUBMITTED Contracts may be approved.')
    })

    it('fails with for user who last modified contract', async () => {
      const result = await asyncValidate({contract: {modifiedId: userId, status: 'SUBMITTED'}, userId})

      result.should.eql('A Contract cannot be approved by the last person who modified it.')
    })

    it('passes with submitted contract', async () => {
      const result = await asyncValidate({contract: {contractId, status: 'SUBMITTED', modifiedId: 'somebody Else'}, userId})

      should.not.exist(result)
    })
  })

  describe('callBeforeChangingState', () => {
    let contract, callBeforeChangingState
    beforeEach(() => {
      contract = {status: 'SUBMITTED', modifiedId: 'somebody'}
      callBeforeChangingState = transitionContractInWorkflow.getCall(0).args[0].callBeforeChangingState
    })

    it('calculates and saves crediting rate forecasts', async () => {
      await callBeforeChangingState({contract, userId})
      calculateAndSaveCreditingRateForecast.should.have.been.calledWithExactly(contract)
    })

    it('adds initial crediting rate', async () => {
      await callBeforeChangingState({contract, userId})
      addInitialCreditingRate.should.have.been.calledWithExactly(contract)
    })

    it('updates contract edit history', async () => {
      await callBeforeChangingState({contract, userId})
      updateContractEditHistory.should.have.been.calledWithExactly({effectiveDate: contract.effectiveDate}, 111, userId)
    })
  })
})
