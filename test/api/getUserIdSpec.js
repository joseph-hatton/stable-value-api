const getUserId = require('../../api/getUserId')

const {setLocalUserEnabled, localUser} = getUserId

describe('getUserId', () => {
  let headers

  beforeEach(() => {
    headers = {}
  })

  const apply = () => getUserId(headers)

  it('returns local user when enabled and no user is found in headers', () => {
    setLocalUserEnabled(true)

    apply().should.equal(localUser)
  })

  it('returns undefined when local user is not enabled and no user is found in headers', () => {
    setLocalUserEnabled(false)

    should.not.exist(apply())
  })

  it('returns user id from headers if present', () => {
    setLocalUserEnabled(true)
    const userId = 'userFromHeaders'
    headers['user-id'] = userId

    apply().should.equal(userId)
  })
})
