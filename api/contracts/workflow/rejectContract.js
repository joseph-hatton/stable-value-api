const transitionContractInWorkflow = require('./transitionContractInWorkflow')

const validateCanTransition = async ({contract, userId}) => {
  if (contract.status !== 'SUBMITTED') {
    return 'Only SUBMITTED Contracts may be rejected.'
  }
}

module.exports = transitionContractInWorkflow({
  actionVerb: 'reject',
  nextContractStatus: 'PENDING',
  validateCanTransition
})
