const log = require('../../log')
const selectFromDb = require('../db/selectFromDb')
const schema = require('./monthlyCyclesSchema')

module.exports = async () => {
  log.info({domain: schema.tableShort}, 'get latest monthly cycle')

  const results = await selectFromDb(schema, {executeOptions: { maxRows: 1 }})

  log.info({domain: schema.tableShort, results}, 'get latest monthly cycle')
  return results[0]
}
