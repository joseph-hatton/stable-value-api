const log = require('../../log')
const {ldap: {queryUrl, overrideAdEntitlements}, isPointingToProductionDatabase, isDeployed} = require('../config')
const agent = require('superagent')
const nonprodEntitlements = require('./nonprodEntitlements.json')

module.exports = async (user) => {
  user = user.toLowerCase()
  log.info({user, isPointingToProductionDatabase, overrideAdEntitlements, userNonProdOverride: Boolean(nonprodEntitlements[user]), isDeployed}, 'get user groups')
  if ((!isPointingToProductionDatabase && overrideAdEntitlements && nonprodEntitlements[user]) || !isDeployed) {
    log.info({user}, 'using alt permissions')
    return nonprodEntitlements[user] || {groups: []}
  }
  try {
    const request = await agent.post(`${queryUrl}/query`).set('Content-Type', 'application/json')
    .send({
      filter: `(sAMAccountName=${user})`,
      attributes: ['memberOf']
    })

    log.info({user}, 'received user groups')

    return {
      groups: JSON.parse(request.text)[0].memberOf.map((item) => /(CN=)(.*?),.*/.exec(item)[2].toLowerCase())
    }
  } catch (e) {
    log.info({e}, 'Error connecting to dapper or parsing dapper data')
    return {groups: []}
  }
}
