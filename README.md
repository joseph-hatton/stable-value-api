# Stable Value API

Microservice for:

+ Creating and updating companies

---

## Deployed URLs

* DEV
    * [ui](https://stable-value-dev.rgare.net)
    * [api](https://stable-value-api-dev.rgare.net)
* TEST
    * [ui](https://stable-value-test.rgare.net)
    * [api](https://stable-value-api-test.rgare.net)
* UAT
    * [ui](https://stable-value-uat.rgare.net)
    * [api](https://stable-value-api-uat.rgare.net)
* PROD

---

## Running Locally

* Install docker
* Log into RGA's docker registry
* set env variables for connection info:
```
export ORACLE_PASSWORD=****
export ORACLE_HOST=stldora20.rgare.net
export ORACLE_PORT=1534
export ORACLE_SERVICE_NAME=STLDRGA1

npm run install

npm run dev

```

---

## Build docker

```
docker build -t sv-api .
```

## Logs

[Nagios Log Server](https://monitor.rgare.net/nagioslogserver/)

### Example Log Queries

* specific container and log line
    * `container_name: "stable-value-dev_api*" AND doc.msg: "update market value api"`
* specific range up till now (ensure date picker at top includes at least this span (it overrules timestamp querying))
    * `container_name: "stable-value-dev_api*" AND doc.msg: "update market value api" AND @timestamp:[now-17h TO now]`
* specific relative range (ensure date picker at top includes at least this span (it overrules timestamp querying))
    * `container_name: "stable-value-dev_api*" AND doc.msg: "update market value api" AND @timestamp:[now-18h TO now-17h]`

---

## VCS

* [TFS Stable Value API](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/stable-value-api)
* [TFS Stable Value UI](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/stable-value-ui)
* [TFS Deployment Project](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/common-deployment)

---

## Monitoring

We are running our own grafana/prometheus/and other tools Docker Stack to monitor our nonprod (and hopefully soon prod) docker swarms.  

* NonProd
    * [Grafana](http://stldstbvldock01.rgare.net:3000)
        * [Docker Swarm Nodes Dashboard](http://stldstbvldock01.rgare.net:3000/dashboard/db/docker-swarm-nodes)
        * [Docker Swarm Services Dashboard](http://stldstbvldock01.rgare.net:3000/dashboard/db/docker-swarm-services)
        * [Host Data Dashboard](http://stldstbvldock01.rgare.net:3000/dashboard/db/host-data)
        * [Prometheus Dashboard](http://stldstbvldock01.rgare.net:3000/dashboard/db/docker-swarm-services)
    * [Prometheus DB](http://stldstbvldock01.rgare.net:9090/graph)
    * [Deploy this Stack via Jenkins](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/deploy%20non-prod%20monitoring/)
    * [TFS](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fmonitoring%2Fnonprod&version=GBmaster&_a=contents)
    * Alerts are configured as Docker Configs and are deployed via the Jenkins build.
        * [Alerts compose config file definition and usage](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fmonitoring%2Fnonprod%2Fmonitoring-compose.yml&version=GBmaster&_a=contents)
        * You can see `./prometheus/rules/swarm_node.rules` is one of the config files.  Edit these and redeploy to change what gets alerted on.

---

## CI

[Stable Value Jenkins Builds](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/)

* [build api](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/build%20api/)
* [build ui](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/build%20ui/)
* [deploy to dev](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/deploy%20to%20dev/)
* [test the dev api](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/test%20the%20dev%20api/)
* [deploy to test](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/deploy%20to%20test/)

---

## Code Quality

* [SonarQube](https://stlpsnrqube01.rgare.net/dashboard?id=StableValueAPI)

---

## Hosts

* **dev**, **test**, **uat**
    * stldstbvldock01.rgare.net
    * stldstbvldock02.rgare.net
    * stldstbvldock03.rgare.net
* **prod**
    * stlpstbvldock01.rgare.net
    * stlpstbvldock02.rgare.net
    * stlpstbvldock03.rgare.net
    
---

## Secret/Password Management - Vault

RGA has a vault secret store to manage application configuration and secrets.

All secrets for NON-PROD fast and StableValue is all put into StableValue_dev (since they all share the same deployment scripts)

---

## Testing deployed API

### Manually
1. Getting a token
    1. Go to the UI of the deployed environment
    2. Log into Secure Auth
    3. Append /__auth/current-user/ to the url
    4. Copy the session.token (this is your Secure Auth token)
    5. curl -H 'Authorization: Bearer YOUR_TOKEN' https://stable-value-api-dev.rgare.net/api/v1/companies

### Automated

Automated API testing is being done with a [Postman](https://www.getpostman.com/) Collection Runner.  These tests were written with version `5.5.0` of the app (not the chrome extension)

The collection has been exported from Postman into a json file at postman/stable-value-api-testing.postman_colleciton.json .

You can import this collection into your own Postman for updating.

You can also run this directly via a package.json script `npm run test:api`.

Jenkins build listed above.

#### Known Problems

A full run of the Postman suite requires the current month to also be the active month in stable value.  This might not be true if the database you are running against has not been reloaded from Prod recently.  

## Dev

This api uses swagger to enforce data types and is the entry point into seeing which apis are available.

* /api/swagger/swagger.yaml

The swagger.yaml references controllers in the /api/controllers directory by the `x-swagger-router-controller` tag and speficic operations with the `operationId` tag.


