const createError = require('http-errors')
const log = require('../../../log')
const insertIntoDb = require('../../db/insertIntoDb')
const selectByIdFromDb = require('../../db/selectByIdFromDb')
const getUserId = require('../../getUserId')
const buildDefaultParams = require('../../common/buildDefaultParams')
const validateThenCallApi = require('../../common/validateThenCallApi')
const contractSchema = require('../../contracts/contractSchema')
const getCreditingRatesByContract = require('../creditingRate/getCreditingRatesByContract')
const schema = require('../creditingRatesSchema')

const canAdd = (contract) => contract && contract.status === 'ACTIVE'

const buildInsertParams = (userId, body, contractId) =>
  buildDefaultParams({columns: schema.columns},
    body,
    {
      contractId,
      status: 'AWAITING',
      locked: false,
      createdId: userId,
      modifiedId: userId
    })

const attemptInsertApi = async (schema, body, res, next, {contractId, userId}) => {
  const dbContract = await selectByIdFromDb(contractSchema, contractId)

  if (canAdd(dbContract)) {
    const creditingRatesWithSameReferenceDate = await getCreditingRatesByContract(contractId, {referenceDate: body.referenceDate})
    log.info({creditingRatesWithSameReferenceDate}, 'did we get crediting rates?')
    if (creditingRatesWithSameReferenceDate && creditingRatesWithSameReferenceDate.length > 0) {
      throw createError.BadRequest('Reference date must be unique for each market value for the contract.')
    } else {
      const insertParams = buildInsertParams(userId, body, contractId)
      const result = await insertIntoDb(schema, insertParams)
      res.status(201).json(result)
    }
  } else {
    throw createError.BadRequest('Only Active Contracts can have Market Values added.')
  }
}

module.exports = async ({ params, headers, body }, res, next) => {
    log.info({domain: schema.tableShort}, 'create market value api')
    const userId = getUserId(headers)
    return await validateThenCallApi(schema, body, schema.required.new, res, next, attemptInsertApi, {contractId: params.contractId, userId})
  }
