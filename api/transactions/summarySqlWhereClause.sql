BALANCE.CONTRACT_ID = TRANSACTION.CONTRACT_ID 
AND CONTRACT.CONTRACT_ID = TRANSACTION.CONTRACT_ID
AND BALANCE.EFFECTIVE_DATE = TRANSACTION.EFFECTIVE_DATE 
AND (TRANSACTION.TRANSACTION_TYPE = 'DEPOSIT'
  OR TRANSACTION.TRANSACTION_TYPE = 'WITHDRAWAL'
  OR TRANSACTION.TRANSACTION_TYPE = 'ADJUSTMENT' 
  OR TRANSACTION.TRANSACTION_TYPE = 'FEE_RECEIPT'
  OR TRANSACTION.TRANSACTION_TYPE = 'FEE_ADJUSTMENT'
  OR TRANSACTION.TRANSACTION_TYPE = 'TERMINATION')