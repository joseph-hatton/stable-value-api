/* eslint-disable no-unused-expressions */
const cloneDeep = require('lodash/cloneDeep')
const proxyquire = require('../../proxyquire')
const testSchema = require('../../data/testSchema')
const THE_ID = 8675309
const USER_ID = 'someUser'
const CHANGE_ID = 4443

const DB_EXECUTE_PROMISE_RESULT = 'DB_EXECUTE_PROMISE_RESULT'
const DB_EXECUTE_ONE_RESULT = 'DB_EXECUTE_ONE_RESULT'
const BUILD_SELECT_QUERY_RESULT = 'BUILD_SELECT_QUERY_RESULT'

describe('select from db with pagination', () => {
  let target, schema, params, connection, dbExecutePromise, dbExecuteInOrder, buildSelectQuery, buildSelectCountQuery

  beforeEach(() => {
    params = {
      bColumn: 'bValue',
      cColumn: 'cValue'
    }
    dbExecutePromise = sinon.stub().resolves(DB_EXECUTE_PROMISE_RESULT)
    connection = sinon.spy()
    dbExecuteInOrder = sinon.stub().resolves([['one']])
    buildSelectQuery = sinon.stub().returns(BUILD_SELECT_QUERY_RESULT)
    buildSelectCountQuery = sinon.stub().returns('count query')
    target = proxyquire('api/db/selectFromDbWithPagination', {
      './executors/dbExecutePromise': dbExecutePromise,
      './executors/dbExecuteInOrder': dbExecuteInOrder,
      './queries/buildSelectQuery': buildSelectQuery,
      './queries/buildSelectCountQuery': buildSelectCountQuery
    })
  })

  const executeTarget = async (targetParams = params) => {
    schema = cloneDeep(testSchema.schema)
    await target(schema, targetParams)
    return await dbExecutePromise.getCall(0).args[0](connection)
  }

  describe('execute promise', () => {
    it('returns result', async () => {
      const result = await executeTarget()
      result.should.eql({
        total: 1,
        pageNumber: null,
        pageSize: null,
        results: ['one']
      })
    })
  })

  describe('execute non-paging', () => {
    it('build select query called with', async () => {
      await executeTarget()
      buildSelectQuery.should.have.been.calledWithExactly(schema, true, params)
    })

    it('execute in order called with', async () => {
      await executeTarget()
      dbExecuteInOrder.should.have.been.calledWithExactly(connection, [BUILD_SELECT_QUERY_RESULT])
    })

    it('returns result', async () => {
      const result = await executeTarget()
      result.should.eql({
        total: 1,
        pageNumber: null,
        pageSize: null,
        results: ['one']
      })
    })
  })

  describe('execute paging', () => {
    beforeEach(() => {
      dbExecuteInOrder.resolves([[{one: true}, {two: true}], [{count: 2}]])
      params = {
        bColumn: 'bValue',
        cColumn: 'cValue',
        pagination: {pageNumber: 1, pageSize: 2}
      }
    })

    it('build select query called with', async () => {
      await executeTarget(params)
      buildSelectQuery.should.have.been.calledWithExactly(schema, true, params)
    })

    it('execute in order called with', async () => {
      await executeTarget(params)
      dbExecuteInOrder.should.have.been.calledWithExactly(connection, [BUILD_SELECT_QUERY_RESULT, 'count query'])
    })

    it('returns result', async () => {
      const result = await executeTarget(params)
      result.should.eql({
        total: 2,
        pageNumber: 1,
        pageSize: 2,
        results: [{one: true}, {two: true}]
      })
    })
  })
})
