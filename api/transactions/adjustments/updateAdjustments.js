const Decimal = require('decimal.js')
const log = require('../../../log')
const schema = require('../transactionSchema')
const createAndInsertFeeAdjustmentTransaction = require('./createAndInsertFeeAdjustmentTransaction')
const updateFeeReceiptTransactionWithAdjustmentTransactionId = require('./updateFeeReceiptTransactionWithAdjustmentTransactionId')
const updateExistingFeeAdjustmentTransaction = require('./updateExistingFeeAdjustmentTransaction')
const deleteExistingAdjustmentTransaction = require('./deleteExistingAdjustmentTransaction')

module.exports = async (dbTransaction, clientBody, userId) => {
  log.info({domain: schema.tableShort, transactionId: dbTransaction.transactionId, clientBody, userId}, 'updating adjustments')

  const totalAmount = clientBody.feeDistributions
  .map((feeDistribution) => feeDistribution.adjustment || 0)
  .reduce((total, current) => Decimal(total).add(current).toNumber(), 0)

  log.info({domain: schema.tableShort, transactionId: dbTransaction.adjustmentTransactionId, userId, totalAmount}, 'total amount of fee distribution adjustments')
  if (totalAmount !== 0) {
    if (!dbTransaction.adjustmentTransactionId) {
      const createAdjustmentResponse = await createAndInsertFeeAdjustmentTransaction(dbTransaction, totalAmount, userId)
      await updateFeeReceiptTransactionWithAdjustmentTransactionId(dbTransaction.transactionId, createAdjustmentResponse.transactionId, userId)
    } else {
      await updateExistingFeeAdjustmentTransaction(dbTransaction.adjustmentTransactionId, clientBody, totalAmount, userId)
    }
  } else {
    await deleteExistingAdjustmentTransaction(dbTransaction, userId)
  }
}
