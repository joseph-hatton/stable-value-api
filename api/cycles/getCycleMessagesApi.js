const createError = require('http-errors')
const agent = require('superagent')
const omit = require('lodash/omit')
const {oldAppBaseUrl} = require('../config')
const log = require('../../log')

module.exports = (dailyCycle = true) => {
  const cycleName = (dailyCycle) ? 'dailyCycle' : 'monthlyCycle'
  return async (req, res, next) => {
    const pageNumber = (req.query && req.query.pageNumber) ? req.query.pageNumber * 1 : 0
    const pageSize = (req.query && req.query.pageSize) ? req.query.pageSize * 1 : 200
    const cycleUrl = `${oldAppBaseUrl}/api/${cycleName}?maxRows=${pageSize}&offset=${pageNumber}&order=asc&sort=messageDate`
    log.info({domain: 'cycle', cycleName, cycleUrl, pageNumber, pageSize}, 'get cycle messages api')

    const result = await agent.get(cycleUrl)

    if (result) {
      const jsonResult = {
        total: result.body.totalRecords,
        pageNumber,
        pageSize,
        results: result.body.CycleMessage.map((message) => omit(message, 'link'))
      }
      res.status(200).json(jsonResult)
    } else {
      throw createError.InternalServerError(`Unable to cycle messages at ${cycleUrl}`)
    }
  }
}
